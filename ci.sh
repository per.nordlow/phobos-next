#!/bin/bash

set -euo pipefail

# TODO: Replace this with a `ci.d`

echo '### Building "phobos-next" ...'
dub --root=. build

echo '### Testing "phobos-next" with DMD ...'
dub --root=. test -c unittest

echo '### Testing "phobos-next" with LDC ...'
dub --root=. test -c unittest --compiler=ldc2

echo '### Building and running test application "test/containers" ...'
dub --root=test/containers run

echo '### Building and running test application "test/gc-benchmark" ...'
pushd test/gc-benchmark
./test.sh
popd

echo '### Building and running test application "test/allocators-benchmark" ...'
dub --root=test/allocators-benchmark run --build=release

echo '### Building and running test application "test/class-memuse" ...'
dub --root=test/class-memuse run --build=release

echo '### Building test application "test/lispy-benchmark" ...'
dub --root=test/lispy-benchmark build

echo '### Building test application "test/lpgen" ...'
# TODO: enable when compiles with -dip1000 and linker issues have been fixed
# dub --root=test/lpgen build

echo '### Building library "tcc-d2" ...'
dub --root=tcc-d2 build

echo '### Building library "xdag" ...'
# TODO: enable: dub --root=xdag build

echo '### Building library "zio" ...'
dub --root=zio build
