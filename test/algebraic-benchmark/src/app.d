void main(string[] args) {
	import std.datetime.stopwatch : benchmark;
	import std.meta : Tup = AliasSeq;
	import std.variant : Algebraic;
	import nxt.variant : NAlgebraic = Algebraic;
	import std.stdio : writeln;

	alias Types = Tup!(long, double);
	alias A = Algebraic!Types;
	alias N = NAlgebraic!Types;

	writeln("N.sizeof:", N.sizeof);
	writeln("F.sizeof:", N.sizeof);

	static void test(T)() {
		T x;
		foreach (long i; 0 .. 100) {
			x = i;
			auto y = x;
			auto z = y;
			assert(x == y);
			assert(y == z);
		}
	}

	enum n = 10_000;
	n.benchmark!(test!A, test!N)().writeln();
}
