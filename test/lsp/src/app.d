#!/usr/bin/rdmd

// TODO: `id` and `languageId` is missing

import std.stdio;
import std.json;
import std.conv;
import std.algorithm;
import std.string : strip;

int main(string[] args) {
	eventLoop(args);
	return 0;
}

enum RequestType {
	connect,
	configuration,
	shutdown,
	unknown
}

struct Request {
	RequestType type;
	JSONValue params;
}

Request parseRequest(in JSONValue json) {
    const method = json["method"].str();
    RequestType reqType;
    switch (method) {
    case "connect":
        reqType = RequestType.connect;
        break;
    case "configuration":
        reqType = RequestType.configuration;
        break;
    case "shutdown":
        reqType = RequestType.shutdown;
        break;
    default:
        reqType = RequestType.unknown;
        break;
    }
    return Request(reqType, json["params"]);
}

void handleRequest(Request request) {
	switch (request.type) {
		case RequestType.connect:
			sendResponse(`"Connected successfully"`);
			break;
		case RequestType.configuration:
			sendResponse(`"Configuration updated"`);
			break;
		case RequestType.shutdown:
			sendResponse(`"Shutdown complete"`);
			break;
		default:
			sendResponse(`"Unknown request"`);
			break;
	}
}

void sendResponse(string result) {
	const response = `{"jsonrpc":"2.0","result":` ~ result ~ `}`;
	writefln("Content-Length: %s\r\n\r\n%s", response.length, response);
}

void eventLoop(string[] args) {
	while (true) {
		auto header = stdin.readln();
		if (header.skipOver("Content-Length:")) {
			const length = header.strip.to!size_t;
			stdin.readln(); // Skip the empty line

			scope char[] content = new char[length];
			stdin.rawRead(content);

			auto json = content.parseJSON();
			auto request = json.parseRequest();
			request.handleRequest();

			if (request.type == RequestType.shutdown) {
				break;
			}
		}
	}
}
