module nxt.builtin_traits;

/** Is `true` iff builtin `__rvalue` is defined.
 */
enum hasBuiltinRvalue = __VERSION__ >= 2111 || __traits(compiles, { int x; const y = __rvalue(x); });
