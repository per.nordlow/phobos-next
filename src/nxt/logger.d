module nxt.logger;

@safe:

/++ Logging Format. +/
enum Format { // TODO: Move to `nxt.logger`
	dmd,
	rust,
	/+ TODO: gcc, +/
}

/++ Log level copied from `std.logger.core.LogLevel`. +/
enum LogLevel : ubyte {
	all = 1, /** Lowest possible assignable `LogLevel`. */
	trace = 32, /** `LogLevel` for tracing the execution of the program. */
	info = 64, /** This level is used to display information about the
				program. */
	warning = 96, /** warnings about the program should be displayed with this
				   level. */
	error = 128, /** Information about errors should be logged with this
				   level.*/
	critical = 160, /** Messages that inform about critical errors should be
					logged with this level. */
	fatal = 192,   /** Log messages that describe fatal errors should use this
				  level. */
	off = ubyte.max /** Highest possible `LogLevel`. */
}

static immutable defaultLogLevel = LogLevel.warning;

// TODO: Make mutable shared
private static immutable LogLevel g_nxtLoggerGlobalLogLevel = LogLevel.warning;

// Support format:
// 2024-10-21T17:30:10.563 [warning] __FILE__:__LINE__:__FUNCTION__ MSG
// and support this in compilation-mode.el

void trace(Args...)(scope Args args) pure nothrow @nogc @trusted {
	import nxt.io : ewriteln;
	if (g_nxtLoggerGlobalLogLevel <= LogLevel.trace)
		debug ewriteln("[trace] ", args);
}

void info(Args...)(scope Args args) pure nothrow @nogc @trusted {
	import nxt.io : ewriteln;
	if (g_nxtLoggerGlobalLogLevel <= LogLevel.info)
		debug ewriteln("[info] ", args);
}

void warning(Args...)(scope Args args) pure nothrow @nogc @trusted {
	import nxt.io : ewriteln;
	if (g_nxtLoggerGlobalLogLevel <= LogLevel.warning)
		debug ewriteln("[warning] ", args);
}

void error(Args...)(scope Args args) pure nothrow @nogc @trusted {
	import nxt.io : ewriteln;
	if (g_nxtLoggerGlobalLogLevel <= LogLevel.error)
		debug ewriteln("[error] ", args);
}

void critical(Args...)(scope Args args) pure nothrow @nogc @trusted {
	import nxt.io : ewriteln;
	if (g_nxtLoggerGlobalLogLevel <= LogLevel.critical)
		debug ewriteln("[critical] ", args);
}

void fatal(Args...)(scope Args args) pure nothrow @nogc @trusted {
	import nxt.io : ewriteln;
	if (g_nxtLoggerGlobalLogLevel <= LogLevel.fatal)
		debug ewriteln("[fatal] ", args);
}
