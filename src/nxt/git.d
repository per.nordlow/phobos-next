/++ High-level control of the Git version control system.
	See: https://github.com/s-ludwig/dlibgit
 +/
module nxt.git;

import nxt.effects;

@safe:

/// Depth of git clone.
struct Depth {
	uint value;
	alias value this;
	static immutable Depth min = Depth(1);
	static immutable Depth max = Depth(value.max);
}

import nxt.dbgio;

/++ SHA-1 Digest +/
struct SHA1Digest {
	private enum length = 20;
pure nothrow @nogc:
    ubyte[length] bytes;
    alias bytes this;
}

SHA1Digest parseSHA1Digest(in string hexString) pure @nogc in(hexString.length == 2*SHA1Digest.length) {
	typeof(return) result;
	foreach (const i; 0 .. SHA1Digest.length) {
		const ch0 = hexString[2*i + 0];
		const ch1 = hexString[2*i + 1];
		import nxt.conv : parseHexDigit;
		const uint by0 = ch0.parseHexDigit() << 4;
		const uint by1 = ch1.parseHexDigit();
		result.bytes[i] = cast(ubyte)(by0 + by1);
	}
	return result;
}

@safe pure version(nxt_test) unittest {
    const a = parseSHA1Digest("f4c21152f1952a99f4744e8c41d3ffb8038ae78c");
    const b = parseSHA1Digest("F4C21152F1952A99F4744E8C41D3FFB8038AE78C");
	assert(a == b);
	const c = parseSHA1Digest(a.bytes.toHexString);
	assert(b == c);
}

version(nxt_test) version(unittest) {
	import std.digest : toHexString;
}

/++ Git Repository URI.
	See_Also: https://github.com/fubark/cosmic/blob/master/GitRepoStep.zig
 +/
struct RepoURI {
	import nxt.uri : URI;
	this(string value) pure nothrow @nogc {
		this._value = URI(value); // don't enforce ".git" extension in `value` because it's optional
	}
	URI _value; /+ TODO: replace _value with uri +/
	alias _value this;
}

/++ Git Repository URI relative directory `dir` at `commit`.
 +/
struct RepoURIDir {
	import nxt.path : DirPath;
	this(RepoURI uri, DirPath dir = DirPath.init) pure nothrow @nogc {
		this.uri = uri;
		this.dir = dir;
	}
	RepoURI uri;
	DirPath dir; ///< Sub-directory in `uri`. Can be both in absolute and relative.
	SHA1Digest commit;
}

/++ Git Repository URI relative sub-files `subFiles` at `commit`.
 +/
struct RepoURIFiles {
	import nxt.path : FilePath;
	this(RepoURI uri, FilePath[] subFiles = []) pure nothrow @nogc {
		this.uri = uri;
		this.subFiles = subFiles;
	}
	RepoURI uri;
	FilePath[] subFiles;
	SHA1Digest commit;
}

/++ Git Repository URI accompanied with local clone directory path.
 +/
struct RepoAndDir
{
	import std.exception : enforce;
	import nxt.path : DirPath, buildNormalizedPath, exists;
	import nxt.process : Spawn, spawn, ExitStatus;
	import nxt.logger : LogLevel, trace, info, warning, error;
	import std.file : exists;

	const RepoURI uri;
	const DirPath lrd;			///< Local checkout root directory.
	LogLevel logLevel = LogLevel.warning;

	this(const RepoURI uri, const DirPath lrd = [], in bool echoOutErr = true)
	{
		this.uri = uri;
		if (lrd != lrd.init)
			this.lrd = lrd;
		else {
			import std.path : baseName, stripExtension;
			this.lrd = DirPath(this.uri._value.str.baseName.stripExtension);
		}
	}

	this(this) @disable;

	// Actions:

	auto ref setLogLevel(in LogLevel logLevel) scope pure nothrow @nogc {
		this.logLevel = logLevel;
		return this;
	}

	Spawn cloneOrPull(in bool recursive = true, string branch = [], in Depth depth = Depth.max)
	{
		if (lrd.buildNormalizedPath(DirPath(".git")).exists)
			return pull(recursive);
		else
			return clone(recursive, branch, depth);
	}

	Spawn cloneOrResetHard(in bool recursive = true, string remote = "origin", string branch = [], in Depth depth = Depth.max)
	{
		if (lrd.buildNormalizedPath(DirPath(".git")).exists)
			return resetHard(recursive, remote, branch);
		else
			return clone(recursive, branch, depth);
	}

	Spawn clone(in bool recursive = true, string branch = [], in Depth depth = Depth.max) const
	{
		import std.conv : to;
		if (logLevel <= LogLevel.trace) trace("Cloning ", uri, " to ", lrd);
		return spawn(["git", "clone"]
					 ~ [uri._value.str, lrd.str]
					 ~ (branch.length ? ["-b", branch] : [])
					 ~ (recursive ? ["--recurse-submodules"] : [])
					 ~ (depth != depth.max ? ["--depth", depth.to!string] : []),
					 logLevel);
	}

	Spawn pull(in bool recursive = true)
	{
		if (logLevel <= LogLevel.trace) trace("Pulling ", uri, " to ", lrd);
		return spawn(["git", "-C", lrd.str, "pull"]
					 ~ (recursive ? ["--recurse-submodules"] : []),
					 logLevel);
	}

	Spawn resetHard(in bool recursive = true, string remote = "origin", string branch = [])
	{
		if (logLevel <= LogLevel.trace) trace("Resetting hard ", uri, " to ", lrd);
		if (branch)
			return resetHardTo(remote~"/"~branch, recursive);
		else
			return resetHard(recursive);
	}

	Spawn checkout(string branchName)
	{
		if (logLevel <= LogLevel.trace) trace("Checking out branch ", branchName, " at ", lrd);
		return spawn(["git", "-C", lrd.str, "checkout" , branchName], logLevel);
	}

	Spawn remoteRemove(string name)
	{
		if (logLevel <= LogLevel.trace) trace("Removing remote ", name, " at ", lrd);
		return spawn(["git", "-C", lrd.str, "remote", "remove", name], logLevel);
	}
	alias removeRemote = remoteRemove;

	Spawn remoteAdd(in RepoURI uri, string name)
	{
		if (logLevel <= LogLevel.trace) trace("Adding remote ", uri, " at ", lrd, (name ? " as " ~ name : "") ~ " ...");
		return spawn(["git", "-C", lrd.str, "remote", "add", name, uri._value.str], logLevel);
	}
	alias addRemote = remoteAdd;

	Spawn fetch(string[] names)
	{
		if (logLevel <= LogLevel.trace) trace("Fetching remotes ", names, " at ", lrd);
		if (names)
			return spawn(["git", "-C", lrd.str, "fetch", "--multiple"] ~ names, logLevel);
		else
			return spawn(["git", "-C", lrd.str, "fetch"], logLevel);
	}

	Spawn fetchAll()
	{
		if (logLevel <= LogLevel.trace) trace("Fetching all remotes at ", lrd);
		return spawn(["git", "-C", lrd.str, "fetch", "--all"], logLevel);
	}

	Spawn clean()
	{
		if (logLevel <= LogLevel.trace) trace("Cleaning ", lrd);
		return spawn(["git", "-C", lrd.str, "clean", "-ffdx"], logLevel);
	}

	Spawn resetHard(in bool recursive = true)
	{
		if (logLevel <= LogLevel.trace) trace("Resetting hard ", lrd);
		return spawn(["git", "-C", lrd.str, "reset", "--hard"]
					 ~ (recursive ? ["--recurse-submodules"] : []), logLevel);
	}

	Spawn resetHardTo(string treeish, in bool recursive = true)
	{
		if (logLevel <= LogLevel.trace) trace("Resetting hard ", lrd);
		return spawn(["git", "-C", lrd.str, "reset", "--hard", treeish]
					 ~ (recursive ? ["--recurse-submodules"] : []), logLevel);
	}

	Spawn merge(string[] commits, string[] args = [])
	{
		if (logLevel <= LogLevel.trace) trace("Merging commits ", commits, " with flags ", args, " at ", lrd);
		return spawn(["git", "-C", lrd.str, "merge"] ~ args ~ commits, logLevel);
	}

	Spawn revParse(string treeish, string[] args = []) const
	{
		if (logLevel <= LogLevel.trace) trace("Getting rev-parse of ", treeish, " at ", lrd);
		return spawn(["git", "rev-parse", treeish] ~ args, logLevel);
	}

	// Getters:

@property:

	SHA1Digest commitSHAOf(string treeish) const
	{
		Spawn spawn = revParse(treeish);
		enforce(spawn.wait() == ExitStatus.ok);
		return typeof(return).init; /+ TODO: use `spawn.output` +/
	}
}

bool tryPinHEADCommit(scope ref RepoURIDir rud, const ref RepoAndDir rad) @reads_from_file {
	import nxt.algorithm.searching : skipOver;
	import nxt.algorithm.mutation : stripRight;
	import std.file : readText, FileException;
	import nxt.path : buildPath, FilePath, DirPath;
	const DirPath gitDir = rad.lrd.buildPath(DirPath(".git"));
	const refFile = gitDir.buildPath(FilePath("HEAD"));
	try {
		auto refStr = refFile.str.readText.stripRight('\n');
		const prefix = "ref: ";
		if (refStr.skipOver(prefix)) {
			const shaPath = gitDir.buildPath(FilePath(refStr));
			auto shaStr = shaPath.str.readText.stripRight('\n');
			rud.commit = parseSHA1Digest(shaStr);
			return true;
		}
	} catch (FileException e) {}
	return false;
}

/++ State of repository.
 +/
struct RepoState {
	string commitShaHex;		// SHA of commit in hexadecimal form.
	string branchOrTag;			// Optional.
}
