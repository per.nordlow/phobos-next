/** FNV(Fowler-Noll-Vo) hash implementation.
 *
 * This module conforms to the APIs defined in std.digest.
 */
module nxt.digest.fnv;

public import std.digest;

/**
 * Template API FNV-1(a) hash implementation.
 */
struct FNV(ulong bitLength, bool fnv1a = false) {
	static if (bitLength == 32)
		alias Element = uint;
	else static if (bitLength == 64)
		alias Element = ulong;
	else
		static assert(0, "Unsupported hash length " ~ bitLength.stringof);

pragma(inline, true):

	/// Initializes the digest calculation.
	void start() {
		_hash = fnvOffsetBasis;
	}

	/// Feeds the digest with data.
	void put(scope const(ubyte)[] data...) {
		foreach (const ubyte e; data) {
			static if (fnv1a) {
				_hash ^= e;
				_hash *= fnvPrime;
			} else {
				_hash *= fnvPrime;
				_hash ^= e;
			}
		}
	}
	///
	void put(scope const(char)[] data)
		=> put(cast(const(ubyte)[])data);

	/// Returns the finished FNV digest. This also calls start to reset the internal state.
	ubyte[bitLength / 8] finish() @trusted {
		import std.bitmanip : nativeToBigEndian;
		_result = _hash;
		start();
		return nativeToBigEndian(_result);
	}

	Element get() const => _result;

private:
	// FNV-1 hash parameters
	static if (bitLength == 32) {
		enum Element fnvPrime = 0x1000193U;
		enum Element fnvOffsetBasis = 0x811C9DC5U;
	} else static if (bitLength == 64) {
		enum Element fnvPrime = 0x100000001B3UL;
		enum Element fnvOffsetBasis = 0xCBF29CE484222325UL;
	} else
		static assert(0, "Unsupported hash length " ~ bitLength.stringof);

	Element _hash;
	Element _result;
}

alias FNV32 = FNV!32; /// 32bit FNV-1, hash size is ubyte[4]
alias FNV64 = FNV!64; /// 64bit FNV-1, hash size is ubyte[8]
alias FNV32A = FNV!(32, true); /// 32bit FNV-1a, hash size is ubyte[4]
alias FNV64A = FNV!(64, true); /// 64bit FNV-1a, hash size is ubyte[8]

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	alias FNV32ADigest = WrapperDigest!FNV32A; /// OOP API for 32bit FNV-1a

	const immutable(char)[5] hello = "hello";

	FNV64 fnv64;
	fnv64.start();
	fnv64.put(hello[]);
	assert(toHexString(fnv64.finish()) == "7B495389BDBDD4C7");

	// Template API
	assert(digest!FNV32("abc") == hexString!"439C2F4B");
	assert(digest!FNV64("abc") == hexString!"D8DCCA186BAFADCB");
	assert(digest!FNV32A("abc") == hexString!"1A47E90B");
	assert(digest!FNV64A("abc") == hexString!"E71FA2190541574B");
}

///
nothrow @safe version(nxt_test) unittest {
	alias FNV32ADigest = WrapperDigest!FNV32A; /// OOP API for 32bit FNV-1a

	// OOP API
	Digest fnv = new FNV32ADigest;
	const d = fnv.digest("1234");
	assert(d == hexString!"FDC422FD");
}

/// Convenience aliases for std.digest.digest using the FNV implementation.
auto fnv32Of(T...)(in T data) => digest!(FNV32, T)(data);
/// ditto
auto fnv64Of(T...)(in T data) => digest!(FNV64, T)(data);
/// ditto
auto fnv32aOf(T...)(in T data) => digest!(FNV32A, T)(data);
/// ditto
auto fnv64aOf(T...)(in T data) => digest!(FNV64A, T)(data);

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert(fnv32Of("") == hexString!"811C9DC5");
	assert(fnv64Of("") == hexString!"CBF29CE484222325");
	assert(fnv32aOf("") == hexString!"811C9DC5");
	assert(fnv64aOf("") == hexString!"CBF29CE484222325");
}

version(nxt_test) version(unittest) {
	import std.conv : hexString;
}
