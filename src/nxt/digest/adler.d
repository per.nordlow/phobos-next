/** Adler-32 implementation compliant with `std.digest`. */
module nxt.digest.adler;

public import std.digest;

struct Adler32 {
pure nothrow @safe @nogc:
pragma(inline, true):

	void start() {
		_a = 1;
		_b = 0;
		_tlen = moduloInterval;
	}

	void put(scope const(ubyte)[] data...) {
		foreach (immutable ubyte i; data) {
			_a += i;
			_b += _a;
			--_tlen;
			if (_tlen == 0) {
				_a %= 65_521;
				_b %= 65_521;
				_tlen = moduloInterval;
			}
		}
		if (_tlen != moduloInterval) {
			_a %= 65_521;
			_b %= 65_521;
		}
	}
	///
	void put(scope const(char)[] data) @trusted
		=> put(cast(const(ubyte)[])data);

	ubyte[4] finish() {
		import std.bitmanip : nativeToBigEndian;
		auto a = _a, b = _b;
		start();
		return nativeToBigEndian((b << 16) | a);
	}

private:
	uint _a = void, _b = void;
	uint _tlen = void;
	enum moduloInterval = 5552;
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	Adler32 adler;
	adler.start();
	adler.put("abc");
	assert(adler.finish() == hexString!"024d0127");
	adler.start();
	adler.put("def");
	assert(adler.finish() == hexString!"025F0130");
}

/// Convenience alias for $(D digest) function in std.digest using the Adler32 implementation.
auto adler32Of(T...)(T data) => digest!(Adler32, T)(data);

pure nothrow @safe @nogc version(nxt_test) unittest {
	static assert(isDigest!Adler32);
	assert(adler32Of("abc") == hexString!"024d0127");
	assert(adler32Of("abcdefghijklmnopqrstuvwxyz") == hexString!"90860B20");
}

/// OOP API for Adler32.
alias Adler32Digest = WrapperDigest!Adler32;

///
version(nxt_test) unittest {
	auto adler = new Adler32Digest;
	assert(adler.digest("abc") == hexString!"024d0127");
}

version(nxt_test) version(unittest) {
	import std.conv : hexString;
}
