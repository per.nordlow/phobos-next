/** Extensions to std.file.
	Copyright: Per Nordlöw 2024-.
	License: $(WEB boost.org/LICENSE_1_0.txt, Boost License 1.0).
	Authors: $(WEB Per Nordlöw)
*/
module nxt.file;

import std.stdio : File;
import nxt.path : FileName, FilePath, DirPath;
import nxt.srx : PNode = Node;
import nxt.effects;
public import nxt.dir;

@safe:

private enum PAGESIZE = 4096;

/++ (Data) Format of file contents.

	The `name` can be either a programming language such as "C",
	"C++", "D", etc or a data format such as "JSON", "XML" etc.

	See: https://en.wikipedia.org/wiki/File_format
 +/
struct DataFormat {
	/++ TODO: Make this a `PNode namePattern` to support both, for instance,
    "JavaScript Object Notation" and "JSON". +/
	string name;
	alias name this;
	/++ Pattern matching often associated file contents.
		Also referred to as "file signature".
		See_Also: https://en.wikipedia.org/wiki/List_of_file_signatures
	 +/
	const(PNode) content;

	// TODO: Remove these when `content == rhs.content` is nothrow @nogc
	bool opEquals(scope const ref typeof(this) rhs) const scope pure nothrow @nogc => name == rhs.name && content.opEquals(rhs.content);
	bool opEquals(scope const typeof(this) rhs) const scope pure nothrow @nogc => name == rhs.name && content.opEquals(rhs.content);

	// TODO: Implement toHash
}

pure nothrow @safe @nogc version (nxt_test) {} unittest {
	assert(DataFormat("D") == DataFormat("D"));
	const fmtD = DataFormat("D");
	assert(DataFormat("D") == fmtD);
}

/++ Extension of filename.
	See: https://en.wikipedia.org/wiki/Filename_extension
 +/
struct FileExtension {
	import nxt.algorithm.searching : startsWith;
pure nothrow @nogc:
	this(string str) in(str.startsWith('.')) {
		this.str = str;
	}
	bool opCast(T : bool)() const scope => str !is null;
	string str;
}

pure nothrow @safe @nogc version (nxt_test) {} unittest {
	const fext = FileExtension(".d");
	assert(fext.str == ".d");
}

/** Read file $(D path) into raw array with one extra terminating zero byte.
 *
 * This extra terminating zero (`null`) byte at the end is typically used as a
 * sentinel value to speed up textual parsers or when characters are sent to a
 * C-function taking a zero-terminated string as input.
 *
 * TODO: Add or merge to Phobos?
 *
 * See_Also: https://en.wikipedia.org/wiki/Sentinel_value
 * See_Also: http://forum.dlang.org/post/pdzxpkusvifelumkrtdb@forum.dlang.org
 */
immutable(void)[] rawReadZ(FilePath path) @safe @reads_from_file {
	return File(path.str, `rb`).rawReadZ();
}
/// ditto
immutable(void)[] rawReadZ(scope File file) @trusted @reads_from_file {
	import std.array : uninitializedArray;

	alias Data = ubyte[];
	Data data = uninitializedArray!(Data)(file.size + 1); // one extra for terminator

	file.rawRead(data);
	data[file.size] = '\0';	 // zero terminator for sentinel

	import std.exception : assumeUnique;
	return assumeUnique(data);
}

///
version(Posix)
@safe version(nxt_test) unittest {
	import nxt.algorithm.searching : endsWith;
	const d = cast(const(char)[]) FilePath(`/etc/passwd`).rawReadZ();
	assert(d.endsWith('\0')); // has 0-terminator
}

/++ Find path for `a` (or `FilePath.init` if not found) in `environmentVariableName`.
	TODO: Add caching of result and detect changes via inotify.
 +/
FilePath findExecutable(FileName a, scope const(char)[] environmentVariableName = "PATH") /+nothrow+/ {
	return findFirstFileInPath(a, environmentVariableName, onlyExecutable: true);
}

///
@safe version(nxt_test) unittest {
	version(Posix) {
		assert(findExecutable(FileName("ls")) == FilePath("/usr/bin/ls"));
		assert(!findExecutable(FileName("xyz")));
	}
}

/++ Find path for `a` (or `FilePath.init` if not found) in `environmentVariableName`.
	TODO: Add caching of result and detect changes via inotify.
 +/
FilePath findFirstFileInPath(FileName a, scope const(char)[] environmentVariableName = "PATH", bool onlyExecutable = false) nothrow @reads_from_environment {
	import std.process : environment;
	const envPATH = () @trusted nothrow {
		try
			return environment.get(environmentVariableName, []);
		catch (Exception e)
			return typeof(return).init;
	}();
	if (!envPATH)
		return typeof(return).init;
	import nxt.algorithm.iteration : splitterASCII;
	foreach (const p; envPATH.splitterASCII!(_ => _ == ':')) {
		import nxt.path : buildPath, exists;
		const path = () @trusted { return DirPath(p).buildPath(a); }();
		// pick first match
		if (onlyExecutable && path.toString.isExecutable)
			return path;
		if (path.exists)
			return path;
	}
	return typeof(return).init;
}

///
@safe version(nxt_test) unittest {
	version(Posix) {
		assert(findFirstFileInPath(FileName("ls"), "_") == FilePath.init);
		assert(findFirstFileInPath(FileName("ls"), "_", true) == FilePath.init);
		assert(findFirstFileInPath(FileName("xyz")) == FilePath.init);
		assert(findFirstFileInPath(FileName("xyz")) == FilePath.init);
	}
}

version(Posix)
private bool isExecutable(in char[] path) @trusted nothrow @nogc {
	import std.internal.cstring : tempCString;
	import core.sys.posix.unistd : access, X_OK;
    return access(path.tempCString(), X_OK) == 0;
}

/++ Get path to default temporary directory root.
	See_Also: `std.file.tempDir`
	See: https://forum.dlang.org/post/gg9kds$1at0$1@digitalmars.com
 +/
DirPath tempDir() {
	import std.file : std_tempDir = tempDir;
	return typeof(return)(std_tempDir);
}

///
@safe version(nxt_test) unittest {
	version(Posix) {
		assert(tempDir().str == "/tmp");
	}
}

/** Returns the path to a new (unique) temporary file under `tempDir`.
    See_Also: https://forum.dlang.org/post/ytmwfzmeqjumzfzxithe@forum.dlang.org
    See_Also: https://dlang.org/library/std/stdio/file.tmpfile.html
 */
string tempSubFilePath(string prefix = null, string extension = null) @safe {
	import std.file : tempDir;
	import std.uuid : randomUUID;
	import std.path : buildPath;
	/+ TODO: use allocation via lazy range or nxt.appending.append() +/
	return tempDir().buildPath(prefix ~ randomUUID.toString() ~ extension);
}

///
@safe version(nxt_test) unittest {
	import nxt.algorithm.searching : canFind, endsWith;
	const prefix = "_xyz_";
	const ext = "_ext_";
	const path = tempSubFilePath(prefix, ext);
	assert(path.canFind(prefix));
	assert(path.endsWith(ext));
}

/++ Get path to home directory.
	See_Also: `tempDir`
	See: https://forum.dlang.org/post/gg9kds$1at0$1@digitalmars.com
 +/
DirPath homeDir() @reads_from_environment {
	import std.process : environment;
    version(Windows) {
        // On Windows, USERPROFILE is typically used, but HOMEPATH is an alternative
		if (const home = environment.get("USERPROFILE"))
			return typeof(return)(home);
        // Fallback to HOMEDRIVE + HOMEPATH
        const homeDrive = environment.get("HOMEDRIVE");
        const homePath = environment.get("HOMEPATH");
        if (homeDrive && homePath)
            return typeof(return)(buildPath(homeDrive, homePath));
    } else {
        if (const home = environment.get("HOME"))
			return typeof(return)(home);
    }
    throw new Exception("No home directory environment variable is set.");
}

///
@safe version(nxt_test) unittest {
	version(Posix) {
		import std.path : expandTilde;
		assert(homeDir().str == "~".expandTilde);
	}
}

/++ Get path to the default cache (home) directory.
	See: `XDG_CACHE_HOME`
	See: https://specifications.freedesktop.org/basedir-spec/latest/
	See_Also: `tempDir`.
 +/
DirPath cacheHomeDir() @reads_from_environment {
	import std.process : environment;
    version(Windows) {
        if (const home = environment.get("XDG_CACHE_HOME"))
			return typeof(return)(home);
    } else {
        if (const home = environment.get("XDG_CACHE_HOME"))
			return typeof(return)(home);
    }
	// throw new Exception("The `XDG_CACHE_HOME` environment variable is unset");
	import nxt.path : buildPath;
	return homeDir.buildPath(DirPath(`.cache`));
}

///
@safe version(nxt_test) unittest {
	version(Posix) {
		import nxt.path : buildPath;
		assert(cacheHomeDir() == homeDir.buildPath(DirPath(`.cache`)));
	}
}

/++ Variant of `std.file.remove()` that returns status instead of throwing.

	Modified copy of `std.file.remove()`.

	Returns: `true` iff file of path `name` was successfully removed, `false`
	         otherwise.

	Typically used in class destructors/finalizers that must not throw.
 +/
bool removeIfExists(scope const(char)[] name) @trusted nothrow @nogc {
	import std.internal.cstring : tempCString;
	// implicit conversion to pointer via `TempCStringBuffer` `alias this`:
	scope const(FSChar)* namez = name.tempCString!FSChar();
    version(Windows) {
		return DeleteFileW(namez) == 0;
    } else version(Posix) {
        static import core.stdc.stdio;
		return core.stdc.stdio.remove(namez) == 0;
    }
}

/++ Character type used for operating system filesystem APIs.
	Copied from `std.file`.
 +/
version(Windows)
    private alias FSChar = WCHAR;       // WCHAR can be aliased to wchar or wchar_t
else version(Posix)
    private alias FSChar = char;
else
    static assert(0);

///
@safe nothrow version(nxt_test) unittest {
	// TODO: test `removeIfExists`
}

import std.file : DirEntry;

/++ Identical to `std.file.rmdirRecurse` on POSIX.
	On Windows it removes read-only bits before deleting.
	TODO: Integrate into Phobos as `rmdirRecurse(bool forced)`.
	TODO: Make a non-throwing version bool tryRmdirRecurse(bool forced).
 +/
void rmdirRecurseForced(in DirPath path, bool followSymlink = false) {
	rmdirRecurseForced(path.str, followSymlink);
}
/// ditto
void rmdirRecurseForced(in char[] path, bool followSymlink = false) @trusted {
	// passing `de` as an r-value segfaults so store in l-value
	auto de = DirEntry(cast(string)path);
	rmdirRecurseForced(de, followSymlink);
}
/// ditto
void rmdirRecurseForced(ref DirEntry de, bool followSymlink = false) {
	import std.file : FileException, remove, dirEntries, SpanMode, attrIsDir, rmdir, attrIsDir, setAttributes;
	if (!de.isDir)
		throw new FileException(de.name, "Trying to remove non-directory " ~ de.name);
	if (de.isSymlink) {
		version(Windows)
			rmdir(de.name);
		else
			remove(de.name);
		return;
	}
	foreach (ref e; dirEntries(de.name, SpanMode.depth, followSymlink)) {
		version(Windows) {
			import core.sys.windows.windows : FILE_ATTRIBUTE_READONLY;
			if ((e.attributes & FILE_ATTRIBUTE_READONLY) != 0)
				e.name.setAttributes(e.attributes & ~FILE_ATTRIBUTE_READONLY);
		}
		attrIsDir(e.linkAttributes) ? rmdir(e.name) : remove(e.name);
	}
	rmdir(de.name); // dir itself
}

///
@safe nothrow version(nxt_test) unittest {
	// TODO: test `rmdirRecurseForced`
}

import std.file : PreserveAttributes, preserveAttributesDefault;

/++ Copy directory `from` to `to` recursively. +/
void copyRecurse(scope const(char)[] from, scope const(char)[] to, in PreserveAttributes preserve = preserveAttributesDefault) {
    import std.file : copy, dirEntries, isDir, isFile, mkdirRecurse, SpanMode;
    import std.path : buildPath;
    if (from.isDir()) {
        to.mkdirRecurse();
        const from_ = () @trusted {
            return cast(string) from;
        }();
        foreach (entry; dirEntries(from_, SpanMode.breadth)) {
			const fn = entry.name[from.length + 1 .. $]; // +1 skip separator
            const dst = () @trusted { return to.buildPath(fn); }();
            if (entry.name.isFile())
                entry.name.copy(dst, preserve);
            else
                dst.mkdirRecurse();
        }
    } else
        from.copy(to, preserve);
}
/// ditto
void copyRecurse(DirPath from, DirPath to, in PreserveAttributes preserve = preserveAttributesDefault)
	=> copyRecurse(from.str, to.str, preserve);

/** Create a new temporary file starting with ($D namePrefix) and ending with 6
	randomly defined characters.

    Returns: file descriptor to opened file.
 */
version(linux)
int tempfile(in char[] namePrefix = null) @trusted nothrow {
	import core.stdc.limits : PATH_MAX;
	char[PATH_MAX] buf;
	buf[0 .. namePrefix.length] = namePrefix[]; // copy the name into the mutable buffer
	buf[namePrefix.length .. namePrefix.length + 6] = "XXXXXX"[];
	buf[namePrefix.length + 6] = 0; // make sure it is zero terminated yourself
	import core.sys.posix.stdlib: mkstemp;
	return mkstemp(buf.ptr);
}

///
version(linux)
@safe nothrow version(nxt_test) unittest {
	// const a = tempfile();
}

/++ Returns: `true` iff contents of `filePath` contains `needle`. +/
bool hasContentsContaining(FilePath filePath, scope const(char)[] needle) {
	return hasContentsContaining(filePath.str, needle);
}
/// ditto
bool hasContentsContaining(string filePath, scope const(char)[] needle) {
	import nxt.string_traits : isASCII;
    import std.file : read, readText;
    scope const src = (needle.isASCII
        ? cast(const(char)[]) filePath.read()
        : filePath.readText());
    import std.algorithm.searching : canFind;
    const res = src.canFind(needle);
    import core.memory : GC;
	() @trusted {
		GC.free(cast(void*)src.ptr);
	}();
	return res;
}

///
version(linux)
@safe version(nxt_test) unittest {
	assert("test-data/sample.txt".hasContentsContaining("alpha"));
	assert(FilePath("test-data/sample.txt").hasContentsContaining("alpha"));
	assert(!"test-data/sample.txt".hasContentsContaining("##__"));
	assert(!"test-data/sample.txt".hasContentsContaining("åäö"));
}

/++ Returns: `true` iff contents of filePath contains `needle` using `std.mmfile`. +/
bool hasContentsContainingMM(FilePath filePath, scope const(char)[] needle) {
	return hasContentsContaining(filePath.str, needle);
}
/// ditto
bool hasContentsContainingMM(string filePath, scope const(char)[] needle) @trusted {
    import std.exception : ErrnoException;
    try {
        import std.mmfile : MmFile;
        import std.algorithm.searching : canFind;
        scope mmfile = new MmFile(filePath); // deterministic deallocation upon scope exit
        const src = (cast(const(char)[]) mmfile[]);
        if (!src.canFind(needle))
            return false; // discard
    } catch (ErrnoException e) {
        import core.stdc.errno : EINVAL;
        // if (e.errno != EINVAL) // empty file
        //     warning("Memory mapping failed with errno ", e.errno);
        return false;
    }
    return true;
}

///
version(linux)
@safe version(nxt_test) unittest {
	assert("test-data/sample.txt".hasContentsContainingMM("alpha"));
	assert(FilePath("test-data/sample.txt").hasContentsContainingMM("alpha"));
	assert(!"test-data/sample.txt".hasContentsContainingMM("##__"));
	assert(!"test-data/sample.txt".hasContentsContainingMM("åäö"));
}
