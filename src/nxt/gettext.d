/++ Automation of `gettext` translations.
	See_Also: https://www.gnu.org/software/gettext/manual/html_node/PO-Files.html
 +/
module nxt.gettext;

// version = nxt_integration_test;

private enum show = false;

import std.file : dirEntries, SpanMode;
import nxt.path : FilePath, FileName, DirPath, buildPath, exists;
import nxt.file : homeDir, FExt = FileExtension;
import std.stdio : File;
import std.path : baseName;
import nxt.dbgio : dbg;
import nxt.translation.google : translateViaGoogle;
import nxt.algorithm.searching : skipOver, skipOverBack, startsWith, endsWith;
import nxt.iso_639_1 : LanguageCode, parseLanguageCode;

@safe:

/++ Collect translation messages under `root` in all files matching `fext`.
	TODO: Turn this into a lazy range.
	TODO: Use allocator.
 +/
TrsByLang collectTranslationsForLanguagesUnder(in DirPath root, in FExt fext = FExt(".po")) @trusted {
	typeof(return) res;
	foreach (const ref dent; root.str.dirEntries(SpanMode.shallow)) {
		if (!dent.name.endsWith(fext.str)) continue;
		// TODO: assert(msgs.length == cnt);
		const langStr = dent.name.baseName[0 .. 2];
		if (const lang = langStr.parseLanguageCode(LanguageCode.null_))
			res[lang] = FilePath(dent.name).translations();
	}
	return res;
}

/++ Returns: translation messages in the contents of `path`.
	TODO: Turn this into a lazy range.
	TODO: Use allocator.
 +/
Tr[] translations(in FilePath path) @trusted {
	typeof(return) res;
	auto file = File(path.str, `r`);
	const cnt = file.countTranslationsIn();
	// dbg("Translating ", cnt, " in ", dent.name, "...");
	res.reserve(cnt);
	string id, str;
	file.seek(0);
	foreach (ln; file.byLine) {
		if (ln.startsWith('#')) continue;
		if (!ln.skipOver(`msg`)) continue;
		if (!ln.skipOverBack('"')) continue;
		if (ln.skipOver(`id "`)) {
			id = ln.idup; // `.idup` because `byLine` is transient
		} else if (ln.skipOver(`str "`)) {
			str = ln.idup; // `.idup` because `byLine` is transient
			if (id.length && str.length)
				res ~= Tr(id, str);
			id = null;
			str = null;
		}
	}
	// TODO: assert(res.length == cnt);
	return res;
}

private size_t countTranslationsIn(ref File file) @trusted {
	size_t res;
	foreach (ln; file.byLine)
		if (ln.skipOver(`msgid "`) && ln.length >= 2)
			res += 1;
	return res;
}

/++ Line. +/
private alias Line = const(char)[];

/++ Translation in ".po" file.
 +/
private struct Tr {
	string id;  /++< Original message `msgid` (usually in English). +/
	string str; /++< Translated message `msgstr` (in destination|target language). +/
}

/++ Translations by language. +/
alias TrsByLang = const(Tr)[][LanguageCode];

/++ Create index from English message to array of translation destinations. +/
DestsByEn indexByEnglish(in TrsByLang tbf) {
	typeof(return) res;
	foreach (const ref kv; tbf.byKeyValue) {
		const lang = kv.key;
		const Tr[] trs = kv.value;
		foreach (const ref tr; trs)
			res[tr.id] ~= Dst(lang, tr.str);
	}
	return res;
}

/++ Translation Destination. +/
private struct Dst {
	LanguageCode lang;
	string msg;
}

ref DestsByEn completeViaGoogle(return ref DestsByEn dbe) {
	import nxt.traits_ex : enumMembers;
	size_t nE, nM, nS;
	foreach (const ref kv; dbe.byKeyValue) {
		const EnMsg msgEn = kv.key;
		const Dst[] dests = kv.value;
		foreach (const langName; __traits(allMembers, LanguageCode)) {
			alias lang = __traits(getMember, LanguageCode, langName);
			import std.algorithm.searching : canFind;
			if (!dests.canFind!(dst => dst.lang == lang)) {
				// TODO: Translate emsg from `LanguageCode.en` to `dst.lang` and append to `dests`
				nM += 1;
			} else
				nE += 1;
		}
	}
	if (show) dbg("Total Existing Translations: ", nE);
	if (show) dbg("Total Missing Translations : ", nM);
	if (show) dbg("Total Successful Translations : ", nS);
	return dbe;
}

/++ English Message. +/
alias EnMsg = string;

/++ Translations by English Message. +/
alias DestsByEn = const(Dst)[][EnMsg];

version(nxt_integration_test)
@safe version(nxt_test) unittest {
	const root = homeDir.buildPath(DirPath("Work/godot/editor/translations/editor"));
	if (root.exists) {
		auto tbf = root.collectTranslationsForLanguagesUnder();
		if (show) dbg("Language Count: ", tbf.length);
		assert(tbf.length <= 35);
		if (show) dbg("Swedish Translations Count: ", tbf[LanguageCode.sv].length);
		assert(tbf[LanguageCode.sv].length >= 2017);
		auto dbe = tbf.indexByEnglish();
		if (show) dbg("Total Message Count: ", dbe.length);
		assert(dbe.length >= 4636);
		dbe.completeViaGoogle();
	}
}

//  LocalWords:  msgid msgstr LocalWords
