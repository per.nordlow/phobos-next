/** ISO 639-1. */
module nxt.iso_639_1;

@safe:

/** ISO 639-1 language code.
 *
 * See_Also: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
 * See_Also: https://github.com/LBeaudoux/iso639
 * See_Also: http://www.lingoes.net/en/translator/langcode.htm
 * See_Also: http://www.mathguide.de/info/tools/languagecode.html
 * See_Also: http://msdn.microsoft.com/en-us/library/ms533052(v=vs.85).aspx
 */
enum LanguageCode : LanguageT {
	null_,													///< Nullable support

	ab,		/// Abkhaz	Caucasus	needed!
	am,		/// Amharic	Ethiopia, Egypt	ሰላም (Salām)
	aa,		/// Afar	(Ethiopia, Eritrea, Djibouti Salaamata)
	ae,		/// Avestan Iran (extinct)
	af, 	/// Afrikaans
	ak,		/// Akan
	an,		/// Aragonese
	ar,		/// Arabic
	as,		/// Assamese
	az,		/// Azerbaijani (Azeri)
	ba,		/// Baskhir: Volga, Urals, Central Asia
	be,		/// Belarusian
	bg,		/// Bulgarian
	bn,		/// Bengali (Bangla): Bangladesh, India
	bo,		/// Tibetan
	br,		/// Breton: France
	bs,		/// Bosnian
	ca,		/// Catalan/Valencian (Spain)
	cs,		/// Czech: Czech Republic
	cy,		/// Welch: Wales, Heddwch, Tangnefedd
	da,		/// Danish: Denmark, Greenland
	de,		/// German: Germany, Austria, Switzerland, Liechtenstein, Italy, Belgium
	dz,		/// Dzongkha	Bhutan	གཞི་བདེ (gzhi-bde)
	el,		/// Greek: Greece, Cyprus
	en,		/// English
	eo,		/// Esperanto
	es,		/// Spanish
	et,		/// Estonian
	eu,		/// Basque: Spain, France
	fa,		/// Persian (Farsi): Iran, Iraq, Afghanistan, Pakistan, Azerbaijan
	fi,		/// Finnish: Finland, Sweden, Russia
	fj,		/// Fijian: Fiji
	fo,		/// Faroese (Faeroese): Faroe Islands
	fr,		/// French: France, Belgium, Canada, Caribbean, West Africa, Polynesia
	ga,		/// Irish: Ireland
	gd,		/// Scottish Gaelic: Scotland
	gl,		/// Galician (Gallegan): Spain, Portugal
	gv,		/// Manx: Isle of Man
	ha,		/// Hausa: Nigeria
	he,		/// Hebrew: Israel
	hi,		/// Hindi: India, Nepal, Uganda, Suriname
	hr,		/// Croatian: Croatia
	hu,		/// Hungarian: Hungary, Romania, Slovakia
	hy,		/// Armenian: Armenia
	in_,	/// Indonesian
	io,		/// Ido: Nigeria
	is_,	/// Icelandic
	it,		/// Italian: Italy, Switzerland
	ja,		/// Japanese, 日本語: Japan
	ka,		/// Georgian: Georgia
	kk,		/// Kazakh: Kazakhstan
	km,		/// Khmer: Cambodia
	kn,		/// Kannada: India
	ko,		/// Korean: Korea
	ku,		/// Kurdish: Kurdistan (Turkey, Syria, Iran, Iraq)
	ky,		/// Kirghiz (Kyrgyz): Kirghizstan, China
	la,		/// Latin: Rome (extinct)
	lo,		/// Lao: Laos
	lt,		/// Lithuanian: Lithuania
	lv,		/// Latvian: Latvia
	mg,		/// Malagasy (Malgache): Madagascar
	mk,		/// Macedonian: Macedonia
	mn,		/// Mongolian: Mongolia
	ms,		/// Malay: Malaysia
	mt,		/// Maltese: Malta
	my,		/// Burmese: Myanmar
	ne,		/// Nepali: Nepal
	nl,		/// Dutch (Flemish): Netherlands, Belgium
	no,		/// Norwegian: Norway
	oc,		/// Occitan (Provençal, Languedocian): France
	pl,		/// Polish
	ps,		/// Pashto: Afghanistan, Iran, Pakistan
	pt,		/// Portuguese: Portugal, Brazil, Angola, Mozambique, Cape Verde, Guinea-Bissau
	ro,		/// Romanian: Romania, Hungary
	ru,		/// Russian
	sa,		/// Sanskrit: India (extinct, liturgical)
	si,		/// Sinhalese: Sri Lanka
	sk,		/// Slovak: Slovak Republic
	sl,		/// Slovene, Slovenian: Slovenia, Austria, Italy
	sm,		/// Samoan: Samoa
	sq,		/// Albanian: Albania, Kosovo
	sr,		/// Serbian: Serbia, Montenegro, Bosnia
	sv,		/// Swedish
	sw,		/// Swahili: East Africa
	ta,		/// Tamil: India
	te,		/// Telugu: India
	tg,		/// Tajik: Tajikistan, Afghanistan
	th,		/// Thai: Thailand
	tk,		/// Turkmen: Turkmenistan, Afghanistan
	tl,		/// Tagalog (Pilipino): Philippines
	tr,		/// Turkish: Turkey, Cyprus
	uk,		/// Ukrainian
	ur,		/// Urdu: Pakistan, India, Central Asia
	uz,		/// Uzbek: Uzbekistan, Central Asia, China
	vi,		/// Vietnamese: Viet Nam
	vo,		/// Volapük
	wa,		/// Waloon: Belgium
	yi,		/// Yiddish: Israel, USA, Russia
	zh,		/// Chinese (Mandarin, Putonghua): China, Singapore

	hole_ = LanguageT.max,				  // `HybridHashMap` hole support
}

private alias LanguageT = ubyte;

/** TODO: Remove when `__traits(documentation)` is merged */
string toEnglishString(in LanguageCode lcd) pure nothrow @nogc {
	with (LanguageCode) {
		final switch (lcd) {
		case null_: return `null_`;
		case hole_: return `hole_`;
		case aa: return `Afar`;
		case ab: return `Abkhaz`;
		case ae: return `Avestan`;
		case af: return `Afrikaans`;
		case ak: return `Akan`;
		case am: return `Amharic`;
		case an: return `Aragonese`;
		case ar: return `Arabic`;
		case as: return `Assamese`;
		case az: return `Azerbaijani`;
		case ba: return `Baskhir`;
		case be: return `Belarusian`;
		case bg: return `Bulgarian`;
		case bn: return `Bengali`;
		case bo: return `Tibetan`;
		case br: return `Breton`;
		case bs: return `Bosnian`;
		case ca: return `Catalan`;
		case cs: return `Czech`;
		case cy: return `Welch`;
		case da: return `Danish`;
		case de: return `German`;
		case dz: return `Dzongkha`;
		case el: return `Greek`;
		case en: return `English`;
		case eo: return `Esperanto`;
		case es: return `Spanish`;
		case et: return `Estonian`;
		case eu: return `Basque`;
		case fa: return `Persian`;
		case fi: return `Finnish`;
		case fj: return `Fiji`;
		case fo: return `Faroese`;
		case fr: return `French`;
		case ga: return `Irish`;
		case gd: return `Gaelic`; // Scottish Gaelic
		case gl: return `Galician`;
		case gv: return `Manx`;
		case ha: return `Hausa`;
		case he: return `Hebrew`;
		case hi: return `Hindi`;
		case hr: return `Croatian`;
		case hu: return `Hungarian`;
		case hy: return `Armenian`;
		case in_: return `Indonesian`;
		case io: return `Ido`;
		case is_: return `Icelandic`;
		case it: return `Italian`;
		case ja: return `Japanese`;
		case ka: return `Georgian`;
		case kk: return `Kazakh`;
		case km: return `Khmer`;
		case kn: return `Kannada`;
		case ko: return `Korean`;
		case ku: return `Kurdish`;
		case ky: return `Kyrgyz`;
		case la: return `Latin`;
		case lo: return `Lao`;
		case lt: return `Lithuanian`;
		case lv: return `Latvian`;
		case mg: return `Malagasy`;
		case mk: return `Macedonian`;
		case mn: return `Mongolian`;
		case ms: return `Malay`;
		case mt: return `Maltese`;
		case my: return `Burmese`;
		case ne: return `Nepali`;
		case nl: return `Dutch`;
		case no: return `Norwegian`;
		case oc: return `Occitan`;
		case pl: return `Polish`;
		case ps: return `Pashto`;
		case pt: return `Portuguese`;
		case ro: return `Romanian`;
		case ru: return `Russian`;
		case sa: return `Sanskrit`;
		case si: return `Sinhalese`;
		case sk: return `Slovak`;
		case sl: return `Slovene`;
		case sm: return `Samoan`;
		case sq: return `Albanian`;
		case sr: return `Serbian`;
		case sv: return `Swedish`;
		case sw: return `Swahili`;
		case ta: return `Tamil`;
		case te: return `Tegulu`;
		case tg: return `Tajik`;
		case th: return `Thai`;
		case tk: return `Turkmen`;
		case tl: return `Tagalog`;
		case tr: return `Turkish`;
		case uk: return `Ukrainian`;
		case ur: return `Urdu`;
		case uz: return `Uzbek`;
		case vi: return `Vietnamese`;
		case vo: return `Volapük`;
		case wa: return `Waloon`;
		case yi: return `Yiddish`;
		case zh: return `Chinese`;
		}
	}
}

/// Parse `str` into a `LanguageCode`.
LanguageCode parseLanguageCode(scope const(char)[] str, in LanguageCode defaultLang) pure nothrow @safe @nogc {
	switch (str) {
		case `is`:
			return LanguageCode.is_;
		case `in`:
			return LanguageCode.in_;
		default:
			import nxt.conv_ex : toDefaulted;
			return typeof(return)(str.toDefaulted!LanguageCode(defaultLang));
	}
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(`is`.parseLanguageCode(LanguageCode.null_) == LanguageCode.is_);
	assert(`in`.parseLanguageCode(LanguageCode.null_) == LanguageCode.in_);
	assert(`_`.parseLanguageCode(LanguageCode.null_) == LanguageCode.null_);
	assert(`_`.parseLanguageCode(LanguageCode.en) == LanguageCode.en);
	assert(`sv`.parseLanguageCode(LanguageCode.null_) == LanguageCode.sv);
	assert(`en`.parseLanguageCode(LanguageCode.null_) == LanguageCode.en);
}

/** Returns: `true` iff `lcd` capitalizes common nouns. */
bool capitalizesCommonNoun(in LanguageCode lcd) pure nothrow @safe @nogc => lcd == LanguageCode.de;

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert(LanguageCode.de.capitalizesCommonNoun);
	assert(!LanguageCode.en.capitalizesCommonNoun);
}

/** ISO 639-1 language code as a 2-character ASCII string. */
struct LanguageCodeString {
	private alias Chars = char[2];
	this(in LanguageCode lcd) pure nothrow @nogc {
		with (LanguageCode) outer:
			final switch (lcd) {
				static foreach (memberName; __traits(allMembers, LanguageCode)) {
					static if (memberName == "null_") {
						case null_: chars = "\0\0"; break outer;
					} else static if (memberName == "hole_") {
						case hole_: chars = `__`; break	outer;
					} else {
						case __traits(getMember, LanguageCode, memberName):
							chars = memberName[0..Chars.sizeof]; break outer;
					}
				}
			}
	}
	private static immutable Chars nullChars = "\0\0";
	Chars chars = nullChars;
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert(LanguageCodeString.init == LanguageCodeString(LanguageCode.null_));
	assert(LanguageCodeString(LanguageCode.null_).chars == "\0\0");
	assert(LanguageCodeString(LanguageCode.hole_).chars == "__");
	assert(LanguageCodeString(LanguageCode.en).chars == "en");
	assert(LanguageCodeString(LanguageCode.sv).chars == "sv");
	assert(LanguageCodeString(LanguageCode.in_).chars == "in");
	assert(LanguageCodeString(LanguageCode.is_).chars == "is");
}
