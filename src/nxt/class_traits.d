module nxt.class_traits;

/++ Get derived (sub) classes of `T` present in the same module as `T`. +/
template AdjacentDerivedClassesOf(T) if (is(T == class)) {
	import std.meta : AliasSeq;
	alias AdjacentDerivedClassesOf = AliasSeq!();
    static if (__traits(compiles, __traits(parent, T))) {
        alias P = __traits(parent, T);
		static if (is(P == module)) {
			static foreach (memberStr; __traits(allMembers, P)) {
				static if (!is(__traits(getMember, P, memberStr) == T) &&
						   is(__traits(getMember, P, memberStr) : T)) {
					AdjacentDerivedClassesOf = AliasSeq!(AdjacentDerivedClassesOf, __traits(getMember, P, memberStr));
				}
			}
		}
	}
}

///
pure nothrow @safe version(nxt_test) unittest {
	import std.meta : AliasSeq;
	alias Subs = AdjacentDerivedClassesOf!SampleBase;
	static assert(is(Subs == AliasSeq!(SampleDerived1, SampleDerived2, SampleDerived3)));
	SampleBase d1 = new SampleDerived1();
	// dbg(d1);
	static foreach (S; Subs) {
		if (const s1 = cast(const(S))d1) {
			// dbg(s1);
			goto done;
		}
	}
	// dbg(d1);
done:
}

/++ Get bottom-most derived (sub) class of `T` present in the same module as `T` if any or `T` if none was found. +/
template BottomMostAdjacentDerivedClassOf(T) if (is(T == class)) {
	// TODO: Use class info to further derive
	alias DerivedClasses = AdjacentDerivedClassesOf!(T);
	static if (DerivedClasses.length)
		alias BottomMostAdjacentDerivedClassOf = DerivedClasses[$-1]; // the last is most likely to be the bottom-most
	else
		alias BottomMostAdjacentDerivedClassOf = T;
}

version(nxt_test) version(unittest) {
	static class SampleBase { int x; }
	static class SampleDerived1 : SampleBase { int y; }
	static class SampleDerived2 : SampleBase { int y; }
	static class SampleDerived3 : SampleDerived2 { int y; }
	static class OtherClass { int z; }
	static struct SomeStruct {}
	import nxt.dbgio;
}
