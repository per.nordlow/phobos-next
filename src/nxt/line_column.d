/** Conversions from string/file offset to line and column.
	See_Also: https://gcc.gnu.org/codingconventions.html#Diagnostics
    See_Also: https://clang.llvm.org/diagnostics.html
 */
module nxt.line_column;

import nxt.path : FilePath;
import nxt.string_traits : isASCII;

public import nxt.offset : Offset;

@safe:

/++ Source Line.
 +/
alias Line = uint;

/++ Source Column.
 +/
alias Column = uint;

/** Line and column, both 0-based byte offsets.
 *
 * Uses 32-bit unsigned precision for line and column offet, for now, like
 * tree-sitter does.
 */
struct LineColumn {
pure nothrow @nogc:
	Line line;					///< 0-based line byte offset.
	Column column;				///< 0-based column byte offset.
}

/** Convert byte offset `offset` in text-like contents `txt` to (line, column)
 *  byte offsets.
 *
 * The returned line byte offset and column byte offsets both start at zero.
 *
 * TODO: Extend to support UTF-8 in column offset when `asUTF8` is set.
 * TODO: Move to Phobos `std.string`?
 */
LineColumn scanLineColumnToOffset(in char[] txt, in Offset offset, in bool asUTF8 = false) pure nothrow @nogc
in(offset.sz <= txt.length)
in(!asUTF8 && txt.isASCII) {
	// find 0-based column offset
	size_t c = offset.sz;	  // cursor offset
	while (c != 0) {
		if (txt[c - 1] == '\n' ||
			txt[c - 1] == '\r')
			break;
		c -= 1;
	}
	// `c` is now at beginning of line

	/+ TODO: count UTF-8 chars in `txt[c .. offset.sz]` +/
	const column = offset.sz - c; // column byte offset

	// find 0-based line offset
	size_t lineCounter = 0;
	while (c != 0) {
		c -= 1;
		if (txt[c] == '\n') {
			if (c != 0 && txt[c - 1] == '\r') // DOS-style line ending "\r\n"
				c -= 1;
			else {} // Unix-style line ending "\n"
			lineCounter += 1;
		} else if (txt[c] == '\r') // Old Mac-style line ending "\r"
			lineCounter += 1;
		else {}				// no line ending at `c`
	}

	return typeof(return)(cast(Line)lineCounter, cast(Column)column);
}
alias offsetToLineColumn = scanLineColumnToOffset;
alias offsetToPosition = offsetToLineColumn;

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	auto x = "\nx\n y\rz\r\nw";
	assert(x.length == 10);
	assert(x.scanLineColumnToOffset(Offset(0)) == LineColumn(0, 0));
	assert(x.scanLineColumnToOffset(Offset(1)) == LineColumn(1, 0));
	assert(x.scanLineColumnToOffset(Offset(2)) == LineColumn(1, 1));
	assert(x.scanLineColumnToOffset(Offset(3)) == LineColumn(2, 0));
	assert(x.scanLineColumnToOffset(Offset(4)) == LineColumn(2, 1));
	assert(x.scanLineColumnToOffset(Offset(6)) == LineColumn(3, 0));
	assert(x.scanLineColumnToOffset(Offset(7)) == LineColumn(3, 1));
	assert(x.scanLineColumnToOffset(Offset(8)) == LineColumn(4, 0));
	assert(x.scanLineColumnToOffset(Offset(9)) == LineColumn(4, 0));
	assert(x.scanLineColumnToOffset(Offset(10)) == LineColumn(4, 1));
}

version(nxt_test) version(unittest) {
	import nxt.dbgio;
}
