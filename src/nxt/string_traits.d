module nxt.string_traits;

@safe:

/** Returns: `true` iff `x` is an ASCII (7-bit clean) set of `char`s.
 *
 * See_Also: `std.ascii.isASCII`.
 * See_Also: https://forum.dlang.org/post/syaqzkpybhvdehbhffjn@forum.dlang.org
 */
bool isASCII(in char[] input) pure nothrow @nogc {
	foreach (const e; cast(const(ubyte)[])input)
		if (e >= 0x80)
			return false;
	return true;
}

///
pure @safe version(nxt_test) unittest {
	assert(``.isASCII);
	assert(`_`.isASCII);
	assert(`a`.isASCII);
	assert(`ab`.isASCII);
	assert(`abc`.isASCII);
	assert(!`å`.isASCII);
	assert(!`åä`.isASCII);
	assert(!`åäö`.isASCII);
}
