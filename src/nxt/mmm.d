/++ Manual Memory Management. +/
module nxt.mmm;

/++ Allocate an instance of T using `malloc`. +/
private ref void[T.sizeof] mallocate(T)() @system {
	import core.memory : pureMalloc;
    return pureMalloc(T.sizeof)[0 .. T.sizeof];
}

///
pure nothrow @safe version(nxt_test) unittest {
	static struct S { int i = 1, j = 4, k = 9; }
	version(none) // TODO: Make this not crash.
	() @trusted {
		auto ps = mallocate!S();
		import core.memory : pureFree;
		pureFree(ps.ptr);
	}();
}

@safe:
