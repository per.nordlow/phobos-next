/** Extensions to `std.process`.
 *
 * Wrappers around commands such `git`, `patch`, etc.
 *
 * Original version https://gist.github.com/PetarKirov/b4c8b64e7fc9bb7391901bcb541ddf3a
 *
 * See_Also: https://github.com/CyberShadow/ae/blob/master/sys/git.d
 * See_Also: https://forum.dlang.org/post/ziqgqpkdjolplyfztulp@forum.dlang.org
 *
 * TODO: integrate @CyberShadow’s ae.sys.git at ae/sys/git.d
 */
module nxt.process;

import std.exception : enforce;
import std.format : format;
import std.file : exists, isDir, isFile;
import std.path : absolutePath, buildNormalizedPath, dirName, relativePath;
import std.process : pipeProcess;
import std.stdio : File, stdout, stderr;
import std.algorithm.mutation : move;
import nxt.logger : LogLevel, defaultLogLevel, trace, info, warning, error;

@safe:

/** Process exit status (code).
 *
 * See: https://en.wikipedia.org/wiki/Exit_status
 */
struct ExitStatus {
	static immutable ok = typeof(this)(0);
	int value;
	bool opCast(T : bool)() const scope => value != 0;
}

/** Process spawn state.
	Action accessing predeclared file system resources.
 */
struct Spawn {
	import std.process : Pid, ProcessPipes;

	this(ProcessPipes processPipes, in LogLevel logLevel = defaultLogLevel) {
		this.processPipes = processPipes;
		this.logLevel = logLevel;
	}

	this(this) @disable;		// avoid copying `File`s for now

	auto ref setLogLevel(in LogLevel logLevel) scope pure nothrow @nogc {
		this.logLevel = logLevel;
		return this;
	}

	ExitStatus wait() {
		if (logLevel <= LogLevel.trace) .trace("Waiting");

		import std.process : wait;
		auto res = typeof(return)(processPipes.pid.wait());

		static void echo(File src, ref File dst, in string name) in(src.isOpen) in(dst.isOpen) {
			import nxt.io : ewriteln;
			if (src.eof)
				return; // skip if empty
			ewriteln("  - ", name, ":");
			src.flush();
			import std.algorithm.mutation: copy;
			ewriteln("src:",src, "dst:",dst, "stdout:",stdout, "stderr:",stderr);
			() @trusted { src.byLine().copy(dst.lockingBinaryWriter); } (); /+ TODO: writeIndented +/
		}

		if (res == ExitStatus.ok) {
			if (logLevel <= LogLevel.trace)
				.trace("Process exited successfully");
		} else {
			if (logLevel <= LogLevel.error)
				.error("Process exited unsuccessfully with exit status ", res);
		}

		import std.stdio : stdout, stderr;
		if (res != ExitStatus.ok && logLevel <= LogLevel.info) {
			() @trusted { if (processPipes.stdout != stdout) echo(processPipes.stdout, stdout, "OUT"); } ();
		}
		if (res != ExitStatus.ok && logLevel <= LogLevel.warning) {
			() @trusted { if (processPipes.stderr != stderr) echo(processPipes.stderr, stderr, "ERR"); } ();
		}

		return res;
	}

package:
	ProcessPipes processPipes;
	LogLevel logLevel;
}

Spawn spawn(scope const(char[])[] args, in LogLevel logLevel = defaultLogLevel) {
	import std.process : spawnProcess;
	if (logLevel <= LogLevel.trace) .trace("Spawning ", args);
	return typeof(return)(pipeProcess(args), logLevel);
}

version(none): // TODO: Complete

import std.process;
import std.string : toStringz;
import std.exception : enforce;
import core.sys.posix.unistd;
import core.sys.posix.sys.types;
import core.sys.posix.sys.wait;
import core.sys.posix.fcntl;
import core.sys.posix.signal;
import core.sys.linux.sys.prctl;
import core.stdc.stdlib;
import core.stdc.stdio;
import core.stdc.errno;
import core.runtime;
import std.stdio;

// Define ptrace constants if not already defined
version (linux) {
    enum PTRACE_TRACEME = 0;
    enum PTRACE_SYSCALL = 24;
    enum PTRACE_CONT = 7;
}

alias TraceCallback = void function(int pid, int status);

struct TraceFlags {
    bool onExec;
    bool onFork;
    bool onSyscall;
    bool onExit;
}

/++
 + Guard running of processes using tracing of processes.
 + Uses system call `ptrace` on Linux.
 +
 + Params:
 +     args = Command and arguments to execute
 +     flags = Tracing flags
 +     cb = Callback function to be called on trace events
 +
 + Returns: ProcessPipes structure for the traced process
 +
 + Throws: Exception if process creation or tracing fails
 +/
ProcessPipes pipeProcessTraced(string[] args, TraceFlags flags, TraceCallback cb) {
    int[2] fds;
    enforce(pipe(fds.ptr) != -1, "Failed to create pipe: " ~ strerror(errno).to!string);

    auto pid = fork();
    if (pid == -1) {
        throw new Exception("Failed to fork");
    }

    if (pid == 0) {
        // Child process
        close(fds[0]);

        // Attach to the child process using ptrace
        enforce(ptrace(PTRACE_TRACEME, 0, null, null) != -1, "ptrace failed: " ~ strerror(errno).to!string);

        // Set parent death signal
        enforce(prctl(PR_SET_PDEATHSIG, SIGHUP) != -1, "prctl failed: " ~ strerror(errno).to!string);

        // Execute the target program
        execvp(args[0].toStringz, args.map!(a => a.toStringz).array.ptr);

        // If execvp fails
        stderr.writefln("execvp failed: %s", strerror(errno));
        _exit(1);
    } else {
        // Parent process
        close(fds[1]);

        int status;
        while (true) {
            auto wpid = waitpid(pid, &status, 0);
            enforce(wpid != -1, "waitpid failed: " ~ strerror(errno).to!string);

            if (WIFEXITED(status) || WIFSIGNALED(status)) {
                if (flags.onExit) {
                    cb(pid, status);
                }
                break;
            }

            if (WIFSTOPPED(status)) {
                int signal = WSTOPSIG(status);

                if (flags.onSyscall && signal == SIGTRAP) {
                    cb(pid, status);
                    enforce(ptrace(PTRACE_SYSCALL, pid, null, null) != -1, "ptrace failed: " ~ strerror(errno).to!string);
                } else if (flags.onExec && signal == SIGTRAP) {
                    cb(pid, status);
                    enforce(ptrace(PTRACE_CONT, pid, null, null) != -1, "ptrace failed: " ~ strerror(errno).to!string);
                } else if (flags.onFork && (signal == SIGTRAP || signal == SIGSTOP)) {
                    cb(pid, status);
                    enforce(ptrace(PTRACE_CONT, pid, null, null) != -1, "ptrace failed: " ~ strerror(errno).to!string);
                } else {
                    // Forward the signal to the child process
                    enforce(ptrace(PTRACE_CONT, pid, null, cast(void*)signal) != -1, "ptrace failed: " ~ strerror(errno).to!string);
                }
            }
        }
    }

    // Create and return ProcessPipes
    return pipeProcess(args, Redirect.all);
}

pure nothrow @safe version(nxt_test) unittest {
    import core.thread : Thread;
    import core.time : msecs;

    static void myTraceCallback(int pid, int status) {
        writefln("Tracepoint occurred: PID=%d, STATUS=%d", pid, status);
    }

    TraceFlags flags = TraceFlags(true, false, true, true);

    // Use a simple command that's likely to exist on most systems
    auto process = pipeProcessTraced(["ls", "-l"], flags, &myTraceCallback);

    // Read the output
    string output = process.stdout.readln();

    // Wait for the process to finish
    process.pid.wait();

    // Check if we got some output
    assert(output.length > 0, "Expected some output from 'ls -l'");
}

pure nothrow @safe version(nxt_test) unittest {
    import std.file : remove;
    import std.path : buildPath, dirName;
    import std.array : array;
}
