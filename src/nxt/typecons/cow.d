/++ Copy-on-Write reference counting (RC) wrapper types.
	See: `std.typecons.RefCounted`.
 +/
module nxt.typecons.cow;

/** Copying-on-Write (CoW) wrapper around array-like type `T`.
 */
struct CoW(T) if (is(typeof(T.init[0])/+hasIndexing+/) && is(typeof(T.init.length) : size_t/+hasLength+/)) {

	private alias E = typeof(T.init[0]);

	this(E e) nothrow @trusted {
		_store = [e];
	}

	ref typeof(this) opIndexAssign(E)(E elt, in size_t i) @trusted {
		version(D_Coverage) {} else version(LDC) pragma(inline, true);
		copyIfAliased();
		_store[] = elt;
		return this;
	}
	/// ditto
	ref typeof(this) opSliceAssign(E)(E[] elts, in size_t i, in size_t j) @trusted
	if (is(typeof(T.init[])/+hasSlicing+/)) {
		version(D_Coverage) {} else version(LDC) pragma(inline, true);
		copyIfAliased();
		_store[i .. j] = elts;
		return this;
	}

	/++ Element access by value is @trusted. +/
	E opIndex(in size_t i) const @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		return (cast(CoW!(T)*)&_store)._store.refCountedPayload[i];
	}

	/++ Non-mutable slicing must be `@system const return scope`. +/
	const(E)[] opSlice(in size_t i = 0, in size_t j = this.length) const return scope @system {
		version(D_Coverage) {} else pragma(inline, true);
		return (cast(CoW!(T)*)&_store)._store.refCountedPayload[i .. j];
	}

	/++ Mutable slicing must be `@system const return scope` and check uniqueness. +/
	E[] opSlice(in size_t i = 0, in size_t j = this.length) return scope @system
	in(refCount == 1) /+unique reference+/ {
		version(D_Coverage) {} else pragma(inline, true);
		return (cast(CoW!(T)*)&_store)._store.refCountedPayload[i .. j];
	}

@property:

	size_t length() const @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		return (cast(CoW!(T)*)&_store)._store.refCountedPayload.length;
	}
	/// ditto
	alias opDollar = length;

	size_t refCount() const {
		version(D_Coverage) {} else pragma(inline, true);
		return _store.refCountedStore.refCount;
	}

private:

	/++ If `this` is aliased (`refCount >= 2`) duplicate it to guarantee it holding a unique
		reference (pointer) inside `_store`.
	 +/
	void copyIfAliased() {
		version(D_Coverage) {} else version(LDC) pragma(inline, true);
		if (_store.refCountedStore.refCount >= 2)
			_store = Store(_store.refCountedPayload.dup);
	}

	import std.typecons : RC = SafeRefCounted;
	alias Store = RC!T;
	Store _store;
}

///
pure nothrow @safe version(nxt_test) unittest {
	alias A = int[];
	alias C = CoW!(A);

	auto c = C(42);
	assert(c.length == 1);

	assert(c[0] == 42);
	assert(c.refCount == 1);

	c[0] = 43; // `opIndexAssign`
	assert(c[0] == 43);
	assert(c.refCount == 1);

	auto d = c;
	assert(c.refCount == 2);
	assert(d.refCount == 2);

	c[0] = 44; // `opIndexAssign`, copies on write
	assert(c[0] == 44);
	assert(c.refCount == 1);
	assert(d.refCount == 1);

	const e = c;
	assert(c.refCount == 2);
	assert(e.refCount == 2);
	c[0 .. 1] = [45]; // `opSliceAssign`, copies on write
	assert(c[0] == 45);
	assert(c.refCount == 1);
	assert(e.refCount == 1);

	() @trusted { // mutable slicing must be @system
		assert(c[0 .. 1] == [45]);
		assert(c[0 .. $] == [45]);
	}();

	() @trusted { // non-mutable slicing must be @system
		assert(e[0 .. 1] == [44]);
		assert(e[0 .. $] == [44]);
	}();
}

/++ Copy-on-Write Reference Counted `T`.
	See: `std.typecons.RefCounted`.
	See_Also: https://tour.dlang.org/tour/en/gems/opdispatch-opapply
	See_Also: https://forum.dlang.org/post/ltfhlijvlliaeybpcteb@forum.dlang.org
	TODO: Make this work and make public.
 +/
private struct RefCountedCoW_alternative(T) {
	this(T value) {
		_store = Store(value);
	}
	auto opDispatch(string memberName, Args...)(Args args) {
		// TODO: Make this work using https://tour.dlang.org/tour/en/gems/opdispatch-opapply
		version(none) {
			static if (__traits(hasMember, T, memberName) &&
				   is(typeof(__traits(getMember, T.init, memberName)) == function)) {
				enum bool isConstMember = __traits(compiles, __traits(getMember, cast(const T)T.init, memberName));
				if (!isConstMember && _store.refCount > 1)
				_store = _store.dup();
				return __traits(getMember, _store.get(), memberName)(args);
			} else
				static assert(false, "No such member in T: " ~ memberName);
		}
	}
	void opDispatch(string memberName, Args...)(Args args) {
		__traits(getMember, _store.get, memberName)(args);
	}
private:
	import std.typecons : RC = SafeRefCounted;
	alias Store = RC!T;
	Store _store;
}

version(none):

pure nothrow @safe @nogc version(nxt_test) unittest {
	auto x = RefCountedCoW_alternative!(S)(S(42));
	auto y = x;
	assert(x.getX() == 42);
	assert(y.getX() == 42);
	y.increment();
	assert(x.getX() == 42);
	assert(y.getX() == 43);
}

version(nxt_test) version(unittest) {
	struct S {
		int x;
		void increment() {
			x += 1;
		}
		int getX() const {
			return x;
		}
	}
}
