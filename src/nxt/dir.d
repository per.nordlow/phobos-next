/** Extensions to std.file.dirEntries.
	Copyright: Per Nordlöw 2024-.
	License: $(WEB boost.org/LICENSE_1_0.txt, Boost License 1.0).
	Authors: $(WEB Per Nordlöw)
*/
module nxt.dir;

import std.file : dirEntries, SpanMode, isDir, DirEntry;
import nxt.path : DirPath, DirName, buildPath, exists;

@safe:

/++ Directory Scanning Flags|Options. +/
struct ScanFlags {
	alias Depth = ushort; // as path length <= 4096 on all architectures
	Depth depthMin = 0;
	Depth depthLength = Depth.max;
	bool followSymlink = true;
}

void dirEntriesX(in char[] root,
				in ScanFlags scanFlags = ScanFlags.init,
				in ScanFlags.Depth depth = ScanFlags.Depth.init) {
	import std.file : std_dirEntries = dirEntries, SpanMode;
	const root_ = () @trusted { return cast(string)(root); }();
	foreach (ref dent; std_dirEntries(root_, SpanMode.shallow, scanFlags.followSymlink)) {
		const depth1 = cast(ScanFlags.Depth)(depth + 1);
		if (dent.isDir && depth1 < scanFlags.depthMin + scanFlags.depthLength)
			dirEntriesX(dent.name, scanFlags, depth1);
		else if (depth >= scanFlags.depthMin) {
			assert(0, "TODO: Turn into a range");
		}
	}
}
/// ditto
void dirEntriesX(DirPath root,
				in ScanFlags scanFlags = ScanFlags.init,
				in ScanFlags.Depth depth = ScanFlags.Depth.init) @trusted {
	dirEntriesX(root.str, scanFlags, depth);
}

version(nxt_test) version(unittest) {
import nxt.dbgio : dbg;
}
