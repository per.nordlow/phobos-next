module nxt.io.sink;

@safe:

/++ Static to Dynamic Growable Sink. +/
struct SDSink(uint requestedSmallSize = 256) {
	/// TODO: When possible, infer upper bound in call to `T.toString` or signal it via a member `T.toStringMaxLength`.
	enum smallSize = requestedSmallSize;
private:
	char[smallSize] _smallStore; // placed first for common case
	size_t _length;
	char[] _largeStore; // TODO: Replace with: char* _largePtr; to avoid one more word
public pure scope nothrow:
	// TODO: @disable this(this);
	void reserve(in size_t capacity) {
		version(D_Coverage) {} else pragma(inline, true);
		if (capacity > _smallStore.length) {
			const needGrowth = !isLarge;
			_largeStore.reserve(capacity);
	 		if (needGrowth) {
				_largeStore.length = _length;
				_largeStore[0 .. _length] = _smallStore[0 .. _length];
			}
		}
	}
	void put(in char[] es) {
		reserve(_length + es.length);
		if (isLarge)
			_largeStore ~= es;
		else
			_smallStore[_length .. _length + es.length] = es;
		_length += es.length;
	}
	void put(in char e) {
		reserve(_length + 1);
		if (isLarge)
			_largeStore ~= e;
		else
			_smallStore[_length] = e;
		_length += 1;
	}
    void opCall(in char[] es) {
        put(es);
    }
    void opCall(in char e) {
		version(D_Coverage) {} else pragma(inline, true);
        put(e);
    }
	inout(char)[] data() inout scope return /+@scope+/ @property @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		if (isLarge)
			return _largeStore[0 .. _length];
		else
			return _smallStore[0 .. _length];
	}
	size_t length() inout return scope @property {
		version(D_Coverage) {} else pragma(inline, true);
		return _length;
	}
	bool isLarge() const return scope @property {
		version(D_Coverage) {} else pragma(inline, true);
		return _largeStore.ptr !is null;
	}
}

public auto sdSink(uint requestedSmallSize = 256)() {
	return SDSink!(requestedSmallSize).init;
}

@safe nothrow version(nxt_test) unittest {
	auto sink = sdSink();
	alias S = typeof(sink);
	sink.reserve(2);
	sink("a");
	sink('b');
	assert(!sink.isLarge);
	assert(sink.length == 2);
	assert(sink.data == "ab");
	sink.reserve(S.smallSize);
	assert(!sink.isLarge);
	sink.reserve(S.smallSize + 1);
	assert(sink.isLarge);
	assert(sink.length == 2);
	assert(sink.data == "ab");
	sink("c");
	sink('d');
	assert(sink.length == 4);
	assert(sink.data == "abcd");
}
