module nxt.io.format;

import nxt.visual : ColorTheme;

@safe:

/++ {Writing|Printing|Serialization} Format. +/
@safe struct Format {
pure nothrow @safe @nogc:
	static typeof(this) plain() {
		typeof(return) result;
		result.showAddresses = true;
		result.showClassValues = true;
		result._dereferencePointerValuesUnsafe = false;
		result.showEnumeratorEnumType = true;
		result.showEnumatorValues = true;
		result.useANSIColors = true;
		return result;
	}
	static typeof(this) compact() {
		import nxt.algorithm.mutation : strip;
		typeof(return) result = typeof(this).plain;
		result.multiLine = false;
		result.fieldTypeSeparator = result.fieldTypeSeparator.strip;
		result.fieldNameSuffix = result.fieldNameSuffix.strip;
		result.elementSeparator = result.elementSeparator.strip;
		result.aggregateFieldSeparator = result.aggregateFieldSeparator.strip;
		return result;
	}
	static typeof(this) dbg() {
		typeof(return) result = typeof(this).plain;
		result.showFieldNames = true;
		result.showRangeLengths = true;
		result.showClassInfoNames = true;
		result.elementMaxCount = 16;
		return result;
	}
	static typeof(this) pretty() {
		typeof(return) result = typeof(this).plain;
		result.showFieldNames = true;
		result.useANSIColors = true;
		return result;
	}
	static typeof(this) fancy() {
		typeof(return) result = typeof(this).pretty;
		result.showClassInfoNames = true;
		result.showRangeLengths = true;
		return result;
	}
	static typeof(this) everything() {
		typeof(return) result = typeof(this).fancy;
		result.multiLine = true;
		return result;
	}
	static typeof(this) debugging() {
		typeof(return) result = typeof(this).fancy;
		result.showArrayOfVoidValues = true;
		result.abbreviateInitValues = true;
		result.elementMaxCount = 16;
		result.multiLine = true;
		result.ignoreToString = true;
		return result;
	}
	static typeof(this) compactJSON(in size_t elementMaxCount = size_t.max) {
		typeof(return) result = typeof(this).init;
		result.showFieldNames = true;
		result.elementMaxCount = elementMaxCount;
		result.aggregatePrefix = "{";
		result.aggregateSuffix = "}";
		result.fieldNamePrefix = `"`;
		result.fieldNameSuffix = `":`;
		return result;
	}
	static typeof(this) prettyJSON(in size_t elementMaxCount = size_t.max) {
		typeof(return) result = typeof(this).compactJSON;
		result.multiLine = true;
		result.elementMaxCount = elementMaxCount;
		result.aggregatePrefix = "{ ";
		result.aggregateSuffix = " }";
		result.fieldNamePrefix = `"`;
		result.fieldNameSuffix = `": `;
		return result;
	}
	size_t level = 0; ///< Level of (aggregate) nesting starting at 0.
	string indentation = "\t"; ///< Indentation string.
	size_t elementMaxCount = size_t.max; ///< Show at most this many (array slice) range elements.
	// Prefixes and Suffixes:
	char arrayPrefix = '['; ///< {Array|Range} prefix.
	char arraySuffix = ']'; ///< {Array|Range} suffix.
	char mapPrefix = '['; ///< (Hash) Map (container) (Associative Array) prefix.
	char mapSuffix = ']'; ///< (Hash) Map (container) (Associative Array) suffix.
	char setPrefix = '{'; ///< (Hash) Set container prefix. TODO: Use
	char setSuffix = '}'; ///< (Hash) Set container suffix. TODO: Use
	string aggregatePrefix = "("; ///< Aggregate fields prefix.
	string aggregateSuffix = ")"; ///< Aggregate fields suffix.
	char backReferencePrefix = '#'; ///< Backward reference prefix.
	string fieldNamePrefix = ""; ///< Field name prefix. Typically either "" or ".".
	string fieldNameSuffix = ": "; ///< Field name suffix.
	string rangeLengthSeparator = "$:"; ///< Separator between (array) range literal and its length (element count).
	string staticPrefix = "#"; ///< Prefix for statically known type and size information.
	string pathWrapperSymbol = ``; ///< Wrapper symbol when printing paths and URIs to standard out(`stdout`) and standard error (`stderr`). TODO: Use. +/
	// Separators:
	string fieldTypeSeparator = " "; ///< Field type separator.
	string elementSeparator = ", "; ///< {Array|Range} element separator.
	string aggregateFieldSeparator = ", "; ///< Aggregate field separator.
	string mapKeyValueSeparator = ": "; ///< Separator between key and value for each element in a map (including associative array).
	// Serialization:
	bool keyNameUDAs; ///< UDAs for key name(s). See_Also: `@serdeKeys`, `@serdeKeyOut`.
	bool ignoreFieldNameUDAs; ///< UDAs for field name(s) to ignore. See_Also: `@serdeIgnore`.
	// Visual
	ColorTheme colorTheme;
	// Flags:
	// TODO: Pack these using bitfields when that doesn't crash the compiler.
	bool quoteChars; ///< Wrap characters {char|wchar|dchar} in ASCII character '\''.
	bool quoteStrings; ///< Wrap strings {string|wstring|dstring} in ASCII character '"'.
	bool showAddresses; ///< Show address of classes and pointers.
	bool showClassValues; ///< Show fields of non-null classes (instead of just pointer).
	bool showClassInfoNames; ///< Use `.classinfo.name` to display the name of a class instance.
	package bool _dereferencePointerValuesUnsafe; ///< Show values of non-null pointers (instead of just pointer). WARNING: Unsafe.
	bool showEnumeratorEnumType; ///< Show enumerators as `EnumType.enumeratorValue` instead of `enumeratorValue`.
	bool showEnumatorValues; ///< Show values of enumerators instead of their names.
	bool showFieldNames; ///< Show names of fields.
	bool showFieldTypes; ///< Show types of values.
	bool showArrayOfVoidValues; ///< Show values of void arrays as ubytes instead of `[?]`.
	bool showRangeLengths; ///< Show lengths of non-array(slice) ranges when available.
	bool showRangeElements; ///< Show elements of non-array(slice ranges (including infinite ones) instead of printing their type like Phobos does. TODO: Use
	bool abbreviateInitValues; ///< Abbreviate default values to `.init` when it reduces the size of the output.
	bool hideInitValues; ///< Hide default values to `.init`.
	bool multiLine; ///< Span multiple lines using `indent`.
	bool useANSIColors; ///< Use different (colored) fonts for different types. TODO: Use `nxt.ansi_escape`.
	bool ignoreToString; ///< Avoid using `T.toString` member(s) when formatting `T`.
	bool showAggregateRangesAsTables; ///< Show ranges of aggregates as tables. TODO: Use
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert(Format.plain != Format.compact);
	assert(Format.plain != Format.pretty);
	assert(Format.pretty != Format.fancy);
	assert(Format.pretty != Format.dbg);
	assert(Format.fancy != Format.debugging);
	assert(Format.fancy != Format.everything);
	assert(Format.plain != Format.compactJSON);
	assert(Format.plain != Format.prettyJSON);
}
