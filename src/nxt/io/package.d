/** Backwards-compatible extensions of `std.stdio.{f}write{ln}` to `p{f}write{ln}`.
 *
 * See_Also: Rust's `std.io`.
 *
 * See_Also: `core.internal.dassert`
 *
 * TODO: Ask AI to convert all prints to instead use output sink using
 *       either `.put` or `snprintf`.
 *
 * TODO: Ask AI how to create minor mode `backrefs-mode` used in
 *       `compilation-mode` that creates buttons for `#BACKREFNR`. See `backReferencePrefix`.
 *
 * NOTE: Replacing calls to overloaded `fpwrite1` with non-overloaded versions
 *       such as `fpwrite1_char`, `fpwrite1_string` reduces compilation memory usage.
 *
 * TODO: Cast to `arg const` for `T` being `struct` and `class` with `const toString` to avoid template-bloat
 *
 * TODO: Use setlocale to enable correct printing {w|d}char([])
 * #include <wchar.h>
 * #include <locale.h>
 * #include <stdio.h>
 *
 * int main() {
 *     // Set the locale to the user default, which should include Unicode support
 *     setlocale(LC_ALL, "");
 *
 *     wchar_t letter1 = L'å';
 *     wchar_t letter2 = L'ä';
 *     wchar_t letter3 = L'ö';
 *
 *     wprintf(L"%lc %lc %lc\n", letter1, letter2, letter3);
 *
 *     return 0;
 * }
 *
 * In D you use:
 *
 * import core.stdc.locale : setlocale, LC_ALL;
 * () @trusted { setlocale(LC_ALL, ""); }();
 *
 * TODO: Perhaps make public (top) functions throw static exceptions keeping them `@nogc`.
 */
module nxt.io;

// version = show;

import core.stdc.stdio : fputc, fprintf, FILE, EOF, c_fwrite = fwrite;
public import core.stdc.stdio : stdout, stderr, fflush;
import core.stdc.wchar_ : fputwc, fwprintf;
import nxt.visiting : Addrs = Addresses;
import nxt.effects : writes_to_console; // TODO: uncomment references below when `dub test` passes without linker failure
import nxt.ansi_escape : ColorRGB8, putWithSGRs;
public import nxt.io.sink;
public import nxt.io.format;
public import nxt.io.uda;

@safe:

/** {Result|Return} Status.
 *
 * Either number of bytes written or -1 (`EOF`) on error.
 */
struct Status {
	static immutable none = typeof(this)(0);
	long value; /++ Actual value returned from C function typically either `.*put.*` `.*print.*`, `.*write.*`, etc. +/
	bool opCast(T : bool)() const scope => value < 0;
}
private alias St = Status;

/** Pretty-formatted `fwrite(sm, ...)`. */
St fpwrite(Args...)(scope FILE* sm, in Format fmt, in Args args) /+TODO: @writes_to_console+/ {
	scope Addrs addrs;
	foreach (ref arg; args) {
		static if (is(typeof(arg) == enum))
			sm.fpwrite1_enum(arg, fmt, addrs);
		else
			sm.fpwrite1(arg, fmt, addrs);
	}
	return St.none;
}

/** Pretty-formatted `fwrite(stderr, ...)`. */
St epwrite(Args...)(in Format fmt, in Args args) /+TODO: @writes_to_console+/
	=> fpwrite(stderr, fmt, args);

/** Alternative to `std.stdio.fwrite`. */
St fwrite(Args...)(scope FILE* sm, in Args args) /+/+TODO: @writes_to_console+/+/
	=> sm.fpwrite!(Args)(Format.init, args);

/** Alternative to `std.stdio.write(stdout)`. */
St write(Args...)(in Args args) /+TODO: @writes_to_console+/
	=> stdout.fwrite(args);

/** Alternative to `std.stdio.write(stderr)`. */
St ewrite(Args...)(in Args args) /+TODO: @writes_to_console+/
	=> stderr.fwrite(args);

/** Alternative to `std.stdio.writeln(stderr)`. */
St ewriteln(Args...)(in Args args) /+TODO: @writes_to_console+/
	=> stderr.fwriteln(args);

/** Pretty-formatted `fwriteln`. */
St fpwriteln(Args...)(scope FILE* sm, in Format fmt, in Args args) /+/+TODO: @writes_to_console+/+/ {
	{ const st = sm.fpwrite!(Args)(fmt, args); if (st == St(EOF)) { return st; } }
	{ const st = sm.fpwrite1_char('\n'); if (st == St(EOF)) { return st; } }
	// sm.fflush();
	return St.none;
}

/** Pretty-formatted `writeln`. */
St pwriteln(Args...)(in Format fmt, in Args args) /+/+TODO: @writes_to_console+/+/
	=> stdout.fpwriteln(fmt, args);
/** Pretty-formatted `stderr.writeln`. */
St epwriteln(Args...)(in Format fmt, in Args args) /+TODO: @writes_to_console+/
	=> stderr.fpwriteln(fmt, args);

/** Alternative to `std.stdio.fwriteln`. */
St fwriteln(Args...)(scope FILE* sm, in Args args) /+TODO: @writes_to_console+/ {
	{ const st = sm.fwrite(args); if (st == St(EOF)) { return st; } }
	{ const st = sm.fpwrite1_char('\n'); if (st == St(EOF)) { return st; } }
	// sm.fflush();
	return St.none;
}

/** Alternative to `std.stdio.writeln`. */
St writeln(Args...)(in Args args) /+/+TODO: @writes_to_console+/+/
	=> stdout.fwriteln(args);

/// core.time.Duration
version(none)
@safe nothrow version(nxt_test) unittest {
	import core.time : Duration;
	auto fmt = Format.fancy;
	fmt.ignoreToString = true;
	fmt.pwriteln(Duration.init);
}

version(none)
@safe version(nxt_test) unittest {
	import core.time : Duration;
	writeln(Duration.init);
}

package:

/** Pretty-formatted write single argument `arg` to `sm`. */
St fpwrite1(T)(scope FILE* sm, in T arg, in Format fmt, scope ref Addrs addrs) /+TODO: @writes_to_console+/ {
	// special handling when `T` == `T.init`:
	static if (is(T == class) ||
			   is(T == const(_U1)*, _U1) ||
			   __traits(isAssociativeArray, T)) {
		const isInit = arg is T.init;
	} else static if (is(T : const(_U2)[], _U2)) /+isDynamicArray+/ {
		const isInit = arg == T.init;
	} else static if (is(T : const(void[]))) {
		const isInit = arg == T.init;
	} else static if (__traits(isStaticArray, T)) {
		const isInit = arg == T.init;
	} else static if (is(arg == T.init)) {
		const isInit = () @trusted { return arg == T.init; }();
	} else {
		// pragma(msg, __FILE__, "(", __LINE__, ",1): Debug: TODO: Handle ", T);
		const isInit = false; // default to not initialized for unhandled cases
	}
	if (isInit) {
		// pragma(msg, __FILE__, "(", __LINE__, ",1): Debug: TODO: Handling ", T);
		if (fmt.hideInitValues)
			return St.none;
		static if (is(T : _U1[_N1], _U1, size_t _N1) /+|| is(T : __vector(_U2[_N2]), _U2, size_t _N2)+/) {
			if (fmt.abbreviateInitValues) {
				{ const st = sm.fpwrite1_string(T.stringof); if (st == St(EOF)) return st; }
				{ const st = sm.fpwrite1_string(".init"); if (st == St(EOF)) return st; }
			}
		}
	}
	// TODO: perhaps use this to support pretty printing of string-wrapper types such as `FilePath`, `DirPath`, etc.
	/+static if (is(typeof(arg.str) : const(char)[])) {
		return sm.fpwrite1(sink.data, fmt, addrs);
	} else +/
	static if (__traits(hasMember, T, "toString")) {
		if (!fmt.ignoreToString) {
			scope sink = sdSink();
			// TODO: Ideally, for each `T` calculate the maximum size of the buffer needed and use that as the size of the stack array buffer
			static if (T.stringof == "Duration") {
				// TODO: Remove this special casing and pass a delegate sink as `Duration.toString` does but make it @nogc instead
				return sm.fpwrite1(arg.toString, fmt, addrs);
			} else static if (is(typeof(arg.toString(sink, fmt)) == void)) {
				() @trusted { arg.toString(sink, fmt); }();
				return sm.fpwrite1(sink.data, fmt, addrs);
			} else static if (is(typeof(arg.toString(sink)) == void)) {
				() @trusted { arg.toString(sink); }();
				return sm.fpwrite1(sink.data, fmt, addrs);
			} else {
				static if (is(typeof(arg.toString) : const(char)[])) {
					const str =	() @trusted { return arg.toString; /+avoid scope compilation errors+/ }();
					{ const st = sm.fpwrite1_string(str, quote: fmt.quoteStrings); if (st == St(EOF)) return st; }
					return St.none;
				} else {
					// pragma(msg, __FILE__, "(", __LINE__, ",1): Debug: TODO: Call toString with  T:", T);
					Format fmt_ = fmt;
					fmt_.ignoreToString = true;
					return sm.fpwrite1(arg, fmt_, addrs);
				}
			}
		}
	}
	static if (is(T == enum)) {
		sm.fpwrite1_enum(arg, fmt, addrs);
	} else static if (is(T == enum)) {
		static assert(0, "TODO: Branch on `fmt.showEnumatorValues`");
	} else static if (is(T : __vector(U[N]), U, size_t N)) /+ must come before `__traits(isArithmetic, T)` below because `is(__traits(isArithmetic, float4)` holds: +/ {
		{ const st = sm.fpwrite1ArrayExceptString(arg, fmt, addrs, truncated: false, isStatic: true); if (st == St(EOF)) return st; }
	} else static if (is(immutable T == immutable bool)) {
		{ const st = sm.fpwrite1_string(arg ? "true" : "false"); if (st == St(EOF)) return st; }
	} else static if (__traits(isArithmetic, T)) {
		static if (is(immutable T == immutable char)) {
			if (fmt.quoteChars) { const st = fputc('\'', sm); if (st == St(EOF)) return st; }
			{ const st = fputc(arg, sm); if (st == St(EOF)) return st; }
			if (fmt.quoteChars) { const st = fputc('\'', sm); if (st == St(EOF)) return st; }
		} else static if (is(immutable T == immutable wchar) || is(immutable T == immutable dchar)) {
			if (fmt.quoteChars) { const st = fputc('\'', sm); if (st == St(EOF)) return st; }
			{ const st = fputwc(arg, sm); if (st == St(EOF)) return st; }
			if (fmt.quoteChars) { const st = fputc('\'', sm); if (st == St(EOF)) return st; }
		} else {
			// See: `getPrintfFormat` in "core/internal/dassert.d"
			static      if (is(immutable T == immutable byte))
				immutable pff = "%hhd";
			else static if (is(immutable T == immutable ubyte))
				immutable pff = "%hhu";
			else static if (is(immutable T == immutable short))
				immutable pff = "%hd";
			else static if (is(immutable T == immutable ushort))
				immutable pff = "%hu";
			else static if (is(immutable T == immutable int))
				immutable pff = "%d";
			else static if (is(immutable T == immutable uint))
				immutable pff = "%u";
			else static if (is(immutable T == immutable long))
				immutable pff = "%lld";
			else static if (is(immutable T == immutable ulong))
				immutable pff = "%llu";
			else static if (is(immutable T == immutable float))
				immutable pff = "%g"; // or %e %g
			else static if (is(immutable T == immutable double))
				immutable pff = "%lg"; // or %le %lg
			else static if (is(immutable T == immutable real))
				immutable pff = "%Lg"; // or %Le %Lg
			else
				static assert(0, "TODO: Handle argument of type " ~ T.stringof);
			() @trusted {
				{ const st = sm.fprintf(pff.ptr, arg); if (st == St(EOF)) return st; }
			} ();
		}
	} else static if (is(T : U[N], U, size_t N)) { // isStaticArray
		enum has_char = is(U == char);
		enum has_wchar = is(U == wchar);
		enum has_dchar = is(U == dchar);
		static if (has_char || has_wchar || has_dchar) { // `isSomeChar`
			if (fmt.quoteStrings)
				{ const st = sm.fpwrite1_char('"'); if (s1 == St(EOF)) return st; }
			static if (has_char)
				{ const st = sm.fpwrite1_string(arg, quote: fmt.quoteStrings, isStatic: true); if (st == St(EOF)) { return st; } }
			else static if (has_wchar)
				{ const st = sm.fpwrite1_wstring(arg, quote: fmt.quoteStrings, isStatic: true); if (st == St(EOF)) { return st; } }
			else static if (has_dchar)
				{ const st = sm.fpwrite1_dstring(arg, quote: fmt.quoteStrings,isStatic: true); if (st == St(EOF)) { return st; } }
			else
				static assert(0, "TODO: Handle static array of element type " ~ U.stringof);
			if (fmt.quoteStrings)
				{ const st = sm.fpwrite1_char('"'); if (st == St(EOF)) return st; }
		} else {
			{ const st = sm.fpwrite1ArrayExceptString(arg[]/+ `arg[]` avoids template bloat +/, fmt, addrs, truncated: false, isStatic: true); if (st == St(EOF)) { return st; } }
		}
	} else static if (is(T : const(U)[], U)) { // isDynamicArray
		enum has_char = is(U == char);
		enum has_wchar = is(U == wchar);
		enum has_dchar = is(U == dchar);
		static if (has_char || has_wchar || has_dchar) { // `isSomeChar`
			if (fmt.quoteStrings)
				{ const st = sm.fpwrite1_char('"'); if (st == St(EOF)) return st; }
			static if (has_char)
				{ const st = sm.fpwrite1_string(arg, quote: fmt.quoteStrings, isStatic: false); if (st == St(EOF)) { return st; } }
			else static if (has_wchar)
				{ const st = sm.fpwrite1_wstring(arg, quote: fmt.quoteStrings, isStatic: false); if (st == St(EOF)) { return st; } }
			else static if (has_dchar)
				{ const st = sm.fpwrite1_dstring(arg, quote: fmt.quoteStrings,isStatic: false); if (st == St(EOF)) { return st; } }
			else
				static assert(0, "TODO: Handle static array of element type " ~ U.stringof);
			if (fmt.quoteStrings)
				{ const st = sm.fpwrite1_char('"'); if (st == St(EOF)) { return st; } }
		} else {
			import std.algorithm.comparison : min;
			const truncated = arg.length > fmt.elementMaxCount;
			{ const st = sm.fpwrite1ArrayExceptString(arg[0 .. min(arg.length, fmt.elementMaxCount)], fmt, addrs, truncated: truncated, isStatic: false); if (st == St(EOF)) { return st; } }
		}
	} else static if (is(__traits(isAssociativeArray, T)) || is(typeof(T.init.byKeyValue))) { // isMapLike
		{ const st = sm.fpwrite1_char(fmt.mapPrefix); if (st == St(EOF)) { return st; } }
		static if (is(T : TK[TV], TK, TV)) {
			// TK and TV set
		} else { // isMapLike
			alias TK = typeof(T.init.keys[0]);
			alias TV = typeof(T.init.values[0]);
		}
		// element formats. TODO: only duplicate if `fmt.indentation` is not already `null`
		Format keyFmt = fmt; // key format
		Format valFmt = fmt; // value format
		static if (!is(TK == struct) && !is(TK == class))
			keyFmt.indentation = null;
		static if (!is(TV == struct) && !is(TV == class))
			valFmt.indentation = null;
		// print values:
		size_t i;
		foreach (ref kv; arg.byKeyValue) {
			if (i)
				{ const st1_ = sm.fpwrite1_string(fmt.elementSeparator); if (st1_ == St(EOF)) { return st1_; } }
			sm.fpwrite1(kv.key, keyFmt, addrs);
			{ const st = sm.fpwrite1_string(fmt.mapKeyValueSeparator); if (st == St(EOF)) { return st; } }
			sm.fpwrite1(kv.value, valFmt, addrs);
			i += 1;
		}
		{ const st = sm.fpwrite1_char(fmt.mapSuffix); if (st == St(EOF)) { return st; } }
	} else static if (is(T == struct)) { // struct
		sm.fpwriteAggregate(arg, fmt, addrs);
    } else static if (is(T == class) || is(T == const(_U)*, _U)) { // isAddress
        const isNull = arg is null;
		if (isNull) {
			{ const st = sm.fpwrite1_string("null"); if (st == St(EOF)) { return st; } }
			return St.none;
		}

		const addr = () @trusted { return cast(const void*)arg; }();
		if (fmt.showAddresses)
			{ const st = sm.fpwrite1_addr(addr, fmt); if (st == St(EOF)) { return st; } }
		import nxt.algorithm.searching : indexOf;
		const ix = addrs[].indexOf(addr);
		if (ix != -1) { // `addr` already printed
			{ const st = sm.fpwrite1_char(fmt.backReferencePrefix); if (st == St(EOF)) { return st; } }
			{ const st = sm.fpwrite1(ix, fmt, addrs); if (st == St(EOF)) { return st; } }
			return St.none;
		}
		addrs ~= addr; // `addrs`.lifetime <= `arg`.lifetime
        static if (is(T == class)) {
			if (fmt.showClassValues)
				{ const st = sm.fpwrite1ClassValue(arg, fmt, addrs); if (st == St(EOF)) return st; }
        } else {
			if (fmt._dereferencePointerValuesUnsafe) {
				{ const st = sm.fpwrite1_string(" -> "); if (st == St(EOF)) { return st; } }
				static if (is(typeof(*arg))) {
					static if (__traits(compiles, { const _ = cast()*arg; }/+isCopyable+/)) {
						{ const st = () @trusted { return sm.fpwrite1(cast(const)*arg, fmt, addrs); }(); if (st == St(EOF)) { return st; } }
					} else { // non-copyable or opaque forward-declared types
						{ const st = sm.fpwrite1_char('?'); if (st == St(EOF)) { return st; } }
					}
				} else { // `arg is void*`
					{ const st = sm.fpwrite1_char('?'); if (st == St(EOF)) { return st; } }
				}
			}
        }
	} else {
		pragma(msg, __FILE__, "(", __LINE__, ",1): Warning: ", "TODO: Handle argument of type " ~ T.stringof);
	}
	return St.none;
}

St fpwrite1ClassValue(T)(scope FILE* sm, in T arg, in Format fmt, scope ref Addrs addrs) /+TODO: @writes_to_console+/ {
	{ const st = sm.fpwrite1_string(" . "); if (st == St(EOF)) { return st; } }
	{ const st = sm.fpwriteAggregate(arg, fmt, addrs); if (st == St(EOF)) { return st; } }
	return St.none;
}

St fpwrite1ArrayExceptString(T)(scope FILE* sm, in T arg, in Format fmt, scope ref Addrs addrs, in bool truncated, in bool isStatic) /+TODO: @writes_to_console+/ {
	{ const st = sm.fpwrite1_char(fmt.arrayPrefix); if (st == St(EOF)) { return st; } }
	static if (is(immutable T == immutable void[])) {
		if (fmt.showArrayOfVoidValues) {
			ubyte[] bytes;
			() @trusted {
				bytes = cast(ubyte[])arg/+to mutable to avoid template-bloat+/;
			}();
			sm.fpwriteArrayElements(bytes, fmt, addrs, truncated); // TODO: perhaps show as hex instead
		} else if (arg.length != 0) { // if any unprintable elements
			{ const st = sm.fpwrite1_string("?"); if (st == St(EOF)) { return st; } }
		}
	} else {
		{ const st = sm.fpwriteArrayElements(arg[], fmt, addrs, truncated); if (st == St(EOF)) { return st; } }
	}
	{ const st = sm.fpwrite1_char(fmt.arraySuffix); if (st == St(EOF)) { return st; } }
	if (fmt.showRangeLengths) {
		{ const st = sm.fpwrite1_string(fmt.rangeLengthSeparator); if (st == St(EOF)) { return st; } }
		if (isStatic) {
			{ const st = sm.fpwrite1_string(fmt.staticPrefix); if (st == St(EOF)) { return st; } }
		}
		{ const st = sm.fpwrite1_ulong(arg.length, fmt); if (st == St(EOF)) { return st; } }
	}
	return St.none;
}

St fpwriteArrayElements(E)(scope FILE* sm, in E[] arg, in Format fmt, scope ref Addrs addrs, bool truncated) /+TODO: @writes_to_console+/ {
	Format efmt = fmt; // element format
	efmt.quoteChars = true; // mimics `std.stdio`
	efmt.quoteStrings = true; // mimics `std.stdio`
	static if (!(is(E == struct) || is(E == union) || is(E == class)))
		efmt.indentation = null;
	foreach (const i, ref elt; arg) {
		if (i)
			{ const st1_ = sm.fpwrite1_string(fmt.elementSeparator); if (st1_ == St(EOF)) { return st1_; } }
		{ const st = sm.fpwrite1(cast(const)elt/+just cast away qualifiers such as shared for now+/, efmt, addrs); if (st == St(EOF)) { return st; } }
	}
	if (truncated && arg.length != 0)
		{ const st = sm.fpwrite1_string(", …"); if (st == St(EOF)) { return st; } }
	return St.none;
}

St fpwriteAggregate(T)(scope FILE* sm, in T arg, in Format fmt, scope ref Addrs addrs) /+TODO: @writes_to_console+/
if (is(T == struct) || is(T == class)) {
	// name:
	bool nameWritten = false;
	static if (is(T == class)) {
		static if (is(typeof(arg.classinfo.name) : const(char)[])) {
			if (fmt.showClassInfoNames) {
				{ const st = sm.fpwrite1_string(arg.classinfo.name); if (st == St(EOF)) { return st; } }
				nameWritten = true;
			}
		}
	}
	if (!nameWritten)
		{ const st = sm.fpwrite1_type(T.stringof, T.mangleof); if (st == St(EOF)) { return st; } }

	{ const st = sm.fpwrite1_string(fmt.aggregatePrefix); if (st == St(EOF)) { return st; } }

	// fields:
	static if (is(T == class)) {
		{ const st = fpwriteBottomMostDerivedClassFieldsOf!(T)(sm ,arg, fmt, addrs); if (st == St(EOF)) { return st; } }
	} else {
		uint fieldOffset = 0;
		{ const st = fpwriteAggregateFieldsOf!(T)(sm ,arg, fmt, addrs, fieldOffset); if (st == St(EOF)) { return st; } }
	}

	{ const st = sm.fpwrite1_string(fmt.aggregateSuffix); if (st == St(EOF)) { return st; } }
	return St.none;
}

St fpwriteBottomMostDerivedClassFieldsOf(T)(scope FILE* sm, in T arg, in Format fmt, scope ref Addrs addrs) /+TODO: @writes_to_console+/
if (is(T == class)) {
	import nxt.class_traits : AdjacentDerivedClassesOf;
	alias Ds = AdjacentDerivedClassesOf!(T);
	uint fieldOffset = 0;
	static if (Ds.length) { // (some of) `T`'s derived classes found
		/+ reverse as it's likely that the last is the bottom-most.
		   TODO: Replace this with exact logic. +/
		foreach_reverse (D; Ds) {
			if (const argD = cast(const(D))arg) { // TODO: Exclude C++ classes from this as this fails
				{ const st = sm.fpwriteAllBaseClassFieldsOf(argD, fmt, addrs, fieldOffset: fieldOffset); if (st == St(EOF)) { return st; } }
				goto done;
			}
		}
	} else {
		{ const st = sm.fpwriteAllBaseClassFieldsOf(arg, fmt, addrs, fieldOffset: fieldOffset); if (st == St(EOF)) { return st; } }
	}
done:
	return St.none;
}

St fpwriteAllBaseClassFieldsOf(T)(scope FILE* sm, in T arg, in Format fmt, scope ref Addrs addrs, ref uint fieldOffset) /+TODO: @writes_to_console+/
if (is(T == class)) {
	{ const st = sm.fpwriteAggregateFieldsOf(arg, fmt, addrs, fieldOffset: fieldOffset); if (st == St(EOF)) { return st; } }
	static if (is(T P == super)) {
		foreach (Base; P) {
			sm.fpwriteAllBaseClassFieldsOf(cast(const(Base))arg, fmt, addrs, fieldOffset: fieldOffset);
		}
	}
	return St.none;
}

St fpwriteAggregateFieldsOf(T)(scope FILE* sm, in T arg, in Format fmt, scope ref Addrs addrs, ref uint fieldOffset) /+TODO: @writes_to_console+/
if (is(T == struct) || is(T == union) || is(T == class)) {
	Format ffmt = fmt; // field format
	ffmt.quoteChars = true; // mimics `std.stdio`
	ffmt.quoteStrings = true; // mimics `std.stdio`
	ffmt.level += 1;
	enum isNested = !is(T == class) && __traits(isNested, T);
	static foreach (fieldSymbol; T.tupleof[0 .. $ - isNested]) {{
		enum fieldName = fieldSymbol.stringof;
		if (fieldOffset >= 1)
			{ const st = sm.fpwrite1_string(fmt.aggregateFieldSeparator); if (st == St(EOF)) return st; }
		if (fmt.multiLine)
			{ const st = sm.fpwrite1_char('\n'); if (st == St(EOF)) return st; }
		{ const st = sm.indent(ffmt); if (st == St(EOF)) return st; }
		if (fmt.showFieldTypes) {
			alias FieldType = typeof(fieldSymbol);
			{ const st = sm.fpwrite1_type(FieldType.stringof, FieldType.mangleof); if (st == St(EOF)) { return st; } }
			{ const st = sm.fpwrite1_string(fmt.fieldTypeSeparator); if (st == St(EOF)) { return st; } }
		}
		if (fmt.showFieldNames) {
			{ const st = sm.fpwrite1_string(fmt.fieldNamePrefix); if (st == St(EOF)) { return st; } }
			{ const st = sm.fpwrite1_string(fieldName); if (st == St(EOF)) { return st; } }
			{ const st = sm.fpwrite1_string(fmt.fieldNameSuffix); if (st == St(EOF)) { return st; } }
		}
		const st = () @trusted {
			static if (is(typeof(fieldSymbol) == enum))
				{ const st = sm.fpwrite1_enum(__traits(getMember, arg, fieldName), ffmt, addrs); if (st == St(EOF)) { return st; } }
			else {
				try {
					/+just cast away qualifiers such as shared for now+/
					{ const st = sm.fpwrite1(cast(const)__traits(getMember, arg, fieldName), ffmt, addrs); if (st == St(EOF)) { return st; } }
				} catch (Exception _) {
					{ const st = sm.fpwrite1_string("TODO: Avoid this Exception that is maybe triggered by casting away shared above"); if (st == St(EOF)) { return st; } }
				}
			}
			return St.none;
		}();
		if (st) return st;
		fieldOffset += 1;
	}}
	return St.none;
}

St fpwrite1(scope FILE* sm, in   char arg, in Format fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return sm.fpwrite1_char(arg, fmt.quoteChars);
}
St fpwrite1(scope FILE* sm, in  wchar arg, in Format fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return sm.fpwrite1_wchar(arg, fmt.quoteChars);
}
St fpwrite1(scope FILE* sm, in  dchar arg, in Format fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return sm.fpwrite1_dchar(arg, fmt.quoteChars);
}

St fpwrite1(scope FILE* sm, in char[] arg, in Format fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return sm.fpwrite1_string(arg, quote: fmt.quoteStrings);
}
St fpwrite1(scope FILE* sm, in wchar[] arg, in Format fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return sm.fpwrite1_wstring(arg, quote: fmt.quoteStrings);
}
St fpwrite1(scope FILE* sm, in dchar[] arg, in Format fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return sm.fpwrite1_dstring(arg, quote: fmt.quoteStrings);
}

St fpwrite1(scope FILE* sm, in   bool arg, in Format _fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return sm.fpwrite1_string(arg ? "true" : "false");
}
St fpwrite1(scope FILE* sm, in   byte arg, in Format _fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%hhd".ptr, arg));
}
St fpwrite1(scope FILE* sm, in  ubyte arg, in Format _fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%hhu".ptr, arg));
}

St fpwrite1(scope FILE* sm, in  short arg, in Format _fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%hd".ptr, arg));
}
St fpwrite1(scope FILE* sm, in ushort arg, in Format _fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%hu".ptr, arg));
}

St fpwrite1(scope FILE* sm, in    int arg, in Format _fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%d".ptr, arg));
}
St fpwrite1(scope FILE* sm, in   uint arg, in Format _fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%u".ptr, arg));
}

St fpwrite1(scope FILE* sm, in   long arg, in Format fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return sm.fpwrite1_long(arg, fmt);
}
St fpwrite1(scope FILE* sm, in  ulong arg, in Format fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return sm.fpwrite1_ulong(arg, fmt);
}

St fpwrite1(scope FILE* sm, in  float arg, in Format _fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%g".ptr, arg));
}
St fpwrite1(scope FILE* sm, in double arg, in Format _fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%lg".ptr, arg));
}
St fpwrite1(scope FILE* sm, in real arg, in Format _fmt, scope ref Addrs addrs) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%Lg".ptr, arg));
}

St fpwrite1_enum(T)(scope FILE* sm, in T arg, in Format fmt, scope ref Addrs addrs) @trusted nothrow @nogc if (is(T == enum)) {
	if (fmt.showEnumeratorEnumType) {
		{ const st = sm.fpwrite1_string(T.stringof, false); if (st == St(EOF)) { return st; } }
		{ const st = sm.fpwrite1_char('.', false); if (st == St(EOF)) { return st; } }
	}
	return sm.fpwrite1_string(arg.enumToString!T(fmt, "__unknown__"), false);
}

St fpwrite1_char(scope FILE* sm, in char arg, in bool quote = false) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	if (quote) { const st = St(fputc('\'', sm)); if (st == St(EOF)) return st; }
	{ const st = St(fputc(arg, sm)); if (st == St(EOF)) return st; }
	if (quote) { const st = St(fputc('\'', sm)); if (st == St(EOF)) return st; }
	return St.none;
}

St fpwrite1_wchar(scope FILE* sm, in wchar arg, in bool quote = false) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	if (quote) { const st = St(fputc('\'', sm)); if (st == St(EOF)) return st; }
	{ const st = St(fputwc(arg, sm)); if (st == St(EOF)) return st; }
	if (quote) { const st = St(fputc('\'', sm)); if (st == St(EOF)) return st; }
	return St.none;
}

St fpwrite1_dchar(scope FILE* sm, in dchar arg, in bool quote = false) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	if (quote) { const st = St(fputc('\'', sm)); if (st == St(EOF)) return st; }
	{ const st = St(fputwc(arg, sm)); if (st == St(EOF)) return st; }
	if (quote) { const st = St(fputc('\'', sm)); if (st == St(EOF)) return st; }
	return St.none;
}

St fpwrite1_string(scope FILE* sm, in char[] arg, in bool quote = false, in bool _isStatic = false) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	if (quote) { const st = St(fputc('"', sm)); if (st == St(EOF)) return st; }
	if (arg.length)
		{ const st = St(c_fwrite(&arg[0], 1, arg.length, sm)); if (st == St(EOF)) { return st; } }
	if (quote) { const st = St(fputc('"', sm)); if (st == St(EOF)) return st; }
	return St.none;
}

St fpwrite1_wstring(scope FILE* sm, in wchar[] arg, in bool quote = false, in bool _isStatic = false) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	if (quote) { const st = St(fputc('"', sm)); if (st == St(EOF)) return st; }
	if (arg.length)
		{ const st = St(c_fwrite(&arg[0], 1, arg.length, sm)); if (st == St(EOF)) return st; }
	if (quote) { const st = St(fputc('"', sm)); if (st == St(EOF)) return st; }
	return St.none;
}

St fpwrite1_dstring(scope FILE* sm, in dchar[] arg, in bool quote = false, in bool _isStatic = false) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	if (quote) { const st = St(fputc('"', sm)); if (st == St(EOF)) return st; }
	if (arg.length)
		{ const st = St(c_fwrite(&arg[0], 1, arg.length, sm)); if (st == St(EOF)) return st; }
	if (quote) { const st = St(fputc('"', sm)); if (st == St(EOF)) return st; }
	return St.none;
}

St fpwrite1_long(scope FILE* sm, in  long arg, in Format _fmt) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%lld".ptr, arg));
}

St fpwrite1_ulong(scope FILE* sm, in  ulong arg, in Format _fmt = Format.init) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%llu".ptr, arg));
}

St fpwrite1_addr(scope FILE* sm, in  void* arg, in Format _fmt = Format.init) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	return St(sm.fprintf("%lX".ptr, cast(size_t)arg));
}

St fpwrite1_type(scope FILE* sm, in char[] stringOf, in char[] mangleOf) @trusted nothrow @nogc /+TODO: @writes_to_console+/ {
	import nxt.algorithm.searching : startsWith;
	if (mangleOf.startsWith("S3__C")) {
		const stp = sm.fpwrite1_string("C:"); // C type prefix
		if (stp == St(EOF)) { return stp; }
	} else if (mangleOf.startsWith("_Z")) {
		const stp = sm.fpwrite1_string("C++:"); // C++ type prefix
		if (stp == St(EOF)) { return stp; }
	}
	const st = sm.fpwrite1_string(stringOf); if (st == St(EOF)) return st;
	return St.none;
}

string enumToString(T)(in T arg, in Format fmt, string defaultValue) if (is(T == enum)) {
	switch (arg) { // instead of slower `std.conv.to`:
		static foreach (member; __traits(allMembers, T)) { // instead of slower `EnumMembers`
		case __traits(getMember, T, member):
			return member;
		}
	default:
		return defaultValue;
	}
}

St indent(scope FILE* sm, in Format fmt) nothrow @nogc /+TODO: @writes_to_console+/ {
	if (fmt.multiLine)
		foreach (_; 0 .. fmt.level) {
			{ const st = sm.fpwrite1_string(fmt.indentation, false); if (st == St(EOF)) { return st; } }
		}
	return St.none;
}

/// string
version(none)
@safe nothrow version(nxt_test) unittest {
	const fmt = Format.fancy;
	fmt.pwriteln(["a"]);
}

/// bitfields
version (none)
@safe nothrow version(nxt_test) unittest {
	struct S {
		ubyte u7:7;
		ubyte u6:6;
		ubyte u5:5;
		ubyte u4:4;
		ubyte u3:3;
		ubyte u2:2;
		ubyte u1:1;
		ubyte b1:1;
	}
	const fmt = Format.fancy;
	fmt.pwriteln(S.init);
}

/// classes
version(none)
@safe nothrow version(nxt_test) unittest {
	const SampleBase b = new SampleDerived2();
	Format.dbg.pwriteln(b);
}

version(nxt_test) version(unittest) {
	static private class SampleBase { int x; }
	static private class SampleDerived1 : SampleBase { int y; }
	static private class SampleDerived2 : SampleDerived1 { int z; }
}

///
version(none)
@safe nothrow version(nxt_test) unittest {
	import core.simd;
	static struct Uncopyable { this(this) @disable; int _x; }
	scope const int* x = new int(42);
	enum NormalEnum {
		first = 0,
		second = 1,
	}
	writeln(NormalEnum.first);
	writeln(NormalEnum.second);
	enum StringEnum {
		OPENED = "opened",
		MERGED = "merged",
	}
	class Base {
		const int baseField1;
		const int baseField2;
	}
	class Derived : Base {
		this(int derivedField1, int derivedField2) {
			this.derivedField1 = derivedField1;
			this.derivedField2 = derivedField2;
		}
		const int derivedField1;
		const int derivedField2;
	}
	struct T {
		int x = 111, y = 222;
	}
	struct U {
		const bool b_f = false;
		const bool b_t = true;
	}
	struct V {
		const char ch_a = 'a';
		const wchar wch = 'ä';
		const dchar dch = 'ö';
		const c_ch = 'b';
		const i_ch = 'c';
	}
	struct W {
		const str = "åäö";
		const char[3] str3 = "abc";
		const wstring wstr = "åäö";
		const dstring dstr = "åäö";
		const ubyte[] ua = [1,2,3];
		const ubyte[3] ub = [1,2,3];
		const void[] va = cast(void[])[1,2,3];
	}
	struct X {
		const  byte[3] ba = [byte.min, 0, byte.max];
		const short[3] sa = [short.min, 0, short.max];
		const   int[3] ia = [int.min, 0, int.max];
		const  long[3] la = [long.min, 0, long.max];
	}
	struct Y {
		const  float[3] fa = [-float.infinity, 3.14, +float.infinity];
		const double[3] da = [-double.infinity, 3.333333333, +double.infinity];
		const   real[3] ra = [-real.infinity, 0, +real.infinity];
	}
	struct OpaqueStruct;
	extern(C) struct OpaqueCStruct;
	struct S {
		T t;
		U u;
		V v;
		W w;
		X x;
		Y y;
		int ix = 32;
		const int iy = 42;
		immutable int iz = 52;
		const(int)* null_p;
		const(int)* xp;
		NormalEnum normalEnum;
		StringEnum stringEnum; // TODO: use
		const aa = [1:1, 2:2];
		float4 f4;
		void* voidPtr;
		Derived derived;
		Base baseBeingDerived;
		const(OpaqueStruct)* opaqueStruct;
		const(OpaqueCStruct)* opaqueCStruct;
		// TODO: Uncomment and `xreduce` this: Error: cannot take address of bit-field `ub4_0`
		// TODO: Might require special handling of bitfields via: `__traits(isBitfield, T)`
		// ubyte ub4_0 : 4;
		// ubyte ub4_1 : 4;
	}

	S s;
	s.xp = x;
	s.voidPtr = new int(42);
	s.derived = new Derived(33, 44);
	s.baseBeingDerived = new Derived(55, 66);

	writeln(Uncopyable.init);

	Format fmt;

	fmt = Format.init;
	pwriteln(fmt, s);

	fmt = Format(showFieldNames: true);
	pwriteln(fmt, s);

	fmt = Format(showFieldTypes: true);
	pwriteln(fmt, s);

	fmt = Format(_dereferencePointerValuesUnsafe: true);
	pwriteln(fmt, s);

	fmt = Format(showEnumeratorEnumType: true);
	pwriteln(fmt, s);

	fmt = Format.fancy;
	fmt.pwriteln(s);

}

// TODO: why does this work when others fail?
private void foo(Args...)(in Args args) /+TODO: @writes_to_console+/ {
}
@safe pure version(nxt_test) unittest {
	foo();
}

version(nxt_test) version(unittest)
{
	import nxt.dbgio;
}
