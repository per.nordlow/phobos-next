/++ Faster casting (of classes).
	Copied from SDC.
 +/
module nxt.fastcast;

version = AllowDuplicateTypeInfo;

U fastCast(U, T)(T t) if(is(T == class) && is(U == class) && is(U : T)) in(cast(U) t) {
	return *(cast(U*) &t);
}

// TODO: Check that `T` is an enum member.
U fastCast(U, T)(T t) if(is(U == union)) {
	static assert("TODO: Activate with tests");
	return *(cast(U*) &t);
}
