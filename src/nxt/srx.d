/++ Symbol Byte|String Patterns a la Emacs' rx.el.

	Concepts and namings are inspired by regular expressions, symbolic (regular)
	expressions (Emacs' rx.el package), predicate logic and grammars.

	A `string` is matched by its raw sequence thus solely UTF-8 is supported.

	Copyright: Per Nordlöw 2024-.
	License: $(WEB boost.org/LICENSE_1_0.txt, Boost License 1.0).
	Authors: $(WEB Per Nordlöw)

	TODO: Generalize `Anchor` to a pattern at a specific offset to be used in binary files.

	TODO: Use std.typecons.RefCounted Copy-on-Write (CoW) structs instead of GC
    	  allocated. Use `nxt.packed_variant.WordVariant` and pack discriminator type in lower 3 bits
    	  making room for 8 types.

	TODO: Extend `Node` classes to represent `xlg.el` read by `LispFileParser`.

	TODO: Merge with lpgen.d Node classes and tree-sitter

	TODO: Add pattern for expressing inference with `infer`
   (infer
	((x instanceOf X) &
	 (X subClassOf Y))
	 (x instaceOf Y))
 +/
module nxt.srx;

import std.algorithm : find, all, map, min, max, joiner;
import std.range : empty;
import std.array : array;
import std.string : representation;
import std.traits : isSomeString;
import nxt.find_ex : findAcronymAt, FindContext;
import nxt.dbgio : dbg;
import nxt.path : FilePath;
import nxt.packed_variant : WordVariant;
import nxt.effects;
import nxt.algorithm.searching : countInitialized;

enum bool showNormalizeDiagnostics = false;

@safe:

/++ Captures object contains submatches captured during a call to `match`.
	See: `std.regex.Captures`, `std.algorithm.searching.findSplit`,
	`nxt.algorithm.searching.findSplit`. +/
static struct Captures(T) {
	private T[] _haystack;
	private size_t _offset; // hit offset
pragma(inline, true) pure nothrow @nogc:
	inout(T)[] opIndex(in size_t i) inout {
		switch (i) {
		case 0: return pre;
		case 1: return separator;
		case 2: return post;
		default: return [];
		}
	}
	inout(T)[] pre() @trusted inout		  => _haystack.ptr[0 .. _offset];
	inout(T)[] separator() @trusted inout => !empty ? _haystack.ptr[_offset .. _offset + 1] : _haystack[$ .. $];
	inout(T)[] post() @trusted inout	  => !empty ? _haystack.ptr[_offset + 1 .. _haystack.length] : _haystack[$ .. $];
	bool opCast(T : bool)() const		  => !empty;
	private bool empty() const @property  => _haystack.length == _offset;
}

/++ Mutable raw data element. +/
private alias RawElement = ubyte;

/++ Mutable raw data. +/
private alias Raw = RawElement[];

/++ Immutable raw data. +/
private alias IRaw = immutable(RawElement)[];

/++ Input characters. +/
private alias Input = const(char)[];

/++ Offset into input characters. +/
private alias IOffset = size_t;

/++ {Substitution|Replacement} of pattern `src` with string `dst`. +/
struct Substitution {
	const Node src; /++< Source. +/
	string dst; /++< Destination. +/
	bool caseInsensitive; ///< Flag for case-insensitive match.
	/++ Returns: `true` if it's guaranteed that `this` has no effect, that
		`dst` is always equal to every possible match of `src`.
	 +/
	bool isNoOp() const scope pure nothrow /+@nogc+/ {
		// checks in order of likelyness
		if (!src.isStatic)
			return false;
		if (src.bounds == Bounds(0) &&
			dst.length == 0)
			return true;
		if (caseInsensitive)
			/+ TODO: if `src` match alphabetic characters (has literals with
               alphabetic characters). Use Node.byPossible!dchar.isalpha(). +/
			return false;
		if (const match = dst.matchFirst(src))
			return match.length == dst.length;
		return false;
	}
}

@safe pure version(nxt_test) unittest {
	assert(Subst("".lit, "").isNoOp);
	assert(Subst("_".lit, "_").isNoOp);
	assert(Subst("alpha".lit, "alpha").isNoOp);
	assert(Subst("".lit, "", caseInsensitive: true).isNoOp);
	assert(!Subst("a".lit, "a", caseInsensitive: true).isNoOp);
	assert(!Subst("alpha".lit, "beta").isNoOp);
	assert(!Subst("alpha".lit, "b").isNoOp);
	assert(!Subst(alt(["1".lit, "22".lit]), "_").isNoOp);
}

/++ Apply all occurrencies of `substitution` in `inp`.
	TODO: Use `Captures`
	See: `std.regex.replaceAll` +/
inout(char)[] replaceAll(scope return ref inout(char)[] inp, in Substitution sub, bool skipNoOpCheck = false) pure @trusted  {
	import std.array : Appender;

	if (!skipNoOpCheck &&
		sub.isNoOp)
		return inp; // skip self-replacement

	Appender!(inout(char)[]) result; // TODO: pre-calculate length

	const isStatic = sub.src.isStatic; // TODO: get rid of the need for this

	while (true) {
		if (const inpM = inp.matchFirst(sub.src)) {
			const offMB = inpM.ptr - inp.ptr; // offset to match beginning
			const lenM = isStatic ? sub.src.bounds.low : inpM.length; // length of match
			const offME = inpM.ptr - inp.ptr + lenM; // offset to match end
			if (offME == 0)
				break; // guarantee termination
			result ~= cast(typeof(return))inp[0 .. offMB];
			result ~= sub.dst;
			inp = inp[offME .. $];
		} else {
			result ~= inp;
			break;
		}
	}

	return cast(typeof(return))result.data;
}

@safe pure version(nxt_test) unittest {
	auto inp = "aa bb";
	const res = "a bb";
	assert(inp.replaceAll(Subst("aa".lit, "a")) == res);
}

@safe pure version(nxt_test) unittest {
	auto inp = " aa aa ";
	const res = " a a ";
	assert(inp.replaceAll(Subst("aa".lit, "a")) == res);
}

@safe pure version(nxt_test) unittest {
	auto inp = "aa aa bb bb aa";
	const res = "a a bb bb a";
	assert(inp.replaceAll(Subst("aa".lit, "a")) == res);
}

/++ Apply all `substitutions` to `inp`.
	See: `std.regex.replaceAll` +/
inout(char)[] replaceAll(scope return ref inout(char)[] inp, in Substitution[] subs) pure @trusted @reads_from_file @writes_to_file {
	// TODO: inline this loop into `replaceAll` above
	foreach (const ref sub; subs)
		inp = inp.replaceAll(sub);
	return inp;
}

@safe pure version(nxt_test) unittest {
	auto inp = "aa bb";
	const subs = [Subst("a".lit, "a"),
				  Subst("b".lit, "b")];
	assert(inp.replaceAll(subs) == inp);
}

@safe pure version(nxt_test) unittest {
	auto inp = "_expression _expression _statement";
	auto subs = [Subst("_expression".lit, "_expr"),
				 Subst("_statement".lit, "_stmt")];
	assert(inp.replaceAll(subs) == "_expr _expr _stmt");
}

/++ Apply all `substitutions` to contents of `ip` and write result to `op`.
	See: `std.regex.replaceAll` +/
void replaceAllInto(in FilePath ip, in Substitution[] subs, in FilePath op) @trusted @reads_from_file @writes_to_file {
	import std.file : readText, write;
    auto content = cast(char[])ip.str.readText(); // TODO: functionize to `nxt.file.readText(FilePath)`
	op.str.write(content.replaceAll(subs)); // TODO: functionize to `nxt.file.write(FilePath, str)`
}

/++ Apply all `substitutions` to contents of `path` inplace.
	See: `std.regex.replaceAll` +/
void replaceAllInplace(in FilePath iop, in Substitution[] subs) @trusted @reads_from_file @writes_to_file {
	replaceAllInto(iop, subs, iop);
}

/++ Matching `inp` with `node`.
	Compatibility with `std.regex.matchFirst`. +/
inout(char)[] matchFirst(scope return /+ref+/ inout(char)[] inp, const scope Node node) pure nothrow /+@nogc+/ {
	if (!node)
		return [];
	return inp.match(node);
}

///
@safe pure version(nxt_test) unittest {
	const x = "a";
	assert(x.matchFirst("b".lit) is []);
	assert(x.matchFirst(null) is []);
}

///
@safe pure version(nxt_test) unittest {
	const x = "ab";
	assert(x.matchFirst(lit(x[0 .. 1])) is x[0 .. $]);
	assert(x.matchFirst(lit(x[1 .. 2])) is x[1 .. 2]);
}

/++ Match `inp` with `node`.
	Returns: Matched slice or `[]` if not match. +/
inout(Raw) matchFirst(scope return /+ref+/ inout Raw inp, in Node node) pure nothrow /+@nogc+/ {
	const nodeMandatories = node.mandatories;
	if (!nodeMandatories.empty &&
		(inp.mandatories &
		 nodeMandatories).empty)
		return []; // fast path
	return node.findFirstRawAt(inp);
}

///
@safe pure version(nxt_test) unittest {
	const Raw x = [1,2];
	assert(x.matchFirst(lit(x[0 .. 1])) is x[0 .. $]);
	assert(x.matchFirst(lit(x[1 .. 2])) is x[1 .. 2]);
}

@safe pure version(nxt_test) unittest {
	const block = "block";
	const a = alt([lit("module_definition"),
				   lit("function_definition"),
				   lit("class_definition"),
				   lit("interface_definition"),
				   lit("struct_definition"),
				   lit("compound_statement"),
				   lit("block_statement"),
				   sym(lit(block))]);
	assert(block.matchFirst(a) is block);
	assert("compound_statement".matchFirst(a) == "compound_statement");
}

inout(char)[] match(scope return /+ref+/ inout(char)[] inp, in Node node) @trusted pure nothrow /+@nogc+/ {
	const nodeMandatories = node.mandatories;
	auto raw = inp.representation;
	if (!nodeMandatories.empty &&
		(raw.mandatories &
		 nodeMandatories).empty)
		return []; // fast path
	return cast(typeof(return))raw.matchFirst(node);
}

/++ Pattern Byte Size. +/
alias Size = size_t;

/++ Bounds of Pattern Byte `Size`. +/
struct Bounds {
pure nothrow @nogc @safe:
	static immutable inf = size_t.max;
	this(Size low, Size high) const {
		this.low = low;
		this.high = high;
	}
	this(Size lowAndHigh) const {
		this.low = lowAndHigh;
		this.high = lowAndHigh;
	}
	Size low; ///< Smallest possible.
	Size high; ///< Largest possible.
}

/++ Set of (possible) `Raw` Elements.
	For instance an ASCII string literal `"aaab"` will have `_set['a']` and `_set['b']`
	representing the containing ASCII characters 'a' and 'b'.
	When searching for a single ASCII character $C the search can be quickly discard when
	`$C !in _set`.
 +/
import nxt.container.static_dense_set : StaticDenseSet;
alias RawElementSet = StaticDenseSet!RawElement;

private RawElementSet mandatories(in Raw raw) pure nothrow @safe @nogc {
	typeof(return) res;
	foreach (const e; raw)
		res.insert(e);
	return res;
}

/++ Normalization Flags +/
struct NormalizationFlags {
pure nothrow @nogc:
	static typeof(this) none() {
		typeof(return) res;
		res.reduceNullsAndDuplicates = false;
		res.computeMandatories = false;
		return res;
	}
	static typeof(this) all() {
		typeof(return) res;
		res.reduceNullsAndDuplicates = true;
		res.computeMandatories = true;
		return res;
	}
	bool reduceNullsAndDuplicates; ///< Treat null nodes as empty patterns.
	bool computeMandatories;
}
alias NFlags = NormalizationFlags;

/++ Base Pattern. +/
abstract /+extern(C++)+/ class Node {
pure nothrow @safe:
	pragma(inline, true) {
	bool isLit() const @property @nogc {
		version (DigitalMars) pragma(inline);
		return cast(const(Lit))this !is null;
	}
	private bool isLitOfSize(in size_t size) const @property @nogc {
		const lit = cast(const(Lit))this;
		return lit && lit._raw.length == size;
	}
	bool isSeq() const @property @nogc {
		version (DigitalMars) pragma(inline);
		return cast(const(Seq))this !is null;
	}
	bool isAlt() const @property @nogc {
		version (DigitalMars) pragma(inline);
		return cast(const(Alt))this !is null;
	}
	bool isOpt() const @property @nogc {
		version (DigitalMars) pragma(inline);
		return cast(const(Opt))this !is null;
	}
	bool isRep() const @property @nogc {
		version (DigitalMars) pragma(inline);
		return cast(const(Rep))this !is null;
	}
	bool isAnc() const @property @nogc {
		version (DigitalMars) pragma(inline);
		return cast(const(Anchor))this !is null;
	}
	bool isAncOf(in Anchor.Type atp) const @property @nogc {
		version (DigitalMars) pragma(inline);
		const anc = cast(const(Anchor))this;
		if (!anc)
			return false;
		return anc.type == atp;
	}

	final bool opEquals(scope const ref typeof(this) rhs) const scope nothrow @nogc => this.isEqualTo(rhs); // TODO: why isn't this called?
	final bool opEquals(scope const typeof(this) rhs) const scope nothrow @nogc => this.isEqualTo(rhs); // TODO: why isn't this called?
	private const(Node) opCatImpl(Node rhs) {
		version(D_Coverage) {} else pragma(inline, true);
		return seq([this, rhs], nflags: NFlags.all);
	}
	private const(Node) opAltImpl(Node rhs) {
		version(D_Coverage) {} else pragma(inline, true);
		return alt([this, rhs], nflags: NFlags.all);
	}
	version(none)
	private const(Alt) opAndImpl(Node _rhs) {
		version(D_Coverage) {} else pragma(inline, true);
		assert(false, "TODO");
		// TODO: return new And(this, rhs);
		return null;
	}
 	const(Node) opBinary(string op)(scope const ref typeof(this) rhs) if (op == `==`) => opEquals(rhs); // template can't be overridden
	const(Node) opBinary(string op)(Node rhs) if (op == `~`) => opCatImpl(rhs); // template can't be overridden
	const(Node) opBinary(string op)(Node rhs) if (op == `&`) { static assert(false, "TODO: Implement"); return opAndImpl(rhs); } // template can't be overridden
	const(Node) opBinary(string op)(Node rhs) if (op == `|`) => opAltImpl(rhs); // template can't be overridden
	}

	/++ Check if `this` matches `inp[soff .. $]`.
		Returns: length >= 0 on match or `size_t.max` upon no match. +/
	final size_t omatchStart(in Input inp, in IOffset soff = 0) const @nogc {
		version(D_Coverage) {} else pragma(inline, true);
		return omatchStartRaw(inp.representation, soff);
	}

	/++ Get offset of first occurrence of `this` in `inp[i .. $]` or
    	`size_t.max` if no match of pattern is found. +/
	size_t omatchStartRaw(in Raw inp, in IOffset soff = 0) const scope @nogc;

	/++ Find `this` in `inp` at offset `soff`. +/
	inout(Raw) findFirstRawAt(inout Raw inp, in IOffset soff = 0) const scope /+@nogc+/ {
		// TODO: optimize using skip table for bytes in this pattern
		// TODO: Optimize using std.algorithm.find
		size_t i = soff;
		while (i < inp.length) { // while bytes left at i
			if (inp.length < bounds.low + i)  // and bytes left to find pattern
				return [];
			const hit = omatchStartRaw(inp, i);
			if (hit != size_t.max) // hit at i
				return inp[i..i + hit];
			i++;
		}
		return [];
	}

@property @nogc:
	Bounds bounds() const scope;

	bool isStatic() const scope; /// Returns: true if all possible instances of `this` have the same length.
	bool isConstant() const; /// Returns: true if all possible instances of `this` are equal to the same (literal) constant.

	inout(IRaw) tryGetConstant() inout => []; /// Returns: reference to raw literal bytes if available otherwise `null`.

	/++ Returns: raw elements that must match a given source $(D X) in order for $(D
		this) to match $(D X) somewhere.
		Precalculate in compile stage and use to speed up match.
	 +/
	final RawElementSet mandatories() const scope @trusted @cached {
		if (!_mandatories_cache_initialized) {
			() @trusted {
				*(cast(RawElementSet*)&_mandatories_cache) = _computeMandatories();
			}();
		}
		return _mandatories_cache;
	}
	protected RawElementSet _computeMandatories() const @cached_in!("_mandatories_cache") => ~typeof(return).init; // everything is mandatory
	private RawElementSet _mandatories_cache;
	private bool _mandatories_cache_initialized;
}

/++ Find `node` in `inp` at offset `soff`. +/
inout(char)[] findFirstAt(in Node node, return inout(char)[] inp, in IOffset soff = 0) pure nothrow /+@nogc+/ @trusted {
	return cast(typeof(return))node.findFirstRawAt(cast(IRaw)inp.representation, soff);
}

/// purity of `Node.opEquals`
pure nothrow @safe version (nxt_test) unittest {
	assert(Node.init is Node.init);
	const Node a1 = 'a'.lit;
	const Node a2 = 'a'.lit;
	assert(a1.opEquals(a2));
	// TODO: assert(a1 == a2);
}

/++ Returns: `true` iff `ndA` and `ndB` always matches the same patterns.
	TODO: Generalize to comparison using node hashes.
 +/
private bool isEqualTo(in Node ndA, in Node ndB) pure nothrow @nogc {
	if (ndA is ndB)
		return true;
	if (const litA = cast(const(Lit))ndA)
		if (const litB = cast(const(Lit))ndB)
			return litA._raw == litB._raw;
	return false;
}

@safe pure nothrow version (nxt_test) unittest {
	auto a1 = "a".lit;
	auto a2 = "a".lit;
	auto b = "b".lit;
	assert( a1.isEqualTo(a1));
	assert( a1.isEqualTo(a2));
	assert(!a1.isEqualTo(b));
	auto c = alt([a1, a2], nflags: NFlags.all);
	assert(!a1.isEqualTo(c));
}

version (none) // TODO: Activate
@safe pure nothrow version (nxt_test) unittest {
	const a1 = "a".lit;
	const a2 = "a".lit;
	const b = "b".lit;
	assert(a1 == a1);
	assert(a1 == a2);
	assert(a1 != b);
	const c = alt([a1, a2], nflags: NFlags.all);
	assert(a1 == c);
}

/++ Returns: `true` iff `ndA` always matches some pattern `ndsB`. +/
private bool isEqualToEitherIn(in Node ndA, in Node[] ndsB) pure nothrow @nogc {
	foreach (const ndB; ndsB)
		if (ndA.isEqualTo(ndB))
			return true;
	return false;
}

@safe pure nothrow version (nxt_test) unittest {
	auto a = "a".lit;
	const a1 = "a".lit;
	const a2 = "a".lit;
	assert( a.isEqualToEitherIn([a1, a2]));
	const b = "b".lit;
	const c = "c".lit;
	assert(!a.isEqualToEitherIn([b, c]));
}

/++ Literal Pattern. +/
private /+extern(C++)+/ class Lit : Node {
pure nothrow @safe:
	this(string bytes_) @nogc { this(bytes_.representation); }
	this(in Raw by) { _raw ~= by; }
	this(in dchar ch) { string str; str ~= ch; this(str); }
	this(Raw bytes_) in(bytes_.length != 0) { _raw = bytes_.idup; }
	this(typeof(_raw) bytes_) @nogc  { _raw = bytes_; }
	override size_t omatchStartRaw(in Raw inp, in IOffset soff = 0) const scope {
		import nxt.algorithm.searching : startsWith;
		version(D_Coverage) {} else pragma(inline, true);
		return inp[soff .. $].startsWith(_raw) ? _raw.length : size_t.max;
	}
	override inout(Raw) findFirstRawAt(inout Raw inp, in IOffset soff = 0) const scope nothrow @trusted {
		const result = inp[soff..$].find(_raw); // Uses std.algorithm.find
		/+ IMHO, find should return null on miss for non-empty needle (`_raw`)
		   so adjust so that it does. +/
		return cast(typeof(return)) (result.length != 0 ? result : []);
	}
final:
@property nothrow @nogc:
	IRaw raw() const => _raw;
	alias raw this;
override pragma(inline, true):
	Bounds bounds() const scope => Bounds(_raw.length, _raw.length);
	bool isStatic() const scope => true;
	bool isConstant() const => true;
	inout(IRaw) tryGetConstant() inout => _raw;
	RawElementSet _computeMandatories() const {
		version (DigitalMars) pragma(inline);
		return _raw.mandatories;
	}
private:
	IRaw _raw;
}

/++ Literal. +/
auto lit(Arg)(Arg arg) pure nothrow @safe => new Lit(arg);

pure nothrow @safe version(nxt_test) unittest {
	const ab = lit(``);
	assert(ab.isLit);
	assert(ab.mandatories.empty);
}

pure nothrow @safe version(nxt_test) unittest {
	const ab = lit(`ab`);
	assert(ab.mandatories == RawElementSet("ab".representation));
	assert(ab.mandatories != RawElementSet("abc".representation));
}

pure nothrow @safe version(nxt_test) unittest {
	const _ = lit('_');
	const ab = lit(`ab`);
	assert(`ab`.match(ab));
	version(none) assert(!ab.match(`_`)); // TODO: enable
}

pure nothrow @safe version(nxt_test) unittest {
	immutable ab = `ab`;
	assert(lit('b').omatchStart(`ab`, 1) == 1);
	const a = lit('a');
	assert(a.tryGetConstant == "a");

	const ac = lit(`ac`);
	assert(ac.tryGetConstant == "ac");
	assert(`ac`.match(ac));
	assert(`ac`.match(ac).length == 2);
 	assert(`ca`.match(ac) == []);

	assert(a.isStatic);
	assert(a.isConstant);
	assert(a.tryGetConstant == "a");
	assert(a.omatchStart(ab) == 1);
	assert(lit(ab).omatchStart(`a`) == size_t.max);
	assert(lit(ab).omatchStart(`b`) == size_t.max);

	assert(a.findFirstAt(`cba`) == `a`);
	assert(a.findFirstAt(`ba`) == `a`);
	assert(a.findFirstAt(`a`) == `a`);
	assert(a.findFirstAt(``) == []);
	assert(a.findFirstAt(`b`) == []);
	assert(ac.findFirstAt(`__ac`) == `ac`);
	assert(a.findFirstAt(`b`).length == 0);

	const xyz = lit(`xyz`);
	assert(xyz.length == 3);
	assert(xyz.bounds == Bounds(3));
	assert(xyz.tryGetConstant == "xyz");
}

pure nothrow @safe version(nxt_test) unittest {
	assert(lit(dchar('x')).isLitOfSize(1));
	assert(lit(dchar('ö')).isLitOfSize(2));
}

pure nothrow @safe version(nxt_test) unittest {
	auto ac = lit(`ac`);
	assert(ac.length == 2);
}

/++ (Prefix) Literal Pattern at beginning. +/
private final /+extern(C++)+/ class BLit : Lit {
import nxt.algorithm.searching : startsWith;
pure nothrow @safe:
	this(string bytes_) @nogc { this(bytes_.representation); }
	this(in Raw by) { super(by); }
	this(in dchar ch) { super(ch); }
	this(Raw bytes_) { super(bytes_); }
	this(typeof(_raw) bytes_) @nogc { super(bytes_); }
override @nogc:
	size_t omatchStartRaw(in Raw inp, in IOffset soff = 0) const scope {
		return inp[soff .. $].startsWith(_raw) ? _raw.length : typeof(return).max;
	}
	inout(Raw) findFirstRawAt(inout Raw inp, in IOffset soff = 0) const scope nothrow {
		return inp[soff .. $].startsWith(_raw) ? inp : [];
	}
}

auto blit(Arg)(Arg arg, in NFlags nflags = NFlags.all) pure nothrow @safe {
	auto res = new BLit(arg);
	if (nflags.computeMandatories) { res._computeMandatories(); }
	return res;
}

pure nothrow @safe version(nxt_test) unittest {
	const a = "a";
	assert(blit(a).findFirstRawAt(a.representation) == "a");
	const ab = "ab";
	assert(blit(a).findFirstRawAt(ab.representation) == ab);
	assert(blit(a).findFirstRawAt("".representation) == []);
	assert(blit(a).findFirstRawAt(" ab".representation) == []);
}

/++ (Suffix) Literal Pattern at end. +/
private final /+extern(C++)+/ class ELit : Lit {
import nxt.algorithm.searching : endsWith;
pure nothrow @safe:
	this(string bytes_) @nogc { this(bytes_.representation); }
	this(in Raw by) { super(by); }
	this(in dchar ch) { super(ch); }
	this(Raw bytes_) { super(bytes_); }
	this(typeof(_raw) bytes_) @nogc { super(bytes_); }
override @nogc:
	size_t omatchStartRaw(in Raw inp, in IOffset soff = 0) const scope {
		return inp[soff .. $].endsWith(_raw) ? _raw.length : typeof(return).max;
	}
	inout(Raw) findFirstRawAt(inout Raw inp, in IOffset soff = 0) const scope nothrow {
		return inp[soff .. $].endsWith(_raw) ? inp : [];
	}
}

auto elit(Arg)(Arg arg, in NFlags nflags = NFlags.all) pure nothrow @safe {
	auto res = new ELit(arg);
	if (nflags.computeMandatories) { res._computeMandatories(); }
	return res;
}

pure nothrow @safe version(nxt_test) unittest {
	const b = "b";
	assert(elit(b).findFirstRawAt(b.representation) == "b");
	const ab = "ab";
	assert(elit(b).findFirstRawAt(ab.representation) == ab);
	assert(elit(b).findFirstRawAt("".representation) == []);
	assert(elit(b).findFirstRawAt("ab ".representation) == []);
}

/++ Abstract Super Pattern of 2 or more sub patterns. +/
private abstract /+extern(C++)+/ class SPatt : Node {
pure nothrow @safe:
	this(const(Node)[] subs_) in(subs_.length >= 1) in(subs_.all) {
		_subs = subs_;
	}
	private const(Node)[] _subs;
	invariant(_subs.length >= 1);
}

/++ Sequence of Patterns. +/
private final /+extern(C++)+/ class Seq : SPatt {
pure nothrow @safe:
	this(const Node[] subs_) {
		super(subs_);
	}

	override size_t omatchStartRaw(in Raw inp, in IOffset soff = 0) const scope @nogc in(_subs.length != 0) {
		const c = tryGetConstant;
		if (!c.empty) {
			return (soff + c.length <= inp.length &&   // if equal size and
					c[] == inp[soff..soff + c.length]); // equal contents
		}
		size_t sum = 0;
		size_t off = soff;
		foreach (ix, sub; _subs) { /+ TODO: Reuse std.algorithm instead? +/
			const hit = sub.omatchStartRaw(inp, off);
			if (hit == size_t.max) { sum = hit; break; } // if any miss skip
			sum += hit;
			off += hit;
		}
		// TODO: assert(sum == inp.length);
		return sum;
	}

	/++ Find `this` in `inp` at offset `soff`. +/
	override inout(Raw) findFirstRawAt(inout Raw inp, in IOffset soff = 0) const scope /+@nogc+/ @trusted {
		const bobF = _subs[0].isAncOf(Anchor.Type.bob);
		const eobL = _subs[$ - 1].isAncOf(Anchor.Type.eob);
		if (bobF && eobL) {
			const subsT = _subs[1 .. $ - 1];
			const subT = subsT.length == 1 ? subsT[0] : seq(cast(Node[])subsT);
			const hit = subT.omatchStartRaw(inp, soff: soff);
			if (hit == inp.length) // TODO: this should check that match is complete
				return inp;
		} else if (bobF) {
			const subsT = _subs[1 .. $];
			const subT = subsT.length == 1 ? subsT[0] : seq(cast(Node[]) subsT);
			const hit = subT.omatchStartRaw(inp, soff: soff);
			if (hit == inp.length)
				return inp;
		} else if (eobL) {
			const subsT = _subs[0 .. $ - 1];
			const _subT = subsT.length == 1 ? subsT[0] : seq(cast(Node[]) subsT);
			// TODO: subT.omatchStartRaw(inp, soff: soff)
		}
		return super.findFirstRawAt(inp, soff);
	}

scope @property @nogc:
	override Bounds bounds() const {
		typeof(return) result;
		foreach (const ref sub; _subs) {
			if (sub.bounds.low != size_t.max)
				result.low += sub.bounds.low; // TODO: catch overflow
			if (sub.bounds.high != size_t.max)
				result.high += sub.bounds.high; // TODO: catch overflow
		}
		return result;
	}
	override bool isStatic() const => _subs.all!(a => a.isStatic);
	override bool isConstant() const => _subs.all!(a => a.isConstant);
	override inout(IRaw) tryGetConstant() inout => [];
	override RawElementSet _computeMandatories() const {
		auto res = typeof(return).init;
		foreach (const ref sub; _subs)
			res = res | sub.mandatories;
		return res;
	}
}

/++ Sequence of patterns `subs`. +/
const(Node) seq(scope const(Node)[] subs, in NFlags nflags = NFlags.all) pure nothrow {
	if (nflags != NFlags.none) {
		if (nflags.reduceNullsAndDuplicates) {
			const numI = subs.countInitialized;
			if (numI < subs.length) {
				typeof(subs) subsI; // initialized (non-`null`)
				subsI.reserve(numI);
				foreach (const ref sub; subs)
					if (sub)
						subsI ~= sub;
				subs = subsI;
			}
		}

		if (subs.length == 1)
			return subs[0];

		if (subs.length == 2) {
			if (subs[0].isLit && subs[1].isAncOf(Anchor.Type.eob))
				return blit((cast(const(Lit))subs[0])._raw, nflags);
			if (subs[1].isLit && subs[0].isAncOf(Anchor.Type.bob))
				return elit((cast(const(Lit))subs[1])._raw, nflags);
		}

		static if (showNormalizeDiagnostics) dbg("NORMALIZING ...");

		const(Node)[] subsN; // normalized subs
		// TODO: Preallocate length of `subsN`
		// TOFUNC:

		// TODO: Recurse to `seq` instead:
		for (size_t i = 0; i < subs.length;) {
			static if (showNormalizeDiagnostics) dbg(i);

			if (i + 1 >= subs.length) {
				static if (showNormalizeDiagnostics) dbg("LAST: ", i);
				subsN ~= subs[i]; // normal last case
				i += 1;
				break;
			}

			const j = i + 1; // index of next sub

			if (subs[i].isEqualTo(subs[j])) {
				static if (showNormalizeDiagnostics) dbg("REP: ", i, " equals ", j);
				subsN ~= rep(subs[i], 2); // repeat
				i += 2;
				continue;
			}

			const rawI = subs[i].tryGetConstant;
			const isConstI = subs[i].tryGetConstant !is null;
			static if (showNormalizeDiagnostics) dbg("rawI: `", cast(string)rawI, "`, isConstant:", isConstI);

			const rawJ = j < subs.length ? subs[j].tryGetConstant : null;
			const isConstJ = subs[j].tryGetConstant !is null;
			static if (showNormalizeDiagnostics) dbg("rawJ: `", cast(string)rawJ, "`, isConstant:", isConstJ);

			if (isConstI && isConstJ) {
				static if (showNormalizeDiagnostics) dbg("MERGE: ", i, " and ", j, " being `", cast(string)(rawI ~ rawJ), "`");
				subsN ~= lit(rawI ~ rawJ); // merge
				i += 2;
				continue;
			}

			static if (showNormalizeDiagnostics) dbg("NORMAL: ", i);
			subsN ~= subs[i]; // normal non-duplicate non-last case
			i += 1;
		}
		assert(subsN.length != 0);
		static if (showNormalizeDiagnostics) dbg(subsN.length);
		return subsN.length == 1 ? subsN[0] : new Seq(subsN);
	} else
		return new Seq(subs);
}

pure nothrow @safe version(nxt_test) unittest {
	const ab = new Seq([lit(``), lit(``)]);
	assert(ab.mandatories.empty);
}

pure nothrow version(nxt_test) unittest {
	auto a = `a`.lit;
	auto b = `b`.lit;
	const s = seq([a, b], nflags: NFlags.all);
	assert(s.isLitOfSize(a.length + b.length));
	assert(s.mandatories.minElement('_') == 'a');
	assert(s.mandatories.maxElement('_') == 'b');
}

pure nothrow @safe version(nxt_test) unittest {
	const a = seq([`a`.lit], nflags: NFlags.all);
	assert(a.isLitOfSize(1));
}

pure nothrow @safe version(nxt_test) unittest {
	const a = seq([`a`.lit, null], nflags: NFlags.all);
	assert(a.isLitOfSize(1));
}

pure nothrow version(nxt_test) unittest {
	auto a = `a`.lit;
	const s = seq([a.opt], nflags: NFlags.all);
	assert(s.isOpt);
	assert(!s.isStatic);
	assert(!s.isConstant);
	assert(s.bounds == Bounds(0, a.length));
}

pure nothrow version(nxt_test) unittest {
	const a_ = `a`;
	const b_ = `b`;
	const s = seq([bob, a_.lit], nflags: NFlags.all);
	assert(s.findFirstAt(a_, 0) is a_);
	assert(s.findFirstAt(a_, 0) !is b_);
}

pure nothrow version(nxt_test) unittest {
	const a_ = `a`;
	const s = buf(a_.lit, nflags: NFlags.all);
	assert(s.findFirstAt(a_, 0) is a_);
}

pure nothrow version(nxt_test) unittest {
	auto a = `a`.lit;
	const s = seq([a, a], nflags: NFlags.all);
	assert(s.isRep);
	assert(s.isStatic);
	assert(s.isConstant);
	assert(s.bounds == Bounds(2 * a.length));
	assert(a.mandatories.minElement('_') == 'a');
	assert(a.mandatories.maxElement('_') == 'a');
}

pure nothrow version(nxt_test) unittest {
	auto a = `a`.lit;
	auto b = `b`.lit;
	const s = seq([a.opt, b.opt], nflags: NFlags.all);
	assert(s.isSeq);
	assert(!s.isStatic);
	assert(!s.isConstant);
	assert(s.bounds == Bounds(0, a.length + b.length));
	assert(s.mandatories.empty);
}

pure nothrow version(nxt_test) unittest {
	assert(`a`.lit.opt.tryGetConstant is []);
	assert(`aa.`.lit.opt.tryGetConstant is []);
}

pure nothrow version(nxt_test) unittest {
	auto a = `a`.lit;
	auto b = `b`.lit;
	const s = seq([a.opt, b], nflags: NFlags.all);
	assert(s.isSeq);
	assert(!s.isStatic);
	assert(!s.isConstant);
	assert(s.bounds.low == b.length);
	assert(s.bounds.high == a.length + b.length);
	assert(s.mandatories == RawElementSet("b".representation));
}

pure nothrow version(nxt_test) unittest {
	auto a = `aa.`.lit;
	auto b = `bb.`.lit;
	auto c = `cc.`.lit;
	auto d = `dd.`.lit;
	const s = seq([a.opt, b, c.opt, d], nflags: NFlags.all);
	assert(s.isSeq);
	assert(!s.isStatic);
	assert(!s.isConstant);
	assert(s.bounds.low == b.length + d.length);
	assert(s.bounds.high == a.length + b.length + c.length + d.length);
	assert(s.mandatories == RawElementSet(".bd".representation));
}

pure nothrow @safe version(nxt_test) unittest {
	const inp = `alpha`;
	const s = seq([`al.`.lit, `pha`.lit], nflags: NFlags.all);
	assert(s.isLit);
	assert(s.isStatic);
	assert(s.isConstant);
	assert(s.omatchStart(inp)); // TODO: this should fail
	assert(s.bounds == Bounds(6));
	assert(s.tryGetConstant == "al.pha");
	assert(s.mandatories == RawElementSet("alpha.".representation));
}

pure nothrow @safe version(nxt_test) unittest {
	const s = `al`.lit ~ `pha`.lit;
	assert(s.isStatic);
	assert(s.isConstant);
	assert(s.tryGetConstant == "alpha");
	assert(s.mandatories == RawElementSet("alpha".representation));
}

pure nothrow @safe version(nxt_test) unittest {
	const s = seq([`a`.lit, `l`.lit], nflags: NFlags.none);
	assert(s.isSeq);
	assert(s.omatchStart(`al`) == 2);
	// TODO: assert(s.omatchStart(`a`) == size_t.max); // `s not found in `a`
	assert(s.mandatories == RawElementSet("al".representation));
}

pure nothrow @safe version(nxt_test) unittest {
	const s = seq([`a`.lit, `l`.lit], nflags: NFlags.all);
	assert(s.isLit);
	assert(s.omatchStart(`a`) == size_t.max); // `s not found in `a`
	// TODO: assert(s.tryGetConstant == "s");
	assert(s.mandatories == RawElementSet("al".representation));
}

/// exercise `nflags` of two equivalent adjance front subs
pure nothrow version(nxt_test) unittest {
	const a1 = `a`.lit;
	const a2 = `a`.lit; // redundant with `a1`
	const b = `b`.lit;
	const ab = alt([a1, a2, b], nflags: NFlags.all);
	assert(ab.isAlt);
	// TODO: assert(ab._subs.length == 2);
	assert(ab.tryGetConstant is []);
	assert(ab.mandatories.empty);
}

/// exercise `nflags` of two equivalent adjance back subs
pure nothrow version(nxt_test) unittest {
	const a1 = `a`.lit;
	const a2 = `a`.lit; // redundant with `a1`
	const b = `b`.lit;
	const ab = alt([b, a1, a2], nflags: NFlags.all);
	assert(ab.isAlt);
	// TODO: assert(ab._subs.length == 2);
	assert(ab.tryGetConstant is []);
}

/++ Alternative of Patterns. +/
private final /+extern(C++)+/ class Alt : SPatt {
pure nothrow @safe:
	this(const Node[] subs_) {
		super(subs_);
	}

	size_t atIx(in Input inp, in IOffset soff, out size_t alt_hix) const {
		version(D_Coverage) {} else pragma(inline, true);
		return omatchStartRaw(inp.representation, soff, alt_hix);
	}

	void opOpAssign(string s : "~")(Node sub) {
		version(D_Coverage) {} else pragma(inline, true);
		if (!nflags || !sub.isEqualToEitherIn(super._subs))
			super._subs ~= sub;
	}

	/++ Get Length of hit at index `soff` in `inp` or `size_t.max` if none.	 +/
	size_t omatchStartRaw(in Raw inp, in IOffset soff, out size_t alt_hix) const scope @nogc in(!_subs.empty) {
		size_t hit = 0;
		size_t off = soff;
		foreach (ix, sub; _subs) { /+ TODO: Reuse std.algorithm instead? +/
			hit = sub.omatchStartRaw(inp[off..$]);					 // match alternative
			if (hit != size_t.max) { alt_hix = ix; break; } // if any hit were done
		}
		return hit;
	}

	override size_t omatchStartRaw(in Raw inp, in IOffset soff = 0) const scope @nogc {
		version(D_Coverage) {} else pragma(inline, true);
		size_t alt_hix;
		return omatchStartRaw(inp, soff, alt_hix);
	}

	/++ Find `this` in `inp` at offset `soff`. +/
	override inout(Raw) findFirstRawAt(inout Raw inp, in IOffset soff = 0 ) const scope @trusted {
		const inp_ = cast(const(Raw))inp;
		assert(!_subs.empty);	/+ TODO: Move to in contract? +/
		switch (_subs.length) {
			case 1:
				const a0 = _subs[0].tryGetConstant;
				if (!a0.empty) {
					auto hit = inp[soff..$].find(a0); // Use: second argument to return alt_hix
					return hit.length != 0 ? hit : [];
				} else
					return _subs[0].findFirstRawAt(inp, soff); // recurse to it
			case 2:
				const a0 = _subs[0].tryGetConstant;
				const a1 = _subs[1].tryGetConstant;
				if (!a0.empty &&
					!a1.empty) {
					const hit = inp_[soff..$].find(a0, a1); // Use: second argument to return alt_hix
					return cast(typeof(return))hit[0];
				}
				break;
			case 3:
				const a0 = _subs[0].tryGetConstant;
				const a1 = _subs[1].tryGetConstant;
				const a2 = _subs[2].tryGetConstant;
				if (!a0.empty &&
					!a1.empty &&
					!a2.empty) {
					const hit = inp_[soff..$].find(a0, a1, a2); // Use: second argument to return alt_hix
					return cast(typeof(return))hit[0];
				}
				break;
			case 4:
				const a0 = _subs[0].tryGetConstant;
				const a1 = _subs[1].tryGetConstant;
				const a2 = _subs[2].tryGetConstant;
				const a3 = _subs[3].tryGetConstant;
				if (!a0.empty &&
					!a1.empty &&
					!a2.empty &&
					!a3.empty) {
					const hit = inp_[soff..$].find(a0, a1, a2, a3); // Use: second argument to return alt_hix
					return cast(typeof(return))hit[0];
				}
				break;
			case 5:
				const a0 = _subs[0].tryGetConstant;
				const a1 = _subs[1].tryGetConstant;
				const a2 = _subs[2].tryGetConstant;
				const a3 = _subs[3].tryGetConstant;
				const a4 = _subs[4].tryGetConstant;
				if (!a0.empty &&
					!a1.empty &&
					!a2.empty &&
					!a3.empty &&
					!a4.empty) {
					const hit = inp_[soff..$].find(a0, a1, a2, a3, a4); // Use: second argument to return alt_hix
					return cast(typeof(return))hit[0];
				}
				break;
			default:
				break;
		}
		return super.findFirstRawAt(inp, soff); // revert to base case
	}

override @property:
	Bounds bounds() const scope {
		auto result = typeof(return)(size_t.max, size_t.min);
		foreach (const ref sub; _subs) {
			result.low = min(result.low, sub.bounds.low);
			result.high = max(result.high, sub.bounds.high);
		}
		return result;
	}
	bool isStatic() const scope {
		/+ TODO: Merge these loops using tuple algorithm. +/
		auto mins = _subs.map!(a => a.bounds.low);
		auto maxs = _subs.map!(a => a.bounds.high);
		import nxt.predicates: allEqual;
		return (mins.allEqual && maxs.allEqual);
	}
	bool isConstant() const {
		if (_subs.length == 0)
			return true;
		else if (_subs.length == 1) {
			import std.range: front;
			return _subs.front.isConstant;
		} else
			return false;	   /+ TODO: Maybe handle case when _subs are different. +/
	}
	override RawElementSet _computeMandatories() const {
		auto res = ~typeof(return).init;
		foreach (const ref sub; _subs)
			res = res & sub.mandatories;
		return res;
	}
}

pure nothrow @safe version(nxt_test) unittest {
	const ab = new Alt([lit(``), lit(``)]);
	assert(ab.mandatories.empty);
}

pure nothrow @safe version(nxt_test) unittest {
	const ab = new Alt([lit(`ab`), lit(`ab`)]);
	assert(ab.mandatories == RawElementSet("ab".representation));
}

pure nothrow @safe version(nxt_test) unittest {
	const ab = new Alt([lit(`a`), lit(`b`)]);
	assert(ab.mandatories.empty);
}

pure nothrow @safe version(nxt_test) unittest {
	const ab = new Alt([lit(`ab`), lit(`bc`)]);
	assert(ab.mandatories == RawElementSet("b".representation));
}

/++ Alternative of patterns `subs`.
	By convenience `subs` are allowed to contain `null` when
    `nflags.reduceNullsAndDuplicates` is set.
 +/
const(Node) alt(const(Node)[] subs, in NFlags nflags = NFlags.all) pure nothrow {
	if (nflags == NFlags.none)
		return new Alt(subs);
	if (nflags.reduceNullsAndDuplicates) {
		const numI = subs.countInitialized;
		if (numI < subs.length) {
			typeof(subs) subsI; // initialized (non-`null`)
			subsI.reserve(numI);
			foreach (const ref sub; subs)
				if (sub)
					subsI ~= sub;
			subs = subsI;
		}
		import nxt.algorithm.searching : skipOverFront, skipOverBack, id;
		// TODO: Reuse generic algorithm deduplicate that uses
		// std.algorithm.sort to temporary or inplace if `subs_` is a unique
		// reference:
		while (subs.length >= 2 && subs.skipOverFront!(id)(subs[1])) {}
		while (subs.length >= 2 && subs.skipOverBack!(id)(subs[$-2])) {}
	}
	if (subs.length == 1)
		return subs[0];
	if (subs.length == 0)
		return null;
	return new Alt(subs);
}

pure nothrow @safe version(nxt_test) unittest {
	const a = alt([lit("module_definition"),
		 lit("function_definition"),
		 lit("class_definition"),
		 lit("interface_definition"),
		 lit("struct_definition"),
		 lit("compound_statement"),
		 lit("block_statement"),
		 sym(lit("block")),
		 ]);
	assert(a.isAlt);
	assert(a.mandatories == RawElementSet("o".representation));
	assert(a.omatchStart(`block_statement`));
	assert(a.omatchStart(`compound_statement`));
	assert(a.omatchStart(`block`));
}

pure nothrow @safe version(nxt_test) unittest {
	const a = alt([Node.init], nflags: NFlags.all);
	assert(!a);
}

pure nothrow @safe version(nxt_test) unittest {
	const a = alt([`a`.lit], nflags: NFlags.none);
	assert(a.isAlt);
	assert(!a.mandatories.empty);
}

pure nothrow @safe version(nxt_test) unittest {
	const a = alt([`a`.lit], nflags: NFlags.all);
	assert(a.isLit);
	assert(!a.mandatories.empty);
}

pure nothrow @safe version(nxt_test) unittest {
	const a = alt([`a`.lit, null], nflags: NFlags.all);
	assert(a.isLit);
	assert(!a.mandatories.empty);
}

pure nothrow @safe version(nxt_test) unittest {
	const a = alt([`a`.lit, `b`.lit], nflags: NFlags.all);
	assert(a.isAlt);
	assert(a.mandatories.empty);
}

pure nothrow @safe version(nxt_test) unittest {
	const al = `a`.lit;
	const a = alt([al, al], nflags: NFlags.all);
	assert(a.isLit);
}

pure nothrow @safe version(nxt_test) unittest {
	immutable a_b = alt([`a`.lit, `b`.lit], nflags: NFlags.all);
	assert(a_b.isAlt);
	assert(a_b.bounds == Bounds(1));

	const a__b = `a`.lit | `b`.lit;
	assert(a__b.bounds == Bounds(1));

	assert(a_b.isStatic);
	assert(!a_b.isConstant);
	assert(a_b.omatchStart(`a`));
	assert(a_b.omatchStart(`b`));
	assert(a_b.omatchStart(`c`) == size_t.max);

	const hix = size_t.max;
	// TODO: a_b.atIx(`a`, 0, hix); assert(hix == 0);
	// TODO: a_b.atIx(`b`, 0, hix); assert(hix == 1);

	/* assert(alt.omatchStart(`a`) == size_t.max); */
	/* assert(alt.omatchStart(``) == size_t.max); */

	immutable a = opt(lit(`a`));
	immutable aa = opt(lit(`aa`));
	assert(!aa.isConstant);

	immutable aa_bb = alt([lit(`aa`), lit(`bb`)], nflags: NFlags.all);
	assert(aa_bb.isStatic);
	assert(!aa_bb.isConstant);
	assert(aa_bb.bounds == Bounds(2));

	immutable a_bb = alt([lit(`a`), lit(`bb`)], nflags: NFlags.all);
	assert(!a_bb.isStatic);
	assert(!a_bb.isConstant);
	assert(a_bb.bounds == Bounds(1, 2));

	const string _aa = `_aa`;
	assert(aa_bb.findFirstAt(_aa) == `aa`);

	const string _bb = `_bb`;
	assert(aa_bb.findFirstAt(_bb) == `bb`);

	assert(a.findFirstAt(`b`) == []);
	assert(aa.findFirstAt(`cc`) == []);
	assert(aa_bb.findFirstAt(`cc`) == []);
	assert(aa_bb.findFirstAt(``) == []);
}

version (none) // TODO:
pure nothrow @safe version(nxt_test) unittest {
	auto a_b = alt([`a`.lit, `b`.lit], nflags: NFlags.all);
	a_b ~= `c`.lit;

	assert(a_b.isStatic);
	assert(!a_b.isConstant);
	assert(a_b.omatchStart(`a`));
	assert(a_b.omatchStart(`b`));
	assert(a_b.omatchStart(`x`) == size_t.max);

	size_t hix = size_t.max;
	a_b.atIx(`a`, 0, hix); assert(hix == 0);
	a_b.atIx(`b`, 0, hix); assert(hix == 1);
}

version (none) // TODO:
pure nothrow @safe version(nxt_test) unittest {
	auto a_b = alt([`a`.lit, `b`.lit], nflags: NFlags.all);
	a_b ~= `c`.lit;

	assert(a_b.isStatic);
	assert(!a_b.isConstant);
	assert(a_b.omatchStart(`a`));
	assert(a_b.omatchStart(`b`));
	assert(a_b.omatchStart(`x`) == size_t.max);

	size_t hix = size_t.max;
	a_b.atIx(`a`, 0, hix); assert(hix == 0);
	a_b.atIx(`b`, 0, hix); assert(hix == 1);
}

private final /+extern(C++)+/ class Space : Node {
pure nothrow @safe @nogc override const scope:
	size_t omatchStartRaw(in Raw inp, in IOffset soff = 0) {
		version(D_Coverage) {} else pragma(inline, true);
		import std.ascii: isWhite;
		return soff < inp.length && isWhite(inp[soff]) ? 1 : size_t.max;
	}
@property pragma(inline, true):
	Bounds bounds() => typeof(return)(1, 1);
	bool isStatic() => true;
	bool isConstant() => false;
	override RawElementSet _computeMandatories() const {
		auto res = typeof(return).init;
		res.insert(' ');
		res.insert('\n');
		res.insert('\t');
		res.insert('\r');
		res.insert('\v');
		res.insert('\f');
		return res;
	}
}

auto ws() pure nothrow @safe => new Space();

pure nothrow @safe version(nxt_test) unittest {
	auto ws_ = ws;
	assert(ws_.bounds == Bounds(1));
	assert(ws_.isStatic);
	assert(!ws_.isConstant);
	assert(ws_.omatchStart(` `) == 1);
	assert(ws_.omatchStart("\t") == 1);
	assert(ws_.omatchStart("\n") == 1);
}

/++ Abstract Singleton Super Pattern. +/
private abstract /+extern(C++)+/ class SPatt1 : Node {
pure nothrow @safe @nogc:
	this(const Node sub) {
		this.sub = sub;
	}
	private const Node sub;
}

/++ Optional Sub Pattern $(D count) times. +/
private final /+extern(C++)+/ class Opt : SPatt1 {
pure nothrow @safe @nogc scope:
	this(const Node sub) in(sub) { super(sub); }
override const:
	size_t omatchStartRaw(in Raw inp, in IOffset soff = 0) {
		version(D_Coverage) {} else pragma(inline, true);
		assert(soff <= inp.length); // include equality because inp might be empty and size zero
		const hit = sub.omatchStartRaw(inp[soff..$]);
		return hit == size_t.max ? 0 : hit;
	}
@property pragma(inline, true):
	Bounds bounds() => typeof(return)(0, sub.bounds.high);
	bool isStatic() => false;
	bool isConstant() => false;
	RawElementSet _computeMandatories() => typeof(return).init;
}

pure nothrow @safe version(nxt_test) unittest {
	assert(lit(``).opt.mandatories.empty);
	assert(lit(`a`).opt.mandatories.empty);
	assert(lit(`ab`).opt.mandatories.empty);
}

/++ Optional `sub`. +/
const(Node) opt(const Node sub, in NFlags nflags = NFlags.all) pure nothrow {
	if (nflags != NFlags.none) {
		if (auto optS = cast(const Opt)sub)
			return optS; // optional of optional is optional
	}
	return new Opt(sub);
}

pure nothrow @safe version(nxt_test) unittest {
	auto oa = `a`.lit.opt;
	assert(oa.omatchStart(`a`) == 1);
	assert(oa.omatchStart(`b`) == 0);
	auto ooa = oa.opt;
	assert(ooa.isOpt);
	assert(ooa.omatchStart(`a`) == 1);
	assert(ooa.omatchStart(`b`) == 0);
}

/++ Repetition of Sub Pattern. +/
private final /+extern(C++)+/ class Rep : SPatt1 {
pure nothrow @safe @nogc:
	/++ Repetition of Sub Pattern `sub` `count` times. +/
	this(const Node sub, size_t count) in(count >= 2) {
		super(sub);
		this.countReq = count;
		this.countOpt = 0; // fixed length repetion
	}
	/++ Repetition Sub Pattern `sub` [`countMin` .. `countMax`] times. +/
	this(const Node sub, size_t countMin, size_t countMax)
	in(countMin <= countMax)
	in(countMax >= 2) {
		super(sub);
		this.countReq = countMin;
		this.countOpt = countMax - countMin;
	}
override const:
	size_t omatchStartRaw(in Raw inp, in IOffset soff = 0) scope {
		size_t sum = 0;
		size_t off = soff;
		/* mandatory */
		foreach (ix; 0..countReq) /+ TODO: Reuse std.algorithm instead? +/ {
			const hit = sub.omatchStartRaw(inp[off..$]);
			if (hit == size_t.max) { return hit; } // if any miss skip
			off += hit;
			sum += hit;
		}
		/* optional part */
		foreach (ix; countReq..countReq + countOpt) /+ TODO: Reuse std.algorithm instead? +/ {
			const hit = sub.omatchStartRaw(inp[off..$]);
			if (hit == size_t.max) { break; } // if any miss just break
			off += hit;
			sum += hit;
		}
		return sum;
	}
	RawElementSet _computeMandatories() => sub.mandatories;

	// invariant { assert(countReq); }
	size_t countReq; // Required.
	size_t countOpt; // Optional.

@property scope pragma(inline, true):
	Bounds bounds() => typeof(return)(countReq*sub.bounds.high, (countReq + countOpt)*sub.bounds.high);
	bool isStatic() => bounds.low == bounds.high && sub.isStatic;
	bool isConstant() => bounds.low == bounds.high && sub.isConstant;
}

auto rep(Args...)(Args args) => new Rep(args); // repetition
auto zom(Args...)(Args args) => new Rep(args, 0, size_t.max); // zero or more
auto oom(Args...)(Args args) => new Rep(args, 1, size_t.max); // one or more

pure nothrow @safe version(nxt_test) unittest {
	assert(lit(``).rep(2, 3).mandatories == RawElementSet("".representation));
	assert(lit(`a`).rep(2, 3).mandatories == RawElementSet("a".representation));
	assert(lit(`ab`).rep(2, 3).mandatories == RawElementSet("ab".representation));
}

pure nothrow @safe version(nxt_test) unittest {
	auto l = 'a'.lit;

	const l5 = l.rep(5);
	assert(l5.isConstant);
	assert(l5.omatchStart(`aaaa`) == size_t.max);
	assert(l5.omatchStart(`aaaaa`));
	assert(l5.omatchStart(`aaaaaaa`));
	assert(l5.isStatic);
	assert(l5.bounds == Bounds(5));

	const countMin = 2;
	const countMax = 3;
	const l23 = l.rep(countMin, countMax);
	assert(l23.omatchStart(`a`) == size_t.max);
	assert(l23.omatchStart(`aa`) == 2);
	assert(l23.omatchStart(`aaa`) == 3);
	assert(l23.omatchStart(`aaaa`) == 3);
	assert(!l23.isConstant);
	assert(l23.bounds == Bounds(countMin, countMax));
}

pure nothrow @safe version(nxt_test) unittest {
	auto l = 'a'.lit;
	const l5 = l.rep(5);
	assert(l5.isStatic);
	assert(l5.isConstant);
}

pure nothrow @safe version(nxt_test) unittest {
	auto l = 'a'.lit;
	auto l5 = l.opt.rep(5);
	assert(!l5.isStatic);
	assert(!l5.isConstant);
}

private final /+extern(C++)+/ class Anchor : Node {
	enum Type : ubyte {
		/+ Block/File/String: +/
		bob, ///< Beginning Of \em Buffer/Block/File/String. @b Emacs: `\``
		// beginningOfBuffer = bob,
		// beginningOfBlock = bob,
		// beginningOfFile = bob,
		// beginningOfString = bob,
		// fileStart = bob,
		eob, ///< End Of \em Buffer/Block/File/String. @b Emacs: `\'`
		// endOfBuffer = eob,
		// endOfBlock = eob,
		// endOfFile = eob,
		// endOfString = eob,
		// fileEnd = eob,

		/+ Line: +/
		bol, ///< Beginning Of \em Line. @b Emacs: `^`
		// beginningOfLine = bol,
		// lineStart = bol,
		eol, ///< End Of \em Line. @b Emacs: `$`
		// endOfLine = eol,
		// lineEnd = eol,

		/+ Symbol|Identifier (typically in code): +/
		bos, ///< Beginning Of \em Symbol. @b Emacs: `\_<`
		// beginningOfSymbol = bos,
		// symbolStart = bos,
		eos, ///< End Of \em Symbol. @b Emacs: `\_>`
		// endOfSymbol = eos,
		// symbolEnd = eos,

		/+ Word either as a hole symbol or as part of symbol such as the word `do` in `do_this`: +/
		bow, ///< Beginning Of \em Word. @b Emacs: `\<`
		// beginningOfWord = bow,
		// wordStart = bow,
		eow, ///< End Of \em Word. @b Emacs: `\>`
		// endOfWord = eow,
		// wordEnd = eow,
	}

pure nothrow @safe:

	this(Type type) { this.type = type; }

	override size_t omatchStartRaw(in Raw inp, in IOffset soff = 0) const scope {
		import nxt.algorithm.comparison : among;
		assert(soff <= inp.length); // include equality because inp might be empty and size zero
		bool ok = false;
		import std.ascii : isAlphaNum /* , newline */;
		final switch (type) {
		/* Buffer: */
		case Type.bob: ok = (soff == 0); break;
		case Type.eob: ok = (soff == inp.length); break;
		// TODO: Use `std.unicode` instead `std.ascii`:
		/* Line: */
		case Type.bol:
			// TODO: might need revision:
			ok = (soff == 0 ||
				  inp[soff-1].among(0x0d, 0x0a));
			break;
		case Type.eol:
			// TODO: might need revision:
			ok = (soff == inp.length ||
				  inp[soff].among(0x0d, 0x0a));
			break;
		/* Word: */
		case Type.bow:
			ok = ((soff == 0 ||
				   !inp[soff-1].isAlphaNum) &&
				  (soff < inp.length &&
				   inp[soff].isAlphaNum));
			break;
		case Type.eow:
			ok = ((soff == inp.length ||
				   !inp[soff].isAlphaNum) &&
				  (soff >= 1 &&
				   inp[soff-1].isAlphaNum));
			break;
		/* Symbol|Identifier: */
		case Type.bos:
			ok = ((soff == 0 ||
				   (!inp[soff-1].isAlphaNum &&
					inp[soff-1] != '_')) && /+ TODO: Make '_' language-dependent +/
				  (soff < inp.length &&
				   (inp[soff].isAlphaNum ||
					inp[soff] == '_')));
			break;
		case Type.eos:
			ok = ((soff == inp.length ||
				  (!inp[soff].isAlphaNum &&
				   inp[soff] != '_')) && /+ TODO: Make '_' language-dependent +/
				  (soff >= 1 &&
				   (inp[soff - 1].isAlphaNum ||
					inp[soff - 1] == '_')));
			break;
		}
		return ok ? 0 : size_t.max;
	}
	private Type type;
@property override const scope nothrow @nogc pragma(inline, true):
	Bounds bounds() => typeof(return)(0, 0);
	bool isStatic() => true;
	bool isConstant() => true;
	RawElementSet _computeMandatories() => typeof(return).init;
}

Anchor bob() pure nothrow => new Anchor(Anchor.Type.bob);
Anchor eob() pure nothrow => new Anchor(Anchor.Type.eob);
Anchor bol() pure nothrow => new Anchor(Anchor.Type.bol);
Anchor eol() pure nothrow => new Anchor(Anchor.Type.eol);
Anchor bos() pure nothrow => new Anchor(Anchor.Type.bos);
Anchor eos() pure nothrow => new Anchor(Anchor.Type.eos);
Anchor bow() pure nothrow => new Anchor(Anchor.Type.bow);
Anchor eow() pure nothrow => new Anchor(Anchor.Type.eow);

pure nothrow version(nxt_test) unittest {
	foreach (const s; [bob, eob, bol, eol, bos, eos, bow, eow]) {
		assert(s.isAnc);
		assert(s.isStatic);
		assert(s.isConstant);
		assert(s.bounds == Bounds(0));
	}
}

/++ Wrap `sub` as a buffer. +/
const(Node) buf(in Node sub, in NFlags nflags = NFlags.all) pure nothrow => seq([bob, sub, eob], nflags); // TODO: Perhaps find a better name

/++ Wrap `sub` as a line. +/
const(Node) lin(in Node sub, in NFlags nflags = NFlags.all) pure nothrow => seq([bol, sub, eol], nflags);

/++ Wrap `sub` as a symbol. +/
const(Node) sym(in Node sub, in NFlags nflags = NFlags.all) pure nothrow => seq([bos, sub, eos], nflags);
/// ditto

/++ Wrap `sub` as a identifier. +/
const(Node) idn(in Node sub, in NFlags nflags = NFlags.all) pure nothrow => sym(sub, nflags); // reuse symbol for now

/++ Wrap `sub` as a word. +/
const(Node) wrd(in Node sub, in NFlags nflags = NFlags.all) pure nothrow => seq([bow, sub, eow], nflags);

pure nothrow @safe version(nxt_test) unittest {
	auto l = lit("ab");
	foreach (const x; [buf(l), lin(l), sym(l), idn(l), wrd(l)]) {
		assert(x.isStatic);
		assert(x.isConstant);
		assert(x.bounds == Bounds(2));
		assert(x.omatchStart(`ab`) == 2);
	}
}

pure nothrow @safe version(nxt_test) unittest {
	const bob_ = bob;
	const eob_ = eob;
	assert(bob_.omatchStart(`ab`) == 0);
	assert(eob_.omatchStart(`ab`, 2) == 0);

 	const bol_ = bol;
 	assert(bol_.omatchStart(`ab`) == 0);
 	assert(bol_.omatchStart("a\nb", 2) == 0);
 	assert(bol_.omatchStart("a\nb", 1) == size_t.max);

 	const eol_ = eol;
 	assert(eol_.omatchStart(`ab`, 2) == 0);
 	assert(eol_.omatchStart("a\nb", 1) == 0);
 	assert(eol_.omatchStart("a\nb", 2) == size_t.max);

 	const bos_ = bos;
 	assert(bos_.omatchStart(`ab`) == 0);
 	assert(bos_.omatchStart(` ab`, 1) == 0);
 	assert(bos_.omatchStart(` ab`) == size_t.max);
 	assert(bos_.omatchStart(`_ab `, 0) == 0);
 	assert(bos_.omatchStart(` _ab `, 1) == 0);

 	const eos_ = eos;
 	assert(eos_.omatchStart(`ab`, 2) == 0);
 	assert(eos_.omatchStart(`a_b `, 1) == size_t.max);
  	assert(eos_.omatchStart(`ab `, 2) == 0);
  	assert(eos_.omatchStart(`ab`, 2) == 0);
  	assert(eos_.omatchStart(`ab_`, 2) == size_t.max);
 	assert(eos_.omatchStart(`_ab_`, 4) == 0);
 	assert(eos_.omatchStart(`_ab_ `, 4) == 0);

 	const bow_ = bow;
 	const eow_ = eow;

 	assert(bow_.omatchStart(`ab`) == 0);
 	assert(bow_.omatchStart(` ab`) == size_t.max);

 	assert(eow_.omatchStart(`ab`, 2) == 0);
 	assert(eow_.omatchStart(`ab `, 0) == size_t.max);

 	assert(bow_.omatchStart(` ab `, 1) == 0);
 	assert(bow_.omatchStart(` ab `, 0) == size_t.max);

 	assert(eow_.omatchStart(` ab `, 3) == 0);
 	assert(eow_.omatchStart(` ab `, 4) == size_t.max);

 	assert(bow_.omatchStart(`_ab `, 1) == 0);
 	assert(eow_.omatchStart(`_ab_`, 3) == 0);

 	auto l = lit(`ab`);
 	auto w = wrd(l);
 	assert(w.omatchStart(`ab`) == 2);
 	assert(w.omatchStart(`ab_c`) == 2);

 	auto s = sym(l);
 	assert(s.omatchStart(`ab`) == 2);
 	assert(s.omatchStart(`ab_c`) == size_t.max);

 	assert(bob_.findFirstAt(`a`) == []);
 	assert(bob_.findFirstAt(`a`).ptr != null);
 	assert(eob_.findFirstAt(`a`) == []);

 	assert(bol_.findFirstAt(`a`) == []);
 	assert(bol_.findFirstAt(`a`).ptr != null);
 	assert(eol_.findFirstAt(`a`) == []);

 	assert(bow_.findFirstAt(`a`) == []);
 	assert(bow_.findFirstAt(`a`).ptr != null);
 	assert(eow_.findFirstAt(`a`) == []);

 	assert(bos_.findFirstAt(`a`) == []);
 	assert(bos_.findFirstAt(`a`).ptr != null);
 	assert(eos_.findFirstAt(`a`) == []);
// 	/+ TODO: This fails assert(eos_.findFirstAt(`a`).ptr != null); +/
}

/++ Create Matcher for a UNIX Shell $(LUCKY Shebang) Pattern.
	Example: #!/bin/env rdmd
	See_Also: https://en.wikipedia.org/wiki/Shebang_(Unix) +/
auto ref shebangLine(const(Node) interpreter) pure nothrow @safe {
	return seq([bob,
				`#!`.lit,
				`/usr`.lit.opt,
				`/bin/env`.lit.opt,
				ws.oom,
				interpreter]);
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(`rdmd`.lit.shebangLine
				 .omatchStart(`#!/bin/env rdmd`) == 15);
	assert(`rdmd`.lit.shebangLine
				 .omatchStart(`#!/usr/bin/env rdmd`) == 19);
	auto rgdmd = alt([`rdmd`.lit, `gdmd`.lit], nflags: NFlags.all);
	assert(rgdmd.shebangLine
				.omatchStart(`#!/usr/bin/env rdmd-dev`) == 19);
	assert(rgdmd.shebangLine
				.omatchStart(`#!/usr/bin/env gdmd`) == 19);
}

///
pure nothrow @safe version(nxt_test) unittest {
	auto x = alt([elit("_body"), elit("compound_statement")]);
	assert(x.findFirstAt(`function_body`) == `_body`);
	assert(x.findFirstAt(`body_`).empty);
	assert(x.findFirstAt(`compound_statement`) == `compound_statement`);
	assert(x.findFirstAt(`xxx`).empty);
	// TODO: assert(x.findFirstAt(`xxx`) is []);
}

version(nxt_test) version(unittest) alias Subst = Substitution;
