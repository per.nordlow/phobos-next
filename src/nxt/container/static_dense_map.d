module nxt.container.static_dense_map;

/++ Map of elements with keys of type `K` and values of type `V`, densely stored in a bit array.
 +
 + This data structure provides fast membership checking and value retrieval for keys within a small range.
 +
 + See_Also: `std.typecons.BitFlags`
 +
 + TODO: Merge with `static_dense_set.d`
 +/
struct StaticDenseMap(K, V) if (K.sizeof == 1) {
    import core.bitop : bt, bts, btr, btc, bsf, bsr, popcnt;

	alias B = ubyte; /++< Raw key type. +/
	alias W = size_t; /++< Word type for bit operations. +/

	struct KeyValue {
		K key;
		V value;
	}
	alias Element = KeyValue;

	this(KeyValue element) {
        version(D_Coverage) {} else pragma(inline, true);
		insert(element.key, element.value);
	}

	this(K key, V value) {
        version(D_Coverage) {} else pragma(inline, true);
		insert(key, value);
	}

	this(in KeyValue[] elements) {
		foreach (const ref element; elements)
			insert(element.key, element.value);
	}

	/++ Returns: `true` if `key` is present in `this`. +/
    bool contains(in K key) const @trusted {
        version(D_Coverage) {} else pragma(inline, true);
		return bt(_keysStore.ptr, cast(B)key) != 0;
    }
	/// ditto
	alias has = contains;

	/++ Inserts or updates the value associated with `key`. +/
    bool insert(in K key, V value) @trusted {
        version(D_Coverage) {} else pragma(inline, true);
		const isNew = !contains(key);
		if (isNew)
			bts(_keysStore.ptr, cast(B)key);
		_values[key] = value;
		return isNew;
    }

	/++ Removes the key-value element with `key` from `this`. +/
    bool remove(in K key) @trusted {
        version(D_Coverage) {} else pragma(inline, true);
		if (contains(key)) { // Check existence using contains
			btr(_keysStore.ptr, cast(B)key); // Clear the bit
			return true;
		}
		return false;
    }

	/++ Retrieves the value associated with `key`, or a default if not found. +/
    V getOr(in K key, return scope V defaultValue) const @trusted {
        version(D_Coverage) {} else pragma(inline, true);
		return contains(key) ? _values[key] : defaultValue;
    }

    /++ Clears all key-value elements. +/
	void clear() {
        version(D_Coverage) {} else pragma(inline, true);
        _keysStore[] = 0;
		import std.traits : hasIndirections;
		static if (hasIndirections!V) {
			// TODO: run destructors
			_values[] = V.init; // zero pointer
		}
    }

    bool empty() const @property {
        version(D_Coverage) {} else pragma(inline, true);
        return _keysStore == _keysStore.init;
    }

    /++ Returns: Number of elements stored. +/
    uint countElements() const @property {
		uint res;
		foreach (word; _keysStore)
			res += popcnt(word);
        return res;
    }
	alias count = countElements;
	alias occupancy = count;

	import nxt.io : Format;

	void toString(Sink)(scope ref Sink sink, in Format fmt = Format.init) const @property {
		import std.conv : to;
		sink.put(fmt.mapPrefix);
		bool other = false;
		foreach (const k; K.min .. K.max + 1) {
			if (!contains(k))
				continue;
			if (other)
				sink.put(fmt.elementSeparator);
			other = true;
			sink.put(k.to!string); // TODO: avoid call to `std.conv.to`
			sink.put(fmt.mapKeyValueSeparator);
			sink.put(_values[k].to!string); // TODO: avoid call to `std.conv.to`
		}
		sink.put(fmt.mapSuffix);
	}

private:
	W[4] _keysStore;
	V[K.max + 1] _values; ///< Values associated with each key in `_keysStore`.
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	auto m = M(K(42), V(42));
	assert(!m.empty);
	assert(m.countElements == 1);
	m.clear();
	assert(m.empty);
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	auto m = M(E(K(42), V(42)));
	assert(!m.empty);
	assert(m.countElements == 1);
	m.clear();
	assert(m.empty);
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	auto m = M([E(K(42), V(42))]);
	assert(!m.empty);
	assert(m.countElements == 1);
	m.clear();
	assert(m.empty);
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	M m;
	assert(m.empty);
	assert(m.countElements == 0);

	assert(m.insert(5, 42));
	assert(!m.empty);

	assert(m.insert(6, 43));
	assert(m.getOr(5, -1) == 42);
	assert(m.contains(5));
	assert(m.getOr(6, -1) == 43);
	assert(m.contains(6));
	assert(m.getOr(7, -1) == -1); // 7 is not in the map
	assert(!m.contains(7));

	assert(m.remove(5));
	assert(!m.remove(5));
	assert(!m.contains(5));

	assert(m.countElements == 1);

	assert(m.remove(6));
	assert(!m.remove(6));
	assert(!m.contains(6));

	assert(m.countElements == 0);
	assert(m.empty);

	m.clear();
	assert(m.empty);
}

///
version(nxt_test) version(unittest) {
	alias K = ubyte;
	alias V = int;
	alias M = StaticDenseMap!(K, V);
	alias E = M.KeyValue;
}
