module nxt.container.array_map;

/++ Map of elements with keys of type `K` and values of type `V` stored in a array-like store.
 +/
struct ArrayMap(K, V, bool sorted = false, bool separated = false) {
	import std.traits : hasIndirections;

	struct KeyValue {
		K key;
		V value;
	}

	this(KeyValue element) {
        version(D_Coverage) {} else pragma(inline, true);
		insert(element.key, element.value);
	}

	this(K key, V value) {
        version(D_Coverage) {} else pragma(inline, true);
		insert(key, value);
	}

	this(KeyValue[] elements) {
		foreach (const ref element; elements)
			insert(element.key, element.value);
	}

	size_t capacity() const @property {
        version(D_Coverage) {} else pragma(inline, true);
		return _store.capacity;
	}

	size_t length() const @property {
        version(D_Coverage) {} else pragma(inline, true);
		return _store.length;
	}

	void reserve(in size_t requestedCapacity) {
		version(D_Coverage) {} else pragma(inline, true);
		if (requestedCapacity > capacity)
			_store.reserve(requestedCapacity);
	}

	/++ Returns: `true` if `key` is present in `this`. +/
    bool canFind(in K key) const {
		foreach (const ref e; _store)
			if (e.key == key)
				return true;
		return false;
    }
	/++ Returns: `true` if `element` is present in `this`. +/
    bool canFind(in KeyValue element) const {
		foreach (const ref e; _store)
			if (e == element)
				return true;
		return false;
    }
	/// ditto
	static if (sorted) {
		/// Define because `canFind` has time-complexity O(log(length)).
		alias contains = canFind;
		/// ditto
		alias has = canFind;
	}

    bool insert(KeyValue element) {
        version(D_Coverage) {} else pragma(inline, true);
		const isNew = !canFind(element.key);
		if (isNew)
			_store ~= element;
		return isNew;
    }
	/// ditto
    bool insert(K key, V value) => insert(KeyValue(key, value));

	/++ Remove element `element` from `this`. +/
    bool remove(in KeyValue element) {
		auto iHit = size_t.max;
		foreach (const i, const ref e; _store)
			if (e == element) {
				iHit = i;
				break;
			}
		if (iHit == size_t.max)
			return false;
		removeAt(iHit);
		return true;
    }

	/++ Remove element with `key` from `this`. +/
    bool remove(in K key) {
		auto iHit = size_t.max;
		foreach (const i, const ref e; _store)
			if (e.key == key) {
				iHit = i;
				break;
			}
		if (iHit == size_t.max)
			return false;
		removeAt(iHit);
		return true;
    }

	/++ Remove the element at index `i`. +/
    private void removeAt(in size_t i) {
        version(D_Coverage) {} else pragma(inline, true);
		_store[i .. $ - 1] = _store[i + 1 .. $];
		static if (hasIndirections!KeyValue)
			_store[$ - 1] = KeyValue.init; // zero pointers
		_store.length -= 1;
	}

	/++ Retrieves the value associated with `key`, or a `defaultValue` if not found. +/
    ref inout(V) getOr(in K key, return ref scope inout(V) defaultValue) inout {
		foreach (ref e; _store)
			if (e.key == key)
				return e.value;
		return defaultValue;
    }

    /++ Clears all key-value elements. +/
	void clear() {
        version(D_Coverage) {} else pragma(inline, true);
		static if (hasIndirections!KeyValue)
			_store[] = Store.init; // zero pointers
        _store.length = 0;
    }

    bool empty() const @property {
        version(D_Coverage) {} else pragma(inline, true);
        return _store.length == 0;
    }

	static if (!separated) {
		inout(KeyValue)[] opSlice() inout {
			version(D_Coverage) {} pragma(inline, true);
			return _store[];
		}
		inout(KeyValue)[] opSlice(in size_t i, in size_t j) inout {
			version(D_Coverage) {} pragma(inline, true);
			return _store[i .. j];
		}
	}

	import nxt.io : Format;

	void toString(Sink)(scope ref Sink sink, in Format fmt = Format.init) const @property {
		import std.conv : to;
		sink.put(fmt.mapPrefix);
		bool other = false;
		foreach (const ref e; _store[]) {
			if (other)
				sink.put(fmt.elementSeparator);
			other = true;
			static if (is(typeof(sink.put(e.key))))
				sink.put(e.key);
			else
				sink.put(e.key.to!string); // TODO: avoid call to `std.conv.to`
			sink.put(fmt.mapKeyValueSeparator);
			static if (is(typeof(sink.put(e.value))))
				sink.put(e.value);
			else
				sink.put(e.value.to!string); // TODO: avoid call to `std.conv.to`
		}
		sink.put(fmt.mapSuffix);
	}

private:
	static if (separated)
		// TODO: Implement
		struct Store {
			size_t capacity;
			size_t length;
			K* keys;
			V* values;
		}
	else
		alias Store = KeyValue[];
	Store _store;
}

///
pure nothrow @safe version(nxt_test) unittest {
	const e = E(K(42), V(42));
	assert(M(K(42), V(42))[] == [e]);
	assert(M(E(K(42), V(42)))[] == [e]);
	assert(M([E(K(42), V(42))])[] == [e]);
	assert(M([E(K(42), V(42))])[0 .. 1] == [e]);

	const V defaultValue = -1;
	M m;
	assert(m.empty);
	assert(m.capacity == 0);
	assert(m.length == 0);
	m.reserve(1);
	assert(m.capacity == 1);

	assert(!m.canFind(5));
	assert(!m.canFind(E(5, 42)));
	assert(m.insert(5, 42));
	assert(m.canFind(E(5, 42)));
	assert(m.getOr(5, defaultValue) == 42);
	assert(m.canFind(5));

	assert(m.capacity != 0);
	assert(!m.empty);

	assert(!m.canFind(6));
	assert(m.insert(6, 43));
	assert(m.getOr(6, defaultValue) == 43);
	assert(m.canFind(6));

	assert(m.getOr(7, defaultValue) == defaultValue);
	assert(!m.canFind(7));

	assert(m.remove(5));
	assert(!m.remove(5));
	assert(!m.canFind(5));

	assert(m.remove(6));
	assert(!m.remove(6));
	assert(!m.canFind(6));

	assert(m.insert(6, 43));
	assert(m.remove(E(6, 43)));
	assert(!m.remove(E(6, 43)));

	assert(m.empty);

	m.clear();
	assert(m.empty);
	assert(m.capacity == 0);
}

///
version(nxt_test) version(unittest) {
	alias K = ubyte;
	alias V = int;
	alias M = ArrayMap!(K, V);
	alias E = M.KeyValue;
}
