module nxt.container.static_dense_set;

/++ Set of elements of type `E` densly stored in a bit array.
 +
 + Can be seen as a generalization of `std.typecons.BitFlags` to integer types.
 +
 + Used to implement fast membership checking in sets of types with small value
 + ranges.
 +
 + See_Also: `std.typecons.BitFlags`
 +
 + TODO: Merge with `static_dense_map.d`
 +/
struct StaticDenseSet(E) if (E.sizeof == 1) {
	import core.bitop : bt, bts, btr, btc, bsf, bsr, popcnt;

	alias B = ubyte; /++< Raw element type. +/
	alias W = size_t; /++< Word type for bit operations. +/

	this(E element) {
		version(D_Coverage) {} else pragma(inline, true);
		insert(element);
	}

	this(in E[] elements) {
		foreach (const ref element; elements)
			insert(element);
	}

	/++ Returns: `true` iff element `element` is present in `this`. +/
	bool contains(in E element) const @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		return bt(_store.ptr, cast(B)element) != 0;
	}
	/// ditto
	alias has = contains;

	/++ Returns: `true` iff element `element` was added to `this`. +/
	bool insert(in E element) @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		return bts(_store.ptr, cast(B)element) == 0;
	}
	alias add = insert;

	/++ Returns: `true` iff element `element` was removed from `this`. +/
	bool remove(in E element) @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		return btr(_store.ptr, cast(B)element) != 0;
	}

	bool toggle(in E element) @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		return btc(_store.ptr, cast(B)element) == 0;
	}

	E minElement(const E defaultValue) const @property {
		foreach (const wi, const word; _store)
			if (word)
				return cast(E)(wi * nW + bsf(word));
		return defaultValue;
	}

	E maxElement(const E defaultValue) const @property {
		foreach_reverse (const wi, const word; _store)
			if (word)
				return cast(E)(wi * nW + bsr(word));
		return defaultValue;
	}

	typeof(this) opUnary(string op : "~")() const {
		version(D_Coverage) {} else pragma(inline, true);
		typeof(return) res = void;
		res._store[] = ~_store[];
		return res;
	}

	typeof(this) opBinary(string op)(in typeof(this) rhs) const
	if (op == "&" || op == "|" || op == "^") {
		version(D_Coverage) {} else pragma(inline, true);
		typeof(return) res = void;
		mixin("res._store[] = _store[] " ~ op ~ " rhs._store[];");
		return res;
	}

	void opOpAssign(string op)(in typeof(this) rhs)
	if (op == "&" || op == "|" || op == "^") {
		version(D_Coverage) {} else pragma(inline, true);
		mixin("_store[] = _store[] " ~ op ~ " rhs._store[];");
	}

	void clear() {
		version(D_Coverage) {} else pragma(inline, true);
		_store[] = 0;
	}

	bool empty() const @property {
		version(D_Coverage) {} else pragma(inline, true);
		return this == typeof(this).init;
	}

	/++ Returns: Number of elements stored. +/
	uint countElements() const @property {
		typeof(return) res;
		foreach (const ref word; _store)
			res += popcnt(word);
		return res;
	}
	alias count = countElements;
	alias occupancy = count;

	bool full() const @property {
		version(D_Coverage) {} else pragma(inline, true);
		return this == ~typeof(this).init;
	}

	import nxt.io : Format;

	void toString(Sink)(scope ref Sink sink, in Format fmt = Format.init) const @property {
		import std.conv : to;
		sink.put(fmt.setPrefix);
		bool other = false;
		foreach (const e; E.min .. E.max + 1) {
			if (!contains(e))
				continue;
			if (other)
				sink.put(fmt.elementSeparator);
			other = true;
			sink.put(e.to!string); // TODO: avoid call to `std.conv.to`
		}
		sink.put(fmt.setSuffix);
	}

private:
	enum nW = 8*W.sizeof;
	W[4] _store; // Bit array to represent the presence of elements.
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	alias S = StaticDenseSet!char;
	S s1;
	S s2;

	assert(s1.empty);
	assert(s1.insert('5'));
	assert(!s1.empty);
	assert(s1.insert('6'));
	assert(!s1.empty);

	assert(s2.empty);
	assert(s2.insert('6'));
	assert(!s2.empty);
	assert(s2.insert('7'));
	assert(!s2.empty);

	assert(s1.contains('5'));
	assert(s1.contains('6'));
	assert(!s1.contains('7'));
	assert(s2.contains('7'));

	const s3 = s1 | s2;
	assert(s3.contains('5'));
	assert(s3.contains('6'));
	assert(s3.contains('7'));

	const s4 = s1 & s2;
	assert(!s4.contains('5'));
	assert(s4.contains('6'));
	assert(!s4.contains('7'));

	const s5 = s1 ^ s2;
	assert(s5.contains('5'));
	assert(!s5.contains('6'));
	assert(s5.contains('7'));

	const s6 = ~s5;
	assert(!s6.contains('5'));
	assert(s6.contains('6'));
	assert(!s6.contains('7'));

	assert(s1.remove('6'));
	assert(!s1.remove('6'));
	assert(!s1.contains('6'));

	s2.clear();
	assert(!s2.contains('6'));
	assert(!s2.contains('7'));

	assert(s2.empty);
	assert((~s2).full);

	auto s7 = S("123");
	assert(!s7.contains('0'));
	assert(s7.contains('1'));
	assert(s7.contains('2'));
	assert(s7.contains('3'));
}

pure nothrow @safe @nogc version(nxt_test) unittest {
	alias E = ubyte;
	alias S = StaticDenseSet!E;

	S s1;
	assert(s1.minElement(255) == 255);
	assert(s1.maxElement(255) == 255);

	assert(s1.insert(E.min));
	assert(!s1.insert(E.min));
	assert(s1.minElement(255) == E.min);
	assert(s1.maxElement(255) == E.min);

	assert(s1.insert(1));
	assert(!s1.insert(1));
	assert(s1.minElement(255) == E.min);
	assert(s1.maxElement(255) == 1);

	assert(s1.insert(E.max));
	assert(!s1.insert(E.max));
	assert(s1.contains(E.max));
	assert(s1.minElement(128) == 0);
	assert(s1.maxElement(128) == E.max);
}

pure nothrow @safe version(nxt_test) unittest {
	alias E = char;
	alias S = StaticDenseSet!E;
	S s;
	s.insert(E.min);
	s.insert(E.max);
	auto sink = sdSink();
	s.toString(sink);
	assert(sink.data == "{0, 255}");
}

pure nothrow @safe @nogc version(nxt_test) unittest {
	alias E = ubyte;
	alias S = StaticDenseSet!E;
	S s1;
	assert(s1.toggle(0));
	assert(s1.contains(0));
	assert(!s1.toggle(0));
	assert(!s1.contains(0));
	assert(s1.empty);
}

pure nothrow @safe @nogc version(nxt_test) unittest {
	alias E = ubyte;
	alias S = StaticDenseSet!E;
	auto s = S(42);
	assert(s.count == 1);
	s.insert(43);
	assert(s.count == 2);
	s.insert(E.min);
	assert(s.count == 3);
	s.insert(E.max);
	assert(s.count == 4);
	assert(!s.empty);
}

version(nxt_test) version(unittest) {
	import nxt.io.sink : sdSink;
}
