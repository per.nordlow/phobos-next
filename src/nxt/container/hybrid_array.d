/** Hybrid array containers.
 */
module nxt.hybrid_array;

import std.traits : isIntegral;
import nxt.filters : isDynamicDenseSetable;
import std.experimental.allocator.gc_allocator : GCAllocator;

/** Hybrid container combining `DynamicDenseSet` growable into `DynamicArray`.
 *
 * Has O(1) unordered element access via slicing.
 *
 * For use in graph algorithms with limited index ranges.
 *
 * TODO: better name?
 */
struct DynamicDenseSetGrowableArray(E, Allocator = GCAllocator)
if (isDynamicDenseSetable!E) {
	import nxt.filters : DynamicDenseSet, Growable, Copyable;
	import nxt.container.dynamic_array : DynamicArray;

	alias ElementType = E;

	this(this) @disable;

	/** Insert element `e`.
		Returns: precense status of element before insertion.
	*/
	bool insert()(E e) /*tlm*/ {
		version(D_Coverage) {} else pragma(inline, true);
		/+ TODO: this doesn’t seem right: +/
		const hit = _set.insert(e);
		if (!hit)
			_array.insertBack(e);
		return hit;
	}
	alias put = insert;		 // OutputRange compatibility

	/// Check if element `e` is stored/contained.
	bool contains()(E e) const /*tlm*/ {
		version(D_Coverage) {} else pragma(inline, true);
		return _set.contains(e);
	}
	/// ditto
	bool opBinaryRight(string op)(E e) const if (op == "in") {
		version(D_Coverage) {} else pragma(inline, true);
		return contains(e);
	}
	/// ditto
	alias has = contains;

	/// Get length.
	@property size_t length() const {
		version(D_Coverage) {} else pragma(inline, true);
		return _array.length;
	}

	/// Non-mutable slicing.
	auto opSlice() const {
		version(D_Coverage) {} else pragma(inline, true);
		return _array[];
	}

	/// Clear contents.
	void clear()() /*tlm*/ {
		version(D_Coverage) {} else pragma(inline, true);
		_set.clear();
		_array.clear();
	}

private:
	/+ TODO: merge into store with only one length and capcity +/
	DynamicDenseSet!(E, Growable.yes, Copyable.no) _set;
	DynamicArray!(E, Allocator) _array;
}

pure nothrow @safe:

version(nxt_test) unittest {
	DynamicDenseSetGrowableArray!uint x;

	assert(!x.insert(42));
	assert(x.contains(42));
	assert(x[] == [42].s);

	assert(x.insert(42));
	assert(x.contains(42));
	assert(x[] == [42].s);

	assert(!x.insert(43));
	assert(x.contains(43));
	assert(x[] == [42, 43].s);

	x.clear();
	assert(x.length == 0);

	assert(!x.insert(44));
	assert(x.contains(44));
	assert(x[] == [44].s);
}

version(nxt_test) version(unittest) {
	import nxt.array_help : s;
}
