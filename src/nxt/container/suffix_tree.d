module nxt.container.suffix_tree;

struct SuffixTree {
    import std.algorithm : min;

pure nothrow @safe:

    private struct Node {
        pure nothrow @safe:

        ubyte[] _edge;
        Node*[] _subs;

        this(ubyte[] _edge) {
            this._edge = _edge;
        }

        this(immutable(ubyte)[] _edge) {
            this._edge = _edge.dup;
        }

        void addChild(scope const(ubyte)[] _edge) {
            _subs ~= new Node(_edge.idup);
        }
    }

    this(ubyte[] sequence) {
        this._root = Node.init;
        buildTree(sequence);
    }

    this(immutable(ubyte)[] sequence) {
        this._root = Node.init;
        buildTree(sequence);
    }

    private void buildTree(scope const(ubyte)[] text) {
        foreach (const i; 0 .. text.length)
            addSuffix(text[i .. $]);
    }

    private void addSuffix(scope const(ubyte)[] suffix) {
        Node* currentNode = &_root;
        auto remainder = suffix;

        while (remainder.length != 0) {
            bool found = false;
            foreach (ref childPtr; currentNode._subs) {
                auto child = childPtr;

                // Compare first byte to find a matching _edge
                if (child._edge[0] == remainder[0]) {
                    size_t matchLength = min(child._edge.length, remainder.length);

                    // Find the longest matching prefix with the current _edge
                    foreach (const size_t j; 0 .. matchLength) {
                        if (child._edge[j] != remainder[j]) {
                            // Split the _edge at the mismatch point
                            auto splitNode = new Node(child._edge[0 .. j]);
                            splitNode._subs ~= childPtr;
                            child._edge = child._edge[j .. $];
                            childPtr = splitNode;
                            currentNode = splitNode;
                            found = true;
                            break;
                        }
                    }

                    // If entire _edge matches, move to this child
                    if (!found) {
                        currentNode = child;
                        remainder = remainder[matchLength .. $];
                        found = true;
                    }
                }
            }

            // If no matching child is found, create a new child for the remainder
            if (!found) {
                currentNode.addChild(remainder);
                break;
            }
        }
    }

    bool contains(scope const(ubyte)[] sequence) {
        Node* currentNode = &_root;
        auto remainder = sequence;

        while (remainder.length != 0) {
            bool found = false;
            foreach (ref childPtr; currentNode._subs) {
                auto child = childPtr;

                // Compare the first byte to find the matching child
                if (child._edge[0] == remainder[0]) {
                    size_t matchLength = min(child._edge.length, remainder.length);

                    // Check if this _edge fully matches part of the sequence
                    if (remainder[0 .. matchLength] == child._edge[0 .. matchLength]) {
                        remainder = remainder[matchLength .. $];
                        currentNode = child;
                        found = true;
                        break;
                    }
                }
            }

            // If no matching child found, sequence is not in the tree
            if (!found) return false;
        }

        return true;
    }
	/// ditto
	alias has = contains;

    private Node _root;
}

pure @safe version(nxt_test) unittest {
    import std.string : representation;

    const s = "hello world";
    auto t = SuffixTree(s.representation);

    assert(t.contains("d".representation));
    assert(t.contains("o".representation));

    assert(t.contains("hello".representation));
    assert(t.contains("world".representation));

    assert(!t.contains("helloo".representation));
    assert(!t.contains("word".representation));
    assert(!t.contains(" worlds".representation));
    assert(!t.contains("hello_world".representation));

    assert(!t.contains("x".representation));
    assert(t.contains(" ".representation));

	// TODO: Fails for
	version(none)
	foreach (const i; 0 .. s.length - 1) {
		const ss = s[i .. $];
		dbg(ss);
		assert(t.contains(ss.representation));
	}
}

version(unittest) {
    import std.string : representation;
	import nxt.io.dbg;
}
