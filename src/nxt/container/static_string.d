/** Statically allocated strings with compile-time known lengths.
 */
module nxt.static_string;

import nxt.container.static_array;

@safe:

/** Stack-allocated string of maximum length of `capacity.`
 *
 * Similar to `mir.small_string` at http://mir-algorithm.libmir.org/mir_small_string.html.
 */
alias StringN(uint capacity, bool borrowChecked = false)
	= StaticArray!(immutable(char), capacity, borrowChecked);

/** Stack-allocated wstring of maximum length of `capacity.` */
alias WStringN(uint capacity, bool borrowChecked = false)
	= StaticArray!(immutable(wchar), capacity, borrowChecked);

/** Stack-allocated dstring of maximum length of `capacity.` */
alias DStringN(uint capacity, bool borrowChecked = false)
	= StaticArray!(immutable(dchar), capacity, borrowChecked);

/** Stack-allocated mutable string of maximum length of `capacity.` */
alias MutableStringN(uint capacity, bool borrowChecked = false)
	= StaticArray!(char, capacity, borrowChecked);

/** Stack-allocated mutable wstring of maximum length of `capacity.` */
alias MutableWStringN(uint capacity, bool borrowChecked = false)
	= StaticArray!(char, capacity, borrowChecked);

/** Stack-allocated mutable dstring of maximum length of `capacity.` */
alias MutableDStringN(uint capacity, bool borrowChecked = false)
	= StaticArray!(char, capacity, borrowChecked);

/// scope checked string
version(none)
pure @safe version(nxt_test) unittest {
	enum capacity = 15;
	foreach (StrN; AliasSeq!(StringN// , WStringN, DStringN
				 )) {
		alias String15 = StrN!(capacity);

		typeof(String15.init[0])[] xs;
		assert(xs.length == 0);
		auto x = String15("alphas");

		assert(x[0] == 'a');
		assert(x[$ - 1] == 's');

		assert(x[0 .. 2] == "al");
		assert(x[] == "alphas");

		const y = String15("åäö_åäöå"); // fits in 15 chars
		assert(y.length == capacity);
	}
}

/// scope checked string
version(none)
pure version(nxt_test) unittest {
	enum capacity = 15;
	foreach (Str; AliasSeq!(StringN!capacity,
							WStringN!capacity,
							DStringN!capacity)) {
		static assert(!mustAddGCRange!Str);
		static if (hasPreviewDIP1000) {
			static assert(!__traits(compiles, {
						auto f() @safe pure
						{
							auto x = Str("alphas");
							auto y = x[];
							return y;   // errors with -dip1000
						}
					}));
		}
	}
}

/// assignment from `const` to `immutable` element type
version(none)
pure @safe version(nxt_test) unittest {
	enum capacity = 15;
	alias String15 = StringN!(capacity);
	static assert(!mustAddGCRange!String15);
	enum n = 4;
	immutable char[n] _ = ['a', 'b', 'c', 'd'];
	auto x = String15(_[]);
	assert(x.length == 4);
	assert(x[] == _);
	foreach_reverse (const i; 0 .. n)
		assert(x.takeBack() == _[i]);
	assert(x.empty);
}

/// borrow checking
version(none)
@system pure version(nxt_test) unittest {
	enum capacity = 15;
	alias String15 = StringN!(capacity, true);
	static assert(String15.readBorrowCountMax == 7);
	static assert(!mustAddGCRange!String15);

	auto x = String15("alpha");

	assert(x[] == "alpha");

	{
		auto xw1 = x[];
		assert(x.isWriteBorrowed);
		assert(x.isBorrowed);
	}

	auto xr1 = (cast(const)x)[];
	assert(x.readBorrowCount == 1);

	auto xr2 = (cast(const)x)[];
	assert(x.readBorrowCount == 2);

	auto xr3 = (cast(const)x)[];
	assert(x.readBorrowCount == 3);

	auto xr4 = (cast(const)x)[];
	assert(x.readBorrowCount == 4);

	auto xr5 = (cast(const)x)[];
	assert(x.readBorrowCount == 5);

	auto xr6 = (cast(const)x)[];
	assert(x.readBorrowCount == 6);

	auto xr7 = (cast(const)x)[];
	assert(x.readBorrowCount == 7);

	assertThrown!AssertError((cast(const)x)[]);
}

version(nxt_test) version(unittest) {
	import std.meta : AliasSeq;
	import std.exception : assertThrown;
	import core.exception : AssertError;
	import nxt.container.traits : mustAddGCRange;
	import nxt.dip_traits : hasPreviewDIP1000;
}
