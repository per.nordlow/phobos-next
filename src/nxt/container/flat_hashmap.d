module nxt.container.flat_hashmap;

// TODO: Fix and enable
version(none):

/++ Hash map with flat storage. +/
struct FlatHashMap(K, V = void) {
	import nxt.prime_modulo;

    @disable this(this);

    this(in size_t initialCapacity) {
        const capacity = _getNextPrimeCapacity(initialCapacity);
        _store = new Entry[capacity];
    }

    void insert(K key, V value) {
        if (_count > _store.length * _loadFactorP / _loadFactorQ)
            _resize();
        const index = _findIndexOfVacantSlot(key);
        if (!_store[index].occupied)
            _count++;
        _store[index] = Entry(key, value, true);
    }
	alias add = insert;
	alias put = insert;

    V* tryGetValuePtr(K key) {
        const index = _findIndexOfVacantSlot(key);
        if (_store[index].occupied && _store[index].key == key)
            return &_store[index].value;
        return null;
    }
	alias tryGet = tryGetValuePtr;

    bool remove(K key) {
        const index = _findIndexOfVacantSlot(key);
        if (_store[index].occupied && _store[index].key == key) {
            _store[index].occupied = false;
            _count--;
            return true;
        }
        return false;
    }

    @property size_t length() const => _count;

private:
	size_t _hash(K key) const {
		return typeid(K).getHash(&key) % (_store.length - 1);
	}


	size_t _findIndexOfVacantSlot(K key) const {
		size_t index = _hash(key);
		size_t i = 1;
		while (_store[index].occupied && _store[index].key != key) {
			index = (index + i) & (_store.length - 1);
			i++;
		}
		return index;
	}

	void _resize() {
		auto oldEntries = _store;
		_store = new Entry[_getNextPrimeCapacity(_store.length * 2)];
		_count = 0;

		foreach (ref entry; oldEntries)
			if (entry.occupied)
				put(entry.key, entry.value);
	}

	size_t _getNextPrimeCapacity(in size_t minCapacity) {
		foreach (prime; capacityPrimes)
			if (prime >= minCapacity)
				return prime;
		return capacityPrimes[$-1];  // Return the largest prime if minCapacity is too large
	}

	struct Entry {
		K key;
		V value;
		bool occupied;
	}

	Entry[] _store;
	size_t _count; ///< Number of occupied elements in `_store`.
	PrimeIndex _pix; ///< TODO: Use.

	enum _loadFactorP = 3; ///< Numerator for load factor.
	enum _loadFactorQ = 4; ///< Denominator for load factor.
}

@safe pure version(nxt_test) unittest {
	alias M = FlatHashMap!(int, int);
	M x;
}
