module nxt.container.sso_array;

import std.experimental.allocator.common : isAllocator;
import std.experimental.allocator.gc_allocator : GCAllocator;

/** Small-Size-Optimized (SSO) `Array`.
 *
 * See_Also: https://forum.dlang.org/post/ifspcvfkwsnvyrdfngpw@forum.dlang.org
 */
struct SSOArray(T, size_t smallCapacity, Allocator = GCAllocator)
if (smallCapacity >= 1 && isAllocator!Allocator) {
	static if (!__traits(isPOD, T))
		import nxt.lifetime : move;

	void insertBack(T x) @trusted {
		reserve(length + 1);
		if (_isLarge) {
			static if (__traits(isPOD, T))
				_large.put(x);
			else
				_large.put(x.move);
		} else {
			static if (__traits(isPOD, T))
				_small.put(x);
			else
				_small.put(x.move);
		}
	}
	alias put = insertBack;

	void reserve(in size_t requestedCapacity) @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		if (requestedCapacity > smallCapacity)
			makeLarge();
		if (_isLarge)
			_large.reserve(requestedCapacity);
	}

	private void makeLarge() @trusted{
		if (_isLarge)
			return;
		import std.algorithm.mutation : moveEmplaceAll;

		/// Store of `capacity` number of elements.
		/+ TODO: use store constructor. Don't initialize if  +/
		import core.internal.traits : hasElaborateDestructor;
		static if (hasElaborateDestructor!T)
			T[smallCapacity] tmp;
		else
			T[smallCapacity] tmp = void;
		moveEmplaceAll(_small[], tmp[0 .. _small.length]);

		// TODO: Use: new (_large) Large(Large.init); // placement new
		import core.lifetime : emplace;
		emplace!Large(&_large);

		_large.put(tmp[]);
		_isLarge = 1;
	}

	size_t length() const @property @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		if (_isLarge)
			return _large.length;
		else
			return _small.length;
	}
	/// ditto
	alias opDollar = length;

	ref inout(T) opIndex(in size_t i) inout return scope @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		if (_isLarge)
			return _large[i];
		else
			return _small[i];
	}

	inout(T)[] opSlice(in size_t i, in size_t j) inout return scope @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		if (_isLarge)
			return _large[i .. j];
		else
			return _small[i .. j];
	}

	inout(T)[] opSlice() inout return scope @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		if (_isLarge)
			return _large[];
		else
			return _small[];
	}

	size_t capacity() const @property @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		if (_isLarge)
			return _large.capacity;
		else
			return smallCapacity;
	}

private:
	import nxt.container.dynamic_array : DynamicArray;
	import nxt.container.static_array : StaticArray;

	alias Small = StaticArray!(T, smallCapacity);
	alias Large = DynamicArray!(T, Allocator);
	union {
		Small _small;
		Large _large;
	}
	/+ TODO: Pack suitable bit of union above by introspecing
	   `typeof(Small.tupleof)` and `typeof(Large.tupleof[$-1])`.
	   +/
	bool _isLarge;
}

pure nothrow @safe version(nxt_test) unittest {
	enum smallCapacity = 2;
	alias A = SSOArray!(byte, smallCapacity);
	A a;
	assert(a.capacity == smallCapacity);
	assert(a.length == 0);
	assert(!a._isLarge);
	a.put(11);
	assert(a.capacity == smallCapacity);
	assert(a.length == 1);
	assert(!a._isLarge);
	assert(a[0] == 11);
	assert(a[0 .. 1] == [11]);
	a.put(12);
	assert(a.capacity == smallCapacity);
	assert(a.length == 2);
	assert(!a._isLarge);
	assert(a[] == [11, 12]);
	assert(a[0 .. 2] == [11, 12]);
	assert(a[1] == 12);
	a.put(13);
	assert(a.capacity >= 3);
	assert(a.length == 3);
	assert(a._isLarge);
	assert(a[] == [11, 12, 13]);
	assert(a[0 .. 3] == [11, 12, 13]);
	assert(a[2] == 13);
	a.put(14);
	assert(a.capacity >= 4);
	assert(a.length == 4);
	assert(a._isLarge);
	assert(a[3] == 14);
	assert(a[] == [11, 12, 13, 14]);
	assert(a[0 .. 4] == [11, 12, 13, 14]);
	static if (hasPreviewDIP1000) {
		static assert(!__traits(compiles, {
					auto f() @safe pure {
						scope const x = SSOArray!(byte, 2)();
						const y = x[];
						return y;   // errors with -dip1000
					}
				}));
	}
}

version(nxt_test) version(unittest)
{
	import nxt.dip_traits : hasPreviewDIP1000;
}
