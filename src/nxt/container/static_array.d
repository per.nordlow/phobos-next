/** Statically allocated arrays with compile-time known lengths.
 */
module nxt.container.static_array;

@safe pure:

/++ Statically allocated `T`-array of fixed pre-allocated length.
 +
 + Similar to C++'s `std::static_vector<T, Capacity>`
 + Similar to Rust's `fixedvec`: https://docs.rs/fixedvec/0.2.4/fixedvec/
 + Similar to `mir.small_array` at http://mir-algorithm.libmir.org/mir_small_array.html
 +
 + See_Also: https://en.cppreference.com/w/cpp/container/array
 +
 + TODO: Merge member functions with basic_+_array.d and array_ex.d
 +/
struct StaticArray(T, uint capacity_) {
	import core.exception : onRangeError;
	import nxt.lifetime : move, moveEmplace;
	import core.internal.traits : hasElaborateDestructor;
	import std.bitmanip : bitfields;
	import std.traits : isSomeChar, isAssignable, hasIndirections;
	import nxt.container.traits : mustAddGCRange;

	enum capacity = capacity_; // for public use

	static if (capacity <= ubyte.max) {
		static if (T.sizeof == 1)
			private alias Length = ubyte; // pack length
		else
			private alias Length = uint;
	} else static if (capacity <= ushort.max) {
		static if (T.sizeof <= 2)
			private alias Length = uint; // pack length
		else
			private alias Length = uint;
	} else
		static assert("Too large requested capacity " ~ capacity);

	/// Is `true` if `U` can be assigned to the elements of `this`.
	private enum canAssignElementFromType(U) = isAssignable!(T, U);

	/// Empty.
	void clear() @nogc {
		version(D_Coverage) {} else pragma(inline, true);
		releaseElementsStore();
		resetInternalData();
	}

	/// Release elements and internal store.
	private void releaseElementsStore() @trusted @nogc {
		foreach (const i; 0 .. length) {
			static if (!__traits(isPOD, T) && hasElaborateDestructor!T)
				.destroy(_store.ptr[i]);
			else static if (hasIndirections!T) // avoid GC mark-phase dereference
				_store.ptr[i] = T.init; // TODO: nullify pointers only
		}
	}

	/// Reset internal data.
	private void resetInternalData() @nogc {
		version(D_Coverage) {} else pragma(inline, true);
		_length = 0;
	}

	/// Construct from element `values`.
	static typeof(this) fromValuesUnsafe(U)(U[] values) @system if (__traits(isCopyable, U) && canAssignElementFromType!U) // prevent accidental move of l-value `values` in array calls
	{
		typeof(return) that;			  /+ TODO: use Store constructor: +/
		that._store[0 .. values.length] = values;
		that._length = cast(Length)values.length;
		return that;
	}

	/// No default copying.
	this(this) @disable;

	static if ((!__traits(isPOD, T) && hasElaborateDestructor!T)) {
		version(D_Coverage) {} else pragma(inline, true);
		~this() nothrow @nogc {
			releaseElementsStore();
		}
	}

	/** Add elements `es` to the back.
	 * Throws when array becomes full.
	 */
	void insertBack(Es...)(Es es) @trusted
	if (Es.length <= capacity) /+ TODO: use `isAssignable` +/ {
		version(assert) if (_length + Es.length > capacity) onRangeError(); // `Arguments don't fit in array`. Rely on normal bounds checks for release build.
		static foreach (const i, e; es) {
			static if (__traits(isPOD, T))
				_store[_length + i] = e;
			else
				moveEmplace(e, _store[_length + i]); /+ TODO: remove when compiler does this +/
		}
		_length = cast(Length)(_length + Es.length); /+ TODO: better? +/
	}
	/// ditto
	alias put = insertBack;	   // `OutputRange` support

	/** Add elements `es` to the back. */
	void opOpAssign(string op, Us...)(Us values)
	if (op == "~" && values.length >= 1 && allSatisfy!(canAssignElementFromType, Us)) {
		version(D_Coverage) {} else pragma(inline, true);
		insertBack(values.move()); /+ TODO: remove `move` when compiler does it for +/
	}

	import std.traits : isMutable;
	static if (isMutable!T) {
		/** Pop first (front) element. */
		auto ref popFront() in(_length) {
			/+ TODO: is there a reusable Phobos function for this? +/
			foreach (const i; 0 .. _length - 1)
				move(_store[i + 1], _store[i]); // like `_store[i] = _store[i + 1];` but more generic
			_length = cast(typeof(_length))(_length - 1); /+ TODO: better? +/
			return this;
		}
	}

	/** Pop last (back) element. */
	void popBack()() @trusted in(_length) /*tlm*/ {
		version(D_Coverage) {} else pragma(inline, true);
		_length = cast(Length)(_length - 1); /+ TODO: better? +/
		static if (!__traits(isPOD, T) && hasElaborateDestructor!T)
			.destroy(_store.ptr[_length]);
		else static if (mustAddGCRange!T) // avoid GC mark-phase dereference
			_store.ptr[_length] = T.init; // TODO: nullify pointers only
	}

	T takeBack()() @trusted in(_length) /*tlm*/ {
		version(D_Coverage) {} else pragma(inline, true);
		static if (__traits(isPOD, T))
			return _store.ptr[--_length]; // no move needed
		else
			return move(_store.ptr[--_length]); // move is indeed need here
	}

	/** Pop the `n` last (back) elements. */
	void popBackN()(in size_t n) @trusted in(length >= n) /*tlm*/ {
		_length = cast(Length)(_length - n); /+ TODO: better? +/
		static if (!__traits(isPOD, T) && hasElaborateDestructor!T)
			foreach (const i; 0 .. n)
				.destroy(_store.ptr[_length + i]);
		else static if (mustAddGCRange!T) // avoid GC mark-phase dereference
			foreach (const i; 0 .. n)
				_store.ptr[_length + i] = T.init; // TODO: nullify pointers only
	}

	/** Move element at `index` to return. */
	static if (isMutable!T) {
		/** Pop element at `index`. */
		void popAt()(in size_t index) /*tlm*/ @trusted @("complexity", "O(length)") in(index < _length) {
			static if (!__traits(isPOD, T) && hasElaborateDestructor!T)
				.destroy(_store.ptr[index]);
			shiftToFrontAt(index);
			_length = cast(Length)(_length - 1);
		}

		T moveAt()(in size_t index) /*tlm*/ @trusted @("complexity", "O(length)") in(index < _length) {
			auto value = _store.ptr[index].move();
			shiftToFrontAt(index);
			_length = cast(Length)(_length - 1);
			return value;
		}

		private void shiftToFrontAt()(in size_t index) /*tlm*/ @trusted
		{
			foreach (const i; 0 .. _length - (index + 1)) {
				immutable si = index + i + 1; // source index
				immutable ti = index + i;	 // target index
				moveEmplace(_store.ptr[si],
							_store.ptr[ti]);
			}
		}
	}

	/** Index operator. */
	pragma(inline, true)
	ref inout(T) opIndex(in size_t i) inout return => _store[i];

	/// Get slice in range `i` .. `j`.
	pragma(inline, true);
	inout(T)[] opSlice(in size_t i, in size_t j) inout return => _store[i .. j];

	/// Get full slice.
	pragma(inline, true)
	inout(T)[] opSlice() inout @trusted return /+ TODO: remove @trusted? +/ => _store[0 .. _length];

	@property pragma(inline, true) {
		/** Get length. */
		auto length() const { return _length; }
		/// ditto
		alias opDollar = length;
	}

	/** Comparison for equality. */
	bool opEquals()(const scope auto ref typeof(this) rhs) const => this[] == rhs[];
	/// ditto
	bool opEquals(U)(const scope U[] rhs) const if (is(typeof(T[].init == U[].init))) => this[] == rhs;

	/// Store of `capacity` number of elements.
	/+ TODO: use store constructor. Don't initialize if  +/
	static if (hasIndirections!T)
		private T[capacity] _store;
	else
		private T[capacity] _store = void;
	private Length _length;
}

/** Returns: `true` iff `this` is empty, `false` otherwise. */
bool empty(T, uint capacity)(in StaticArray!(T, capacity) sa) {
	version(D_Coverage) {} else pragma(inline, true);
	return sa._length == 0;
}

/** Returns: `true` iff `this` is full, `false` otherwise. */
bool full(T, uint capacity)(in StaticArray!(T, capacity) sa) {
	version(D_Coverage) {} else pragma(inline, true);
	return sa._length == sa.capacity;
}

/** First (front) element of `sa`. */
ref inout(T) front(T, uint capacity)(inout ref return StaticArray!(T, capacity) sa) {
	version(D_Coverage) {} else pragma(inline, true);
	return sa._store[0];
}

/** Last (back) element of `sa`. */
ref inout(T) back(T, uint capacity)(inout ref return StaticArray!(T, capacity) sa) {
	version(D_Coverage) {} else pragma(inline, true);
	return sa._store[sa._length - 1];
}

/// construct from array may throw
pure @safe @nogc version(nxt_test) unittest {
	alias T = int;
	alias A = StaticArray!(T, 3);
	static assert(!mustAddGCRange!A);
	A a;
	a.insertBack(1);
	a.insertBack(2);
	assert(a[] == [1, 2].s);
}

/// unsafe construct from array
pure nothrow @nogc version(nxt_test) unittest {
	alias T = int;
	alias A = StaticArray!(T, 3);
	static assert(!mustAddGCRange!A);
	() @trusted {
		const a = A.fromValuesUnsafe([1, 2, 3].s);
		assert(a[] == [1, 2, 3].s);
	}();
}

/// construct from scalars is nothrow
pure nothrow @safe @nogc version(nxt_test) unittest {
	alias T = int;
	alias A = StaticArray!(T, 3);
	static assert(!mustAddGCRange!A);
	auto a = A();
	a.insertBack(1);
	a.insertBack(2);
	a.insertBack(3);
	assert(a[] == [1, 2, 3].s);
	static assert(!__traits(compiles, { const _ = A(1, 2, 3, 4); }));
}

pure @safe version(nxt_test) unittest {
	static assert(mustAddGCRange!(StaticArray!(string, 1)));
	static assert(mustAddGCRange!(StaticArray!(string, 2)));
}

///
pure @safe version(nxt_test) unittest {
	import std.exception : assertNotThrown;

	alias T = char;
	enum capacity = 3;

	alias A = StaticArray!(T, capacity);
	static assert(!mustAddGCRange!A);
	static assert(A.sizeof == T.sizeof*capacity + 1);

	import std.range.primitives : isOutputRange;
	static assert(isOutputRange!(A, T));

	auto ab = A();
	ab.insertBack('a');
	ab.insertBack('b');
	assert(!ab.empty);
	assert(ab[0] == 'a');
	assert(ab.front == 'a');
	assert(ab.back == 'b');
	assert(ab.length == 2);
	assert(ab[] == "ab");
	assert(ab[0 .. 1] == "a");
	() @trusted { assertNotThrown(ab.insertBack('_')); }();
	assert(ab[] == "ab_");
	ab.popBack();
	assert(ab[] == "ab");

	ab.popBackN(2);
	assert(ab.empty);
	() @trusted { assertNotThrown(ab.insertBack('a', 'b')); }();

	auto abc = A();
	abc.insertBack('a');
	abc.insertBack('b');
	abc.insertBack('c');
	assert(!abc.empty);
	assert(abc.front == 'a');
	assert(abc.back == 'c');
	assert(abc.length == 3);
	assert(abc[] == "abc");
	assert(ab[0 .. 2] == "ab");
	assert(abc.full);
	static assert(!__traits(compiles, { const abcd = A('a', 'b', 'c', 'd'); })); // too many elements

	assert(ab[] == "ab");
	ab.popFront();
	assert(ab[] == "b");

	auto xy = A();
	xy.insertBack('x');
	xy.insertBack('y');
	assert(!xy.empty);
	assert(xy[0] == 'x');
	assert(xy.front == 'x');
	assert(xy.back == 'y');
	assert(xy.length == 2);
	assert(xy[] == "xy");
	assert(xy[0 .. 1] == "x");

	auto xyz = A();
	xyz.insertBack('x');
	xyz.insertBack('y');
	xyz.insertBack('z');
	assert(!xyz.empty);
	assert(xyz.front == 'x');
	assert(xyz.back == 'z');
	assert(xyz.length == 3);
	assert(xyz[] == "xyz");
	assert(xyz.full);
	static assert(!__traits(compiles, { const xyzw = A('x', 'y', 'z', 'w'); })); // too many elements
}

///
pure @safe version(nxt_test) unittest {
	static void testAsSomeString(T)() {
		enum capacity = 15;
		alias A = StaticArray!(T, capacity);
		static assert(A.sizeof == capacity + 1);
		static assert(!mustAddGCRange!A);
		auto a = A();
		a.insertBack('a');
		a.insertBack('b');
		a.insertBack('c');
		assert(a[] == "abc");
	}
	foreach (T; AliasSeq!(char)) {
		testAsSomeString!T();
	}
}

/// equality
pure @safe version(nxt_test) unittest {
	alias A = StaticArray!(int, 15);
	static assert(!mustAddGCRange!A);
	A a;
	a.insertBack(1);
	a.insertBack(2);
	A b;
	b.insertBack(1);
	b.insertBack(2);
	assert(a == b);
}

pure @safe version(nxt_test) unittest {
	class C { const int value; }
	alias A = StaticArray!(C, 2);
	static assert(mustAddGCRange!A);
}

/// equality
@system pure nothrow @nogc version(nxt_test) unittest {
	enum capacity = 15;
	alias A = StaticArray!(int, capacity);
	assert(A.fromValuesUnsafe([1, 2, 3].s) ==
		   A.fromValuesUnsafe([1, 2, 3].s));
	const ax = [1, 2, 3].s;
	assert(A.fromValuesUnsafe([1, 2, 3].s) == ax);
	assert(A.fromValuesUnsafe([1, 2, 3].s) == ax[]);
	const cx = [1, 2, 3].s;
	assert(A.fromValuesUnsafe([1, 2, 3].s) == cx);
	assert(A.fromValuesUnsafe([1, 2, 3].s) == cx[]);
	immutable ix = [1, 2, 3].s;
	assert(A.fromValuesUnsafe([1, 2, 3].s) == ix);
	assert(A.fromValuesUnsafe([1, 2, 3].s) == ix[]);
}

version(nxt_test) version(unittest) {
	import std.meta : AliasSeq;
	import std.exception : assertThrown;
	import core.exception : AssertError;
	import nxt.array_help : s;
	import nxt.container.traits : mustAddGCRange;
	import nxt.dip_traits : hasPreviewDIP1000;
}
