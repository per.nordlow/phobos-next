/++ Translations.
	Test: dmd -version=nxt_integration_test -version=show -vcolumns -preview=in -preview=dip1000 -g -checkaction=context -allinst -unittest -i -I../.. -main -run google.d
 +/
module nxt.translation.google;

// version = nxt_integration_test;

import nxt.iso_639_1 : LanguageCode, LanguageCodeString;

@safe:

/++ Translate `text` from `src` to `dst` via Google Translate.
	If `src` is `LanguageCode.init` autodetect source language.
	See_Also: https://pypi.org/project/googletrans/
 +/
string translateViaGoogle(in char[] text, in LanguageCode src = LanguageCode.init, in LanguageCode dst) {
	return translateViaGoogle(text, LanguageCodeString(src), LanguageCodeString(dst));
}

/++ Translate `text` from `src` to `dst` via Google Translate.
	If `src` is `LanguageCodeString.init` autodetect source language.
 +/
private string translateViaGoogle(in char[] text, in LanguageCodeString src, in LanguageCodeString dst) @trusted {
	import std.uri : encode;

	import std.net.curl : HTTP;
	const srcDefaulted = src != LanguageCodeString.init ? src : LanguageCodeString(LanguageCode.en);
	const msg = "http://translate.googleapis.com/translate_a/single?client=gtx&sl="~srcDefaulted.chars ~"&tl="~dst.chars ~ "&dt=t&q="~encode(text);
	auto http = HTTP(msg);
	http.onReceiveStatusLine = (HTTP.StatusLine status) {
		import std.conv : to;
		if (status.code != 200)
			throw new Exception("Error " ~ status.code.to!string ~ " (" ~ status.reason.to!string ~ ")");
	};

	string rspJson;
	http.onReceive = (ubyte[] data) {
		rspJson = cast(string) data;
		return data.length;
	};

	http.perform();

	typeof(return) result;
	import std.json : parseJSON, JSONType;
	foreach (const entry; rspJson.parseJSON().array) {
		import std.exception : enforce;
		if (entry.type is JSONType.array) {
			import std.algorithm : each;
			foreach (const second; entry.array) {
				if (second.type is JSONType.array && second[0].type is JSONType.string) {
					result ~= second[0].str;
				}
			}
		}
	}

	return result;
}

@safe version(nxt_integration_test) unittest {
	const x = "Text intended for translation into LANGUAGE";
 	assert(x.tr(LC.en, LC.de) == "Text, der in die SPRACHE übersetzt werden soll");
	assert(x.tr(LC.en, LC.tr) == "LANGUAGE diline çevrilmesi amaçlanan metin");
	assert(x.tr(LC.en, LC.sv) == "Text avsedd för översättning till SPRÅK");
}

@safe version(nxt_integration_test) {
	private static alias tr = translateViaGoogle;
	private static alias LC = LanguageCode;
}
