/** Visual Attributes. */
module nxt.visual;

public import nxt.ansi_escape : Attrs, ColorRGB8;

@safe:

/++ Visual Color Theme.
 +  TODO: Auto-extract from Emacs.
 +
 + TODO:
 + // static if (S == "URI")
 + // 	sw(w, arg, w); /+ TODO: SGR.yellowForegroundColor +/
 + // else static if (S == "URL")
 + // 	sw(w, arg, w); /+ TODO: SGR.yellowForegroundColor +/
 + // else static if (S == "Path")
 + // 	sw(w, arg, w); /+ TODO: SGR.whiteForegroundColor +/
 + // else static if (S == "FilePath")
 + // 	sw(w, arg, w); /+ TODO: SGR.whiteForegroundColor +/
 + // else static if (S == "DirPath")
 + // 	sw(w, arg, w); /+ TODO: SGR.cyanForegroundColor +/
 + // else static if (S == "ExeFilePath")
 + // 	sw(w, arg, w); /+ TODO: SGR.lightRedForegroundColor +/
 + // else static if (S == "FileName")
 + // 	sw(w, arg, w); /+ TODO: SGR.whiteForegroundColor +/
 + // else static if (S == "DirName")
 + // 	sw(w, arg, w); /+ TODO: SGR.redForegroundColor +/
 + // else static if (S == "ExitStatus")
 + // 	sw(w, arg, w); /+ TODO: SGR.greenForegroundColor if arg == 0, otherwise SGR.redForegroundColor +/
 +/
@safe struct ColorTheme {
	alias A = Attrs;
	alias C = ColorRGB8;
	A typeName = A(foregroundColor: C(0,0,255)); /++ Type name. +/
	A variableName = A(foregroundColor: C(255,255,0)); /++ Variable name. +/
	A memoryAddress = A(foregroundColor: C(128,128,128)); /++ Memory Address. +/
	A numericValue = A(foregroundColor: C(255,128,0)); /++ Numeric Value. +/
	A filePath = A(foregroundColor: C(255,255,255)); /++ File path. +/
	A dirPath = A(foregroundColor: C(255,255,255)); /++ Directory path. +/
}
