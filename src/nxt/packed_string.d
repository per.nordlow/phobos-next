module nxt.packed_string;

/++ String packed into one (raw) machine word.
 +
 + Length is stored in upper `lengthBits` of pointer/word `_raw`.
 +
 + All length bits set means memory bits of `_raw` points to `string`. This
 + makes `PackedString` `nothrow`.
 +
 + TODO: Propose API for specifying this is: `__traits(adressBitMask, declaration,
 + mask)` where `declaration.sizeof == size_t.sizeof`.
 +/
@safe struct PackedString {
private:
	enum totalBits = 8 * _raw.sizeof;

	/++ Number of bits used for properties.
		Currently null-termination is the only attribute.
	 +/
	enum attributeBits = 1;

	enum nullTerminatorBit = totalBits - 1;

	enum nullTerminatorMask = (1UL << nullTerminatorBit);

	/++ Number of bits used for length. +/
	enum lengthBits = 15;

	/++ Number of bits used for memory address.
		2024-01-01: Address space of a single Linux user process is 47 bits.
	 +/
	enum addressBits = 48;

	/// Bit mask of length part.
	enum lengthMask = ((1UL << lengthBits) - 1) << addressBits;

	/// Bit mask of address part.
	enum addressMask = (1UL << addressBits) - 1;

	/// Capacity of small variant where `length` fits in `lengthBits`.
	enum smallCapacity = (1UL << lengthBits) - 1;

public const pure nothrow @nogc:

	this(string x, in bool nullTerminatorFlag = false) @trusted
	in(!((cast(size_t)x.ptr) & ~addressMask), _faultyAddressMessage) {
		const nullBits = nullTerminatorFlag ? nullTerminatorMask : 0;
		if (x.length <= smallCapacity)
			_raw = cast(size_t)(x.ptr) | (x.length << addressBits) | nullBits;
		else
			assert(0, "TODO: Allocate big string at memory pointed by `_ptr`");
	}

	this(in char[] x, in bool nullTerminatorFlag = false) @trusted
	in(!((cast(size_t)x.ptr) & ~addressMask), _faultyAddressMessage) {
		assert(0, "TODO: Duplicate memory at `x`.");
	}

@property:

	/++ Check if `this` is equal to `rhs`. +/
	bool opEquals(in typeof(this) rhs) const scope @trusted	{
		version(D_Coverage) {} else pragma(inline, true);
		return opSlice == rhs.opSlice;
	}

	/++ Check if `this` is equal to `rhs`. +/
	bool opEquals(in char[] rhs) const scope @trusted	{
		version(D_Coverage) {} else pragma(inline, true);
		return opSlice == rhs;
	}

	/++ Returns: `true` iff this is a large string, otherwise `false.` +/
	bool isLarge() scope @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		return (_raw & lengthMask) == (2^^lengthBits) - 1;
	}

	/++ Returns: `true` iff `this.ptr` points to a null-terminated C-style
		string where `this.length` excludes the null terminator.
	 +/
	bool isNullTerminated() scope @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		return (_raw & nullTerminatorMask) != 0;
	}

	/// Get pointer to characters.
	inout(char)* ptr() inout @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		return cast(typeof(return))(_raw & addressMask);
	}

	/// Get length.
	size_t length() scope @safe {
		version(D_Coverage) {} else pragma(inline, true);
		return (_raw & lengthMask) >> addressBits;
	}

	/// Get slice.
	inout(char)[] opSlice() inout return scope @trusted {
		version(D_Coverage) {} else pragma(inline, true);
		return ptr[0 .. length];
	}

	void toString(Sink)(scope ref Sink sink) scope => sink(opSlice());
	alias opSlice this;

private:
	size_t _raw;
	static immutable _faultyAddressMessage =
		"Address uses more than 48 least significant bits!!!";
}

///
pure @safe version(nxt_test) unittest {
	const s = "alpha";

	const x = PS(s, false);
	assert(x.length == s.length);
	assert(!x.isNullTerminated);

	const y = PS(s, true);
	assert(y.length == s.length);
	assert(y.isNullTerminated);

	assert(x == y);
}

///
pure @safe version(nxt_test) unittest {
	const s = "alpha";
	const p = PS(s);
	assert(p.ptr == s.ptr);
	assert(p.length == s.length);
	assert(p[] == s);
	assert(p == s);
	assert(!p.isNullTerminated);
	assert(!p.isLarge);
}

///
pure @safe version(nxt_test) unittest {
	string s;
	s.length = PS.smallCapacity;
	const p = PS(s);
	assert(p.ptr == s.ptr);
	assert(p.length == s.length);
	assert(p[] == s);
	assert(p == s);
	assert(p is s);
	assert(!p.isNullTerminated);
	assert(!p.isLarge);
}

version(nxt_test) version(unittest) {
	alias PS = PackedString;
	static assert(PS.attributeBits + PS.lengthBits + PS.addressBits == PS.totalBits);
	static assert(PS.sizeof == size_t.sizeof);
	static assert(PS.totalBits == 64);
	static assert(PS.addressBits == 48);
	static assert(PS.smallCapacity == 32_767);
	static assert(PS.lengthMask == 0x7fff_0000_0000_0000);
	static assert(PS.addressMask == 0x0000_ffff_ffff_ffff);
}
