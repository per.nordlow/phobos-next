/++ User-Defined Effects.

	Mathematical total function (Koka's `total`) is already present in D via the
	`pure` qualifier.

	See_Also: http://dpldocs.info/this-week-in-d/Blog.Posted_2022_08_15.html
	See_Also: https://koka-lang.github.io/koka/doc/book.html#sec-semantics-of-effects
	See_Also: https://koka-lang.github.io/koka/doc/book.html#why-effects

	TODO: Expression relationships between lifetimes of aggregate types and their instances.
 +/
module nxt.effects;

@safe:

/++ Set on a function method `m` of `T` to indicate that a call to `m` doesn't
	invalidate any internal state of instances of `T`. +/
enum stable;

/++ Expands to `pragma(inline)` by D preprocessor. +/
enum pin;

/++ Expands to `pragma(inline, true)` by D preprocessor. +/
enum pit;

/++ Expands to `pragma(inline, false)` by D preprocessor. +/
enum pif;

/++ UDA of (member) function whose returned value is (internally) cached in
	RAM.

	Cached return value is stored in field named `fieldName_`.

	TODO: Can we realize caching and `in bool reload` automatically using a
    mixin generating the wrapper function automatically?
 +/
enum caches_return_value;
alias cached = caches_return_value;

/++ UDA of (member) function whose returned value is (internally) cached in
	RAM.

	Cached return value is stored in field named `fieldName_`.

	See_Also: `caches_return_value`
 +/
struct caches_return_value_in_field_named(string fieldName_) {
	enum fieldName = fieldName_;
}
alias cached_in = caches_return_value_in_field_named;

/++ UDA of (member) function whose returned value is (externally) cached
	(to disk).
 +/
enum caches_return_value_to_disk;

/++ UDA of function that writes to `stdout` or `stderr`.
	In Koka this is `console`.
 +/
enum writes_to_console;
alias console = writes_to_console; // Koka-compliant alias.

/++ UDA of function that opens as file.
 +/
enum opens_file;

/++ UDA of function that closes as file.
 +/
enum closes_file;

/++ UDA of function that writes filesystem.
 +/
enum reads_from_file;

/++ UDA of function that writes filesystem.
 +/
enum writes_to_file;

/++ UDA of function that reads from process environment.
 +/
enum reads_from_environment;

/++ UDA of function that writes to process environment.
 +/
enum writes_to_environment;

/++ UDA of function that (dynamically) allocates memory either with
	GC or `malloc()`. +/
enum allocates_on_heap;

/++ UDA of function that allocates memory with the built-in GC. +/
enum allocates_on_gc_heap;

/++ UDA of function that has non-deterministic behaviour.
	Typically reads an extern resource such as the system clock.
	In Koka this is `ndet`.
	+/
enum non_deterministic;
private alias ndet = non_deterministic;	// Koka-compliant alias.

/++ UDA of function that may throw an `Exception|Error`.
	In Koka this is `exn`.
	+/
enum throws;
private alias exn = throws;	// Koka-compliant alias.

/++ UDA of function that never terminates.
	In Koka this is `div`.
	+/
enum never_terminates;
private alias non_terminating = never_terminates;
private alias divergent = never_terminates;
private alias div = non_terminating; // Koka-compliant alias.
