module nxt.ada_defs;

/// Logical Operators
static immutable operatorsLogical = ["and", "or", "xor"];

/// Relational Operators
static immutable operatorsRelational = ["/=", "=", "<", "<=", ">", ">="];

/// Binary Adding Operators
static immutable operatorsBinaryAdding = ["+", "-", "&"];

/// Unary Adding Operators
static immutable operatorsUnaryAdding = ["+", "-"];

/// Multiplying Operators
static immutable operatorsMultiplying = ["*", "/", "mod", "rem"];

/// Parens
static immutable operatorsParens = ["(", ")", "[", "]", "{", "}"];

/// Assignment
static immutable operatorsAssignment = [":="];

/// Other Operators
static immutable operatorsOther = ["**", "not", "abs", "in",
					   ".", ",", ";", "..",
					   "<>",
					   "<<",
					   ">>"];

/// Operators
static immutable operators = (operatorsLogical
				  ~ operatorsRelational
				  ~ operatorsBinaryAdding
				  ~ operatorsUnaryAdding
				  ~ operatorsMultiplying
				  ~ operatorsParens
				  ~ operatorsAssignment
				  ~ operatorsOther
	);

/// Kewords Ada 83
static immutable keywords83 = [ "abort", "else", "new", "return", "abs", "elsif", "not", "reverse",
					"end", "null", "accept", "entry", "select", "access", "exception", "of", "separate",
					"exit", "or", "subtype", "all", "others", "and", "for", "out", "array",
					"function", "task", "at", "package", "terminate", "generic", "pragma", "then", "begin", "goto", "private",
					"type", "body", "procedure", "if", "case", "in", "use", "constant", "is", "raise",
					"range", "when", "declare", "limited", "record", "while", "delay", "loop", "rem", "with", "delta", "renames",
					"digits", "mod", "xor", "do", ];

/// New Kewords in Ada 95
static immutable keywordsNew95 = ["abstract", "aliased", "tagged", "protected", "until", "requeue"];

/// New Kewords in Ada 2005
static immutable keywordsNew2005 = ["synchronized", "overriding", "interface"];

/// New Kewords in Ada 2012
static immutable keywordsNew2012 = ["some"];

/// Kewords in Ada 2012
static immutable keywords2012 = (keywords83 ~ keywordsNew95 ~ keywordsNew2005 ~ keywordsNew2012);
