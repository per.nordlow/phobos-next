/++ URIs.
 +/
module nxt.uri;

import nxt.path : FileName, FilePath, DirName, DirPath;

@safe:

/++ URI.
	See_Also: https://en.wikipedia.org/wiki/URI
 +/
struct URI {
    string str;
pure nothrow @nogc const:
    bool opCast(T : bool)() scope => str !is null;
const @property:
    string toString() => str;
    /+ Components: +/
    /++ Returns: scheme component if present otherwise `[]` +/
    string scheme() {
		import nxt.algorithm.searching : indexOf;
		version(D_Coverage) {} else pragma(inline, true);
        const ix = str.indexOf(':');
        return ix >= 0 ? str[0 .. ix] : [];
    }
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(URI("www.sunet.se").toString == "www.sunet.se");
	assert(URI("https://www.sunet.se").scheme == "https");
	assert(URI("www.sunet.se").scheme is []);
    assert(URI("https://example.com/path?query=value#fragment").scheme() == "https");
}

/++ URN.
	See_Also: https://en.wikipedia.org/wiki/URN
 +/
struct URN {
	URI uri;
pure nothrow @nogc const @property:
	bool opCast(T : bool)() scope => cast(bool)uri;
	string toString() => uri.str;
}

/++ File URI having the prefix `file://`.
 +/
struct FileURI {
	URI uri;
pure nothrow @nogc:
	this(string str) { this.uri = URI(str); }
	this(FilePath path) { this.uri = URI(path.str); }
const @property:
	bool opCast(T : bool)() scope => uri.str !is null;
	string toString() => uri.str;
	FilePath toPath() {
		import nxt.algorithm.searching : skipOverFront;
		string str_ = uri.str;
		if (str_.skipOverFront("file://")) {
			// TODO: warn?
		}
		return typeof(return)(str_);
	}
}

///
pure nothrow @safe version(nxt_test) unittest {
	const path = FilePath("/etc/passwd");
	const str = "file://" ~ path.str;
	const uri = FileURI(str);
	assert(uri.toPath == path);
	assert(uri.toString == str);
}

/++ Directory URI.
 +/
struct DirURI {
	URI uri;
pure nothrow @nogc:
	this(string str) { this.uri = URI(str); }
	this(DirPath path) { this.uri = URI(path.str); }
const @property:
	bool opCast(T : bool)() scope => uri.str !is null;
	string toString() => uri.str;
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(DirURI("www.sunet.se").toString == "www.sunet.se");
	assert(DirURI(DirPath("/etc/", false)).toString == "/etc/");
}

///
version(Posix) nothrow @safe version(nxt_test) unittest {
	const dmd = "https://github.com/dlang/dmd/";
	assert(URI(dmd).baseName.str == "dmd");
	assert(FileURI(dmd).baseName.str == "dmd");
	assert(DirURI(dmd).baseName.str == "dmd");
}

///
version(Posix) nothrow @safe version(nxt_test) unittest {
	assert(!DirURI("/etc/").extension);
	assert(!FileURI("/etc/passwd").extension);
}

///
version(Posix) nothrow @safe version(nxt_test) unittest {
	assert( DirURI("/etc/").exists);
	assert( FileURI("/etc/passwd").exists);
}

/++ File URI and Offset (in bytes).
 +/
struct FileURIOffset {
	import nxt.offset : Offset;
	FileURI uri;
	Offset offset;
}

/++ File URI and Region (in bytes).
 +/
struct FileURIRegion {
	import nxt.region : Region;
	FileURI uri;
	Region region;
}

/// Get basename of `a`.
DirName baseName(URI a) pure nothrow @nogc => typeof(return)(std_baseName(a.str));
/// ditto
FileName baseName(FileURI a) pure nothrow @nogc => typeof(return)(std_baseName(a.uri.str));
/// ditto
DirName baseName(DirURI a) pure nothrow @nogc => typeof(return)(std_baseName(a.uri.str));

/// Check if `a` exists.
bool exists(in FileURI a) nothrow @nogc => typeof(return)(std_exists(a.uri.str));
/// ditto
bool exists(in DirURI a) nothrow @nogc => typeof(return)(std_exists(a.uri.str));

/// Returns: extension of `a` if any, otherwise `[]`.
string extension(FileURI a) pure nothrow @nogc => std_extension(a.uri.str);
/// ditto
string extension(DirURI a) pure nothrow @nogc => std_extension(a.uri.str);

private import std.path : std_baseName = baseName;
private import std.file : std_exists = exists;
private import std.path : std_extension = extension;
