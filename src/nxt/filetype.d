/** Common filetypes.
	Copyright: Per Nordlöw 2024-.
	License: $(WEB boost.org/LICENSE_1_0.txt, Boost License 1.0).
	Authors: $(WEB Per Nordlöw)
*/
module nxt.filetype;

import nxt.srx : PNode = Node;
import nxt.path : FileName, FilePath;
import nxt.file : DataFormat;

@safe:

/++ Type of File (on disk).
 +/
struct FileType {
	/++ Type of matching to perform. +/
	enum MatchStrategy : ubyte {
		nameOnly, ///< Match name only. Default.
		contentOnly, ///< Match name only.
	}
	alias MatchRanking = uint; // TODO: Use
	DataFormat format; ///< Format associated with file type.
	const PNode fileName; ///< Matches commonly used file name(s).
	///< Set matcher for commonly used file name(s).
	void setFileName(const(PNode) fileName) @trusted { // tail-const is missing
		auto fileNameMut = cast(PNode*)&this.fileName;
		*fileNameMut = cast(PNode)fileName;
	}
}

/++ Returns: pattern matching a file name ending with '.' ~ `s`. +/
const(PNode) fileExtension(const PNode s) pure nothrow {
	import nxt.srx : seq, lit, eob;
	return seq([lit('.'), s, eob()]);
}

typeof(FileName.str) matchFirst(FileName fname, const FileType ftype) nothrow /+@nogc+/ {
	version(D_Coverage) {} else pragma(inline, true);
	return matchFirst(FilePath(fname.str), ftype, FileType.MatchStrategy.nameOnly);
}

/// ditto
@safe nothrow version(nxt_test) unittest {
	import nxt.srx : lit;
	auto app_d = FileName("app.d");
	const ftypeD = FileType(DataFormat("D"), fileName: fileExtension(lit("d")));
	const ftypeC = FileType(DataFormat("C"), fileName: fileExtension(lit("c")));
	assert( app_d.matchFirst(ftypeD));
	assert(!app_d.matchFirst(ftypeC));
	auto app = FileName("app");
	assert(!app.matchFirst(ftypeD));
	assert(!app.matchFirst(ftypeC));
	auto app_c = FileName("app.c");
	assert(!app_c.matchFirst(ftypeD));
	assert( app_c.matchFirst(ftypeC));
}

typeof(FilePath.str) matchFirst(FilePath path, const FileType ftype, in FileType.MatchStrategy strategy = FileType.MatchStrategy.nameOnly) nothrow @trusted /+@nogc+/ {
	import nxt.srx : matchFirst;
	final switch (strategy) {
	case FileType.MatchStrategy.nameOnly:
		import nxt.path : baseName;
		return path.baseName.str.matchFirst(ftype.fileName);
	case FileType.MatchStrategy.contentOnly: {
		try {
			// import nxt.dbgio : dbg;
			// dbg("MMAPPING ", path.str);
			import std.mmfile : MmFile;
			scope mmf = new MmFile(path.str);
			const scope raw = cast(const(ubyte)[]) mmf[];
			if (raw is null)
				return typeof(return).init;
			const hit = raw.matchFirst(ftype.format.content);
			return (hit !is null) ? path.str : typeof(return).init;
		} catch (Exception _) {
			// ignore
		}
		return typeof(return).init;
	}
	}
}

typeof(FilePath.str) matchFirstInEitherOf(FilePath path, const FileType[] ftypes, in FileType.MatchStrategy match = FileType.MatchStrategy.init) nothrow /+@nogc+/ {
	foreach (const ref ftype; ftypes)
		if (auto hit = path.matchFirst(ftype, match))
			return hit;
	return typeof(return).init;
}

/++ Get the ELF file type.
	See_Also: https://en.wikipedia.org/wiki/Executable_and_Linkable_Format
 +/
FileType newELFType() pure nothrow {
	import nxt.srx : seq, bob, lit, alt;
	alias fext = fileExtension;
	const magic = "\x7FELF";
	assert(magic.length == 4);
	return typeof(return)(DataFormat("ELF", seq([bob, lit(magic)])),
						  fext(alt([lit("axf"), lit("bin"), lit("elf"), lit("o"),
									lit("out"), lit("prx"), lit("puff"), lit("ko"),
									lit("mod"), lit("so")])));
}

@safe nothrow version(nxt_test) version(Posix) unittest {
	const ftype = newELFType();
	const ftypes = [ftype];

	const ao = FilePath("a.o");
	assert(ao.matchFirst(ftype, FileType.MatchStrategy.nameOnly));
	assert(ao.matchFirstInEitherOf(ftypes, FileType.MatchStrategy.nameOnly));
	// TODO: assert(!ao.matchFirstInEitherOf(ftypes, FileType.MatchStrategy.contentOnly));

	const pw = FilePath("/etc/passwd");
	assert(!pw.matchFirstInEitherOf(ftypes, FileType.MatchStrategy.nameOnly));
	assert(!pw.matchFirstInEitherOf(ftypes, FileType.MatchStrategy.contentOnly));

	const ls = FilePath("/bin/ls");
	assert(!ls.matchFirst(ftype, FileType.MatchStrategy.nameOnly));
	// TODO: assert( ls.matchFirst(ftype, FileType.MatchStrategy.contentOnly));
	assert(!ls.matchFirstInEitherOf(ftypes, FileType.MatchStrategy.nameOnly));
	// TODO: assert( ls.matchFirstInEitherOf(ftypes, FileType.MatchStrategy.contentOnly));
}

version(nxt_test) version(unittest) {
import nxt.path : FilePath;
}
