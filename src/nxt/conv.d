/++ Conversion that use result types instead of exception handling.
 +/
module nxt.conv;

import std.ascii : isHexDigit;
import nxt.result : Result;

@safe:

/++ Parse hexadecimal ASCII character in `ch`.
	Uses `& 0xf` rather than `cast(typeof(return))`.
	See_Also: https://forum.dlang.org/post/crtodvgdjzjkmjvzlozm@forum.dlang.org.
 +/
ubyte parseHexDigit(in dchar ch) pure nothrow @nogc in(ch.isHexDigit) {
	switch (ch) {
	case '0': .. case '9':
		return (ch - '0') & 0xf;
	case 'a': .. case 'f':
		return (ch - 'a' + 10) & 0xf;
	case 'A': .. case 'F':
		return (ch - 'A' + 10) & 0xf;
	default:
		assert(0, "Non-hexadecimal character");
	}
}

///
@safe pure version(nxt_test) unittest {
	assert('0'.parseHexDigit == 0);
	assert('9'.parseHexDigit == 9);
	assert('a'.parseHexDigit == 10);
	assert('A'.parseHexDigit == 10);
	assert('f'.parseHexDigit == 15);
	assert('F'.parseHexDigit == 15);
}

/++ Try to parse `s` as a `T`. +/
Result!(T) tryParse(T)(scope const(char)[] s, in bool disallowSeparators = false) pure nothrow @nogc
if (__traits(isArithmetic, T) && __traits(isIntegral, T)) {
	alias R = typeof(return);

	// accumulator type
	static if (__traits(isUnsigned, T))
		alias A = ulong;
	else
		alias A = long;

	if (!s.length) // empty
		return R.invalid;

	// strip optional leading {plus|minus} sign
	bool minus;
	if (s.length) {
		if (s[0] == '-')
			minus = true;
		if (s[0] == '+' || s[0] == '-')
			s = s[1 .. $];
	}

	// accumulate
	A curr = 0; // current accumulated value
	bool started;
    foreach (const c; s) {
		if (c == '_') {
	        if (!started || disallowSeparators)
				return R.invalid;
			else
				continue;
		}
        if (c < '0' || c > '9') // non-digit
			return R.invalid;
        A prev = curr;
        curr = 10 * curr + (c - '0'); // accumulate
        if (curr < prev) // overflow
			return R.invalid;
		started = true;
    }

	// range check
	assert(T.min <= curr);
	assert(curr <= T.max);

    return R(cast(T)curr);
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	foreach (T; IntegralTypes) {
		assert(!"".tryParse!T.isValue); // empty
		assert(!"_".tryParse!T.isValue); // non-digit
		assert(!"!".tryParse!T.isValue); // non-digit
		assert(*"+0".tryParse!T == 0);
		assert(!*"+0".tryParse!T);
		assert(*"+1".tryParse!T == 1);
		assert(*"-0".tryParse!T == 0);
		assert(*"0".tryParse!T == 0);
		assert(*"1".tryParse!T == 1);
		assert(*"2".tryParse!T == 2);
		assert(*"10".tryParse!T == 10);
		assert(*"11".tryParse!T == 11);
		assert(*"0_".tryParse!T == 0);
		assert(*"127_".tryParse!T == 127);
	}
	// unsigned min
	foreach (T; UnsignedTypes) {
		assert(*"0".tryParse!T == T.min);
		assert(*"+0".tryParse!T == T.min);
	}
	// unsigned max
	assert(*"255".tryParse!ubyte == 255);
	assert(*"65535".tryParse!ushort == 65_535);
	assert(*"4294967295".tryParse!uint == 4_294_967_295);
	assert(*"18446744073709551615".tryParse!ulong == ulong.max);
	// signed max
	assert(*"9223372036854775807".tryParse!long == long.max);
	// overflow
	assert(!"18446744073709551616".tryParse!ulong);
}

version(nxt_test) version(unittest) {
	import std.meta : AliasSeq;
	alias UnsignedTypes = AliasSeq!(ubyte, ushort, uint, ulong);
	alias SignedTypes = AliasSeq!(byte, short, int, long);
	alias IntegralTypes = AliasSeq!(ubyte, ushort, uint, ulong, byte, short, int, long);
}
