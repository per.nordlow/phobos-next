/++ File system path and name types and their operations.

	See: https://en.cppreference.com/w/cpp/filesystem/path
	See: https://hackage.haskell.org/package/FileSystem-1.0.0/docs/System-FileSystem-Types.html
	See: https://docs.python.org/3/library/pathlib.html
	See_Also: `dmd.root.filename`
 +/
module nxt.path;

import nxt.algorithm.searching : startsWith, endsWith, canFind, indexOf, skipOverFront;
import nxt.algorithm.mutation : stripRight;

@safe:

/++ Path.

	The concept of a "pure path" doesn't need to be modelled in D as
	it has `pure` functions.  See
	https://docs.python.org/3/library/pathlib.html#pure-paths.

	See: SUMO:`ComputerPath`.
 +/
struct Path {
	string str;
pure nothrow @nogc:
	this(string str, in bool normalize = true) {
		if (normalize) {
			import std.path : dirSeparator;
			enum doubleSep = dirSeparator ~ dirSeparator;
			while (str.startsWith(doubleSep))
				str = str[dirSeparator.length .. $];
			str = str.stripRight(dirSeparator);
		}
		this.str = str;
	}
const @property:
	bool opCast(T : bool)() scope => str !is null;
	string toString() => str;
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(Path("/etc/", false).str == "/etc/");
	assert(Path("/etc/", true).str == "/etc");
	assert(Path("foo", true).str == "foo");
	assert(Path("//usr/bin/", true) == Path("/usr/bin"));
	assert(Path("/usr/bin/", false).toString == "/usr/bin/");
	assert(Path("//usr/bin/", false).toString == "//usr/bin/");
	assert(Path("/usr/bin/").toString == "/usr/bin");
	assert(Path("//usr/bin/").toString == "/usr/bin");
}

/++ (Regular) file path (on local file system).
	See: https://hackage.haskell.org/package/filepath-1.5.0.0/docs/System-FilePath.html#t:FilePath
 +/
struct FilePath {
	Path path;
	alias path this;
pure nothrow @nogc:
	this(string str, in bool normalize = true)
	in(!str.endsWith('/')) {
		this.path = Path(str, normalize);
	}
const @property:
	AbsFilePath asAbsolute() => typeof(return)(path.str);
	alias asAbs = asAbsolute;
	RelFilePath asRelative() => typeof(return)(path.str);
	alias asRel = asRelative;
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(FilePath("foo") == Path("foo"));
	assert(FilePath("foo", false).str == "foo");
	assert(FilePath("foo", true).str == "foo");
	assert(FilePath("/bin").asAbsolute == FilePath("/bin").asAbsolute);
	assert(FilePath("bin").asRelative == FilePath("bin").asRelative);
}

/++ Absolute (regular) file path (on local file system). +/
struct AbsFilePath {
	FilePath filePath;
pure nothrow @nogc:
	this(string str, in bool normalize = true)
	in(str.startsWith('/')) {
		this.filePath = FilePath(str, normalize);
	}
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(AbsFilePath("//etc/passwd") == AbsFilePath("/etc/passwd"));
}

/++ Relative (regular) file path (on local file system). +/
struct RelFilePath {
	FilePath filePath;
pure nothrow @nogc:
	this(string str, in bool normalize = true)
	in(!str.startsWith('/')) {
		this.filePath = FilePath(str, normalize);
	}
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(RelFilePath("etc/passwd") == RelFilePath("etc/passwd"));
}

/++ Directory path (on local file system).
	See: SUMO:`ComputerDirectory`.
 +/
struct DirPath {
	Path path;
	alias path this;
pure nothrow @nogc:
	this(string path, in bool normalize = true) {
		this.path = Path(path, normalize);
	}
const @property:
	AbsDirPath asAbsolute() => typeof(return)(path.str);
	alias asAbs = asAbsolute;
	RelDirPath asRelative() => typeof(return)(path.str);
	alias asRel = asRelative;
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(DirPath("/etc") == Path("/etc"));
	assert(DirPath("/etc") == DirPath("/etc"));
	assert(DirPath("/etc/", false).str == "/etc/");
	assert(DirPath("/etc/", true).str == "/etc");
	assert(DirPath("/etc").asAbsolute == DirPath("/etc/").asAbsolute);
	assert(DirPath("etc").asRelative == DirPath("etc/").asRelative);
}

/++ Absolute directory path (on local file system). +/
struct AbsDirPath {
	DirPath dirPath;
pure nothrow @nogc:
	this(string str, in bool normalize = true)
	in(str.startsWith('/')) {
		this.dirPath = DirPath(str, normalize);
	}
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(AbsDirPath("//etc") == AbsDirPath("/etc/"));
	assert(AbsDirPath("/etc/") == AbsDirPath("/etc/"));
}

/++ Relative directory path (on local file system). +/
struct RelDirPath {
	DirPath dirPath;
pure nothrow @nogc:
	this(string str, in bool normalize = true)
	in(!str.startsWith('/')) {
		this.dirPath = DirPath(str, normalize);
	}
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(RelDirPath("etc") == RelDirPath("etc/"));
}

/++ File name (either local or remote).
 +/
struct FileName {
	string str;
pure nothrow @nogc:
	this(string str) in(!str.canFind('/')) {
		this.str = str;
	}
const:
	bool opCast(T : bool)() scope => str !is null;
	string toString() @property => str;
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(FileName(".emacs").str == ".emacs");
	assert(FileName(".emacs").toString == ".emacs");
}

/++ Directory name (either local or remote).
 +/
struct DirName {
	string str;
pure nothrow @nogc:
	this(string str) in(!str.canFind('/')) {
		this.str = str;
	}
const @property:
	bool opCast(T : bool)() scope => str !is null;
	string toString() => str;
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(DirName(".emacs.d").str == ".emacs.d");
	assert(DirName(".emacs.d").toString == ".emacs.d");
}

/++ Executable file path (on local file system).
 +/
struct ExeFilePath {
	FilePath filePath;
	alias filePath this;
pure nothrow @nogc:
	this(string path) {
		this.filePath = FilePath(path);
	}
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(ExeFilePath("/bin/ls") == Path("/bin/ls"));
}

/++ Written (input) path (on local file system). +/
struct RdPath {
	Path path;
	alias path this;
pure nothrow @nogc:
	this(string str) {
		this.path = Path(str);
	}
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(RdPath("/usr/bin/") == Path("/usr/bin/"));
}

/++ Written (output) path (on local file system). +/
struct WrPath {
	Path path;
	alias path this;
pure nothrow @nogc:
	this(string str) {
		this.path = Path(str);
	}
}

///
pure nothrow @safe version(nxt_test) unittest {
	assert(WrPath("/usr/bin/") == Path("/usr/bin/"));
}

private import std.path : std_expandTilde = expandTilde;

/++ Expand tilde in `a`.
	TODO: remove `@trusted` when scope inference is works.
 +/
FilePath expandTilde(scope return FilePath a) nothrow @trusted => typeof(return)(std_expandTilde(a.path.str));
/// ditto
DirPath expandTilde(scope return DirPath a) nothrow @trusted => typeof(return)(std_expandTilde(a.path.str));

///
@safe nothrow version(nxt_test) unittest {
	assert(FilePath("~").expandTilde);
	assert(DirPath("~").expandTilde);
}

private import std.path : std_buildPath = buildPath;

/// Build path `a`/`b`.
FilePath buildPath(DirPath a, FilePath b) pure nothrow => typeof(return)(std_buildPath(a.path.str, b.path.str));
/// ditto
DirPath buildPath(DirPath a, DirPath b) pure nothrow => typeof(return)(std_buildPath(a.path.str, b.path.str));
/// ditto
DirPath buildPath(DirPath a, DirName b) pure nothrow => typeof(return)(std_buildPath(a.path.str, b.str));
/// ditto
FilePath buildPath(DirPath a, FileName b) pure nothrow => typeof(return)(std_buildPath(a.path.str, b.str));

///
pure nothrow @safe version(nxt_test) unittest {
	assert(DirPath("/etc").buildPath(FileName("foo")) == FilePath("/etc/foo"));
	assert(DirPath("/etc").buildPath(FilePath("foo")) == FilePath("/etc/foo"));
	assert(DirPath("/usr").buildPath(DirName("bin")) == DirPath("/usr/bin"));
	assert(DirPath("/usr").buildPath(DirPath("bin")) == DirPath("/usr/bin"));
	assert(DirPath("/usr").buildPath(DirPath("/bin")) == DirPath("/bin"));
}

private import std.path : std_buildNormalizedPath = buildNormalizedPath;

/// Build path `a`/`b`.
FilePath buildNormalizedPath(DirPath a, FilePath b) pure nothrow => typeof(return)(std_buildNormalizedPath(a.path.str, b.path.str));
/// ditto
DirPath buildNormalizedPath(DirPath a, DirPath b) pure nothrow => typeof(return)(std_buildNormalizedPath(a.path.str, b.path.str));
/// ditto
DirPath buildNormalizedPath(DirPath a, DirName b) pure nothrow => typeof(return)(std_buildNormalizedPath(a.path.str, b.str));
/// ditto
FilePath buildNormalizedPath(DirPath a, FileName b) pure nothrow => typeof(return)(std_buildNormalizedPath(a.path.str, b.str));

///
pure nothrow @safe version(nxt_test) unittest {
	assert(DirPath("/etc").buildNormalizedPath(FileName("foo")) == FilePath("/etc/foo"));
	assert(DirPath("/etc").buildNormalizedPath(FilePath("foo")) == FilePath("/etc/foo"));
	assert(DirPath("/usr").buildNormalizedPath(DirName("bin")) == DirPath("/usr/bin"));
	assert(DirPath("/usr").buildNormalizedPath(DirPath("bin")) == DirPath("/usr/bin"));
	assert(DirPath("/usr").buildNormalizedPath(DirPath("/bin")) == DirPath("/bin"));
}

/// Get basename of `a`.
FileName baseName(FilePath a) pure nothrow @nogc => typeof(return)(std_baseName(a.str));
/// ditto
DirName baseName(DirPath a) pure nothrow @nogc => typeof(return)(std_baseName(a.str));

///
version(Posix) nothrow @safe version(nxt_test) unittest {
	assert(FilePath("/etc/foo").baseName.str == "foo");
	assert(DirPath("/etc/").baseName.str == "etc");
	const dmd = "https://github.com/dlang/dmd/";
}

/// Check if `a` exists.
bool exists(in Path a) nothrow @nogc => typeof(return)(std_exists(a.str));
/// ditto
bool exists(in FilePath a) nothrow @nogc => typeof(return)(std_exists(a.str));
/// ditto
bool exists(in DirPath a) nothrow @nogc => typeof(return)(std_exists(a.str));
/// ditto
bool exists(in FileName a) nothrow @nogc => typeof(return)(std_exists(a.str));
/// ditto
bool exists(in DirName a) nothrow @nogc => typeof(return)(std_exists(a.str));

/// Returns: extension of `a` if any, otherwise `[]`.
string extension(Path a) pure nothrow @nogc => std_extension(a.str);
/// ditto
string extension(FilePath a) pure nothrow @nogc => std_extension(a.str);
/// ditto
string extension(DirPath a) pure nothrow @nogc => std_extension(a.str);
/// ditto
string extension(FileName a) pure nothrow @nogc => std_extension(a.str);
/// ditto
string extension(DirName a) pure nothrow @nogc => std_extension(a.str);

///
version(Posix) nothrow @safe version(nxt_test) unittest {
	assert(Path("/tmp/app.c").extension == ".c");
	assert(!Path("/etc/").extension);
	assert(!DirPath("/etc/").extension);
	assert(!DirPath("/etcxyz/").extension);
	assert(!FilePath("/etc/passwd").extension);
	assert(!FileName("dsfdsfdsfdsfdfdsf").extension);
	assert(!DirName("dsfdsfdsfdsfdfdsf").extension);
}

/// verify `a.toString` when `a` being `scope` parameter
version(none) // TODO: Remove or make compile
pure nothrow @safe @nogc version(nxt_test) unittest {
	import std.meta : AliasSeq;

	static foreach (T; AliasSeq!(Path, FilePath, DirPath, FileName, DirName)) {{
		static void f(in T a) { const _ = a.toString; }
		f(T.init);
	}}
}

///
version(Posix) nothrow @safe version(nxt_test) unittest {
	assert( Path("/etc/").exists);
	assert(!Path("/etcxyz/").exists);
	assert( DirPath("/etc/").exists);
	assert(!DirPath("/etcxyz/").exists);
	assert( FilePath("/etc/passwd").exists);
	assert(!FileName("dsfdsfdsfdsfdfdsf").exists);
	assert(!DirName("dsfdsfdsfdsfdfdsf").exists);
}

/++ Returns: `true` iff name is `a` backup. +/
bool isBackup(in FileName name) pure nothrow @nogc { // TODO: Extend
	version(D_Coverage) {} else pragma(inline, true);
	return name.str.endsWith('~');
}
/// ditto
bool isBackup(in FilePath path) pure nothrow @nogc { // TODO: Extend
	version(D_Coverage) {} else pragma(inline, true);
	return path.str.endsWith('~');
}

///
nothrow @safe version(nxt_test) unittest {
	assert(FileName("app.c~").isBackup);
	assert(FilePath("/tmp/app.c~").isBackup);
}

/++ Returns: path in canonicalized form. +/
auto asCanonicalPath(Path)(scope return Path path) {
    import std.path : asAbsolutePath, asNormalizedPath, expandTilde;
    return path.expandTilde.asAbsolutePath.asNormalizedPath;
}

private import std.path : std_baseName = baseName;
private import std.file : std_exists = exists;
private import std.path : std_extension = extension;
