/++ Command-Line-Interface (CLI) utilities.

	Expose CLI based on a root type T, where T is typically a struct, class or
	module. By default it creates an instance of an aggregate and expose members
	as sub-command. This handles D types, files, etc.

    Reflects on `nxt.path`, `string`, and other built-in types using
    `arg.to!Arg`.

    Constructor of `T` is also reflected as flags before sub-command.

	TODO: Use in MLParser: - `--requestedFormats=C,D` or `--fmts` because arg is
	a Fmt[] - `scan dirPath`

    Auto-gen of help also works if any arg in args is --help or -h.
 +/
module nxt.cli;

import nxt.effects;

/++ Match method to use when matching CLI sub-commands with member functions. +/
enum Match {
	exact,
	prefix,
	acronym,
}

struct Flags {
	import core.stdc.stdio : FILE, fopen, fclose;

	@disable this(this); // avoid potential aliasing of pointer members

	~this() @trusted scope nothrow @nogc @closes_file {
		if (outFile)
		 	fclose(outFile);
		if (errFile)
		 	fclose(errFile);
	}

	static Flags directToDevNull() @trusted nothrow @nogc @opens_file {
		typeof(return) res;
		res.outFile = fopen("/dev/null", "wb");
		res.errFile = fopen("/dev/null", "wb");
		return res;
	}

private:
	FILE* outFile; // defaults to `core.stdc.stdio.stdout` if not set
	FILE* errFile; // defaults to `core.stdc.stdio.stderr` if not set
}

/++ Evaluate `scmd` as a CLI-like sub-command calling a member of `agg` of type `T`.
	Typically called with the `args` passed to a `main` function.

	Returns: `true` iff the command `scmd` result in a call to `T`-member named `scmd[1]`.

	TODO: Support both
	- EXE scan("/tmp/")
	- EXE scan /tmp/
	where the former is inputted in bash as
      EXE 'scan("/tmp/")'
 +/
bool evalMemberCommand(T)(ref T agg, in string exe, in const(string)[] scmd, scope Flags flags = Flags.init) @trusted/+TODO: remove @trusted+/ @writes_to_file @writes_to_console
if (is(T == struct) || is(T == class)) {
	import core.internal.traits : Unqual;
	import std.traits : isCallable, Parameters, isSomeFunction;
	import nxt.path : FilePath, DirPath;
	import nxt.algorithm : canFind;
	import nxt.io : fwrite, fwriteln;
	import core.stdc.stdio : stdout, stderr;

	auto fO = flags.outFile ? flags.outFile : stdout;
	auto fE = flags.errFile ? flags.errFile : stderr;

	if (scmd.length == 0)
		return false;

	const showHelp = scmd.canFind("--help") || scmd.canFind("-h");

	if (showHelp) {
		fO.fwriteln("Usage: ", exe ? exe : "", " [SUB-COMMAND]");
		fO.fwriteln("Sub Commands:");
	}

	foreach (const mmbName; __traits(allMembers, T)) { /+ member name +/
		static immutable qmn = T.stringof ~ '.' ~ mmbName; /+ qualified +/
		alias mmb = __traits(getMember, agg, mmbName);
		static if (__traits(getVisibility, mmb) == "public" && isSomeFunction!(mmb)) { // TODO: perhaps include other visibilies later on
			static if (!is(mmb) /+ not a type +/ && !(mmbName == "opAssign" || (mmbName.length >= 2 && mmbName[0 .. 2] == "__"))) /+ non-generated members like `__ctor` +/ {
				alias params = Parameters!(mmb);
				if (showHelp) {
					fO.fwrite("  - ", mmbName, "(");
					foreach (const i, param; params) {
						if (i) fO.fwrite(", ");
						fO.fwrite(Unqual!(param).stringof);
					}
					fO.fwriteln(")");
				} else {
					auto sargs = scmd[1 .. $];
					switch (sargs.length) {
					case 0: // nullary
						static if (__traits(compiles, { mixin(`agg.`~mmbName~`();`); })) { /+ nullary function +/
							mixin(`agg.`~mmbName~`();`); // call
							return true;
						}
						break;
					case 1: // unary
						/+ TODO: If `mmbName` has overloads check if its first
                           argument is `FilePath` or `DirPath` and `sargs[0]` startsWith
                           `file:///` +/
						// __traits(compiles, ..) check is needed if mmbName takes optional parameters 1...
						static if (is(params[0] : const(FilePath)) && __traits(compiles, { mixin(`agg.`~mmbName~`(FilePath.init);`); })) {
							mixin(`agg.`~mmbName~`(FilePath(sargs[0]));`); // call
							return true;
						}
						static if (is(params[0] : const(DirPath)) && __traits(compiles, { mixin(`agg.`~mmbName~`(DirPath.init);`); })) {
							mixin(`agg.`~mmbName~`(DirPath(sargs[0]));`); // call
							return true;
						}
						break;
					default:
						fO.fwriteln("Cannot pass arguments to sub-command ", mmbName);
						break;
					}
				}
			}
		}
	}
	return false;
}

///
@safe @writes_to_file version(nxt_test) unittest {
	import nxt.path : DirPath;

	static struct S {
	pure nothrow @safe @nogc:
		@disable this(this);
		void f1() scope {
			f1Count += 1;
		}
		void f2(in int inc = 1) scope { // TODO: support f2:32
			f2Count += inc;
		}
		void scan(in DirPath path) {
			_path = path;
			_scanDone = true;
		}
		private uint f1Count;
		uint f2Count;
		DirPath _path;
		bool _scanDone;
	}
	S s;

	assert(!s.evalMemberCommand(null, ["--help"], Flags.directToDevNull));
	assert(!s.evalMemberCommand(null, ["-h"], Flags.directToDevNull));
	assert(!s.evalMemberCommand(null, [], Flags.directToDevNull));
	assert(s.evalMemberCommand(null, [""], Flags.directToDevNull));
	assert(s.evalMemberCommand(null, ["_"], Flags.directToDevNull));

	assert(s.f1Count == 2);
	s.evalMemberCommand(null, ["f1"]);
	assert(s.f1Count == 3);

	assert(s.f2Count == 0);
	s.evalMemberCommand(null, ["f2", "42"]);
	// assert(s.f2Count == 42);

	// TODO: assert(s._path == DirPath.init);
	// TODO: assert(!s._scanDone);
	// TODO: assert(s.evalMemberCommand(null, ["scan", "/tmp"]));
	// TODO: assert(s._path == DirPath("/tmp"));
	// TODO: assert(s._scanDone);
}

version(nxt_test) version(unittest) {
	import nxt.dbgio;
}
