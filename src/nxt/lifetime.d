/++ Variants of `move` and `moveEmplace` in `core.lifetime` that use `__rvalue`. +/
module nxt.lifetime;

@safe:

import nxt.builtin_traits : hasBuiltinRvalue;

static if (hasBuiltinRvalue) {
	import core.stdc.string : memcpy;

	/++ Variant of `core.lifetime.move(source)` that uses `__rvalue`.

		If `T` is a struct with a destructor or postblit defined, source is
        reset to its `.init` value after it is moved into target, otherwise it
        is left unchanged.
	 +/
	T move(T)(return scope ref T source) @trusted {
		static if (__traits(isStaticArray, T)) {
			typeof(return) destination = void;
			foreach (const i, ref e; source) // TODO: use single call to `moveEmplaceAll`
				// TODO: Replace with: new (destination[i]) typeof(T.init[i])(__rvalue(e)); // placement new
				moveEmplace(e, destination[i]);
			return destination;
		} else {
			typeof(return) destination = __rvalue(source); // runs move constructors if present
			static if (is(T == struct)) {
				static if ((__traits(hasMember, T, "__xdtor") || __traits(hasMember, T, "__fieldPostblit"))) {
					static immutable init = T.init;
					memcpy(&source, &init, T.sizeof);
				}
			}
			return destination;
		}
	}

	/++ Variant of `core.lifetime.move(source, destination)` that uses `__rvalue`.

		If `T` is a struct with a destructor or postblit defined, source is
        reset to its `.init` value after it is moved into target, otherwise it
        is left unchanged.
	 +/
	void move(T)(ref T source, ref T destination) @trusted {
		static if (__traits(isStaticArray, T)) {
			foreach (const i, ref e; source) // TODO: use single call to `moveAll`
				move(e, destination[i]);
		} else {
			destination = __rvalue(source); // runs move constructors if present
			static if (is(T == struct)) {
				static if ((__traits(hasMember, T, "__xdtor") || __traits(hasMember, T, "__fieldPostblit"))) {
					static immutable init = T.init;
					memcpy(&source, &init, T.sizeof);
				}
			}
		}

	}

	/++ Variant of `core.lifetime.moveEmplace(source, destination)` that uses `__rvalue`.

		If `T` is a struct with a destructor or postblit defined, source is
        reset to its `.init` value after it is moved into target, otherwise it
        is left unchanged.
	 +/
	void moveEmplace(T)(ref T source, ref T destination) @system {
		static if (__traits(isStaticArray, T)) {
			foreach (const i, ref e; source) // TODO: use single call to `moveEmplaceAll`
				moveEmplace(e, destination[i]);
		} else {
			memcpy(&destination, &source, T.sizeof); // TODO: Use `__rvalue` here?
			static if (is(T == struct)) {
				static if ((__traits(hasMember, T, "__xdtor") || __traits(hasMember, T, "__fieldPostblit"))) {
					static immutable init = T.init;
					memcpy(&source, &init, T.sizeof);
				}
			}
		}
	}
} else
	public import core.lifetime : move, moveEmplace;

/// unary move() of `S`
/+pure+/ nothrow @nogc @safe version(nxt_test) unittest {
	auto x = S(42);
	assert(x == S(42));
	_moveCounter = 0;
	const y = move(x);
	version(none) assert(_moveCounter == 1); // TODO: Enable
	assert(y == S(42));
	version(none) assert(x == S.init); // TODO: Enable
}
/// unary move() of `S2`
/+pure+/ nothrow @safe version(nxt_test) unittest {
	const S2 x0 = [S(42), S(42)];
	S2 x = [S(42), S(42)];
	assert(x == [S(42), S(42)]);
	_moveCounter = 0;
	const y = move(x);
	version(none) assert(_moveCounter == 2); // TODO: Enable
	assert(y == x0);
	version(none) assert(x == x.init); // TODO: Enable
}

/// binary move() of `S`
/+pure+/ nothrow @nogc @safe version(nxt_test) unittest {
	auto x = S(42);
	assert(x == S(42));
	S y;
	_moveCounter = 0;
	move(x, y);
	version(none) assert(_moveCounter == 1); // TODO: Enable
	assert(y == S(42));
	version(none) assert(x == x.init); // TODO: Enable
}
/// binary move() of `S2`
/+pure+/ nothrow @safe version(nxt_test) unittest {
	const S2 x0 = [S(42), S(42)];
	S2 x = [S(42), S(42)];
	assert(x == [S(42), S(42)]);
	S2 y;
	_moveCounter = 0;
	move(x, y);
	version(none) assert(_moveCounter == 2); // TODO: Enable
	assert(y == x0);
	version(none) assert(x == x.init); // TODO: Enable
}

/// moveEmplace() of `S`
/+pure+/ nothrow @trusted version(nxt_test) unittest {
	auto x = S(42);
	assert(x == S(42));
	S y;
	_moveCounter = 0;
	moveEmplace(x, y);
	version(none) assert(_moveCounter == 1); // TODO: Enable
	assert(y == S(42));
	version(none) assert(x == x.init); // TODO: Enable
}
/// moveEmplace()of `S2`
/+pure+/ nothrow @trusted version(nxt_test) unittest {
	const S2 x0 = [S(42), S(42)];
	S2 x = [S(42), S(42)];
	assert(x == [S(42), S(42)]);
	S2 y;
	_moveCounter = 0;
	moveEmplace(x, y);
	version(none) assert(_moveCounter == 2); // TODO: Enable
	assert(y == x0);
	version(none) assert(x == x.init); // TODO: Enable
}

///
pure nothrow @nogc @system unittest
{
    static struct Foo
    {
    pure nothrow @nogc:
        this(int* ptr) { _ptr = ptr; }
        ~this() { if (_ptr) ++*_ptr; }
        int* _ptr;
    }

    int val;
    Foo foo1 = void; // uninitialized
    auto foo2 = Foo(&val); // initialized
    assert(foo2._ptr is &val);

    // Using `move(foo2, foo1)` would have an undefined effect because it would destroy
    // the uninitialized foo1.
    // moveEmplace directly overwrites foo1 without destroying or initializing it first.
    moveEmplace(foo2, foo1);
    assert(foo1._ptr is &val);
    assert(foo2._ptr is null);
    assert(val == 0);
}

pure nothrow @nogc @system unittest
{
    static struct Foo
    {
    pure nothrow @nogc:
        this(int* ptr) { _ptr = ptr; }
        ~this() { if (_ptr) ++*_ptr; }
        int* _ptr;
    }

    int val;
    {
        Foo[1] foo1 = void; // uninitialized
        Foo[1] foo2 = [Foo(&val)];// initialized
        assert(foo2[0]._ptr is &val);

        // Using `move(foo2, foo1)` would have an undefined effect because it would destroy
        // the uninitialized foo1.
        // moveEmplace directly overwrites foo1 without destroying or initializing it first.
        moveEmplace(foo2, foo1);
        assert(foo1[0]._ptr is &val);
        assert(foo2[0]._ptr is null);
        assert(val == 0);
    }
    assert(val == 1);
}

// https://issues.dlang.org/show_bug.cgi?id=25552
version(none) // TODO: Enable
pure nothrow @system unittest
{
    int i;
    struct Nested
    {
    pure nothrow @nogc:
        char[1] arr; // char.init is not 0
        ~this() { ++i; }
    }

    {
        Nested[1] dst = void;
        Nested[1] src = [Nested(['a'])];

        moveEmplace(src, dst);
        assert(i == 0);
        assert(dst[0].arr == ['a']);
        assert(src[0].arr == [char.init]);
		// import nxt.io.dbg;
		// dbg(src[0].tupleof[$-1]);
		// dbg(dst[0].tupleof[$-1]);
        // TODO: assert(dst[0].tupleof[$-1] is src[0].tupleof[$-1]);
    }
    assert(i == 2);
}

version(nxt_test) version(unittest) {
	struct S {
	@safe nothrow @nogc:
		this(int x) {
			this.x = x;
		}
		this(typeof(this) rhs) {
			this.x = rhs.x;
			_moveCounter += 1;
		}
		private int x;
	}
	alias S2 = S[2];
	static private size_t _moveCounter;
}
