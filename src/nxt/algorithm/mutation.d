/++ Extensions and specializations to std.algorithm.mutation.`
 +/
module nxt.algorithm.mutation;

import nxt.algorithm.predicate : eq;

/** Array-specialization of `stripLeft`.
 *
 * See_Also: https://forum.dlang.org/post/dhxwgtaubzbmjaqjmnmq@forum.dlang.org
 */
inout(T)[] stripLeft(alias pred = eq, T)(scope return inout(T)[] haystack, scope const T needle) @trusted {
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	size_t offset = 0;
	while (offset != haystack.length &&
		   pred(haystack.ptr[offset], needle)) /+ TODO: elide range-check +/
		offset += 1;
	return haystack.ptr[offset .. haystack.length];
}
/// ditto
inout(char)[] stripLeft(alias pred = eq)(scope return inout(char)[] haystack) {
	return haystack.stripLeft!(pred)(' ');
}
/// ditto
alias stripPrefix = stripLeft;
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("beta".stripLeft(' ') == "beta");
	assert(" beta".stripLeft(' ') == "beta");
	assert("  beta".stripLeft(' ') == "beta");
	assert("   beta".stripLeft(' ') == "beta");
	assert("   beta".stripLeft() == "beta");
	assert(" _ beta _ ".stripLeft(' ') == "_ beta _ ");
	assert(" _  beta _ ".stripLeft(' ') == "_  beta _ ");
	version(nxt_test) version(unittest) {
		static char[] f()() pure nothrow @safe { char[1] x = "_"; return x[].stripLeft(' '); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}

/** Array-specialization of `stripRight`.
 *
 * See_Also: https://forum.dlang.org/post/dhxwgtaubzbmjaqjmnmq@forum.dlang.org
 */
inout(T)[] stripRight(alias pred = eq, T)(scope return inout(T)[] haystack, scope const T needle) @trusted {
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	size_t offset = haystack.length;
	while (offset != 0 &&
 		   pred(haystack.ptr[offset - 1], needle)) /+ TODO: elide range-check +/
		offset -= 1;
	return haystack.ptr[0 .. offset];
}
/// ditto
inout(T)[] stripRight(alias pred = eq, T)(scope return inout(T)[] haystack, in T[] needles) @trusted {
	import nxt.algorithm.searching : canFind;
	static if (is(T : const(char)))
		foreach (needle; needles)
			assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	size_t offset = haystack.length;
	while (offset != 0 &&
		   needles.canFind!(pred)(haystack.ptr[offset - 1])) /+ TODO: elide range-check +/
		offset -= 1;
	return haystack.ptr[0 .. offset];
}
/// ditto
inout(char)[] stripRight(alias pred = eq)(scope return inout(char)[] haystack) {
	return haystack.stripRight!(pred)([' ', '\t', '\r', '\n']); /+ TODO: `std.ascii.iswhite` instead +/
}
/// ditto
alias stripSuffix = stripRight;
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("beta".stripRight(' ') == "beta");
	assert("beta ".stripRight(' ') == "beta");
	assert("beta  ".stripRight(' ') == "beta");
	assert("beta	".stripRight('	') == "beta");
	assert("beta	".stripRight() == "beta");
	assert(" _ beta _ ".stripRight(' ') == " _ beta _");
	assert(" _  beta _ ".stripRight(' ') == " _  beta _");
	version(nxt_test) version(unittest) {
		static char[] f()() pure nothrow @safe { char[1] x = "_"; return x[].stripRight(' '); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}

/** Array-specialization of `strip`.
 *
 * See_Also: https://forum.dlang.org/post/dhxwgtaubzbmjaqjmnmq@forum.dlang.org
 */
inout(T)[] strip(alias pred = eq, T)(scope return inout(T)[] haystack, scope const T needle) @trusted {
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	size_t leftOffset = 0;
	while (leftOffset != haystack.length &&
		   pred(haystack.ptr[leftOffset], needle)) /+ TODO: elide range-check +/
		leftOffset += 1;
	size_t rightOffset = haystack.length;
	while (rightOffset != leftOffset &&
		   pred(haystack.ptr[rightOffset - 1], needle)) /+ TODO: elide range-check +/
		rightOffset -= 1;
	return haystack.ptr[leftOffset .. rightOffset];
}
/// ditto
inout(char)[] strip()(scope return inout(char)[] haystack) pure nothrow @safe @nogc /*tlm*/ {
	return haystack.strip(' ');
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("beta".strip(' ') == "beta");
	assert(" beta ".strip(' ') == "beta");
	assert("  beta  ".strip(' ') == "beta");
	assert("   beta   ".strip(' ') == "beta");
	assert(" _ beta _ ".strip(' ') == "_ beta _");
	assert(" _  beta _ ".strip(' ') == "_  beta _");
	version(nxt_test) version(unittest) {
		static char[] f()() pure nothrow @safe { char[1] x = "_"; return x[].strip(' '); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const ubyte[3] x = [0, 42, 0];
	assert(x.strip(0) == x[1 .. 2]);
}

version(nxt_test) version(unittest) {
	import nxt.dip_traits : hasPreviewDIP1000;
}
