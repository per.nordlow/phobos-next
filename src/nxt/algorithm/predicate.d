module nxt.algorithm.predicate;

/// Returns: `true` iff `a` and `b` are equal.
bool eq(T)(T a, T b) {
    version(D_Coverage) {} else pragma(inline, true);
	return a == b;
	version(none) {
		// TODO: Activate this or move to compiler: Find plain memory region of
		// plain old data to compare:
		enum isArray = is(T U : U[]);
		static if (!isArray && __traits(isPOD, T) && T.sizeof >= _sizeMin_memcmp) {
			import core.sys.linux.string : memcmp;
			return memcmp(&a, &b, T.sizeof) == 0;
		}
	}
}

/// Returns: `true` iff `a` and `b` are not equal.
bool ne(T)(T a, T b) {
    version(D_Coverage) {} else pragma(inline, true);
	return a != b;
}

/// Returns: `true` iff `a` and `b` are identical (the same).
bool id(T)(T a, T b) {
    version(D_Coverage) {} else pragma(inline, true);
	return a is b;
}

private enum _sizeMin_memcmp = 16;
