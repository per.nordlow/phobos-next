/** Longest Common Subsequence, typically used as a base for writing diff/compare algorithms.

	Copyright: Per Nordlöw 2014-.
	License: $(WEB boost.org/LICENSE_1_0.txt, Boost License 1.0).
	Authors: $(WEB Per Nordlöw)

	See_Also: https://en.wikipedia.org/wiki/Longest_common_subsequence_problem
	See_Also: https://en.wikipedia.org/wiki/Diff

	TODO: Make these work for (UTF-8 encoded) `string`s.
*/
module nxt.algorithm.comparison.lcs;

/** Longest Common Subsequence (LCS).
	See_Also: http://rosettacode.org/wiki/Longest_common_subsequence#Recursive_version

	Time Complexity: O(2^(m + n))

	Space Complexity: O(m + n) for the call stack, plus O(min(m, n)) for storing
    the LCS result, though the repeated concatenation may increase the effective
    memory usage.

	where `m` is `a.length` and `n` is `b.length`.
 */
T[] longestCommonSubsequenceR(T)(in T[] a, in T[] b) {
	if (a.length == 0 || b.length == 0)
		return [];			// undefined
	if (a[0] == b[0])
		return a[0] ~ longestCommonSubsequenceR(a[1 .. $], b[1 .. $]);
	return longestOf(longestCommonSubsequenceR(a, b[1 .. $]),
					 longestCommonSubsequenceR(a[1 .. $], b));
}

/** Longest Common Subsequence (LCS).

	Faster Dynamic Programming Version.

	See_Also: http://rosettacode.org/wiki/Longest_common_subsequence#Faster_dynamic_programming_version
	See_Also: https://github.com/skjha1/Data-Structure-Algorithm-Programs/blob/master/src/Algorithms/Dynamic%20Programming/18%20Print%20LCS.cpp
 */
T[] longestCommonSubsequenceDP(T)(in T[] a, in T[] b) {
	import std.algorithm.comparison : max;

	auto L = new size_t[][](a.length + 1, b.length + 1);

	foreach (const i; 0 .. a.length)
		foreach (const j; 0 .. b.length)
			L[i + 1][j + 1] = (a[i] == b[j]) ? (1 + L[i][j]) : max(L[i + 1][j], L[i][j + 1]);

	import core.internal.traits : Unqual;
	Unqual!T[] result;

	for (auto i = a.length, j = b.length; i > 0 && j > 0; ) {
		if (a[i - 1] == b[j - 1]) {
			result ~= a[i - 1];
			i--;
			j--;
		} else {
			if (L[i][j - 1] < L[i - 1][j])
				i--;
			else
				j--;
		}
	}

	import std.algorithm.mutation : reverse;
	result.reverse();		   // not nothrow
	return result; // TODO: indicate unique
}

/** Longest Common Subsequence (LCS) using Hirschberg.
	Linear-Space Faster Dynamic Programming Version.

	Time Complexity: O(m*n)
	Space Complexity: O(min(m,n))

	To speed up this code on DMD remove the memory allocations from $(D
	lcsLengths), and do not use the $(D retro) range (replace it with $(D
	foreach_reverse))

	See_Also: https://en.wikipedia.org/wiki/Hirschberg%27s_algorithm
	See_Also: http://rosettacode.org/wiki/Longest_common_subsequence#Hirschberg_algorithm_version

	TODO: This fails for certain combinations of UTF-sequences in `xs` and `ys`.
 */
const(T)[] longestCommonSubsequenceHirschberg(T)(in T[] xs, in T[] ys) {
	auto xsInLCS = new bool[xs.length];
	longestCommonSubsequenceHirschbergDo(xs, ys, xsInLCS);
	import std.range : zip;
	import std.algorithm.iteration : filter, map;
	import std.array : array;
	return zip(xs, xsInLCS).filter!q{ a[1] }.map!q{ a[0] }.array; // Not nothrow.
}
/// ditto
inout(char)[] longestCommonSubsequenceHirschberg(inout(char)[] a, inout(char)[] b) @trusted {
	import std.string : representation, assumeUTF;
	return cast(typeof(return))(longestCommonSubsequenceHirschberg(a.representation, b.representation).assumeUTF);
}

private void longestCommonSubsequenceHirschbergDo(T)(in T[] xs, in T[] ys, scope bool[] xsInLCS, in size_t index = 0) {
	const nx = xs.length;
	const ny = ys.length;

	if (nx == 0)
		return;

	if (nx == 1) {
		import nxt.algorithm.searching: canFind;
		if (ys.canFind(xs[0]))
			xsInLCS[index] = true;
	} else {
		const mid = nx / 2;
		const xb = xs[0.. mid];
		const xe = xs[mid .. $];
		const ll_b = lcsLengths(xb, ys);

		import std.range: retro;
		const ll_e = lcsLengths(xe.retro, ys.retro); // retro is slow with DMD

		// import std.algorithm.comparison : max;
		//const k = iota(ny + 1)
		//			  .reduce!(max!(j => ll_b[j] + ll_e[ny - j]));
		import std.range: iota;
		import std.algorithm: minPos;
		import std.typecons: tuple;
		const k = iota(ny + 1).minPos!((i, j) => tuple(ll_b[i] + ll_e[ny - i]) > tuple(ll_b[j] + ll_e[ny - j]))[0];

		longestCommonSubsequenceHirschbergDo(xb, ys[0 .. k], xsInLCS, index);
		longestCommonSubsequenceHirschbergDo(xe, ys[k .. $], xsInLCS, index + mid);
	}
}

/+pure nothrow @safe+/ version(nxt_test) unittest {
	const a = "thisisatest";
	const b = "testing123testing";
	const r = "tsitest";
	assert(r == longestCommonSubsequenceR(a, b));
	assert(r == longestCommonSubsequenceDP(a, b));
	assert(r == longestCommonSubsequenceHirschberg(a, b));
	assert("" == longestCommonSubsequenceHirschberg("", ""));
}

pure nothrow @safe version(nxt_test) unittest {
	const a = [1, 2, 3];
	const b = [4, 5, 6];
	const r = [];
	assert(r == longestCommonSubsequenceR(a, b));
	assert(r == longestCommonSubsequenceDP(a, b));
	assert(r == longestCommonSubsequenceHirschberg(a, b));
}

pure nothrow @safe version(nxt_test) unittest {
	const a = [1, 2, 3, 6];
	const b = [2, 3, 4, 5, 6];
	const r = [2, 3, 6];
	assert(r == longestCommonSubsequenceR(a, b));
	assert(r == longestCommonSubsequenceDP(a, b));
	assert(r == longestCommonSubsequenceHirschberg(a, b));
}

pure nothrow @safe version(nxt_test) unittest {
	alias T = int;
	const n = 300;
	auto a = new T[n];
	auto b = new T[n];
	assert(longestCommonSubsequenceHirschberg(a, b).length == n);
}

version(nxt_benchmark) version(nxt_test) unittest {
	import std.algorithm : levenshteinDistance, levenshteinDistanceAndPath;
	import nxt.random: randInPlaceBlockwise;

	alias T = int;
	const n = 10_000;
	auto a = new T[n];
	auto b = new T[n];

	a.randInPlaceBlockwise();
	b.randInPlaceBlockwise();

	void bLCS() { const d = lcsH(a, b); }
	void bLD() { const d = levenshteinDistance(a, b); }
	void bLDAP() { const dp = levenshteinDistanceAndPath(a, b); }

    import core.time : msecs;
	import std.datetime.stopwatch : benchmark;
	import std.algorithm: map;
	import std.array: array;

	auto r = benchmark!(bLCS, bLD, bLDAP)(1);

	import nxt.io.dbg : dbg;
	dbg(r);
}

/** Get LCS Lengths. */
private size_t[] longestCommonSubsequenceLengths(R)(R xs, R ys) {
	import std.algorithm.comparison : max;
	auto prev = new typeof(return)(1 + ys.length);
	auto curr = new typeof(return)(1 + ys.length);
	foreach (const ref x; xs) {
		import std.algorithm: swap;
		swap(curr, prev);
		size_t i = 0;
		foreach (const ref y; ys) {
			curr[i + 1] = (x == y) ? prev[i] + 1 : max(curr[i], prev[i + 1]);
			i++;
		}
	}
	return curr;
}
private alias lcsLengths = longestCommonSubsequenceLengths;

/++ Returns: Longest of `a` and `b`. +/
private inout(T)[] longestOf(T)(inout(T)[] x, inout(T)[] y) => x.length > y.length ? x : y;

version(nxt_test) version(unittest) {
	private alias lcsH = longestCommonSubsequenceHirschberg;
}
