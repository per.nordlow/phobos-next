/** Longest Common Substring.

	Copyright: Per Nordlöw 2014-.
	License: $(WEB boost.org/LICENSE_1_0.txt, Boost License 1.0).
	Authors: $(WEB Per Nordlöw)

	TODO: Make these work for (UTF-8 encoded) `string`s.
*/

module nxt.algorithm.comparison.lcss;
import std.experimental.logger;
import std.algorithm : max;
import std.range : zip;
import std.stdio;

struct SuffixTreeNode(E) {
    long start;
    long *end;
    SuffixTreeNode!(E)* suffixLink;
    SuffixTreeNode!(E)*[] children;

    this(long start, long* end) {
        this.start = start;
        this.end = end;
        this.suffixLink = null;
        this.children.length = 256; // Initialize children array with 256 elements
    }

    long edgeLength() {
        return *end - start + 1;
    }
}

struct Context(E) {
    SuffixTreeNode!(E)* root;
    SuffixTreeNode!(E)* lastNewNode;
    SuffixTreeNode!(E)* activeNode;

    long activeEdge = -1;
    long activeLength = 0;

    long remainingSuffixCount = 0;
    long leafEnd = -1;
    long* rootEnd;
    long* splitEnd;
    long size = -1;
}

void buildSuffixTree(E)(ref Context!(E) ctx, in E[] text) {
    ctx.size = text.length;
    long rootEnd = long(-1);
    ctx.root = new SuffixTreeNode!(E)(long(-1), &rootEnd);
    ctx.activeNode = ctx.root;
    foreach(const i; 0 .. ctx.size)
        extendSuffixTree!(E)(ctx, text, i);
}

void extendSuffixTree(E)(ref Context!(E) ctx, in E[] text, long pos) {
    ctx.leafEnd = pos;
    ++ctx.remainingSuffixCount;
    ctx.lastNewNode = null;

    while (ctx.remainingSuffixCount > 0) {
        if (ctx.activeLength == 0) {
            ctx.activeEdge = pos;
        }

        if (ctx.activeNode.children[text[ctx.activeEdge]] is null) {
            ctx.activeNode.children[text[ctx.activeEdge]] = new SuffixTreeNode!(E)(start: pos, end: &ctx.leafEnd);

            if (ctx.lastNewNode !is null) {
                ctx.lastNewNode.suffixLink = ctx.activeNode;
                ctx.lastNewNode = null;
            }
        } else {
            SuffixTreeNode!(E)* next = ctx.activeNode.children[text[ctx.activeEdge]];
            if (walkDown(ctx, next)) {
                continue;
            }

            if (text[next.start + ctx.activeLength] == text[pos]) {
                ++ctx.activeLength;
                if (ctx.lastNewNode !is null && ctx.activeNode !is ctx.root) {
                    ctx.lastNewNode.suffixLink = ctx.activeNode;
                    ctx.lastNewNode = null;
                }
                break;
            }

            ctx.splitEnd = new long;
            *ctx.splitEnd = next.start + ctx.activeLength - 1;

            SuffixTreeNode!(E)* split = new SuffixTreeNode!(E)(next.start, ctx.splitEnd);
            ctx.activeNode.children[text[ctx.activeEdge]] = split;

            split.children[text[pos]] = new SuffixTreeNode!(E)(pos, &ctx.leafEnd);
            next.start += ctx.activeLength;
            split.children[text[next.start]] = next;

            if (ctx.lastNewNode !is null) {
                ctx.lastNewNode.suffixLink = split;
            }

            ctx.lastNewNode = split;
        }

        --ctx.remainingSuffixCount;
        if (ctx.activeNode is ctx.root && ctx.activeLength > 0) {
            --ctx.activeLength;
            ctx.activeEdge = pos - ctx.remainingSuffixCount + 1;
        } else if (ctx.activeNode !is ctx.root) {
            ctx.activeNode = ctx.activeNode.suffixLink;
        }
    }
}

bool walkDown(E)(ref Context!(E) ctx, SuffixTreeNode!(E)* currentNode) {
    if (ctx.activeLength >= currentNode.edgeLength) {
        ctx.activeEdge += currentNode.edgeLength;
        ctx.activeLength -= currentNode.edgeLength;
        ctx.activeNode = currentNode;
        return true;
    }
    return false;
}

inout(E)[] lcssUkkonen(E)(inout(E)[] a, inout(E)[] b) pure nothrow @safe {
    Context!(E) ctx;
    buildSuffixTree!(E)(ctx, a ~ "$" ~ b ~ "#");

    long maxLength = 0;
    long start = 0;
    SuffixTreeNode!(E)* node = ctx.root;

    foreach (i; 0 .. ctx.size) {
        if (node !is null && node.edgeLength() > maxLength) {
            maxLength = node.edgeLength();
            start = node.start;
        }
        if (node !is null && node.children[a[i]] !is null) {
            node = node.children[a[i]];
        }
    }

    return a[start .. start + maxLength];
}

version(none)
pure nothrow @safe version(nxt_test) unittest {
	alias E = char;
    string text = "xabxac";
    Context!E ctx;
    ctx.buildSuffixTree!(E)(text);
    assert(ctx.root !is null);

    const result = lcssUkkonen("xabxac", "abcabxab");
    assert(result == "ab", "Expected longest common substring is 'ab'");
}
