/++ Extensions and specializations to std.algorithm.searching.`

	NOTE: `static` qualifier on scoped definitions of `struct Result` is needed
	for `inout` to propagate correctly.
 +/
module nxt.algorithm.searching;

public import nxt.algorithm.predicate : eq, ne, id;

/** Array-specialization of `startsWith`.
 *
 * See_Also: https://d.godbolt.org/z/ejEmrK
 */
bool startsWith(alias pred = eq, T)(scope const T[] haystack, in T[] needle) @trusted {
	version(D_Coverage) {} else pragma(inline, true);
	static if (false && use_stringzilla == true && T.sizeof == 1) {
		extern(C) const(char)* sz_find(scope return const(char)* haystack, size_t h_length, scope const(char)* needle, size_t n_length) pure nothrow @nogc @trusted;
		return sz_find(cast(const(char)*)haystack.ptr, haystack.length, cast(const(char)*)needle.ptr, needle.length) !is null;
	} else {
		if (haystack.length < needle.length)
			return false;
		return pred(haystack.ptr[0 .. needle.length], needle);
	}
}
/// ditto
bool startsWith(alias pred = eq, T)(scope const T[] haystack, scope const T needle) @trusted {
    version(D_Coverage) {} else pragma(inline, true);
    static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	if (haystack.length == 0)
		return false;
	return pred(haystack.ptr[0], needle);
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	static struct N { long x, y; }
	alias H = N[2];
	assert(H.init.startsWith(N.init));
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	static struct N { double x, y; }
	alias H = N[2];
	assert(!H.init.startsWith(N.init));
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const x = "beta version                ";
	assert(x.startsWith("beta"));
	assert(x.startsWith('b'));
	assert(!x.startsWith("_"));
	assert(!"".startsWith("_"));
	assert(!"".startsWith('_'));
}

/** Specialization of `startsWith` and case-insensitive match. */
private bool startsWithCaseInsensitive(T)(scope const(T)[] haystack, scope const(T)[] needle) if (is(T : const(char))) {
    import std.uni : sicmp;
	return haystack.startsWith!((a, b) => sicmp(a, b) == 0)(needle);
}
alias startsWithCI = startsWithCaseInsensitive;
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const x = "betas";
	assert(!x.startsWithCaseInsensitive("_"));
	assert(x.startsWithCaseInsensitive("beta"));
	assert(x.startsWithCaseInsensitive("BETA"));
	assert(x.startsWithCaseInsensitive("BEta"));
	assert(!x.startsWithCaseInsensitive("betas_"));
}

/** Array-specialization of `all` with element `needle`. */
bool all(alias pred = eq, T)(scope const T[] haystack, scope const T needle) @trusted {
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	foreach (const offset; 0 .. haystack.length)
		if (!pred(haystack.ptr[offset], needle))
			return false;
	return true;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("".all('a'));	// matches behaviour of `std.algorithm.searching.any`
	assert("aaa".all('a'));
	assert("aaa".all!(eq)('a'));
	assert(!"aaa".all!(ne)('a'));
	assert("aaa".all!(ne)('b'));
	assert(!"aaa".all!(eq)('b'));
	assert(!"aa_".all('a'));
}

/** Array-specialization of `any` with element `needle`. */
bool any(alias pred = eq, T)(scope const T[] haystack, scope const T needle) @trusted {
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	foreach (const offset; 0 .. haystack.length)
		if (pred(haystack.ptr[offset], needle))
			return true;
	return false;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert(!"".any('a'));  // matches behaviour of `std.algorithm.searching.any`
	assert("aaa".any('a'));
	assert(!"aaa".any!(ne)('a'));
	assert("aaa".any!(ne)('b'));
	assert(!"aaa".any!(eq)('b'));
	assert("aa_".any('a'));
	assert(!"_".any('a'));
}

/** Array-specialization of `endsWith`. */
bool endsWith(alias pred = eq, T)(scope const T[] haystack, in T[] needle) @trusted {
	version(D_Coverage) {} else version(LDC) pragma(inline, true);
	if (haystack.length < needle.length)
		return false;
	return pred(haystack.ptr[haystack.length - needle.length .. haystack.length], needle);
}
/// ditto
bool endsWith(alias pred = eq, T)(scope const T[] haystack, scope const T needle) @trusted {
	version(D_Coverage) {} else version(LDC) pragma(inline, true);
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	if (haystack.length == 0)
		return false;
	return pred(haystack.ptr[haystack.length - 1], needle);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const x = "beta version";
	assert(x.endsWith("version"));
	assert(x.endsWith('n'));
	assert(x.endsWith!(eq)('n'));
	assert(!x.endsWith!(ne)('n'));
	assert(!x.endsWith("_"));
	assert(!x.endsWith!(eq)("_"));
	assert(x.endsWith!(ne)("_"));
	assert(!"".endsWith("_"));
	assert(!"".endsWith('_'));
}

/** Specialization of `endsWith` and case-insensitive match.
 */
private bool endsWithCaseInsensitive(T)(scope const(T)[] haystack, scope const(T)[] needle) if (is(T : const(char))) {
    import std.uni : sicmp;
	return haystack.endsWith!((a, b) => sicmp(a, b) == 0)(needle);
}
alias endsWithCI = endsWithCaseInsensitive;
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const x = " beta";
	assert(!x.endsWithCaseInsensitive("_"));
	assert(x.endsWithCaseInsensitive("beta"));
	assert(x.endsWithCaseInsensitive("BETA"));
	assert(x.endsWithCaseInsensitive("BEta"));
	assert(!x.endsWithCaseInsensitive("betas_"));
}

bool startsWithAmong(alias pred = eq, T)(scope const T[] haystack, scope const T[][] needles) {
	foreach (const ref needle; needles)
		if (haystack.startsWith!(pred)(needle)) /+ TODO: optimize +/
			return true;
	return false;
}
/// ditto
bool startsWithAmong(alias pred = eq, T)(scope const T[] haystack, in T[] needles) {
	foreach (const ref needle; needles)
		if (haystack.startsWith!(pred)(needle)) /+ TODO: optimize +/
			return true;
	return false;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const x = "beta version";
	assert(x.startsWithAmong(["beta", "version", ""]));
	assert(x.startsWithAmong(['b', ' ']));
	assert(x.startsWithAmong("b "));
	assert(!x.startsWithAmong(["_"]));
	assert(!x.startsWithAmong(['_']));
}

bool endsWithAmong(alias pred = eq, T)(scope const T[] haystack, scope const T[][] needles) {
	foreach (const ref needle; needles)
		if (haystack.endsWith!(pred)(needle)) /+ TODO: optimize +/
			return true;
	return false;
}
/// ditto
bool endsWithAmong(alias pred = eq, T)(scope const T[] haystack, in T[] needles) {
	foreach (const ref needle; needles)
		if (haystack.endsWith!(pred)(needle)) /+ TODO: optimize +/
			return true;
	return false;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const x = "beta version";
	assert(x.endsWithAmong(["version"]));
	assert(!x.endsWithAmong(["alpha", "beta"]));
	assert(x.endsWithAmong(["version", ""]));
	assert(x.endsWithAmong(['n', ' ']));
	assert(x.endsWithAmong("n "));
	assert(!x.endsWithAmong(["_"]));
	assert(!x.endsWithAmong(['_']));
}

/** Array-specialization of `findSkip`.
 */
auto findSkip(alias pred = eq, T)(scope ref inout(T)[] haystack, in T[] needle) @trusted {
	const index = haystack.indexOf!(pred)(needle);
	if (index != -1) {
		haystack = haystack.ptr[index + needle.length .. haystack.length];
		return true;
	}
	return false;
}
/// ditto
auto findSkip(alias pred = eq, T)(scope ref inout(T)[] haystack, scope const T needle) @trusted {
	const index = haystack.indexOf!(pred)(needle);
	if (index != -1) {
		haystack = haystack.ptr[index + 1 .. haystack.length];
		return true;
	}
	return false;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const auto x = "abc";
	{
		string y = x;
		const bool ok = y.findSkip("_");
		assert(!ok);
		assert(y is x);
	}
	{
		string y = x;
		const bool ok = y.findSkip("a");
		assert(ok);
		assert(y == x[1 .. $]);
	}
	{
		string y = x;
		const bool ok = y.findSkip("c");
		assert(ok);
		assert(y is x[$ .. $]);
	}
	version(nxt_test) version(unittest) {
		static char[] f()() pure nothrow @safe { char[1] x = "_"; return x[].findSkip(" "); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const auto x = "abc";
	{
		string y = x;
		const bool ok = y.findSkip('_');
		assert(!ok);
		assert(y is x);
	}
	{
		string y = x;
		const bool ok = y.findSkip('a');
		assert(ok);
		assert(y == x[1 .. $]);
	}
	{
		string y = x;
		const bool ok = y.findSkip('c');
		assert(ok);
		assert(y is x[$ .. $]);
	}
	version(nxt_test) version(unittest) {
		static char[] f()() pure nothrow @safe { char[1] x = "_"; return x[].findSkip(' '); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}

/** Array-specialization of `findSkip` that finds the last skip.
 */
auto findLastSkip(alias pred = eq, T)(scope ref inout(T)[] haystack, in T[] needle) @trusted {
	const index = haystack.lastIndexOf!(pred)(needle);
	if (index != -1) {
		haystack = haystack.ptr[index + needle.length .. haystack.length];
		return true;
	}
	return false;
}
///
auto findLastSkip(alias pred = eq, T)(scope ref inout(T)[] haystack, scope const T needle) @trusted {
	const index = haystack.lastIndexOf!(pred)(needle);
	if (index != -1) {
		haystack = haystack.ptr[index + 1 .. haystack.length];
		return true;
	}
	return false;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const auto x = "abacc";
	{
		string y = x;
		const bool ok = y.findLastSkip("_");
		assert(!ok);
		assert(y is x);
	}
	{
		string y = x;
		const bool ok = y.findLastSkip("a");
		assert(ok);
		assert(y == x[3 .. $]);
	}
	{
		string y = x;
		const bool ok = y.findLastSkip("c");
		assert(ok);
		assert(y is x[$ .. $]);
	}
	version(nxt_test) version(unittest) {
		static char[] f()() pure nothrow @safe { char[1] x = "_"; return x[].findLastSkip(" "); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const auto x = "abacc";
	{
		string y = x;
		const bool ok = y.findLastSkip('_');
		assert(!ok);
		assert(y is x);
	}
	{
		string y = x;
		const bool ok = y.findLastSkip('a');
		assert(ok);
		assert(y == x[3 .. $]);
	}
	{
		string y = x;
		const bool ok = y.findLastSkip('c');
		assert(ok);
		assert(y is x[$ .. $]);
	}
	version(nxt_test) version(unittest) {
		static char[] f()() pure nothrow @safe { char[1] x = "_"; return x[].findLastSkip(' '); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}

/** Array-specialization of `skipOver`.
 *
 * See_Also: https://forum.dlang.org/post/dhxwgtaubzbmjaqjmnmq@forum.dlang.org
 */
bool skipOverFront(alias pred = eq, T)(scope ref inout(T)[] haystack, in T[] needle) @trusted {
	version(D_Coverage) {} else version(LDC) pragma(inline, true);
	if (!haystack.startsWith!(pred)(needle))
		return false;
	haystack = haystack.ptr[needle.length .. haystack.length];
	return true;
}
/// ditto
bool skipOverFront(alias pred = eq, T)(scope ref inout(T)[] haystack, scope const T needle) @trusted {
	version(D_Coverage) {} else version(LDC) pragma(inline, true);
	if (!haystack.startsWith!(pred)(needle))
		return false;
	haystack = haystack.ptr[1 .. haystack.length];
	return true;
}
/// ditto
alias skipOver = skipOverFront;
/// ditto
alias skipOverFirst = skipOverFront; /++ Phobos v3 compliance. +/
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	string x = "beta version";
	assert(x.skipOver("beta"));
	assert(x == " version");
	assert(x.skipOver(' '));
	assert(x == "version");
	assert(!x.skipOver("_"));
	assert(x == "version");
	assert(!x.skipOver('_'));
	assert(x == "version");
}
/// constness of haystack and needle
pure nothrow @safe @nogc version(nxt_test) unittest {
	const(char)[] haystack;
	assert(haystack.skipOver(string.init));
	assert(haystack.skipOver((const(char)[]).init));
	assert(haystack.skipOver(char[].init));
}

/** Array-specialization of `skipOverBack`.
 *
 * See: `std.string.chomp`
 * See_Also: https://forum.dlang.org/post/dhxwgtaubzbmjaqjmnmq@forum.dlang.org
 */
bool skipOverBack(alias pred = eq, T)(scope ref inout(T)[] haystack, in T[] needle) @trusted {
	version(D_Coverage) {} else version(LDC) pragma(inline, true);
	if (!haystack.endsWith!(pred)(needle))
		return false;
	haystack = haystack.ptr[0 .. haystack.length - needle.length];
	return true;
}
/// ditto
bool skipOverBack(alias pred = eq, T)(scope ref inout(T)[] haystack, scope const T needle) @trusted {
	version(D_Coverage) {} else version(LDC) pragma(inline, true);
	if (!haystack.endsWith!(pred)(needle))
		return false;
	haystack = haystack.ptr[0 .. haystack.length - 1];
	return true;
}
/// ditto
alias skipOverLast = skipOverBack; /++ Phobos v3 compliance. +/
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	string x = "beta version";
	assert(x.skipOverBack(" version"));
	assert(x == "beta");
	assert(x.skipOverBack('a'));
	assert(x == "bet");
	assert(!x.skipOverBack("_"));
	assert(x == "bet");
	assert(!x.skipOverBack('_'));
	assert(x == "bet");
}

bool skipOverAround(alias pred = eq, T)(scope ref inout(T)[] haystack, in T[] needleFront, in T[] needleBack) @trusted {
	version(D_Coverage) {} else version(LDC) pragma(inline, true);
	if (!haystack.startsWith!(pred)(needleFront) ||
		!haystack.endsWith!(pred)(needleBack))
		return false;
	haystack = haystack.ptr[needleFront.length .. haystack.length - needleBack.length];
	return true;
}
/// ditto
bool skipOverAround(alias pred = eq, T)(scope ref inout(T)[] haystack, scope const T needleFront, scope const T needleBack) @trusted {
	version(D_Coverage) {} else version(LDC) pragma(inline, true);
	if (!haystack.startsWith!(pred)(needleFront) ||
		!haystack.endsWith!(pred)(needleBack))
		return false;
	haystack = haystack.ptr[1 .. haystack.length - 1];
	return true;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	string x = "alpha beta_gamma";
	assert(x.skipOverAround("alpha", "gamma"));
	assert(x == " beta_");
	assert(x.skipOverAround(' ', '_'));
	assert(x == "beta");
	assert(!x.skipOverAround(" ", " "));
	assert(x == "beta");
	assert(!x.skipOverAround(' ', ' '));
	assert(x == "beta");
}

/** Array-specialization of `std.string.chompPrefix`.
 */
inout(T)[] chompPrefix(alias pred = eq, T)(scope return inout(T)[] haystack, in T[] needle) @trusted {
	version(D_Coverage) {} else pragma(inline, true);
	if (haystack.startsWith!(pred)(needle))
		haystack = haystack.ptr[needle.length .. haystack.length];
	return haystack;
}
inout(T)[] chompPrefix(alias pred = eq, T)(scope return inout(T)[] haystack, in T needle) @trusted {
	version(D_Coverage) {} else pragma(inline, true);
	if (haystack.startsWith!(pred)(needle))
		haystack = haystack.ptr[1 .. haystack.length];
	return haystack;
}
/// ditto
inout(char)[] chompPrefix(alias pred = eq)(scope return inout(char)[] haystack) /*tlm*/ {
	version(D_Coverage) {} else pragma(inline, true);
	return haystack.chompPrefix!(pred)(' '); // TODO: Adjust?
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert(chompPrefix("hello world", "he") == "llo world");
	assert(chompPrefix("hello world", "hello w") == "orld");
	assert(chompPrefix("hello world", " world") == "hello world");
	assert(chompPrefix("", "hello") == "");
	version(nxt_test) version(unittest) {
		static char[] f()() pure nothrow @safe { char[1] x = "_"; return x[].chompPrefix(" "); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}

/** Array-specialization of `std.string.chomp`.
 */
inout(T)[] chomp(alias pred = eq, T)(scope return inout(T)[] haystack, in T[] needle) @trusted {
	version(D_Coverage) {} else pragma(inline, true);
	if (haystack.endsWith!(pred)(needle))
		haystack = haystack.ptr[0 .. haystack.length - needle.length];
	return haystack;
}
inout(T)[] chomp(alias pred = eq, T)(scope return inout(T)[] haystack, in T needle) @trusted {
	version(D_Coverage) {} else pragma(inline, true);
	if (haystack.endsWith!(pred)(needle))
		haystack = haystack.ptr[0 .. haystack.length - 1];
	return haystack;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert(chomp("hello world", 'd') == "hello worl");
	assert(chomp(" hello world", "orld") == " hello w");
	assert(chomp(" hello world", " he") == " hello world");
	version(nxt_test) version(unittest) {
		static char[] f()() pure nothrow @safe { char[1] x = "_"; return x[].chomp(" "); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}

/** Array-specialization of `canFind`.
 *
 * NOTE: Opposite to `std.algorithm.searching.canFind`, returns `true` when
 * `needle` is empty as this is according to ChatGPT the most common behaviour
 * in other standard libraries such as in C++'s '`std::string::find` and
 * Python's `str.find()`.
 *
 * TODO: Add optimized implementation for needles with length >=
 * `largeNeedleLength` with no repeat of elements.
 *
 * TODO: reuse `return haystack.indexOf(needle) != -1` in both overloads
 */
bool canFind(alias pred = eq, T)(scope const T[] haystack, in T[] needle) @trusted {
	// enum largeNeedleLength = 4;
	if (needle.length == 0)
		return true;
	if (haystack.length < needle.length)
		return false;
	foreach (const offset; 0 .. haystack.length - needle.length + 1)
		if (pred(haystack.ptr[offset .. offset + needle.length], needle))
			return true;
	return false;
}
/// ditto
bool canFind(alias pred = eq, T)(scope const T[] haystack, scope const T needle) @trusted {
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	if (haystack.length == 0)
		return false;
	foreach (const ref elm; haystack)
		if (pred(elm, needle))
			return true;
	return false;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("".canFind("")); // opposite to Phobos
	assert("_".canFind("")); // opposite to Phobos
	assert(!"".canFind("_"));
	assert(!"a".canFind("_"));
	assert("a".canFind("a"));
	assert(!"a".canFind("ab"));
	assert("ab".canFind("a"));
	assert("ab".canFind("b"));
	assert("ab".canFind("ab"));
	assert(!"a".canFind("ab"));
	assert(!"b".canFind("ab"));
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert(!"".canFind('_'));
	assert(!"a".canFind('_'));
	assert("a".canFind('a'));
	assert("a".canFind('a'));
	assert("ab".canFind('a'));
	assert("ab".canFind('b'));
}

/** Array-specialization of `count`.
 */
ptrdiff_t count(alias pred = eq, T)(scope const T[] haystack, in T[] needle) @trusted {
	if (needle.length == 0)
		return -1;
	size_t res = 0;
	if (haystack.length < needle.length)
		return false;
	foreach (const offset; 0 .. haystack.length - needle.length + 1)
		res += pred(haystack.ptr[offset .. offset + needle.length], needle) ? 1 : 0; // TODO: Optimize
	return res;
}
/// ditto
size_t count(alias pred = eq, T)(scope const T[] haystack, scope const T needle) {
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	typeof(return) res;
	foreach (const ref elm; haystack)
		res += pred(elm, needle) ? 1 : 0;
	return res;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("".count("") == -1); // -1 instead of `assert` that `std.algorithm.count` does
	assert("".count("_") == 0);
	assert("".count(" ") == 0);
	assert(" ".count(" ") == 1);
	assert("abc_abc".count("a") == 2);
	assert("abc_abc".count("abc") == 2);
	assert("_a_a_".count("_") == 3);
	assert("_aaa_".count("a") == 3);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("".count('_') == 0);
	assert("abc_abc".count('a') == 2);
	assert("_abc_abc_".count('_') == 3);
}

/** Returns: Number of intialized values in `haystack`. */
size_t countInitialized(alias pred = eq, T)(scope const T[] haystack)
if (!(is(T == class) || is(T == _*, _)/+isAddress+/)) {
	typeof(return) res;
	foreach (const ref elm; haystack)
		res += !pred(elm, T.init);
	return res;
}
/// ditto
size_t countInitialized(alias pred = id, T)(scope const T[] haystack)
if (is(T == class) || is(T == _*, _)/+isAddress+/) {
	typeof(return) res;
	foreach (const ref elm; haystack)
		res += !pred(elm, T.init);
	return res;
}
alias countInits = countInitialized;
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("".countInitialized == 0);
	assert([0, 0, 11].countInitialized == 1);
	assert([0, 11, 12].countInitialized == 2);
	class C {}
	assert([C.init, C.init].countInitialized == 0);
}
pure nothrow @safe version(nxt_test) unittest {
	class C {}
	assert([C.init, new C()].countInitialized == 1);
	assert([new C(), new C()].countInitialized == 2);
}

/** Array-specialization of `count` and no needle.
 */
size_t count(T)(scope const T[] haystack) {
	version(D_Coverage) {} else pragma(inline, true);
	return haystack.length;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("abc_abc".count == 7);
}

/** Array-specialization of `countAmong`.
 */
ptrdiff_t countAmong(alias pred = eq, T)(scope const T[] haystack, scope const T[][] needles) @trusted {
	if (needles.length == 0)
		return -1;
	size_t res = 0;
	foreach (const ref needle; needles)
		foreach (const offset; 0 .. haystack.length - needle.length + 1)
			res += pred(haystack.ptr[offset .. offset + needle.length], needle) ? 1 : 0;
	return res;
}
/// ditto
ptrdiff_t countAmong(alias pred = eq, T)(scope const T[] haystack, in T[] needles) {
	static if (is(T : const(char)))
		foreach (needle; needles)
			assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	if (needles.length == 0)
		return -1;
	size_t res = 0;
	foreach (const offset; 0 .. haystack.length)
		foreach (const ref needle; needles)
			res += pred(haystack[offset], needle) ? 1 : 0;
	return res;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("".countAmong(string[].init) == -1);
	assert("".countAmong([""]) == 1);
	assert("".countAmong(["_"]) == 0);
	assert("".countAmong([" "]) == 0);
	assert(" ".countAmong([" "]) == 1);
	assert("abc_abc".countAmong(["a"]) == 2);
	assert("abc_abc".countAmong(["abc"]) == 2);
	assert("_a_a_".countAmong(["_"]) == 3);
	assert("_aaa_".countAmong(["a"]) == 3);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("".countAmong(string.init) == -1); // -1 instead of `assert` that `std.algorithm.count` does
	assert("".countAmong("") == -1); // -1 instead of `assert` that `std.algorithm.count` does
	assert(" ".countAmong("") == -1); // -1 instead of `assert` that `std.algorithm.count` does
	assert("".countAmong("_") == 0);
	assert("".countAmong(" ") == 0);
	assert(" ".countAmong(" ") == 1);
	assert("abc_abc".countAmong("a") == 2);
	assert("abc_abc".countAmong(" ") == 0);
	assert("abc_abc".countAmong("ab") == 4);
	assert("abc_abc".countAmong("abc") == 6);
	assert("_a_a_".countAmong("_") == 3);
	assert("_aaa_".countAmong("a") == 3);
}

/** Array-specialization of `indexOf`.
 *
 * TODO: Add optimized implementation for needles with length >=
 * `largeNeedleLength` with no repeat of elements.
 */
ptrdiff_t indexOf(alias pred = eq, T)(scope inout(T)[] haystack, scope const(T)[] needle) @trusted {
	// enum largeNeedleLength = 4;
	if (haystack.length < needle.length)
		return -1;
	foreach (const offset; 0 .. haystack.length - needle.length + 1)
		if (pred(haystack.ptr[offset .. offset + needle.length], needle))
			return offset;
	return -1;
}
/// ditto
ptrdiff_t indexOf(alias pred = eq, T)(scope inout(T)[] haystack, scope const T needle) {
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	foreach (const offset, const ref elm; haystack)
		if (pred(elm, needle))
			return offset;
	return -1;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("".indexOf("") == 0);
	assert("".indexOf!(eq)("") == 0);
	assert("_".indexOf("") == 0);
	assert("_".indexOf!(eq)("") == 0);
	assert("_abc_abc_".indexOf("abc") == 1);
	assert("_abc_abc_".indexOf!(eq)("abc") == 1);
	assert("__abc_".indexOf("abc") == 2);
	assert("__abc_".indexOf!(eq)("abc") == 2);
	assert("a".indexOf("a") == 0);
	assert("a".indexOf!(eq)("a") == 0);
	assert("abc".indexOf("abc") == 0);
	assert("abc".indexOf!(eq)("abc") == 0);
	assert("_".indexOf("a") == -1);
	assert("_".indexOf!(eq)("a") == -1);
	assert("_".indexOf("__") == -1);
	assert("_".indexOf!(eq)("__") == -1);
	assert("__".indexOf("a") == -1);
	assert("__".indexOf!(eq)("a") == -1);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("".indexOf('a') == -1);
	assert("_".indexOf('a') == -1);
	assert("a".indexOf('a') == 0);
	assert("_a".indexOf('a') == 1);
	assert("__a".indexOf('a') == 2);
}

/// ditto
ptrdiff_t indexOfAmong(alias pred = eq, T)(scope const(T)[] haystack, in T[] needles) {
	if (needles.length == 0)
		return -1;
	foreach (const offset, const ref elm; haystack)
		foreach (const ref needle; needles)
			if (pred(elm, needle))
				return offset;
	return -1;
}
/// ditto
alias indexOfAny = indexOfAmong; // Compliance with `std.string.indexOfAny`.
/// ditto
alias indexOfEither = indexOfAmong;
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("".indexOfAmong("a") == -1);
	assert("_".indexOfAmong("a") == -1);
	assert("_a".indexOfAmong("a") == 1);
	assert("_a".indexOfAmong("ab") == 1);
	assert("_b".indexOfAmong("ab") == 1);
	assert("_b".indexOfAmong("_") == 0);
	assert("_b".indexOfAmong("xy") == -1);
	assert("_b".indexOfAmong("") == -1);
}

/** Array-specialization of `lastIndexOf`.
 */
ptrdiff_t lastIndexOf(alias pred = eq, T)(scope const(T)[] haystack, scope const(T)[] needle) @trusted {
	if (haystack.length < needle.length)
		return -1;
	foreach_reverse (const offset; 0 .. haystack.length - needle.length + 1)
		if (pred(haystack.ptr[offset .. offset + needle.length], needle))
			return offset;
	return -1;
}
/// ditto
ptrdiff_t lastIndexOf(alias pred = eq, T)(scope const(T)[] haystack, scope const T needle) {
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	foreach_reverse (const offset, const ref elm; haystack)
		if (pred(elm, needle))
			return offset;
	return -1;
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("".lastIndexOf("") == 0);
	assert("_".lastIndexOf("") == 1);
	assert("_abc_abc_".lastIndexOf("abc") == 5);
	assert("__abc_".lastIndexOf("abc") == 2);
	assert("a".lastIndexOf("a") == 0);
	assert("aa".lastIndexOf("a") == 1);
	assert("abc".lastIndexOf("abc") == 0);
	assert("_".lastIndexOf("a") == -1);
	assert("_".lastIndexOf("__") == -1);
	assert("__".lastIndexOf("a") == -1);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	assert("_".lastIndexOf('a') == -1);
	assert("a".lastIndexOf('a') == 0);
	assert("_a".lastIndexOf('a') == 1);
	assert("__a".lastIndexOf('a') == 2);
	assert("a__a".lastIndexOf('a') == 3);
}

/** Array-specialization of `findSplit`.
 *
 * See_Also: https://forum.dlang.org/post/dhxwgtaubzbmjaqjmnmq@forum.dlang.org
 * See_Also: https://forum.dlang.org/post/zhgajqdhybtbufeiiofp@forum.dlang.org
 */
auto findSplit(alias pred = eq, T)(scope return inout(T)[] haystack, scope const(T)[] needle) {
	static struct Result {
		private T[] _haystack;
		private size_t _offset; // hit offset
		private size_t _length; // hit length
	pragma(inline, true):
		inout(T)[] opIndex(in size_t i) inout {
			switch (i) {
			case 0: return pre;
			case 1: return separator;
			case 2: return post;
			default: return typeof(return).init;
			}
		}
		inout(T)[] pre() @trusted inout		  => _haystack.ptr[0 .. _offset];
		inout(T)[] separator() @trusted inout => _haystack.ptr[_offset .. _offset + _length];
		inout(T)[] post() @trusted inout	  => _haystack.ptr[_offset + _length .. _haystack.length];
		bool opCast(T : bool)() @safe const	  => _haystack.length != _offset;
	}

	assert(needle.length, "Cannot find occurrence of an empty range");
	const index = haystack.indexOf!(pred)(needle);
	if (index >= 0)
		return inout(Result)(haystack, index, needle.length);
	return inout(Result)(haystack, haystack.length, 0); // miss
}
/// ditto
auto findSplit(alias pred = eq, T)(scope return inout(T)[] haystack, scope const T needle) {
	static struct Result {
		private T[] _haystack;
		private size_t _offset; // hit offset
	pragma(inline, true):
		inout(T)[] opIndex(in size_t i) inout {
			switch (i) {
			case 0: return pre;
			case 1: return separator;
			case 2: return post;
			default: return typeof(return).init;
			}
		}
		inout(T)[] pre() @trusted inout		  => _haystack.ptr[0 .. _offset];
		inout(T)[] separator() @trusted inout => !empty ? _haystack.ptr[_offset .. _offset + 1] : _haystack[$ .. $];
		inout(T)[] post() @trusted inout	  => !empty ? _haystack.ptr[_offset + 1 .. _haystack.length] : _haystack[$ .. $];
		bool opCast(T : bool)() const		  => !empty;
		private bool empty() const @property  => _haystack.length == _offset;
	}
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	const index = haystack.indexOf!(pred)(needle);
	if (index >= 0)
		return inout(Result)(haystack, index);
	return inout(Result)(haystack, haystack.length);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const h = "a**b";
	const r = h.findSplit("**");
	assert(r);
	assert(r.pre is h[0 .. 1]);
	assert(r.separator is h[1 .. 3]);
	assert(r.post is h[3 .. 4]);
	version(nxt_test) version(unittest) {
		static auto f()() pure nothrow @safe { char[1] x = "_"; return x[].findSplit(" "); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const h = "a**b";
	const r = h.findSplit("_");
	static assert(r.sizeof == 2 * 2 * size_t.sizeof);
	assert(!r);
	assert(r.pre is h);
	assert(r.separator is h[$ .. $]);
	assert(r.post is h[$ .. $]);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "a*b".findSplit('*');
	static assert(r.sizeof == 3 * size_t.sizeof);
	assert(r);
	assert(r.pre == "a");
	assert(r.separator == "*");
	assert(r.post == "b");
	version(nxt_test) version(unittest) {
		static auto f()() pure nothrow @safe { char[1] x = "_"; return x[].findSplit(' '); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}
/// DIP-1000 scope analysis
version(none)					// TODO. enable
pure nothrow @safe @nogc version(nxt_test) unittest {
	const(char)[] f() pure nothrow @safe
	{
		const char[3] haystack = "a*b";
		auto r = haystack[].findSplit('*');
		static assert(is(typeof(r.pre()) == char[]));
		return r.pre();		 /+ TODO: this should fail +/
	}
	const _ = f();
}

/** Array-specialization of `findLastSplit`.
 */
auto findLastSplit(alias pred = eq, T)(scope return inout(T)[] haystack, scope const(T)[] needle) {
	static struct Result {
		private T[] _haystack;
		private size_t _offset; // hit offset
		private size_t _length; // hit length
	pragma(inline, true):
		inout(T)[] opIndex(in size_t i) inout {
			switch (i) {
			case 0: return pre;
			case 1: return separator;
			case 2: return post;
			default: return typeof(return).init;
			}
		}
		inout(T)[] pre() @trusted inout		  => _haystack.ptr[0 .. _offset];
		inout(T)[] separator() @trusted inout => _haystack.ptr[_offset .. _offset + _length];
		inout(T)[] post() @trusted inout	  => _haystack.ptr[_offset + _length .. _haystack.length];
		bool opCast(T : bool)() @safe const	  => _haystack.length != _offset;
	}
	assert(needle.length, "Cannot find occurrence of an empty range");
	const index = haystack.lastIndexOf!(pred)(needle);
	if (index >= 0)
		return inout(Result)(haystack, index, needle.length);
	return inout(Result)(haystack, haystack.length, 0); // miss
}
/// ditto
auto findLastSplit(alias pred = eq, T)(scope return inout(T)[] haystack, scope const T needle) {
	static struct Result {
		private T[] _haystack;
		private size_t _offset; // hit offset
	pragma(inline, true):
		inout(T)[] opIndex(in size_t i) inout {
			switch (i) {
			case 0: return pre;
			case 1: return separator;
			case 2: return post;
			default: return typeof(return).init;
			}
		}
		inout(T)[] pre() @trusted inout		  => _haystack.ptr[0 .. _offset];
		inout(T)[] separator() @trusted inout => !empty ? _haystack.ptr[_offset .. _offset + 1] : _haystack[$ .. $];
		inout(T)[] post() @trusted inout	  => !empty ? _haystack.ptr[_offset + 1 .. _haystack.length] : _haystack[$ .. $];
		bool opCast(T : bool)() const		  => !empty;
		private bool empty() const @property  => _haystack.length == _offset;
	}
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	const index = haystack.lastIndexOf!(pred)(needle);
	if (index >= 0)
		return inout(Result)(haystack, index);
	return inout(Result)(haystack, haystack.length);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const h = "a**b**c";
	const r = h.findLastSplit("**");
	assert(r);
	assert(r.pre is h[0 .. 4]);
	assert(r.separator is h[4 .. 6]);
	assert(r.post is h[6 .. 7]);
	version(nxt_test) version(unittest) {
		static auto f()() pure nothrow @safe { char[1] x = "_"; return x[].findLastSplit(" "); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const h = "a**b**c";
	const r = h.findLastSplit("_");
	static assert(r.sizeof == 2 * 2 * size_t.sizeof);
	assert(!r);
	assert(r.pre is h);
	assert(r.separator is h[$ .. $]);
	assert(r.post is h[$ .. $]);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "a*b*c".findLastSplit('*');
	static assert(r.sizeof == 3 * size_t.sizeof);
	assert(r);
	assert(r.pre == "a*b");
	assert(r.separator == "*");
	assert(r.post == "c");
	version(nxt_test) version(unittest) {
		static auto f()() pure nothrow @safe { char[1] x = "_"; return x[].findLastSplit(' '); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}
/// DIP-1000 scope analysis
version(none)					/+ TODO: enable +/
pure nothrow @safe @nogc version(nxt_test) unittest {
	const(char)[] f() pure nothrow @safe
	{
		const char[3] haystack = "a*b";
		auto r = haystack[].findLastSplit('*');
		static assert(is(typeof(r.pre()) == char[]));
		return r.pre();		 /+ TODO: this should fail +/
	}
	const _ = f();
}

/** Array-specialization of `findSplitBefore`.
 *
 * See_Also: https://forum.dlang.org/post/dhxwgtaubzbmjaqjmnmq@forum.dlang.org
 * See_Also: https://forum.dlang.org/post/zhgajqdhybtbufeiiofp@forum.dlang.org
 */
auto findSplitBefore(alias pred = eq, T)(scope return inout(T)[] haystack, scope const T needle) {
	static struct Result {
		private T[] _haystack;
		private size_t _offset;
	pragma(inline, true):
		inout(T)[] pre() @trusted inout		 => _haystack.ptr[0 .. _offset];
		inout(T)[] post() @trusted inout	 => !empty ? _haystack.ptr[_offset .. _haystack.length] : _haystack[$ .. $];
		bool opCast(T : bool)() const		 => !empty;
		private bool empty() const @property => _haystack.length == _offset;
	}
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	foreach (const offset, const ref elm; haystack)
		if (pred(elm, needle))
			return inout(Result)(haystack, offset);
	return inout(Result)(haystack, haystack.length);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	char[] haystack;
	auto r = haystack.findSplitBefore('_');
	static assert(is(typeof(r.pre()) == typeof(haystack)));
	static assert(is(typeof(r.post()) == typeof(haystack)));
	assert(!r);
	assert(!r.pre);
	assert(!r.post);
	version(nxt_test) version(unittest) {
		static auto f()() pure nothrow @safe { char[1] x = "_"; return x[].findSplitBefore(' '); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const(char)[] haystack;
	auto r = haystack.findSplitBefore('_');
	static assert(is(typeof(r.pre()) == typeof(haystack)));
	static assert(is(typeof(r.post()) == typeof(haystack)));
	assert(!r);
	assert(!r.pre);
	assert(!r.post);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "a*b".findSplitBefore('*');
	assert(r);
	assert(r.pre == "a");
	assert(r.post == "*b");
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "a*b".findSplitBefore('_');
	assert(!r);
	assert(r.pre == "a*b");
	assert(r.post == "");
}

/** Array-specialization of `findSplitBefore` with explicit needle-only predicate `needlePred`.
 *
 * See_Also: https://forum.dlang.org/post/dhxwgtaubzbmjaqjmnmq@forum.dlang.org
 * See_Also: https://forum.dlang.org/post/zhgajqdhybtbufeiiofp@forum.dlang.org
 */
auto findSplitBefore(alias needlePred, T)(scope return inout(T)[] haystack) {
	static struct Result {
		private T[] _haystack;
		private size_t _offset;
	pragma(inline, true):
		inout(T)[] pre() @trusted inout		 => _haystack.ptr[0 .. _offset];
		inout(T)[] post() @trusted inout	 => !empty ? _haystack.ptr[_offset .. _haystack.length] : _haystack[$ .. $];
		bool opCast(T : bool)() const		 => !empty;
		private bool empty() const @property => _haystack.length == _offset;
	}
	foreach (const offset, const ref elm; haystack)
		if (needlePred(elm))
			return inout(Result)(haystack, offset);
	return inout(Result)(haystack, haystack.length);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	char[] haystack;
	auto r = haystack.findSplitBefore!(_ => _ == '_');
	static assert(is(typeof(r.pre()) == typeof(haystack)));
	static assert(is(typeof(r.post()) == typeof(haystack)));
	assert(!r);
	assert(!r.pre);
	assert(!r.post);
	version(nxt_test) version(unittest) {
		static auto f()() pure nothrow @safe { char[1] x = "_"; return x[].findSplitBefore!(_ => _ == ' '); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const(char)[] haystack;
	auto r = haystack.findSplitBefore!(_ => _ == '_');
	static assert(is(typeof(r.pre()) == typeof(haystack)));
	static assert(is(typeof(r.post()) == const(char)[]));
	assert(!r);
	assert(!r.pre);
	assert(!r.post);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "a*b".findSplitBefore!(_ => _ == '*');
	assert(r);
	assert(r.pre == "a");
	assert(r.post == "*b");
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "a*b".findSplitBefore!(_ => _ == '*' || _ == '+');
	assert(r);
	assert(r.pre == "a");
	assert(r.post == "*b");
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "a+b".findSplitBefore!(_ => _ == '*' || _ == '+');
	assert(r);
	assert(r.pre == "a");
	assert(r.post == "+b");
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "a*b".findSplitBefore!(_ => _ == '_');
	assert(!r);
	assert(r.pre == "a*b");
	assert(r.post == "");
}

/** Array-specialization of `findSplitAfter`.
 *
 * See_Also: https://forum.dlang.org/post/dhxwgtaubzbmjaqjmnmq@forum.dlang.org
 * See_Also: https://forum.dlang.org/post/zhgajqdhybtbufeiiofp@forum.dlang.org
 */
auto findSplitAfter(alias pred = eq, T)(scope return inout(T)[] haystack, scope const T needle) @trusted {
	static struct Result {
		private T[] _haystack;
		private size_t _offset;
	pragma(inline, true):
		inout(T)[] pre() @trusted inout		 => !empty ? _haystack.ptr[0 .. _offset + 1] : _haystack[$ .. $];
		inout(T)[] post() @trusted inout	 => !empty ? _haystack.ptr[_offset + 1 .. _haystack.length] : _haystack[0 .. $];
		bool opCast(T : bool)() const		 => !empty;
		private bool empty() const @property => _haystack.length == _offset;
	}
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	foreach (const offset, const ref elm; haystack)
		if (pred(elm, needle))
			return inout(Result)(haystack, offset);
	return inout(Result)(haystack, haystack.length);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	char[] haystack;
	auto r = haystack.findSplitAfter('_');
	static assert(is(typeof(r.pre()) == typeof(haystack)));
	static assert(is(typeof(r.post()) == typeof(haystack)));
	assert(!r);
	assert(!r.pre);
	assert(!r.post);
	version(nxt_test) version(unittest) {
		static auto f()() pure nothrow @safe { char[1] x = "_"; return x[].findSplitAfter(' '); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const(char)[] haystack;
	auto r = haystack.findSplitAfter('_');
	static assert(is(typeof(r.pre()) == typeof(haystack)));
	static assert(is(typeof(r.post()) == typeof(haystack)));
	assert(!r);
	assert(!r.pre);
	assert(!r.post);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	auto haystack = "a*b";
	auto r = haystack.findSplitAfter('*');
	static assert(is(typeof(r.pre()) == typeof(haystack)));
	static assert(is(typeof(r.post()) == typeof(haystack)));
	assert(r);
	assert(r.pre == "a*");
	assert(r.post == "b");
}

/** Array-specialization of `findLastSplitAfter`.
 *
 * See_Also: https://forum.dlang.org/post/dhxwgtaubzbmjaqjmnmq@forum.dlang.org
 * See_Also: https://forum.dlang.org/post/zhgajqdhybtbufeiiofp@forum.dlang.org
 */
auto findLastSplitAfter(alias pred = eq, T)(scope return inout(T)[] haystack, scope const T needle) @trusted {
	static struct Result {
		private T[] _haystack;
		private size_t _offset;
	pragma(inline, true):
		inout(T)[] pre() @trusted inout		 => !empty ? _haystack.ptr[0 .. _offset + 1] : _haystack[$ .. $];
		inout(T)[] post() @trusted inout	 => !empty ? _haystack.ptr[_offset + 1 .. _haystack.length] : _haystack[0 .. $];
		bool opCast(T : bool)() const		 => !empty;
		private bool empty() const @property => _haystack.length == _offset;
	}
	static if (is(T : const(char)))
		assert(needle < 128); // See_Also: https://forum.dlang.org/post/sjirukypxmmcgdmqbcpe@forum.dlang.org
	const index = haystack.lastIndexOf!(pred)(needle);
	if (index >= 0)
		return inout(Result)(haystack, index);
	return inout(Result)(haystack, haystack.length); // miss
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	char[] haystack;
	auto r = haystack.findLastSplitAfter('_');
	static assert(is(typeof(r.pre()) == typeof(haystack)));
	static assert(is(typeof(r.post()) == typeof(haystack)));
	assert(!r);
	assert(!r.pre);
	assert(!r.post);
	version(nxt_test) version(unittest) {
		static auto f()() pure nothrow @safe { char[1] x = "_"; return x[].findLastSplitAfter(' '); }
		static if (hasPreviewDIP1000) static assert(!__traits(compiles, { const _ = f(); }));
	}
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const(char)[] haystack;
	auto r = haystack.findLastSplitAfter('_');
	static assert(is(typeof(r.pre()) == typeof(haystack)));
	static assert(is(typeof(r.post()) == typeof(haystack)));
	assert(!r);
	assert(!r.pre);
	assert(!r.post);
}
///
pure nothrow @safe @nogc version(nxt_test) unittest {
	auto haystack = "a*b*c";
	auto r = haystack.findLastSplitAfter('*');
	static assert(is(typeof(r.pre()) == typeof(haystack)));
	static assert(is(typeof(r.post()) == typeof(haystack)));
	assert(r);
	assert(r.pre == "a*b*");
	assert(r.post == "c");
}

import std.traits : isExpressions;

/** Like `findSplit` but with multiple separator `needles` known at compile-time
 * to prevent `NarrowString` decoding.
 *
 * TODO: Resort to `memchr` for some case `if (!__ctfe)`.
 * See_Also: https://forum.dlang.org/post/efpbmtyisamwwqgpxnbq@forum.dlang.org
 *
 * See_Also: https://forum.dlang.org/post/ycotlbfsqoupogaplkvf@forum.dlang.org
 */
template findSplitAmong(needles...)
if (needles.length != 0 &&
	isExpressions!needles) {
	import std.meta : allSatisfy;
	import nxt.char_traits : isASCII;

	auto findSplitAmong(Haystack)(const scope return Haystack haystack) @trusted /+ TODO: qualify with `inout` to reduce template bloat +/
	if (is(typeof(Haystack.init[0 .. 0])) && // can be sliced
		is(typeof(Haystack.init[0]) : char) &&
		allSatisfy!(isASCII, needles)) {
		// similar return result to `std.algorithm.searching.findSplit`
		static struct Result {
			/* Only requires 3 words opposite to Phobos' `findSplit`,
			 * `findSplitBefore` and `findSplitAfter`:
			 */

			private Haystack _haystack; // original copy of haystack
			private size_t _offset; // hit offset if any, or `_haystack.length` if miss

			bool opCast(T : bool)() const => !empty;

			inout(Haystack) opIndex(in size_t i) inout {
				switch (i) {
				case 0: return pre;
				case 1: return separator;
				case 2: return post;
				default: return typeof(return).init;
				}
			}

		@property:
			private bool empty() const => _haystack.length == _offset;

			inout(Haystack) pre() inout => _haystack[0 .. _offset];

			inout(Haystack) separator() inout {
				if (empty) { return _haystack[$ .. $]; }
				return _haystack[_offset .. _offset + 1];
			}

			inout(Haystack) post() inout {
				if (empty) { return _haystack[$ .. $]; }
				return _haystack[_offset + 1 .. $];
			}
		}

		enum use_memchr = false;
		static if (use_memchr &&
				   needles.length == 1) {
			// See_Also: https://forum.dlang.org/post/piowvfbimztbqjvieddj@forum.dlang.org
			import core.stdc.string : memchr;
			// extern (C) @system nothrow @nogc pure void* rawmemchr(return const void* s, int c);

			const void* hit = memchr(haystack.ptr, needles[0], haystack.length);
			return Result(haystack, hit ? hit - cast(const(void)*)haystack.ptr : haystack.length);
		} else {
			foreach (immutable offset; 0 .. haystack.length) {
				static if (needles.length == 1) {
					immutable hit = haystack[offset] == needles[0];
				} else {
					import std.algorithm.comparison : among;
					immutable hit = haystack[offset].among!(needles) != 0;
				}
				if (hit)
					return Result(haystack, offset);
			}
			return Result(haystack, haystack.length);
		}
	}
}

template findSplit(needles...)
if (needles.length == 1 &&
	isExpressions!needles) {
	import nxt.char_traits : isASCII;
	auto findSplit(Haystack)(const scope return Haystack haystack) @trusted /+ TODO: qualify with `inout` to reduce template bloat +/
	if (is(typeof(Haystack.init[0 .. 0])) && // can be sliced
		is(typeof(Haystack.init[0]) : char) &&
		isASCII!(needles[0])) {
		return findSplitAmong!(needles)(haystack);
	}
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "a*b".findSplit!('*');
	assert(r);

	assert(r[0] == "a");
	assert(r.pre == "a");

	assert(r[1] == "*");
	assert(r.separator == "*");

	assert(r[2] == "b");
	assert(r.post == "b");
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	auto r = "a+b*c".findSplitAmong!('+', '-');

	static assert(r.sizeof == 24);
	static assert(is(typeof(r.pre) == string));
	static assert(is(typeof(r.separator) == string));
	static assert(is(typeof(r.post) == string));

	assert(r);

	assert(r[0] == "a");
	assert(r.pre == "a");

	assert(r[1] == "+");
	assert(r.separator == "+");

	assert(r[2] == "b*c");
	assert(r.post == "b*c");
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "a+b*c".findSplitAmong!('-', '*');
	assert(r);
	assert(r.pre == "a+b");
	assert(r.separator == "*");
	assert(r.post == "c");
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "a*".findSplitAmong!('*');

	assert(r);

	assert(r[0] == "a");
	assert(r.pre == "a");

	assert(r[1] == "*");
	assert(r.separator == "*");

	assert(r[2] == "");
	assert(r.post == "");
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "*b".findSplitAmong!('*');

	assert(r);

	assert(r[0] == "");
	assert(r.pre == "");

	assert(r[1] == "*");
	assert(r.separator == "*");

	assert(r[2] == "b");
	assert(r.post == "b");
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const r = "*".findSplitAmong!('*');

	assert(r);

	assert(r[0] == "");
	assert(r.pre == "");

	assert(r[1] == "*");
	assert(r.separator == "*");

	assert(r[2] == "");
	assert(r.post == "");
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	static immutable separator_char = '/';

	immutable r = "a+b*c".findSplitAmong!(separator_char);

	static assert(r.sizeof == 24);
	static assert(is(typeof(r.pre) == immutable string));
	static assert(is(typeof(r.separator) == immutable string));
	static assert(is(typeof(r.post) == immutable string));

	assert(!r);

	assert(r.pre == "a+b*c");
	assert(r[0] == "a+b*c");
	assert(r.separator == []);
	assert(r[1] == []);
	assert(r.post == []);
	assert(r[2] == []);
}

/++ Returns: Slice of `haystack` directly before `index` containing an identifier (symbol).
 +  If no identifier is found, `haystack[index .. index]` is returned.
 +  If `index > haystack.length` then `[]` is returned.
 +/
inout(char)[] tryGetIdentifierBefore(inout(char)[] haystack, in size_t index) pure nothrow @safe @nogc {
	import std.ascii : isAlpha, isAlphaNum;
	if (index > haystack.length)
		return []; // indicate mismatch
    size_t i = index;
    while (i != 0) {
		const ch = haystack[i - 1];
		const ok = ch.isAlphaNum || ch == '_';
        if (!ok)
			break;
        i -= 1;
	}
	if (i != index) {
		const ch = haystack[i];
		const ok = ch.isAlpha || ch == '_';
		if (ok)
			return haystack[i .. index];
	}
	return [];
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	{
		const h = "_1";
		assert(h.tryGetIdentifierBefore(1) is h[0 .. 1]);
		assert(h.tryGetIdentifierBefore(2) is h[0 .. 2]);
        assert(h.tryGetIdentifierBefore(3) is []);
	}
	{
		const h = "a";
		assert(h.tryGetIdentifierBefore(1) is h[0 .. 1]);
		assert(h.tryGetIdentifierBefore(2) is []);
	}
	{
		const h = "a1";
		assert(h.tryGetIdentifierBefore(1) is h[0 .. 1]);
		assert(h.tryGetIdentifierBefore(2) is h[0 .. 2]);
        assert(h.tryGetIdentifierBefore(3) is []);
	}
	{
		const h = "11";
		assert(h.tryGetIdentifierBefore(0) is []);
		assert(h.tryGetIdentifierBefore(1) is []);
		assert(h.tryGetIdentifierBefore(2) is []);
	}
	{
		const h = " a";
		assert(h.tryGetIdentifierBefore(0) is []);
		assert(h.tryGetIdentifierBefore(1) is []);
		assert(h.tryGetIdentifierBefore(2) is h[1 .. $]);
		assert(h.tryGetIdentifierBefore(3) is []);
	}
	{
		const h = " a ";
		assert(h.tryGetIdentifierBefore(0) is []);
		assert(h.tryGetIdentifierBefore(1) is []);
		assert(h.tryGetIdentifierBefore(2) is h[1 .. 2]);
		assert(h.tryGetIdentifierBefore(3) is []);
	}
}

/++ Returns: Slice of `haystack` directly after `index` containing an identifier (symbol).
 +  If no identifier is found, `haystack[index .. index]` is returned.
 +  If `index > haystack.length` then `[]` is returned.
 +/
inout(char)[] tryGetIdentifierAfter(inout(char)[] haystack, in size_t index) pure nothrow @safe @nogc {
	import std.ascii : isAlpha, isAlphaNum;
	if (index > haystack.length)
		return [];
    size_t i = index;
    while (i != haystack.length) {
		const ch = haystack[i];
		const ok = (i == index ? ch.isAlpha : ch.isAlphaNum) || ch == '_';
        if (!ok)
            break;
        i += 1;
    }
	if (index != i) {
		const ch = haystack[index];
		const ok = ch.isAlpha || ch == '_';
		if (ok)
			return haystack[index .. i];
	}
	return [];
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	{
		const h = "a";
 		assert(h.tryGetIdentifierAfter(0) is h[0 .. 1]);
 		assert(h.tryGetIdentifierAfter(1) is []);
		assert(h.tryGetIdentifierAfter(2) is []);
	}
	{
		const h = "a1";
		assert(h.tryGetIdentifierAfter(1) is []);
		assert(h.tryGetIdentifierAfter(2) is []);
	}
	{
		const h = "11";
		assert(h.tryGetIdentifierAfter(0) is []);
		assert(h.tryGetIdentifierAfter(1) is []);
		assert(h.tryGetIdentifierAfter(2) is []);
	}
	{
		const h = " a";
		assert(h.tryGetIdentifierAfter(0) is []);
		assert(h.tryGetIdentifierAfter(1) is h[1 .. 2]);
		assert(h.tryGetIdentifierAfter(2) is []);
		assert(h.tryGetIdentifierAfter(3) is []);
	}
	{
		const h = " a ";
		assert(h.tryGetIdentifierAfter(0) is []);
		assert(h.tryGetIdentifierAfter(1) is h[1 .. 2]);
		assert(h.tryGetIdentifierAfter(2) is []);
		assert(h.tryGetIdentifierAfter(3) is []);
	}
}

/++ Returns: Slice of `haystack` directly at (before and after) `index` containing an identifier (symbol).
 +  If no identifier is found, `haystack[index .. index]` is returned.
 +  If `index > haystack.length` then `[]` is returned.
 +/
inout(char)[] tryGetIdentifierAt(inout(char)[] haystack, in size_t index) pure nothrow @safe @nogc {
	import std.ascii : isAlpha, isAlphaNum;
	if (index > haystack.length)
		return [];
    size_t i = index;
    while (i != 0) {
		const ch = haystack[i - 1];
		const ok = ch.isAlphaNum || ch == '_';
        if (!ok)
			break;
        i -= 1;
	}
    size_t j = index;
    while (j != haystack.length) {
		const ch = haystack[j];
		const ok = ch.isAlphaNum || ch == '_';
        if (!ok)
			break;
        j += 1;
	}
	if (i != j) {
		const ch = haystack[i];
		const ok = ch.isAlpha || ch == '_';
		if (ok)
			return haystack[i .. j];
	}
	return [];
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	{
		const h = "_1";
		assert(h.tryGetIdentifierAt(0) is h[0 .. 2]);
		assert(h.tryGetIdentifierAt(1) is h[0 .. 2]);
		assert(h.tryGetIdentifierAt(2) is h[0 .. 2]);
		assert(h.tryGetIdentifierAt(3) is []);
	}
	{
		const h = "a_";
		assert(h.tryGetIdentifierAt(0) is h);
		assert(h.tryGetIdentifierAt(1) is h);
		assert(h.tryGetIdentifierAt(2) is h);
		assert(h.tryGetIdentifierAt(3) is []);
	}
	{
		const h = "a1_";
		assert(h.tryGetIdentifierAt(0) is h);
		assert(h.tryGetIdentifierAt(1) is h);
		assert(h.tryGetIdentifierAt(2) is h);
		assert(h.tryGetIdentifierAt(3) is h);
		assert(h.tryGetIdentifierAt(4) is []);
	}
	{
		const h = "11";
		assert(h.tryGetIdentifierAt(0) is []);
		assert(h.tryGetIdentifierAt(1) is []);
		assert(h.tryGetIdentifierAt(2) is []);
	}
	{
		const h = " a";
		assert(h.tryGetIdentifierAt(0) is []);
		assert(h.tryGetIdentifierAt(1) is h[1 .. $]);
		assert(h.tryGetIdentifierAt(2) is h[1 .. $]);
		assert(h.tryGetIdentifierAt(3) is []);
	}
	{
		const h = " a ";
		assert(h.tryGetIdentifierAt(0) is []);
		assert(h.tryGetIdentifierAt(1) is h[1 .. 2]);
		assert(h.tryGetIdentifierAt(2) is h[1 .. 2]);
		assert(h.tryGetIdentifierAt(3) is []);
	}
}

/** This function returns the index of the `value` if it exist among `values`,
	`size_t.max` otherwise.

	TODO: Should we extend to isRandomAccessRange support? In that case we don't
	get static array support by default.
*/
size_t binarySearch(R, E)(const R[] values, in E value)
if (is(typeof(values[0].init == E.init))) /+ TODO: SortedRange support +/ {
	// value is not in the array if the array is empty
	if (values.length == 0) { return typeof(return).max; }
	immutable mid = values.length / 2; // mid offset
	if (value == values[mid])
		return mid; // direct hit
	else if (value < values[mid])
		return binarySearch(values[0 .. mid], value); // recurse left
	else {
		const index = binarySearch(values[mid + 1 .. $], value); // recurse right
		if (index != typeof(return).max)
			return index + mid + 1; // adjust the index; it is 0-based in the right-hand side slice.
		return index;
	}
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const int[9] x = [1, 3, 5, 6, 8, 9, 10, 13, 15];
	assert(x.binarySearch(0) == size_t.max);
	assert(x.binarySearch(1) == 0);
	assert(x.binarySearch(2) == size_t.max);
	assert(x.binarySearch(3) == 1);
	assert(x.binarySearch(4) == size_t.max);
	assert(x.binarySearch(5) == 2);
	assert(x.binarySearch(6) == 3);
	assert(x.binarySearch(7) == size_t.max);
	assert(x.binarySearch(8) == 4);
	assert(x.binarySearch(9) == 5);
	assert(x.binarySearch(10) == 6);
	assert(x.binarySearch(11) == size_t.max);
	assert(x.binarySearch(12) == size_t.max);
	assert(x.binarySearch(13) == 7);
	assert(x.binarySearch(14) == size_t.max);
	assert(x.binarySearch(15) == 8);
}

import std.range : ElementType, SearchPolicy, SortedRange;

/** Same as `range.contains()` but also outputs `index` where last occurrence of
	`key` is either currently stored (if `true` is returned) or should be stored
	(if `false` is returned) in order to preserve sortedness of `range`.

	The elements of `range` are assumed to be sorted in default (ascending)
	order.

	TODO: Move to member of `SortedRange` either as a new name or as an
	`contains`-overload take an extra `index` as argument.
 */
bool containsStoreIndex(SearchPolicy sp = SearchPolicy.binarySearch, R, V)
					   (R range, V value, out size_t index)
if (is(typeof(ElementType!R.init == V.init)) && is(R == SortedRange!(_), _)) /+ TODO: check for comparsion function +/ {
	/+ TODO: should we optimize for this case? +/
	// if (range.empty)
	// {
	//	 index = 0;
	//	 return false;		   // no hit
	// }
	index = range.length - range.upperBound!sp(value).length; // always larger than zero
	if (index >= 1 && range[index - 1] == value) {
		--index;							 // make index point to last occurrence of `value`
		assert(range.contains(value)); // assert same behaviour as existing contains
		return true;
	}
	assert(!range.contains(value)); // assert same behaviour as existing contains
	return false;
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const int[0] x;
	size_t index;
	import std.range : assumeSorted;
	assert(!x[].assumeSorted.containsStoreIndex(int.min, index) && index == 0);
	assert(!x[].assumeSorted.containsStoreIndex(-1,	  index) && index == 0);
	assert(!x[].assumeSorted.containsStoreIndex(0,	   index) && index == 0);
	assert(!x[].assumeSorted.containsStoreIndex(1,	   index) && index == 0);
	assert(!x[].assumeSorted.containsStoreIndex(int.max, index) && index == 0);
}

///
pure nothrow @safe @nogc version(nxt_test) unittest {
	const int[2] x = [1, 3];
	size_t index;
	import std.range : assumeSorted;
	assert(!x[].assumeSorted.containsStoreIndex(int.min, index) && index == 0);
	assert(!x[].assumeSorted.containsStoreIndex(-1,	  index) && index == 0);
	assert(!x[].assumeSorted.containsStoreIndex(0,	   index) && index == 0);
	assert( x[].assumeSorted.containsStoreIndex(1,	   index) && index == 0);
	assert(!x[].assumeSorted.containsStoreIndex(2,	   index) && index == 1);
	assert( x[].assumeSorted.containsStoreIndex(3,	   index) && index == 1);
	assert(!x[].assumeSorted.containsStoreIndex(4,	   index) && index == 2);
	assert(!x[].assumeSorted.containsStoreIndex(5,	   index) && index == 2);
	assert(!x[].assumeSorted.containsStoreIndex(int.max, index) && index == 2);
}

version(nxt_test)
	version(unittest)
		{ import nxt.dip_traits : hasPreviewDIP1000; }

version(nxt_stringzilla) private enum use_stringzilla = true; else private enum use_stringzilla = false;
