/++ LSP JSON-RPC request, response and notification messages
	TODO: Generate from
	https://microsoft.github.io//language-server-protocol/specifications/lsp/3.17/metaModel/metaModel.json
	in source generate stage in `lsp_metaModel_convert.d`.  +/
module nxt.lsp.messages;

import std.json : JSONValue, parseJSON;

@safe:

/++ Message. +/
struct Message {
    string method;
    JSONValue parameter;
    int id; // id
}
