/++ Language Server Protocol. +/
module nxt.lsp;

public import nxt.lsp.messages;

import core.stdc.stdlib : exit;
import std.json : JSONValue, parseJSON;
import std.conv : to;
import nxt.algorithm.searching : skipOver;
import nxt.algorithm.mutation : strip;
import nxt.logger;

@safe:

/++ Server Context. +/
struct ServerContext {
    string rspBuf; // response buffer
}

/++ Mock Reponse. +/
void mockRsp(ServerContext ctx, string rspTxt) {
    ctx.rspBuf ~= rspTxt;
}

void sendRsp(ServerContext ctx, const int id, JSONValue res) {
    JSONValue rsp;
    rsp["jsonrpc"] = "2.0";
    rsp["id"] = id;
    rsp["result"] = res;

    const rspTxt = rsp.toString(); // responseText
    const cntLen = "Content-Length: " ~ to!string(rspTxt.length) ~ "\r\n\r\n"; // contentLength

    mockRsp(ctx, cntLen ~ rspTxt);
}

void handle_request(ServerContext ctx, Message msg) {
    switch (msg.method) {
        case "initialize": sendInitRsp(ctx, msg.id); break;
        case "textDocument/didOpen": handle_didOpen(ctx, msg.parameter); break;
        case "textDocument/didChange": handle_didChange(ctx, msg.parameter); break;
        case "textDocument/didSave": handle_didSave(ctx, msg.parameter); break;
        case "textDocument/didClose": handle_didClose(ctx, msg.parameter); break;
        case "textDocument/completion": handle_completion(ctx, msg.parameter, msg.id); break;
        case "textDocument/hover": handle_hover(ctx, msg.parameter, msg.id); break;
        case "textDocument/definition": handle_definition(ctx, msg.parameter, msg.id); break;
        case "workspace/symbol": handle_workspaceSymbol(ctx, msg.parameter, msg.id); break;
        case "shutdown": handle_shutdown(ctx, msg.id); break;
        case "textDocument/formatting": handle_formatting(ctx, msg.parameter, msg.id); break;
        case "textDocument/rangeFormatting": handle_rangeFormatting(ctx, msg.parameter, msg.id); break;
        case "textDocument/onTypeFormatting": handle_onTypeFormatting(ctx, msg.parameter, msg.id); break;
        case "textDocument/documentHighlight": handle_documentHighlight(ctx, msg.parameter, msg.id); break;
        case "textDocument/documentSymbol": handle_documentSymbol(ctx, msg.parameter, msg.id); break;
        case "workspace/didChangeConfiguration": handle_didChangeConfig(ctx, msg.parameter); break;
        case "workspace/didChangeWatchedFiles": handle_didChangeWatchedFiles(ctx, msg.parameter); break;
        case "workspace/executeCommand": handle_execCmd(ctx, msg.parameter, msg.id); break;
        case "window/showMessage": handle_showMsg(ctx, msg.parameter); break;
        case "window/logMessage": handle_logMsg(ctx, msg.parameter); break;
        default: error("Unknown method: ", msg.method); // method
    }
}

void sendInitRsp(ServerContext ctx, const int id) { // initializeResponse
    JSONValue res;
    res["capabilities"] = JSONValue([
        "textDocumentSync": JSONValue("incremental"),
        "completionProvider": JSONValue(["resolveProvider": false]),
        "hoverProvider": JSONValue(true),
        "definitionProvider": JSONValue(true),
        "documentSymbolProvider": JSONValue(true)
    ]);
    sendRsp(ctx, id, res);
}

void handle_didOpen(ServerContext ctx, JSONValue prm) {
    auto uri = prm["textDocument"]["uri"].str; // URI
    trace("Client opened file: ", uri);
}

void handle_didChange(ServerContext ctx, JSONValue prm) {
    auto uri = prm["textDocument"]["uri"].str; // URI
    trace("Client changed file: ", uri);
}

void handle_didSave(ServerContext ctx, JSONValue prm) {
    auto uri = prm["textDocument"]["uri"].str; // URI
    trace("Client saved file: ", uri);
}

void handle_didClose(ServerContext ctx, JSONValue prm) {
    auto uri = prm["textDocument"]["uri"].str; // URI
    trace("Client closed file: ", uri);
}

void handle_completion(ServerContext ctx, JSONValue prm, const int id) {
    JSONValue res;
    sendRsp(ctx, id, res);
}

void handle_hover(ServerContext ctx, JSONValue prm, const int id) {
    JSONValue res;
    res["contents"] = JSONValue("Hover information not available.");
    sendRsp(ctx, id, res);
}

void handle_definition(ServerContext ctx, JSONValue prm, const int id) {
    JSONValue res;
    sendRsp(ctx, id, res);
}

void handle_workspaceSymbol(ServerContext ctx, JSONValue prm, const int id) {
    JSONValue res;
    sendRsp(ctx, id, res);
}

void handle_shutdown(ServerContext ctx, const int id) {
    trace("Shutting down...");
    JSONValue res;
    sendRsp(ctx, id, res);
	() @trusted {
		exit(0); // TODO: Cleaner shutdown.
	}();
}

void handle_formatting(ServerContext ctx, JSONValue prm, const int id) {
    JSONValue res;
    sendRsp(ctx, id, res);
}

void handle_rangeFormatting(ServerContext ctx, JSONValue prm, const int id) {
    JSONValue res;
    sendRsp(ctx, id, res);
}

void handle_onTypeFormatting(ServerContext ctx, JSONValue prm, const int id) {
    JSONValue res;
    sendRsp(ctx, id, res);
}

void handle_documentHighlight(ServerContext ctx, JSONValue prm, const int id) {
    JSONValue res;
    sendRsp(ctx, id, res);
}

void handle_documentSymbol(ServerContext ctx, JSONValue prm, const int id) {
    JSONValue res;
    sendRsp(ctx, id, res);
}

void handle_didChangeConfig(ServerContext ctx, JSONValue prm) {
    trace("Configuration changed");
}

void handle_didChangeWatchedFiles(ServerContext ctx, JSONValue prm) {
    trace("Watched files changed");
}

void handle_execCmd(ServerContext ctx, JSONValue prm, const int id) {
    JSONValue res = JSONValue("Command executed");
    sendRsp(ctx, id, res);
}

void handle_showMsg(ServerContext ctx, JSONValue prm) {
    trace("Message: ", prm["message"].str);
}

void handle_logMsg(ServerContext ctx, JSONValue prm) {
    trace("Log: ", prm["message"].str);
}

Message parseMsg(string cnt) { // content
    JSONValue json = parseJSON(cnt);
    Message msg; // message
    msg.method = json["method"].str; // method
    // msg.parameter = json.get("params", JSONValue());
    // msg.id = json.get("id", -1).to!int; // id
    return msg;
}

void reactIO() @trusted {
    while (true) {
        string hdrs; // headers
        string line;
        // while ((line = stdin.readln().chomp) != "") {
		// 	hdrs ~= line ~ "\n"; // headers
        // }

		import std.array : split;
        auto hdrLines = hdrs.split("\n"); // headerLines
        size_t cntLen = 0; // contentLength
        foreach (hdr; hdrLines) { // header
            if (hdr.skipOver("Content-Length:")) {
                cntLen = hdr.strip.to!size_t;
            }
        }

        if (cntLen > 0) {
            char[] cnt = new char[cntLen]; // content
            // stdin.read(cnt.ptr, cntLen);
            auto msg = parseMsg(cast(string)cnt);
            auto ctx = ServerContext();
            handle_request(ctx, msg);
        }
    }
}
