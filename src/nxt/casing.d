module nxt.casing;

import nxt.string_traits : isASCII;

version(nxt_test) version(unittest) {
	import std.algorithm : equal;
}

/** Convert `s` to a lower-cased character range.
 *
 * `s` must contain ASCII characters only.
 */
private auto _loweredASCII(in char[] s) in(s.isASCII) {
	import std.algorithm.iteration : map;
	import std.ascii : toLower;
	return s.map!(ch => ch.toLower);
}

/++ Returns: `s` lowercased if `s` is an ASCII-string, otherwise `defaultValue`. +/
string toLowerIfASCII(in char[] s, string defaultValue) pure nothrow @safe {
	if (!s.isASCII)
		return defaultValue;
	typeof(return) result;
	result.reserve(s.length);
	import std.ascii : toLower;
	foreach (const c; s)
		result ~= c.toLower;
	return result;
}

/++ Returns: `s` lowercased if `s` is an ASCII-string, otherwise `s`. +/
inout(char)[] toLowerIfASCII(return scope inout(char)[] s) pure nothrow @safe {
	if (!s.isASCII)
		return s;
	typeof(return) result;
	result.reserve(s.length);
	import std.ascii : toLower;
	foreach (const c; s)
		result ~= c.toLower;
	return result;
}

///
@safe pure /*TODO: nothrow @nogc*/ version(nxt_test) unittest {
	assert("Lasse".toLowerIfASCII("Lasse").equal("lasse"));
	assert("Åberg".toLowerIfASCII("Åberg").equal("Åberg"));
	assert("Lasse".toLowerIfASCII.equal("lasse"));
	assert("Åberg".toLowerIfASCII.equal("Åberg"));
}

/** Convert string `s` to a lower-cased character range.
 *
 * String may contain Unicode characters.
 */
private auto _loweredUnicode(in char[] s) {
	import std.algorithm.iteration : map;
	import std.uni : toLower;
	import std.traits : isNarrowString;
	import std.utf : byUTF;
	return s.byUTF!dchar.map!(ch => ch.toLower);
}

///
@safe pure /*TODO: nothrow @nogc*/ version(nxt_test) unittest {
	assert("Lasse"._loweredUnicode.equal("lasse"));
	assert("Åberg"._loweredUnicode.equal("åberg"));
}

/** Convert D-style camel-cased string `s` to a range of lower-cased words.
 */
auto loweredCamelCasedUnicode(in char[] s) {
	import std.algorithm.iteration : map;
	import std.uni : isUpper; // D symbol names can only be in ASCII
	/+ TODO: Instead of this add std.ascii.as[Lower|Upper]Case and import std.ascii.asLowerCase +/
	import std.uni : asLowerCase;
	import nxt.slicing : preSlicer;
	return s.preSlicer!isUpper.map!asLowerCase;
}

///
pure @safe version(nxt_test) unittest {
	auto x = "doThis".loweredCamelCasedUnicode;
	assert(x.front.equal("do"));
	x.popFront();
	assert(x.front.equal("this"));
}

/** Returns: D-style camel-cased string `s` as a lower-cased expression with
	words separated with `sep`.
 */
auto loweredCamelCasedUnicodeSpaced(S)(S s, in char[] sep = " ") {
	import std.algorithm.iteration : joiner;
	return loweredCamelCasedUnicode(s).joiner(sep);
}

///
pure @safe version(nxt_test) unittest {
	assert(equal("doThis".loweredCamelCasedUnicodeSpaced,
				 "do this"));
}

/** Convert enumeration value (enumerator) `t` to a range chars.
 */
auto loweredSpacedChars(T)(in T t, in char[] sep = " ")
if (is(T == enum)) {
	import nxt.enum_ex : toStringWithConsecutiveAliases;
	return t.toStringWithConsecutiveAliases
			.loweredCamelCasedUnicodeSpaced(sep);
}

///
pure @safe version(nxt_test) unittest {
	enum Things { isUri, isLink }
	assert(Things.isUri.loweredSpacedChars.equal("is uri"));
	assert(Things.isLink.loweredSpacedChars.equal("is link"));
}

///
pure @safe version(nxt_test) unittest {
	enum Things { isURI, isLink }
	auto r = Things.isURI.loweredSpacedChars;
	alias R = typeof(r);
	import std.range.primitives : ElementType;
	alias E = ElementType!R;
	static assert(is(E == dchar));
}
