/++ Construction of types.

	Typically used for functional construction of containers.

	These generic factory functions prevents the need for defining separate
	member factory functions for each container/collection class opposite to
	what Rust's std.collection types define as `withLength`, `withCapacity`,
	etc. This adhere's to the dry-principle.

	See: nxt.container and std.container.
 +/
module nxt.construction;

import std.range.primitives : isInputRange, ElementType;

/++ Construct an instance of `T` with capacity `capacity`. +/
T makeOfCapacity(T)(size_t capacity)
/+if (is(typeof(T.init.reserve(0)))) +/ {
	T t;
	t.reserve(capacity); /+ TODO: Check that this allowed in template-restriction +/
	return t;
}

///
@safe pure version(nxt_test) unittest {
	alias A = int[];
	const n = 3;
	const a = makeOfCapacity!(A)(n);
	assert(a.capacity == n);
}

/++ Construct an instance of `T` with length `n`. +/
T makeOfLength(T, Length)(Length length) if (is(Length : size_t)) {
	T t;
	t.length = length; /+ TODO: Check that this allowed in template-restriction +/
	return t;
}

///
@safe pure version(nxt_test) unittest {
	alias A = int[];
	const n = 3;
	const a = makeOfLength!(A)(n);
	assert(a.length == n);
}

/++ Construct an instance of `T` with a single `element`. +/
T makeWithElement(T, E)(E element) {
	static if (is(typeof(T.init.reserve(0))))
		T t = makeOfCapacity!T(1);
	else
		T t;
	static if (is(typeof(t ~= element)))
		t ~= element;
	else static if (is(typeof(t.put(element))))
		t.put(element);
	else static if (is(typeof(t.insert(element))))
		t.insert(element);
	else static if (is(typeof(t.add(element))))
		t.add(element);
	else
		static assert(0, "Support type ", T);
	return t;
}

///
@safe pure version(nxt_test) unittest {
	alias A = int[];
	const a = makeWithElement!(A)(42);
	assert(a.capacity >= 1);
	assert(a.length == 1);
}

/++ Construct an instance of `T` with `elements`. +/
T makeWithElements(T, R)(R elements) if (isInputRange!R) {
	static if (is(typeof(T(elements))))
		return T(elements);
	else {
		import std.range.primitives : hasLength;
		static if (is(typeof(T.init.reserve(0))) && hasLength!R)
			T t = makeOfCapacity!T(elements.length);
		else
			T t;
		static if (is(typeof(t ~= elements)))
			t ~= elements;
		else static if (is(typeof(t.put(elements))))
			t.put(elements);
		else static if (is(typeof(t.insert(elements))))
			t.insert(elements);
		else static if (is(typeof(t.add(elements))))
			t.add(elements);
		else
			foreach (const ref element; elements)
				t.insert(element);
		return t;
	}
}

///
@safe pure version(nxt_test) unittest {
	alias A = int[];
	const elements = [1,2,3];
	const a = makeWithElements!(A)(elements);
	assert(a.capacity >= elements.length);
	assert(a.length == elements.length);
}

/// Returns: shallow duplicate of `a`.
T dupShallow(T)(in T a)
if (is(typeof(T.init[])) && // `hasSlicing!T`
	!is(T == const(U)[], U) && // builtin arrays already have `.dup` property
	__traits(isCopyable, ElementType!T)) {
	/+ TODO: delay slicing of `a` when T is a static array for compile-time
       length optimization: +/
	return makeWithElements!(T)(a[]);
}
alias dup = dupShallow;

///
version(none)
@safe pure version(nxt_test) unittest {
	alias A = int[];
	const elements = [1,2,3];
	const a = makeWithElements!(A)(elements);
	const b = a.dupShallow;
	assert(a == b);
	assert(a.ptr !is b.ptr);
}
