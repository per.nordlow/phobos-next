/** Statically allocated arrays with compile-time known lengths.
 */
module nxt.container.static_array;

@safe pure:

/++ Statically allocated `T`-array of fixed pre-allocated length.
 +
 + Similar to C++'s `std::static_vector<T, Capacity>`
 + Similar to Rust's `fixedvec`: https://docs.rs/fixedvec/0.2.4/fixedvec/
 + Similar to `mir.small_array` at http://mir-algorithm.libmir.org/mir_small_array.html
 +
 + See_Also: https://en.cppreference.com/w/cpp/container/array
 +
 + TODO: Make member functions free functions when possible starting with
 + functions qualified as /+tlm+/
 +
 + TODO: Merge member functions with basic_+_array.d and array_ex.d
 +/
struct StaticArray(T, uint capacity_) {
	import core.exception : onRangeError;
	import nxt.lifetime : move, moveEmplace;
	import core.internal.traits : hasElaborateDestructor;
	import std.bitmanip : bitfields;
	import std.traits : isSomeChar, isAssignable, hasIndirections;
	import nxt.container.traits : mustAddGCRange;

	enum capacity = capacity_; // for public use

	/// Store of `capacity` number of elements.
	T[capacity] _store;		 /+ TODO: use store constructor +/

	static if (capacity <= ubyte.max) {
		static if (T.sizeof == 1)
			alias Length = ubyte; // pack length
		else
			alias Length = uint;
	} else static if (capacity <= ushort.max) {
		static if (T.sizeof <= 2)
			alias Length = uint; // pack length
		else
			alias Length = uint;
	} else
		static assert("Too large requested capacity " ~ capacity);
	Length _length;		 /// number of defined elements in `_store`

	/// Is `true` if `U` can be assigned to the elements of `this`.
	private enum isElementAssignable(U) = isAssignable!(T, U);

	/// Empty.
	void clear() @nogc {
		releaseElementsStore();
		resetInternalData();
	}

	/// Release elements and internal store.
	private void releaseElementsStore() @trusted @nogc {
		foreach (const i; 0 .. length) {
			static if (!__traits(isPOD, T) && hasElaborateDestructor!T)
				.destroy(_store.ptr[i]);
			else static if (hasIndirections!T) // avoid GC mark-phase dereference
				_store.ptr[i] = T.init; // TODO: nullify pointers only
		}
	}

	/// Reset internal data.
	private void resetInternalData() @nogc {
		version(D_Coverage) {} else pragma(inline, true);
		_length = 0;
	}

	/// Construct from element `values`.
	version(none)
	this(Us...)(Us values) @trusted if (Us.length <= capacity) {
		static foreach (const i, value; values)
			static if (__traits(isPOD, typeof(value)))
				_store[i] = value;
			else
				moveEmplace(value, _store[i]);
		_length = cast(Length)values.length;
	}

	/// Construct from element `values`.
	version(none)
	this(U)(U[] values) @trusted if (__traits(isCopyable, U)//  &&
		/+ TODO: isElementAssignable!U +/
		) // prevent accidental move of l-value `values` in array calls
	{
		version(assert) if (values.length > capacity) onRangeError(); // `Arguments don't fit in array`
		_store[0 .. values.length] = values;
		_length = cast(Length)values.length;
	}

	/// Construct from element `values`.
	static typeof(this) fromValuesUnsafe(U)(U[] values) @system if (__traits(isCopyable, U) && isElementAssignable!U) // prevent accidental move of l-value `values` in array calls
	{
		typeof(return) that;			  /+ TODO: use Store constructor: +/
		that._store[0 .. values.length] = values;
		that._length = cast(Length)values.length;
		return that;
	}

	/// No default copying.
	this(this) @disable;

	static if (!__traits(isPOD, T) && hasElaborateDestructor!T) {
		~this() nothrow @nogc {
			releaseElementsStore();
		}
	}

	/** Add elements `es` to the back.
	 * Throws when array becomes full.
	 */
	void insertBack(Es...)(Es es) @trusted
	if (Es.length <= capacity) /+ TODO: use `isAssignable` +/ {
		version(assert) if (_length + Es.length > capacity) onRangeError(); // `Arguments don't fit in array`. Rely on normal bounds checks for release build.
		static foreach (const i, e; es) {
			static if (__traits(isPOD, T))
				_store[_length + i] = e;
			else
				moveEmplace(e, _store[_length + i]); /+ TODO: remove when compiler does this +/
		}
		_length = cast(Length)(_length + Es.length); /+ TODO: better? +/
	}
	/// ditto
	alias put = insertBack;	   // `OutputRange` support

	/** Try to add elements `es` to the back.
	 * Returns: `true` iff all `es` were pushed, `false` otherwise.
	 */
	bool insertBackMaybe(Es...)(Es es) @trusted
	if (Es.length <= capacity) /+ TODO: use `isAssignable` +/ {
		version(D_Coverage) {} else version(LDC) pragma(inline, true);
		if (_length + Es.length > capacity) { return false; }
		insertBack(es);
		return true;
	}
	/// ditto
	alias putMaybe = insertBackMaybe;

	/** Add elements `es` to the back.
	 */
	void opOpAssign(string op, Us...)(Us values)
	if (op == "~" && values.length >= 1 && allSatisfy!(isElementAssignable, Us)) {
		insertBack(values.move()); /+ TODO: remove `move` when compiler does it for +/
	}

	import std.traits : isMutable;
	static if (isMutable!T) {
		/** Pop first (front) element. */
		auto ref popFront() in(!empty) {
			/+ TODO: is there a reusable Phobos function for this? +/
			foreach (const i; 0 .. _length - 1)
				move(_store[i + 1], _store[i]); // like `_store[i] = _store[i + 1];` but more generic
			_length = cast(typeof(_length))(_length - 1); /+ TODO: better? +/
			return this;
		}
	}

	/** Pop last (back) element. */
	void popBack()() @trusted in(!empty) /*tlm*/ {
		version(D_Coverage) {} else pragma(inline, true);
		_length = cast(Length)(_length - 1); /+ TODO: better? +/
		static if (!__traits(isPOD, T) && hasElaborateDestructor!T)
			.destroy(_store.ptr[_length]);
		else static if (mustAddGCRange!T) // avoid GC mark-phase dereference
			_store.ptr[_length] = T.init; // TODO: nullify pointers only
	}

	T takeBack()() @trusted in(!empty) /*tlm*/ {
		version(D_Coverage) {} else pragma(inline, true);
		static if (__traits(isPOD, T))
			return _store.ptr[--_length]; // no move needed
		else
			return move(_store.ptr[--_length]); // move is indeed need here
	}

	/** Pop the `n` last (back) elements. */
	void popBackN()(in size_t n) @trusted in(length >= n) /*tlm*/ {
		_length = cast(Length)(_length - n); /+ TODO: better? +/
		static if (!__traits(isPOD, T) && hasElaborateDestructor!T)
			foreach (const i; 0 .. n)
				.destroy(_store.ptr[_length + i]);
		else static if (mustAddGCRange!T) // avoid GC mark-phase dereference
			foreach (const i; 0 .. n)
				_store.ptr[_length + i] = T.init; // TODO: nullify pointers only
	}

	/** Move element at `index` to return. */
	static if (isMutable!T) {
		/** Pop element at `index`. */
		void popAt()(in size_t index) /*tlm*/ @trusted @("complexity", "O(length)") in(index < this.length) {
			static if (!__traits(isPOD, T) && hasElaborateDestructor!T)
				.destroy(_store.ptr[index]);
			shiftToFrontAt(index);
			_length = cast(Length)(_length - 1);
		}

		T moveAt()(in size_t index) /*tlm*/ @trusted @("complexity", "O(length)") in(index < this.length) {
			auto value = _store.ptr[index].move();
			shiftToFrontAt(index);
			_length = cast(Length)(_length - 1);
			return value;
		}

		private void shiftToFrontAt()(in size_t index) /*tlm*/ @trusted
		{
			// TODO: Avoid subtraction in upper foreach bound:
			foreach (const i; 0 .. this.length - (index + 1)) {
				immutable si = index + i + 1; // source index
				immutable ti = index + i;	 // target index
				moveEmplace(_store.ptr[si],
							_store.ptr[ti]);
			}
		}
	}

	/** Index operator. */
	pragma(inline, true)
	ref inout(T) opIndex(in size_t i) inout return => _store[i];

	/** First (front) element. */
	pragma(inline, true)
	ref inout(T) front() inout return => _store[0];

	/** Last (back) element. */
	pragma(inline, true)
	ref inout(T) back() inout return => _store[_length - 1];

	/// Get slice in range `i` .. `j`.
	pragma(inline, true);
	inout(T)[] opSlice(in size_t i, in size_t j) inout @trusted return /+ TODO: remove @trusted? +/
	// in(i <= j)
	// in(j <= _length)
		=> _store[i .. j];

	/// Get full slice.
	pragma(inline, true)
	inout(T)[] opSlice() inout @trusted return /+ TODO: remove @trusted? +/ => _store[0 .. _length];

	@property pragma(inline, true) {
		/** Returns: `true` iff `this` is empty, `false` otherwise. */
		bool empty() const @property { return _length == 0; }

		/** Returns: `true` iff `this` is full, `false` otherwise. */
		bool full() const { return _length == capacity; }

		/** Get length. */
		auto length() const { return _length; }
		alias opDollar = length;	/// ditto
	}

	/** Comparison for equality. */
	bool opEquals()(const scope auto ref typeof(this) rhs) const => this[] == rhs[];
	/// ditto
	bool opEquals(U)(const scope U[] rhs) const if (is(typeof(T[].init == U[].init))) => this[] == rhs;
}

/// construct from array may throw
pure @safe version(nxt_test) unittest {
	alias T = int;
	alias A = StaticArray!(T, 3);
	static assert(!mustAddGCRange!A);
	A a;
	a.insertBack(1);
	a.insertBack(2);
	assert(a[] == [1, 2].s);
}

/// unsafe construct from array
pure nothrow @safe @nogc version(nxt_test) unittest {
	alias T = int;
	alias A = StaticArray!(T, 3);
	static assert(!mustAddGCRange!A);
	() @trusted {
		const a = A.fromValuesUnsafe([1, 2, 3].s);
		assert(a[] == [1, 2, 3].s);
	}();
}

/// construct from scalars is nothrow
pure nothrow @safe @nogc version(nxt_test) unittest {
	alias T = int;
	alias A = StaticArray!(T, 3);
	static assert(!mustAddGCRange!A);
	auto a = A();
	a.insertBack(1);
	a.insertBack(2);
	a.insertBack(3);
	assert(a[] == [1, 2, 3].s);
	static assert(!__traits(compiles, { const _ = A(1, 2, 3, 4); }));
}

pure @safe version(nxt_test) unittest {
	static assert(mustAddGCRange!(StaticArray!(string, 1)));
	static assert(mustAddGCRange!(StaticArray!(string, 2)));
}

///
pure @safe version(nxt_test) unittest {
	import std.exception : assertNotThrown;

	alias T = char;
	enum capacity = 3;

	alias A = StaticArray!(T, capacity);
	static assert(!mustAddGCRange!A);
	static assert(A.sizeof == T.sizeof*capacity + 1);

	import std.range.primitives : isOutputRange;
	static assert(isOutputRange!(A, T));

	auto ab = A();
	ab.insertBack('a');
	ab.insertBack('b');
	assert(!ab.empty);
	assert(ab[0] == 'a');
	assert(ab.front == 'a');
	assert(ab.back == 'b');
	assert(ab.length == 2);
	assert(ab[] == "ab");
	assert(ab[0 .. 1] == "a");
	assertNotThrown(ab.insertBack('_'));
	assert(ab[] == "ab_");
	ab.popBack();
	assert(ab[] == "ab");

	ab.popBackN(2);
	assert(ab.empty);
	assertNotThrown(ab.insertBack('a', 'b'));

	auto abc = A();
	abc.insertBack('a');
	abc.insertBack('b');
	abc.insertBack('c');
	assert(!abc.empty);
	assert(abc.front == 'a');
	assert(abc.back == 'c');
	assert(abc.length == 3);
	assert(abc[] == "abc");
	assert(ab[0 .. 2] == "ab");
	assert(abc.full);
	static assert(!__traits(compiles, { const abcd = A('a', 'b', 'c', 'd'); })); // too many elements

	assert(ab[] == "ab");
	ab.popFront();
	assert(ab[] == "b");

	auto xy = A();
	xy.insertBack('x');
	xy.insertBack('y');
	assert(!xy.empty);
	assert(xy[0] == 'x');
	assert(xy.front == 'x');
	assert(xy.back == 'y');
	assert(xy.length == 2);
	assert(xy[] == "xy");
	assert(xy[0 .. 1] == "x");

	auto xyz = A();
	xyz.insertBack('x');
	xyz.insertBack('y');
	xyz.insertBack('z');
	assert(!xyz.empty);
	assert(xyz.front == 'x');
	assert(xyz.back == 'z');
	assert(xyz.length == 3);
	assert(xyz[] == "xyz");
	assert(xyz.full);
	static assert(!__traits(compiles, { const xyzw = A('x', 'y', 'z', 'w'); })); // too many elements
}

///
pure @safe version(nxt_test) unittest {
	static void testAsSomeString(T)() {
		enum capacity = 15;
		alias A = StaticArray!(T, capacity);
		static assert(A.sizeof == capacity + 1);
		static assert(!mustAddGCRange!A);
		auto a = A();
		a.insertBack('a');
		a.insertBack('b');
		a.insertBack('c');
		assert(a[] == "abc");
	}
	foreach (T; AliasSeq!(char)) {
		testAsSomeString!T();
	}
}

/// equality
pure @safe version(nxt_test) unittest {
	alias A = StaticArray!(int, 15);
	static assert(!mustAddGCRange!A);
	A a;
	a.insertBack(1);
	a.insertBack(2);
	A b;
	b.insertBack(1);
	b.insertBack(2);
	assert(a == b);
}

pure @safe version(nxt_test) unittest {
	class C { const int value; }
	alias A = StaticArray!(C, 2);
	static assert(mustAddGCRange!A);
}

/// `insertBackMaybe` is nothrow @nogc.
pure nothrow @safe @nogc version(nxt_test) unittest {
	alias A = StaticArray!(int, 2);
	scope A s;
	assert(s.insertBackMaybe(42));
	assert(s.insertBackMaybe(43));
	assert(!s.insertBackMaybe(0));
	assert(s.length == 2);
	s.clear();
	assert(s.length == 0);
}

pure nothrow @safe @nogc version(nxt_test) unittest {
	alias E = int*;
	alias A = StaticArray!(E, 2);
	int x = 42;
	scope A s;
	() @trusted {
		assert(s.insertBackMaybe(&x));
	}();
	assert(s.length == 1);
	s.clear();
	assert(s.length == 0);
}

/// equality
@system pure nothrow @nogc version(nxt_test) unittest {
	enum capacity = 15;
	alias A = StaticArray!(int, capacity);
	assert(A.fromValuesUnsafe([1, 2, 3].s) ==
		   A.fromValuesUnsafe([1, 2, 3].s));
	const ax = [1, 2, 3].s;
	assert(A.fromValuesUnsafe([1, 2, 3].s) == ax);
	assert(A.fromValuesUnsafe([1, 2, 3].s) == ax[]);
	const cx = [1, 2, 3].s;
	assert(A.fromValuesUnsafe([1, 2, 3].s) == cx);
	assert(A.fromValuesUnsafe([1, 2, 3].s) == cx[]);
	immutable ix = [1, 2, 3].s;
	assert(A.fromValuesUnsafe([1, 2, 3].s) == ix);
	assert(A.fromValuesUnsafe([1, 2, 3].s) == ix[]);
}

version(nxt_test) version(unittest) {
	import std.meta : AliasSeq;
	import std.exception : assertThrown;
	import core.exception : AssertError;
	import nxt.array_help : s;
	import nxt.container.traits : mustAddGCRange;
	import nxt.dip_traits : hasPreviewDIP1000;
}
