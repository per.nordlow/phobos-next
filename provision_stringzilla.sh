#!/bin/bash

set -euo pipefail

CCACHE=ccache
CC=clang

SOURCE_ROOT="third_party/stringzilla"
TARGET_ROOT="stringzilla-build"

mkdir -p "${TARGET_ROOT}"

# ok to use "-march=native" because of no distribution
# See: https://github.com/ashvardanian/StringZilla/issues/183
"${CCACHE}" "${CC}" -c -O3 -march=native -I"${SOURCE_ROOT}"/include "${SOURCE_ROOT}"/c/lib.c -o "${TARGET_ROOT}"/stringzilla.o
