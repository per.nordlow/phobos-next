#!/bin/bash
set -euo pipefail

THIS_PATH="${BASH_SOURCE[0]}"
THIS_DIR=$(dirname "$THIS_PATH")

tools=("llvm-symbolizer" "cmake" "gcc" "clang" "ccache")

install_apt_packages_of_executables() {
	local packages=()
	for command in "$@"; do
		if ! command -v "$command" >/dev/null 2>&1; then
			packages+=("$command")
		fi
	done
	if [[ ${#packages[@]} -gt 0 ]]; then
		package_list="${packages[@]}"
		echo "Installing missing APT packages: $package_list ..."
		sudo apt install "${packages[@]}"
	fi
}

install_apt_packages_of_executables "${tools[@]}"

# TODO: sudo apt install $(grep -Po 'packages: \K.*' .github/workflows/d.yml)
# llvm-symbolizer is needed for correct ASan diagnostics
# llvm-symbolizer --version

"${THIS_DIR}"/provision_stringzilla.sh
